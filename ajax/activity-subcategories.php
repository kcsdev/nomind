<?php 
// Include the wp-load'er
include('../wp-load.php');

if(isset($_GET['category']))
{
    $html = '';
    
     $parent_cat =  get_term_by('slug', $_GET['category'], EM_TAXONOMY_CATEGORY);
                                    
     $current_term_id =    $parent_cat->term_id;

     $args = array(
        'orderby'           => 'name', 
        'order'             => 'ASC',
        'hide_empty'        => false,
        'hierarchical'      => true,
        'parent'          => $parent_cat->term_id
     ); 


     $subcategories = get_terms(array(EM_TAXONOMY_CATEGORY), $args );
       
        foreach($subcategories as $category)
        {
    
                $html .= sprintf('<option value="%s">%s</option>', $category->slug, $category->name);    
           
        }
        echo $html;
        die();
    }
    else
        echo 'Forbidden';
        die();
    ?>