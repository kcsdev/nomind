<?php 
// Include the wp-load'er
include('../wp-load.php');

if(isset($_POST['to_user']) && isset($_POST['from_user']) && isset($_POST['message_content']))
{
    $tmp_to_all_uids = '|'.$_POST['to_user'].'|';
    $subject = ' :הודעה מדף הפעילות'. $_POST['event_name'];
    messaging_insert_message($_POST['to_user'],$tmp_to_all_uids,$_POST['from_user'], stripslashes(sanitize_text_field($subject)),wp_kses_post($_POST['message_content']),'unread',0);
	messaging_new_message_notification($_POST['to_user'],$_POST['from_user'],stripslashes(sanitize_text_field($subject)),wp_kses_post($_POST['message_content']));
    messaging_insert_sent_message($tmp_to_all_uids,$_POST['from_user'],stripslashes(sanitize_text_field($subject)),wp_kses_post($_POST['message_content']),0);
    echo 'OK';
    die();
}
else
{
    echo 'Forbidden';
    die();
}


    

    