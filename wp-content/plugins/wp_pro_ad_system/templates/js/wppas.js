var paspopupStatus = 0; // set value
var clickable_paszone;


function PASfunctions(){
jQuery(function($){
	

	/* ----------------------------------------------------------------
	 * POPUP
	 * ---------------------------------------------------------------- */
	$("body").on("click", "a.paspopup", function(event){
		//alert('klik')
		var adzone_id = $(this).attr('adzone_id');
		var popup_type = $(this).attr('popup_type');
		var ajaxurl = $(this).attr('ajaxurl');
		
		setTimeout(function(){ 
			loadPASPopup( adzone_id, popup_type, ajaxurl ); // function show popup
		}, 0); // 500 = .5 second delay
	return false;
	});


	$("body").on("click", "div.close_paspopup", function(event){	
		disablePASPopup();  // function close pop up
	});

	$(this).keyup(function(event) {
		if (event.which == 27) { // 27 is 'Ecs' in the keyboard
			disablePASPopup();  // function close pop up
		}
	});
		
	$("body").on("click", "div#backgroundPasPopup", function(event){		
		disablePASPopup();  // function close pop up
	});
	
	
	
	
	
	/* ----------------------------------------------------------------
	 * FLY IN
	 * ---------------------------------------------------------------- */
	$("body").on("click", "div.close_pasflyin", function(event){		
		disablePASFlyIn();  // function close pop up
	});
	
	
	
	
	/* ----------------------------------------------------------------
	 * BACKGROUND ADS
	 * ---------------------------------------------------------------- */
	// Redirect to banner url when pas_container is clicked
	//$(clickable_paszone.pas_container).on('click', function(event){
	$("body").on('click', function(event){
		
		
		var target = $(event.target);
		var target_id = event.target.id;
		var bgpas_defaults = { link_full: '', link_full_target: "_blank", pas_container: "body", link_left: '', link_right: '' };
		var _clickable_paszone = $.extend( {}, bgpas_defaults, clickable_paszone );
		
		
		if (target.is(_clickable_paszone.pas_container) || target_id == _clickable_paszone.pas_container){
			
			// Add links to left and right sides
			if (_clickable_paszone.link_full == "" || _clickable_paszone.link_full == null){
				
				var width = $(document).width();

				if (event.pageX <= (width / 2)){
					// Left side
					if( _clickable_paszone.link_left != '' ){
						var adlink = window.open(_clickable_paszone.link_left, _clickable_paszone.link_left_target);
					}
				}else{
					// Right side
					if( _clickable_paszone.link_right != '' ){
						var adlink = window.open(_clickable_paszone.link_right, _clickable_paszone.link_right_target);
					}
				}
			}else{
				
				if( _clickable_paszone.link_full != "" ){
					// Add link to both sides
					var adlink = window.open(_clickable_paszone.link_full, _clickable_paszone.link_full_target);
				}
			}
			adlink.focus();
		}
	});
	
	
	
	/* ----------------------------------------------------------------
	 * FIXED ADS
	 * ---------------------------------------------------------------- */
	if( $('#pas-sticky-div').length > 0 ){
		//alert('oi')
		var $window = $(window),
			$stickyEl = $('#pas-sticky-div'),
			elTop = $stickyEl.offset().top;
			
			$window.scroll(function() {
				$stickyEl.toggleClass('pas_sticky', $window.scrollTop() > elTop);
			});
	}

});
}

jQuery(document).ready(function($){
	var pas_function = new PASfunctions();
});


/************** start: popup functions. **************/
function loadPASPopup( adzone_id, popup_type, ajaxurl ) {

	jQuery(function($){
	if(paspopupStatus == 0) { // if value is 0, show popup
		
		$(".PasPopupCont").fadeIn(0500); // fadein popup div$
		$(".PasPopupCont").css({visibility: 'visible'});
		//$("#backgroundPasPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
		$("#backgroundPasPopup").fadeIn(0001);
		paspopupStatus = 1; // and set value to 1
		/*
		$.ajax({
		   type: "POST",
		   url: ajaxurl,
		   data: "action=buyandsell_popup_ajax_content&adzone_id="+adzone_id+"&popup_type="+popup_type,
		   success: function( msg ){
				//$('.hide_row').show();
				$('.pro_ads_buyandsell_'+popup_type+'_popup_'+adzone_id+'_ajax_content').html( msg );
				
				// Activate Ajax Uploads
				if(popup_type == 'buy'){
					load_ajax_upload(ajaxurl);
					
				}
		   }
		});
		*/
	}
	
	});
}

function disablePASPopup() {
	jQuery(function($){
		
	if(paspopupStatus == 1) { // if value is 1, close popup
		
		$(".PasPopupCont").fadeOut("normal");
		$("#backgroundPasPopup").fadeOut("normal");
		paspopupStatus = 0;  // and set value to 0
	}
	
	});
}
/************** end: popup functions. **************/


/************** start: Fly in functions. **************/
function loadPASFlyIn( adzone_id, delay, ajaxurl ) {

	jQuery(function($){
		
		var delay_sec = delay*1000;
		
		setTimeout(function(){ 
			//$('.pas_fly_in').fadeIn(0500, function(){
			$('.pas_fly_in').css({'visibility': 'visible'}).effect( "shake" );
			
		}, delay_sec); // 500 = .5 second delay
		
		
	});
}

function disablePASFlyIn(){
	jQuery(function($){
		$('.pas_fly_in').fadeOut("normal");
	});
}
/************** end: Fly in functions. **************/