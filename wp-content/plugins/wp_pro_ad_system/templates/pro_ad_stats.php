<?php
global $pro_ads_statistics;
?>

<div class="wrap">

<?php 
$wpproads_enable_stats = get_option('wpproads_enable_stats', 0);
if($wpproads_enable_stats)
{
	$group = isset($_GET['group']) ? $_GET['group'] : '';
	$group_id = isset( $_GET['group_id']) ? $_GET['group_id'] : '';
	
	echo $pro_ads_statistics->pro_ad_show_statistics(array('rid'  => 4, 'range' => 'day', 'day' => date('d'), 'group' => $group, 'group_id' => $group_id)); 
}
else
{
	?>
    <h2><?php _e('Statistics are disabled','wpproads'); ?></h2>
	<p><?php _e('You can enable statistics on the AD Dashboard under <em>General Settings</em>','wpproads'); ?></p>
    <?php
}
?>


</div>
<!-- end wrap -->