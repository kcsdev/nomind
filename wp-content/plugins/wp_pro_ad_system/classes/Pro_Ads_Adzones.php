<?php
class Pro_Ads_Adzones {	

	public function __construct() 
	{
		
	}
	
	
	
	
	
	/*
	 * Get all adzones
	 *
	 * @access public
	 * @return null
	*/
	public function get_adzones( $custom_args = array() ) 
	{	
		$args = array(
			'posts_per_page'   => -1,
			'post_type'        => 'adzones',
			'post_status'      => 'publish'
		);
		
		return get_posts( array_merge( $args, $custom_args ) );
	}
	
	
	
	
	
	
	
	/*
	 * Get all adzones
	 *
	 * @access public
	 * @return array
	*/
	public function get_adzone_data( $adzone_id, $device_size = '' ) 
	{
		$linked_banner_ids   		   = get_post_meta( $adzone_id, '_linked_banners', true );
		$adzone_description  		   = get_post_meta( $adzone_id, '_adzone_description', true );
		$adzone_rotation_type          = get_post_meta( $adzone_id, '_adzone_rotation_type', true );
		$adzone_rotation               = get_post_meta( $adzone_id, '_adzone_rotation', true );
		$adzone_size                   = get_post_meta( $adzone_id, '_adzone_size'.$device_size, true );
		$adzone_size                   = !empty($adzone_size) ? $adzone_size : get_post_meta( $adzone_id, '_adzone_size', true );
		$adzone_fix_size               = get_post_meta( $adzone_id, '_adzone_fix_size'.$device_size, true );
		$adzone_fix_size               = !empty($adzone_fix_size) ? $adzone_fix_size : 0;
		$responsive          		   = get_post_meta( $adzone_id, '_adzone_responsive'.$device_size, true );
		$responsive                    = !empty($responsive) ? $responsive : get_post_meta( $adzone_id, '_adzone_responsive', true );
		$custom_size                   = get_post_meta( $adzone_id, '_adzone_custom_size'.$device_size, true );
		$custom_size                   = !empty($custom_size) ? $custom_size : get_post_meta( $adzone_id, '_adzone_custom_size', true );
		$grid_horizontal     		   = get_post_meta( $adzone_id, '_adzone_grid_horizontal', true );
		$grid_vertical      		   = get_post_meta( $adzone_id, '_adzone_grid_vertical', true );
		$max_banners        		   = get_post_meta( $adzone_id, '_adzone_max_banners', true );
		$adzone_center      		   = get_post_meta( $adzone_id, '_adzone_center', true );
		$adzone_hide_empty   		   = get_post_meta( $adzone_id, '_adzone_hide_empty', true );
		
		//buyandsell
		$adzone_buyandsell_contract        = get_post_meta( $adzone_id, 'adzone_buyandsell_contract', true );
		$adzone_buyandsell_duration        = get_post_meta( $adzone_id, 'adzone_buyandsell_duration', true );
		$adzone_buyandsell_price           = get_post_meta( $adzone_id, 'adzone_buyandsell_price', true );
		$adzone_buyandsell_est_impressions = get_post_meta( $adzone_id, 'adzone_buyandsell_est_impressions', true );
		
		$grid_horizontal = !empty($grid_horizontal) ? $grid_horizontal : 1;
		$grid_vertical = !empty($grid_vertical) ? $grid_vertical : 1;
		
		$limit = $adzone_rotation ? count($linked_banner_ids) : $grid_horizontal * $grid_vertical;
		
		$size = !empty($adzone_size) ? explode('x', $adzone_size) : '';
		
		if( !$adzone_center )
		{
			if( $adzone_fix_size ) // $adzone_fix_size = 1 means it IS responsive
			{
				$size_str = !empty($size) ? 'max-width:'.$size[0].'px; max-height:'.$size[1].'px;' : ' width:auto; height:auto; display:inline-block; ';
			}
			else
			{
				$size_str = !empty($size) ? 'width:'.$size[0].'px; height:'.$size[1].'px;' : ' width:auto; height:auto; display:inline-block; ';
			}
		}
		else
		{
			if( $adzone_fix_size ) // $adzone_fix_size = 1 means it IS responsive
			{
				$size_str = !empty($size) ? 'max-width:'.$size[0].'px; max-height:'.$size[1].'px;' : ' width:100%; height:auto; ';
			}
			else
			{
				$size_str = !empty($size) ? 'width:'.$size[0].'px; height:'.$size[1].'px;' : ' width:100%; height:auto; '; 
			} 
		}
		
		$orderby = $adzone_rotation ? 'post__in' : 'rand';
		$margin = 3;
		
		$array = array(
			'adzone_id'                         => $adzone_id,       
			'linked_banner_ids'                 => $linked_banner_ids,
			'adzone_description'                => $adzone_description,
			'adzone_rotation_type'              => !empty($adzone_rotation_type) ? $adzone_rotation_type : 'flexslider',
			'adzone_rotation'                   => $adzone_rotation,
			'adzone_size'                       => $adzone_size,
			'responsive'                        => $responsive,
			'custom'                            => $custom_size,
			'orderby'                           => $orderby,
			'grid_horizontal'                   => $grid_horizontal,
			'grid_vertical'                     => $grid_vertical,
			'max_banners'                       => $max_banners,
			'limit'                             => $limit,
			'size'                              => $size,
			'size_str'                          => $size_str,
			'margin'                            => $margin,
			'adzone_center'                     => $adzone_center,
			'adzone_hide_empty'                 => $adzone_hide_empty,
			'adzone_buyandsell_contract'        => $adzone_buyandsell_contract,
			'adzone_buyandsell_duration'        => $adzone_buyandsell_duration,
			'adzone_buyandsell_price'           => $adzone_buyandsell_price,
			'adzone_buyandsell_est_impressions' => $adzone_buyandsell_est_impressions
		);
		
		return $array;
	}
	
	
	
	
	
	
	
	
	/*
	 * Check if adzone is sill available
	 *
	 * @param int $adzone_id, int $force_active_if_selected  (default: 0), int $banner_id (default: 0 // only used if $force_active_if_selected = 1)
	 * @access public
	 * @return int
	*/
	public function check_if_adzone_is_active( $adzone_id, $force_active_if_selected = 0, $banner_id = 0 ) 
	{
		$active = 1;
		$linked_banners = get_post_meta( $adzone_id, '_linked_banners', true );
		$max_banners    = get_post_meta( $adzone_id, '_adzone_max_banners', true );
		
		if( !empty($max_banners) && is_array($linked_banners) && count($linked_banners) >= $max_banners )
		{
			if( $force_active_if_selected )
			{
				$linked_adzones = get_post_meta( $banner_id, '_linked_adzones', true );
				$active = !empty($linked_adzones) ? in_array($adzone_id, $linked_adzones) ? 1 : 0 : 0;
			}
			else
			{
				$active = 0;
			}
		}
		
		return $active;
	}
	
	
	
	
	
	
	
	/*
	 * Check if adzone has available spots
	 *
	 * @param int $adzone_id
	 * @access public
	 * @return int
	*/
	public function check_if_adzone_has_available_spots( $adzone_id ) 
	{
		global $pro_ads_banners;
		
		$arr = $this->get_adzone_data( $adzone_id );	
		
		$max_ads = !empty($arr['max_banners']) ? $arr['max_banners'] : '';
		$active_banners = 0;
		
		// Count active banners
		if( !empty($arr['linked_banner_ids']))
		{
			$banners = $pro_ads_banners->get_banners( 
				array(
					'posts_per_page' => -1,
					'post__in'       => $arr['linked_banner_ids'], 
					'orderby'        => 'rand', 
					'meta_key'       => '_banner_status',
					'meta_value'     => 1
				)
			);
		}
		
		for($i = 0; $i < $arr['limit']; $i++ )
		{
			if( !empty($banners[$i]) )
			{
				$campaign_id = get_post_meta( $banners[$i]->ID, '_banner_campaign_id', true );
				$campaign_status = get_post_meta( $campaign_id, '_campaign_status', true );
				
				$active_banners = $campaign_status == 1 ? $active_banners+1 : $active_banners;
			}
		}
		
		$available_spots = !empty($max_ads) ? $max_ads - $active_banners : 1;
		
		return $available_spots;
	}
	
	
	
	
	
	
	/*
	 * Display Adzone
	 *
	 * @access public
	 * @return null
	*/
	public function display_adzone( $id, $atts = '' ) 
	{	
		global $pro_ads_main, $pro_ads_banners, $pro_ads_statistics, $pro_ads_multisite, $pro_ads_responsive, $pro_ads_bs_templates, $pro_ads_bs_woo_templates, $pro_geo_targeting_main;
		
		if( isset( $id ) )
		{
			$show = 1;
			$html = '';
			$device = $pro_ads_responsive->get_device_type();
			
			extract( shortcode_atts( array(
				'screen'             => $device['prefix'],
				'class'              => '',
				'align'              => '',
				'fixed'              => '',
				'padding'            => '',
				'background_color'   => '',
				'background_pattern' => '',
				'border'             => '',
				'border_color'       => '',
				'border_radius'      => '',
				'border'             => '',
				'border_color'       => '',
				'info_text'          => '',
				'info_text_position' => 'above',
				'font_size'          => 11,
				'font_color'         => '#C0C0C0',
				'text_decoration'    => 'none',
				'flyin'              => 0,
				'flyin_delay'        => 3,
				'corner_curl'        => 0,
				'hide_if_loggedin'   => 0
			), $atts ) );
			
			
			/*
			 * ADD-ON: Geo Targeting
			 *
			 *_______________________________________________________________________________________________________________
			 * Check if "Geo Targeting Plugin" is installed.
			*/
			if( $pro_ads_main->pro_geo_targeting_is_active() )
			{
				$show = $pro_geo_targeting_main->show_content( $id );
			}
			/*
			 *_______________________________________________________________________________________________________________
			*/
			
			
			/*
			 * Hide ads for logged in users
			 */
			$show = $hide_if_loggedin && is_user_logged_in() ? 0 : $show;
			/*
			 * Filter to show/hide adzones
			*/
			$show = apply_filters( 'wp_pro_ads_show_adzone', $show );
			
			
			if( $show )
			{
				$arr = $this->get_adzone_data($id, $screen);
				$active_banners = 0;
				$banners = '';
				
				if( !empty($arr['linked_banner_ids']) )
				{
					$defaults = array(
						'posts_per_page' => $arr['limit'],
						'post__in'       => $arr['linked_banner_ids'], 
						'orderby'        => $arr['orderby'], 
						'meta_key'       => '_banner_status',
						'meta_value'     => 1
						/*'meta_query'     => array(
							array(
								'key'     => '_banner_status',
								'value'   => 1,
								'compare' => '='
							)
						) */
					);
					$args = apply_filters( 'wp_pro_ads_load_adzone_banners', $defaults );
					$data = wp_parse_args( $args, $defaults );
					
					$banners = $pro_ads_banners->get_banners( $data );
					
					/*
					 * ADD-ON: Geo Targeting
					 *
					 *_______________________________________________________________________________________________________________
					 * Check if "Geo Targeting Plugin" is installed.
					*/
					if( $pro_ads_main->pro_geo_targeting_is_active() )
					{
						$banners = $pro_geo_targeting_main->geo_target_before_post($banners, 1);
					}
					/*
					 *_______________________________________________________________________________________________________________
					*/
				}
				
				// ADZONE OPTIONS
				$adzone_center = get_post_meta( $id, '_adzone_center', true );
				$css_center = $arr['responsive'] ? 'text-align:center;' : 'margin: 0 auto; text-align:center;';
				$center_css = $adzone_center ? $css_center : '';
				//$align_padding = $align == 'left' ? ' padding:10px 10px 10px 0;' : ' padding:10px 0 10px 10px;';
				$align_margin = $align == 'left' ? ' margin:10px 10px 10px 0;' : ' margin:10px 0 10px 10px;';
				$align_css = !empty($align) ? ' float:'.$align.'; '.$align_margin : '';
				$fixed_class = !empty($fixed) ? 'id="pas-sticky-div"' : '';
				$rotate_class = $arr['adzone_rotation'] && count($arr['linked_banner_ids']) > 1 ? 'wppasrotate'.rand() : '';
				$rotation_class_main = $rotate_class ? 'rotating_paszone' : '';
				$container_max_width = !empty($arr['size']) ? 'max-width:'.$arr['size'][0].'px;' : '';
				$wppasrotate_style = empty($banners) ? 'width:100%; height:100%;' : '';
				// Adzone border style
				$paszone_container_style = '';
				if(!empty($padding) && !empty($info_text))
				{
					$paszone_container_style.= $info_text_position != 'below' ? 'padding:0 '.$padding.'px '.$padding.'px; ' :  'padding:'.$padding.'px '.$padding.'px 0; ';
				}
				elseif( !empty($padding) )
				{
					$paszone_container_style.= 'padding:'.$padding.'px; ';
				}
				$paszone_container_style.= !empty($background_color) ? 'background-color:'.$background_color.'; ' : $paszone_container_style;
				$paszone_container_style.= !empty($border_radius) ? 'border-radius:'.$border_radius.'px; ' : $paszone_container_style;
				$paszone_container_style.= !empty($border) && !empty($border_color) ? 'border: solid '.$border.'px '.$border_color.'; ' : $paszone_container_style;
				
				
				if( empty($banners) && $arr['adzone_hide_empty'] )
				{
					// hide empty adzones is active
				}
				else
				{
					$html.= '<div '.$fixed_class.' class="paszone_container paszone-container-'.$id.' '.$screen.' '.$class.' '.$background_pattern.'" id="paszonecont_'.$id.'" style="overflow:hidden; '.$container_max_width.' '.$center_css.' '.$align_css.' '.$paszone_container_style.'">';
						$html.= !empty($info_text) && $info_text_position != 'below' ? '<div class="pasinfotxt"><small style="font-size:'.$font_size.'px; color:'.$font_color.'; text-decoration:'.$text_decoration.';">'.$info_text.'</small></div>' : '';
						
						$html.= '<div '.$fixed_class.' class="wpproadszone proadszone-'.$id.' '.$screen.' '.$class.'" id="'.$id.'" ajax="'.admin_url('admin-ajax.php').'" style="overflow:hidden; '.$arr['size_str'].' '.$center_css.' ">'; //'.$align_css.'
							
							//$html.= $rotate_class ? '<div class="'.$rotate_class.'_holder pasrotate_holder">' : '';
							$html .= '<div class="wppasrotate '.$rotation_class_main.' '.$rotate_class.' proadszoneholder-'.$id.'" style="'.$wppasrotate_style.'" >'; // style="display: inline-block; width:100%; height:100%;"
							
							if(!empty($banners))
							{
								foreach( $banners as $banner )
								{
									// check if campaign is active
									$pro_ads_multisite->wpproads_wpmu_load_from_main( array('type' => 'start', 'function' => 'set_blog_id') );
									
									$campaign_id = get_post_meta( $banner->ID, '_banner_campaign_id', true );
									$campaign_status = get_post_meta( $campaign_id, '_campaign_status', true );
									
									$pro_ads_multisite->wpproads_wpmu_load_from_main( array('type' => 'stop', 'function' => 'set_blog_id') );
									
									if( $campaign_status == 1 )
									{
										$html.= $pro_ads_banners->get_banner_item( $banner->ID, $id, '', $screen);
										$pro_ads_statistics->save_impression( $banner->ID, $id );
										$active_banners++;
									}
								}
							}
							else
							{
								/*
								 * ADD-ON: Buy and Sell
								 *
								 *_______________________________________________________________________________________________________________
								 * Check if "Buy and Sell Plugin" is installed.
								*/
								if( $pro_ads_main->buyandsell_is_active() )
								{
									$html.= $pro_ads_bs_templates->buyandsell_placeholder( $id );
								}
								/*
								 *_______________________________________________________________________________________________________________
								*/
								
								/*
								 * ADD-ON: Buy and Sell Woowommerce
								 *
								 *_______________________________________________________________________________________________________________
								 * Check if "Buy and Sell Woocommerce Plugin" is installed.
								*/
								if( $pro_ads_main->buyandsell_woo_is_active() )
								{
									$html.= $pro_ads_bs_woo_templates->buyandsell_placeholder( $id );
								}
								/*
								 *_______________________________________________________________________________________________________________
								*/
							}
							//$html.= $rotate_class ? '</div>' : '';
							$html.= '</div>';
						$html.= '</div>';
						$html.= !empty($info_text) && $info_text_position == 'below' ? '<div class="pasinfotxt"><small style="font-size:'.$font_size.'px; color:'.$font_color.'; text-decoration:'.$text_decoration.';">'.$info_text.'</small></div>' : '';
					$html.= '</div>';
					
					
					if( $arr['adzone_rotation'] && count($arr['linked_banner_ids']) > 1 && $active_banners > 1 )
					{
						$rotation_effect = get_post_meta( $id, '_adzone_rotation_effect', true );
						$rotation_effect = $rotation_effect == 'slide' && $arr['adzone_rotation_type'] == 'showoff' ? 'slideLeft' : $rotation_effect;
						$rotation_effect = $rotation_effect == 'slide' && $arr['adzone_rotation_type'] == 'bxslider' || $rotation_effect == 'slide' && $arr['adzone_rotation_type'] == 'flexslider' ? 'horizontal' : $rotation_effect;
						$rotation_effect = $rotation_effect == 'vertical' && $arr['adzone_rotation_type'] == 'showoff' ? 'slideLeft' : $rotation_effect;
						$rotation_effect = !empty($rotation_effect) ? $rotation_effect : 'fade';
						$rotation_time = get_post_meta( $id, '_adzone_rotation_time', true );
						$rotation_time = !empty($rotation_time) ? $rotation_time*1000 : 5000;
						
						
						if( $arr['adzone_rotation_type'] == 'showoff' || $corner_curl )
						{
							// Load jshowoff javascript file
							wp_enqueue_script('wppas_jshowoff', WP_ADS_TPL_URL . '/js/jquery.jshowoff.min.js', array( 'jquery' ), false, true );
							
							$html.= '<script type="text/javascript">';
								$html.= 'jQuery(document).ready(function($){';
									$html.= '$(".proadszoneholder-'.$id.'").jshowoff({';
										$html.= 'speed: '.$rotation_time.',';
										$html.= 'effect: "'.$rotation_effect.'",';
										//$html.= 'hoverPause: false,';
										$html.= 'controls: false,';
										$html.= 'links: false';
									$html.= '});'; 
								$html.= '});';
							$html.= '</script>';
						}
						else
						{
							// Load bxslider javascript file
							wp_enqueue_script('wppas_bxslider',  WP_ADS_TPL_URL . '/js/jquery.bxslider.min.pas.js', array( 'jquery' ), false, true );
							
							// Check if flyin delay
							$delay = $flyin_delay*1000;
							//$init_delay = $flyin ? 'initDelay: '.$delay.',' : '';
							
							// New rotation option bxslider - @since v4.3.4
							$html.= '<script type="text/javascript">';
								$html.= 'jQuery(window).load(function(){';
									$html.= 'jQuery(function($){';
									
										$html.= $flyin ? 'setTimeout(function(){ ' : '';
										
										$html.= 'var slider_'.$rotate_class.' = $(".'.$rotate_class.'").bxSlider({ ';
											$html.= 'mode: "'.$rotation_effect.'",';
											$html.= 'slideMargin: 5,';
											$html.= 'autoHover: true,';
											$html.= 'adaptiveHeight: true,';
											$html.= 'pager: false,';
											$html.= 'controls: false,';
											$html.= 'auto: true,';
											$html.= 'pause: $(".'.$rotate_class.'").find(".pasli").first().attr("duration"),';
											$html.= 'preloadImages: "all",';
											$html.= 'onSliderLoad: function(){ ';
												$html.= '$(".'.$rotate_class.'").find(".pasli").css("visibility", "visible");';
											$html.= '},';
											$html.= 'onSlideAfter: function( $slideElement, oldIndex, newIndex ){ ';
												$html.= 'slider_'.$rotate_class.'.setPause($($slideElement).attr("duration"));';
											$html.= '}';
										$html.= '});';
										
										$html.= $flyin ? '}, '.$delay.');' : '';
									
									$html.= '});';
								$html.= '});';
							$html.= '</script>';	
							
							
							/*
							// New rotation option - @since v4.3.0
							$html.= '<script type="text/javascript">';
								$html.= 'jQuery(window).load(function(){';
								$html.= 'jQuery(function($){';
								//$html.= 'jQuery(document).ready(function($){';
									$html.= '$hook = $(".'.$rotate_class.'_holder");';
									$html.= '$hook.flexslider({ ';
										$html.= $init_delay;
										$html.= 'selector: ".'.$rotate_class.' > .pasli",';
										$html.= 'animation: "'.$rotation_effect.'",';
										$html.= 'pauseOnHover: true,';
										$html.= 'directionNav: false, controlNav: false,';
										$html.= 'slideshowSpeed: $(".'.$rotate_class.'_holder").find(".pasli").attr("data-duration"),';
										$html.= 'smoothHeight: true,';
										$html.= 'start: function(slider){ ';
											$html.= '$(".'.$rotate_class.'_holder").find(".pasli").css({"visibility": "inherit"});';
											$html.= 'slider.manualPause = false;';
											$html.= 'slider.mouseover(function() {';
												$html.= 'slider.manualPause = true;';
												$html.= 'slider.pause();';
											$html.= '});';
											$html.= 'slider.mouseout(function() {';
												$html.= 'slider.manualPause = false;';
												$html.= 'slider.play();';
											$html.= '});';
										$html.= '},';
										$html.= 'after: function(slider){';
											$html.= 'slider.stop();';
											$html.= 'slider.vars.slideshowSpeed = $(slider.slides[slider.currentSlide]).data("duration");';
											$html.= 'slider.play();';
										$html.= '}';
									$html.= '});';
								$html.= '});';
								$html.= '});';
							$html.= '</script>';
							*/
						}
					}
				}
			}
			
		}
		else
		{
			$html.= __('Woops! we cannot find the adzone your looking for!', 'wpproads');	
		}
		
		return $html;
	}
	
	
	
	
	
	/*
	 * Display Adzone Grid
	 *
	 * @access public
	 * @return null
	*/
	public function display_adzone_grid( $id, $atts = '' )
	{
		global $pro_ads_main, $pro_ads_banners, $pro_ads_statistics, $pro_ads_templates, $pro_ads_bs_templates, $pro_ads_bs_woo_templates, $pro_geo_targeting_main;
		
		if( isset( $id ) )
		{
			$show = 1;
			$html = '';
			
			extract( shortcode_atts( array(
				'class'       => '',
				'align'       => '',
				'fixed'       => '',
				'info_text'   => ''
			), $atts ) );
			
			/*
			 * ADD-ON: Geo Targeting
			 *
			 *_______________________________________________________________________________________________________________
			 * Check if "Geo Targeting Plugin" is installed.
			*/
			if( $pro_ads_main->pro_geo_targeting_is_active() )
			{
				$show = $pro_geo_targeting_main->show_content( $id );
			}
			/*
			 *_______________________________________________________________________________________________________________
			*/
			
			if( $show )
			{
				$arr = $this->get_adzone_data($id);
				$grid_limit = $arr['grid_horizontal'] * $arr['grid_vertical'];
				$responsive = 0;
				$active_banners = 0;
				$banners = '';
				
				if( !empty($arr['size']) )
				{
					$size_str = 'width:'.$arr['size'][0].'px; height:'.$arr['size'][1].'px;';
				}
				else
				{
					$responsive = 1;
					$w = 100/$arr['grid_horizontal'] - $arr['margin']*$arr['grid_horizontal'];
					$size_str = 'width:'.$w.'%; height:auto; min-height:80px;';
				}
				
				if( !empty($arr['linked_banner_ids'] ))
				{
					//$orderby = $arr['adzone_rotation'] ? 'rand' : 'post__in';
					
					$defaults = array(
						'posts_per_page' => $arr['limit'],
						'post__in'       => $arr['linked_banner_ids'], 
						'orderby'        => 'rand', 
						'meta_key'       => '_banner_status',
						'meta_value'     => 1
					);
					$args = apply_filters( 'wp_pro_ads_load_adzone_banners', $defaults );
					$data = wp_parse_args( $args, $defaults );
					$banners = $pro_ads_banners->get_banners( $data );
					
					/*
					 * ADD-ON: Geo Targeting
					 *
					 *_______________________________________________________________________________________________________________
					 * Check if "Geo Targeting Plugin" is installed.
					*/
					if( $pro_ads_main->pro_geo_targeting_is_active() )
					{
						$banners = $pro_geo_targeting_main->geo_target_before_post($banners, 1);
					}
					/*
					 *_______________________________________________________________________________________________________________
					*/
				}
				
				$adzone_center = get_post_meta( $id, '_adzone_center', true );
				$center_css = $adzone_center ? 'text-align:center;' : '';
				$align_padding = $align == 'left' ? ' padding:10px 10px 10px 0;' : ' padding:10px 0 10px 10px;';
				$align_css = !empty($align) ? ' float:'.$align.'; '.$align_padding : '';
				$fixed_class = !empty($fixed) ? 'id="pas-sticky-div"' : '';
				
				if( empty($banners) && $arr['adzone_hide_empty'] )
				{
					// hide ampty adzones is active
				}
				else
				{
					$html.= '<div '.$fixed_class.' class="wpproadszone proadszone-'.$id.' wpproadgrid '.$class.'" style="overflow:hidden; '.$center_css.' '.$align_css.'">';
						$html.= !empty($info_text) ? '<div class="pasinfotxt"><small>'.$info_text.'</small></div>' : '';
						$html.= '<div style="display:inline-block;">';
						
							$b = 1;
							//for($i = 0; $i < $arr['limit']; $i++ )
							for($i = 0; $i < $grid_limit; $i++ )
							{
								// check if campaign is active
								$campaign_status = 1;
								if( !empty($banners[$i]) )
								{
									$campaign_id = get_post_meta( $banners[$i]->ID, '_banner_campaign_id', true );
									$campaign_status = get_post_meta( $campaign_id, '_campaign_status', true );
								}
								
								if( !empty($banners[$i]) && $campaign_status == 1 )
								{
									if( !$responsive )
									{
										$html.= '<div style="float:left; '.$size_str.' margin:'.$arr['margin'].'px;">'.$pro_ads_banners->get_banner_item( $banners[$i]->ID, $id, $arr['adzone_size']).'</div>';
									}
									else
									{
										$html.= '<div style="float:left; '.$size_str.' margin:'.$arr['margin'].'px;">'.$pro_ads_banners->get_banner_item( $banners[$i]->ID, $id).'</div>';
									}
									$pro_ads_statistics->save_impression( $banners[$i]->ID, $id );
									$active_banners++;
								}
								else
								{
									$html.= '<div style="float:left; '.$size_str.' margin:'.$arr['margin'].'px; background:#EEE;">';
										
										/*
										 * ADD-ON: Buy and Sell
										 *
										 *_______________________________________________________________________________________________________________
										 * Check if "Buy and Sell Plugin" is installed.
										*/
										if( $pro_ads_main->buyandsell_is_active() )
										{
											$html.= $pro_ads_bs_templates->buyandsell_placeholder( $id );
										}
										/*
										 *_______________________________________________________________________________________________________________
										*/
										
										/*
										 * ADD-ON: Buy and Sell Woowommerce
										 *
										 *_______________________________________________________________________________________________________________
										 * Check if "Buy and Sell Woocommerce Plugin" is installed.
										*/
										if( $pro_ads_main->buyandsell_woo_is_active() )
										{
											$html.= $pro_ads_bs_woo_templates->buyandsell_placeholder( $id );
										}
										/*
										 *_______________________________________________________________________________________________________________
										*/
										
									$html.= '</div>';
									
								}
								
								if( $b == $arr['grid_horizontal'] )
								{
									$html.= '<div style="clear:both;"></div>';
									$b = 0;
								}
								$b++;
							}
							
							$html.= '<div style="clear:both;"></div>';
						$html.= '</div>';
					$html.= '</div>';
				}
			}
		}
		
		return $html;
	}
	
	
	
	
	
	
	
	/*
	 * Display Adzone as Background ad
	 *
	 * @access public
	 * @return html
	*/
	public function display_adzone_as_background( $atts ) 
	{	
		global $pro_ads_main, $pro_ads_banners, $pro_ads_statistics, $pro_ads_bs_templates, $pro_geo_targeting_main;
		
		extract( shortcode_atts( array(
			'id' => '',
			'container' => '',
			'repeat'   => '',
			'stretch'   => '',
			'bg_color'  => ''
		), $atts ) );
		
		$html = '';
		$adzone_id = $id;
		
		if( !empty($adzone_id))
		{
			$show = 1;
			
			/*
			 * ADD-ON: Geo Targeting
			 *
			 *_______________________________________________________________________________________________________________
			 * Check if "Geo Targeting Plugin" is installed.
			*/
			if( $pro_ads_main->pro_geo_targeting_is_active() )
			{
				$show = $pro_geo_targeting_main->show_content( $id );
			}
			/*
			 *_______________________________________________________________________________________________________________
			*/
			
			if( $show )
			{
				$arr = $this->get_adzone_data($adzone_id);
				$active_banners = 0;
				$banners = '';
				
				if( !empty($arr['linked_banner_ids']) )
				{
					$banners = $pro_ads_banners->get_banners( 
						array(
							'posts_per_page' => $arr['limit'],
							'post__in'       => $arr['linked_banner_ids'], 
							'orderby'        => $arr['orderby'], 
							'meta_key'       => '_banner_status',
							'meta_value'     => 1
						)
					);
					
					
					/*
					 * ADD-ON: Geo Targeting
					 *
					 *_______________________________________________________________________________________________________________
					 * Check if "Geo Targeting Plugin" is installed.
					*/
					if( $pro_ads_main->pro_geo_targeting_is_active() )
					{
						$banners = $pro_geo_targeting_main->geo_target_before_post($banners, 1);
					}
					/*
					 *_______________________________________________________________________________________________________________
					*/
					
					if( $banners )
					{
						$banner_type = get_post_meta( $banners[0]->ID, '_banner_type', true );
						$banner_is_image = $pro_ads_banners->check_if_banner_is_image($banner_type);
						$banner_url = get_post_meta( $banners[0]->ID, '_banner_url', true );
						$banner_link = get_post_meta( $banners[0]->ID, '_banner_link', true );
						$banner_target = get_post_meta( $banners[0]->ID, '_banner_target', true );
						
						$pas_container = empty($container) ? 'body' : $container;
						$bg_repeat = empty($repeat) ? 'no-repeat' : 'repeat';
						$bg_stretch = empty($stretch) ? '' : '-webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;';
						$bg_color = empty($bg_color) ? '' : $bg_color;
						//http://work.tunasite.com/wp-content/uploads/2014/12/bg_ad_example.jpg
						$html.= '<style type="text/css" id="custom-background-css">';
						$html.= 'body { background-position:top center !important; background-color: '.$bg_color.' !important; background-image: url("'.$banner_url.'") !important; background-repeat: '.$bg_repeat.' !important; background-attachment: fixed !important; '.$bg_stretch.'}';
						$html.= '</style>';
						$html.= '<script type="text/javascript">/* <![CDATA[ */';
							$html.= 'var clickable_paszone = {';
							//$html.= '"link_left":"http://www.tunasite.com",';
							//$html.= '"link_right":"http://www.tunasite.com",';
							//$html.= '"link_left_target":"_blank",';
							//$html.= '"link_right_target":"_blank",';
							$html.= '"link_full":"'.addslashes( $pro_ads_banners->pro_ads_create_banner_link($banners[0]->ID, $adzone_id) ).'",';
							$html.= '"link_full_target":"'.$banner_target.'",';
							$html.= '"pas_container":"'.$pas_container.'"';
							$html.= '};';
						$html.= '/* ]]> */'; 
						$html.= 'jQuery(document).ready(function($){$(document).mousemove(function(event){
				var target = $( event.target );
				var target_id = event.target.id;
				
				if(target.is(clickable_paszone.pas_container) || target_id == clickable_paszone.pas_container){
					target.css("cursor", "pointer");
					$("#"+target_id).css("cursor", "pointer");
				}else{
					$(clickable_paszone.pas_container).css("cursor","auto");
					$("#"+clickable_paszone.pas_container).css("cursor","auto");
				}
			}); });</script>';
					}
				}
			}
		}
		else
		{
			$html.= __('Woops! we cannot find the adzone your looking for!', 'wpproads');	
		}
		
		return $html;
	}
	
	
	
	
	
	
	/*
	 * Display Adzone as Corner Curl
	 *
	 * @access public
	 * @return html
	*/
	public function display_adzone_as_corner_curl($atts)
	{
		global $pro_ads_main, $pro_ads_banners, $pro_ads_statistics, $pro_ads_bs_templates;
		
		extract( shortcode_atts( array(
			'id' => '',
			'corner_small'   => 26,
			'corner_big'     => 100,
			'corner_animate' => 1,
		), $atts ) );
		
		wp_enqueue_script('wp_pro_ads_corncurl', WP_ADS_TPL_URL.'/js/corncurl.min.js');
		
		$html = '';
		$adzone_id = $id;
		
		if( !empty($adzone_id))
		{
			$html.= '<div id="corncurl-cont"></div>';
			$html.= '<div id="corncurl-peel"><img src="'.WP_ADS_URL.'images/corner_curl.png"></div>';
			$html.= '<div id="corncurl-small-img"></div>';
			$html.= '<div id="corncurl-bg"><div class="corncurl-content">'.$this->display_adzone( $adzone_id, array('corner_curl' => 1) ).'</div></div>';
			
			$html.= '<script type="text/javascript">';	
				$html.= 'jQuery(document).ready(function($){ PAScorncurl({ corncurlSmall: '.$corner_small.', corncurlBig: '.$corner_big.', cornerAnimate:'.$corner_animate.' }); });';
			$html.= '</script>';
		}
		else
		{
			$html.= __('Woops! we cannot find the adzone your looking for!', 'wpproads');	
		}
		
		return $html;
	}
	
	
	
	
	
	
	
	
	
	/*
	 * Output Adzones
	 *
	 * @access public
	 * @param string $size, int $custom (default: 0), int $responsive (default:0)
	 * @return array or string
	*/
	public function pro_ad_output_adzone_size( $size, $custom = 0, $responsive = 0 )
	{
		if( !$custom && !$responsive )
		{
			$arr = array(
				'468x60'  => 'IAB Full Banner (468 x 60)',
				'120x600' => 'IAB Skyscraper (120 x 600)',
				'728x90'  => 'IAB Leaderboard (728 x 90)',
				'300x250' => 'IAB Medium Rectangle (300 x 250)',
				'120x90'  => 'IAB Button 1 (120 x 90)',
				'160x600' => 'IAB Wide Skyscraper (160 x 600)',
				'120x60'  => 'IAB Button 2 (120 x 60)',
				'125x125' => 'IAB Square Button (125 x 125)',
				'180x150' => 'IAB Rectangle (180 x 150)'
			);
			
			return $arr[$size];
		}
		elseif( $custom )
		{
			$sz = explode('x', $size);	
			return 'Custom ('.$sz[0].' x '.$sz[1].')';
		}
		else
		{
			return __('Full Width (100%)','wpproads');
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	/*
	 * Link Banner to adzone
	 *
	 * @access public
	 * @param 
	 * @return void
	*/
	public function pro_ad_link_banner_to_adzone( $adzone_id, $banner_id, $action_type = '' )
	{
		global $pro_ads_adzones, $pro_ads_banners;
	
		// link banner to adzone
		//update_post_meta( $_POST['aid'], '_linked_banners', ''  );
		$this->pro_ad_adzone_clean_linked_banners_array( $adzone_id );
		$linked_banners = get_post_meta( $adzone_id, '_linked_banners', true );
		$max_banners    = get_post_meta( $adzone_id, '_adzone_max_banners', true );
		$banner_status  = get_post_meta( $banner_id, '_banner_status', true );
		
		if( empty( $linked_banners ))
		{
			if( $pro_ads_adzones->check_if_adzone_is_active( $adzone_id ) && $banner_status == 1 || $pro_ads_adzones->check_if_adzone_is_active( $adzone_id ) && $banner_status == 3)
			{
				$linked_banners = array( $banner_id );
				update_post_meta( $adzone_id, '_linked_banners', array_values(array_filter($linked_banners))  );
				
				// link adzone to banner
				$pro_ads_banners->pro_ad_link_adzone_to_banner( $banner_id, $adzone_id, $action_type );
			}
		}
		else
		{
			if( $action_type == 'remove' )
			{
				if (($key = array_search($banner_id, $linked_banners)) !== false) unset($linked_banners[$key]);
				// link adzone to banner
				$pro_ads_banners->pro_ad_link_adzone_to_banner( $banner_id, $adzone_id, $action_type );
			}
			else
			{
				if( $pro_ads_adzones->check_if_adzone_is_active( $adzone_id ) && $banner_status == 1 || $pro_ads_adzones->check_if_adzone_is_active( $adzone_id ) && $banner_status == 3)
				{
					array_push($linked_banners, $banner_id);
					// link adzone to banner
					$pro_ads_banners->pro_ad_link_adzone_to_banner( $banner_id, $adzone_id, $action_type );
				}
			}
			update_post_meta( $adzone_id, '_linked_banners', array_values(array_filter($linked_banners)) );
		}
	}
	
	
	
	
	
	
	
	/*
	 * Clean linkedbanners array
	 *
	 * @access public
	 * @param 
	 * @return void
	*/
	public function pro_ad_adzone_clean_linked_banners_array( $adzone_id )
	{
		global $pro_ads_adzones, $pro_ads_banners;
		
		$linked_banners = get_post_meta( $adzone_id, '_linked_banners', true );
		
		if( !empty( $linked_banners ))
		{
			foreach( $linked_banners as $banner )
			{
				$check_banner = $pro_ads_banners->get_banners( array( 'post__in' => array( $banner ) ) );
				
				if(empty($check_banner))
				{
					if (($key = array_search($banner, $linked_banners)) !== false) unset($linked_banners[$key]);
					update_post_meta( $adzone_id, '_linked_banners', array_values(array_filter($linked_banners)) );	
				}
			}
		}
		
	}
	
}
?>