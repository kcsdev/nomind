<?php
class Pro_Ads_Main {	


	public $IP = '127.0.0.1';
	
	
	
	public function __construct() 
	{
		$this->get_visitor_ip();
		
		add_action( 'wp_loaded', array( $this, 'wpproads_rss' ) );	
	}
	
	
	
	
	
	
	/*
	 * Get Visitor IP
	 *
	 * @access public
	 * @return IP
	*/
	public function get_visitor_ip()
	{
		// Get the visitor IP    
        if(isset($_SERVER['HTTP_X_FORWARD_FOR']))
        {
            $this->IP = $_SERVER['HTTP_X_FORWARD_FOR'];
        } 
		else 
		{
            $this->IP = $_SERVER['REMOTE_ADDR'];
		}	
	}
	
	
	
	
	
	
	/*
	 * Get Visitor Device
	 *
	 * @since v4.2.9
	 * @access public
	 * @return string
	*/
	public function get_visitor_device()
	{
		global $pro_ads_browser;
		
		$device = 'desktop';
		
		if( $pro_ads_browser->isMobile() )
		{
			$device = 'mobile';
		}
		elseif( $pro_ads_browser->isTablet() )
		{
			$device = 'tablet';
		}
		
		return $device;
	}
	
	
	
	
	
	/*
	 * daily update function
	 *
	 * @access public
	 * @return array
	*/
	public function daily_updates( $force = 0) 
	{	
		global $pro_ads_campaigns, $pro_ads_banners;
		
		$last_update = get_option( 'wpproads_daily_update', 0 );
		$today = date('Y').date('m').date('d');
		
		if( $last_update < $today || $force )
		{
			$pro_ads_campaigns->update_campaign_status();
			
			$banners = $pro_ads_banners->get_banners( 
				array(
					'meta_key'       => '_banner_contract',
					'meta_value'     => 3
				)
			);
			
			foreach( $banners as $banner )
			{
				$pro_ads_banners->update_banner_status( $banner->ID );
			}
				
			update_option( 'wpproads_daily_update', $today );
		}
	}
	
	
	
	
	
	
	/*
	 * Check if ADD_ON Buy and Sell ads is active
	 *
	 * @access public
	 * @return array
	*/
	public function buyandsell_is_active() 
	{
		global $pro_ads_bs_templates;
		
		if( method_exists( $pro_ads_bs_templates, 'buyandsell_placeholder' ) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	
	
	/*
	 * Check if ADD_ON Buy and Sell Woocommerce ads is active
	 *
	 * @access public
	 * @return array
	*/
	public function buyandsell_woo_is_active() 
	{
		global $pro_ads_bs_woo_templates;
		
		if( method_exists( $pro_ads_bs_woo_templates, 'buyandsell_placeholder' ) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	
	
	
	/*
	 * Check if ADD_ON Geo Targetting
	 *
	 * @access public
	 * @return bool
	*/
	public function pro_geo_targeting_is_active() 
	{
		global $pro_geo_targeting;
		
		if( method_exists( $pro_geo_targeting, 'get_user_geo_data' ) )
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
	
	
	
	
	
	
	
	/*
	 * Geo Info - get city and country
	 *
	 * @access public
	 * @return array
	*/
	public function get_geo_info() 
	{	
		$ip = $_SERVER['REMOTE_ADDR'];
		$url = "http://www.geoplugin.net/xml.gp?ip=".$ip;
		//$url = "http://www.geoplugin.net/php.gp?ip=".$ip;
		
		$ch = curl_init();
		$timeout = 5; // set to zero for no timeout
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$content = curl_exec($ch);
		curl_close($ch);
		
		// since V4.1.4 till V4.1.6 and used again since V4.1.8
		$xml = simplexml_load_string($content);
		// since V4.1.7
		//$data = unserialize($content);
		
		/*if( $data !== false )
		{
			$geo = array(
				'city'         => !empty($data['geoplugin_city']) ? $data['geoplugin_city'] : '',
				'country'      => !empty($data['geoplugin_countryName']) ? $data['geoplugin_countryName'] : '',
				'country_cd'   => !empty($data['geoplugin_countryCode']) ? $data['geoplugin_countryCode'] : '',
				'continent_cd' => !empty($data['geoplugin_continentCode']) ? $data['geoplugin_continentCode'] : ''
			);
		}*/
		
		if ($xml !== false) 
		{
			$geo = array(
				'city'         => !empty($xml->geoplugin_city) ? $xml->geoplugin_city : '',
				'country'      => !empty($xml->geoplugin_countryName) ? $xml->geoplugin_countryName : '',
				'country_cd'   => !empty($xml->geoplugin_countryCode) ? $xml->geoplugin_countryCode : '',
				'continent_cd' => !empty($xml->geoplugin_continentCode) ? $xml->geoplugin_continentCode : '',
			);
		}
		else
		{
			$geo = array('city' => '', 'country' => '', 'country_cd' => '', 'continent_cd' => '');
		}
		
		return $geo;
	}
	
	
	
	
	
	
	/*
	 * Detect search engine bots
	 *
	 * @access public
	 * @return int $isbot
	*/
	public function detect_bots()
	{
		$isbot = 0;
		$bots = array(
			'Arachnoidea',
			'FAST-WebCrawler',
			'Fluffy the spider',
			'Googlebot',
			'Gigabot',
			'Gulper',
			'ia_archiver',
			'MantraAgent',
			'MSN',
			'Scooter',
			'Scrubby',
			'Teoma_agent1',
			'Winona',
			'ZyBorg',
			'WebCrawler',
			'W3C_Validator',
			'WDG_Validator',
			'Zealbot',
			'Robozilla',
			'Almaden'
		);
		
		foreach( $bots as $i => $bot )
		{
			if(strstr(strtolower($_SERVER['HTTP_USER_AGENT']), $bot))
			{
				$isbot = 1;
				return $isbot;
			}
		}
		
		return $isbot;
	}
	
	
	
	
	
	
	/*
	 * Adzone RSS feed
	 *
	 * @access public
	 * @return rss
	*/
	public function wpproads_rss( $adzone_id = 0 )
	{
		global $pro_ads_adzones;
		
		if( !empty( $adzone_id ) || isset( $_GET['wpproads-rss'] ) && !empty( $_GET['wpproads-rss'] ) )
		{
			$html = '';
			$adzoneID = !empty( $adzone_id ) ? $adzone_id : $_GET['wpproads-rss'];
			
			// http://kb.mailchimp.com/merge-tags/rss-blog/rss-item-tags
			// Mailchimp RSS code
			// *|RSSITEMS:|* *|RSSITEM:CONTENT_FULL|* *|END:RSSITEMS|*
			
			header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
			
			$html.= '<?xml version="1.0" encoding="UTF-8"?>';
			$html.= '<rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" version="2.0">';	
				$html.= '<channel>';
					$html.= '<title>'.get_bloginfo('name').'</title>';
					$html.= '<atom:link href="'.get_bloginfo('url').'/?wpproads-rss='.$adzoneID.'" rel="self" type="application/rss+xml" />';
					$html.= '<link>'.get_bloginfo('url').'</link>';
					$html.= '<description><![CDATA['.get_bloginfo('description').']]></description>';
					$html.= '<lastBuildDate>'.date('r', time()).'</lastBuildDate>';
					$html.= '<language>'.get_bloginfo('language').'</language>';
					$html.= '<generator>http://wordpress-advertising.com/?v='.WP_ADS_VERSION.'</generator>';
					
					$html.= '<item>';
						$html.= '<title>Adzone</title>';
						$html.= '<link>'.get_bloginfo('url').'</link>';
						$html.= '<guid isPermaLink="false">'.get_bloginfo('url').'/?wpproads-rss='.$adzoneID.'</guid>';
						$html.= '<description><![CDATA[ '.get_the_title( $adzoneID ).' ]]></description>';
						$html.= '<content:encoded><![CDATA['.$pro_ads_adzones->display_adzone( $adzoneID ).']]></content:encoded>';
						$html.= '<pubDate>'.date('r', time()).'</pubDate>';
					$html.= '</item>';
	
				$html.= '</channel>';
			$html.= '</rss>';
			
			echo $html;
			
			exit();
		}
	}
	
	
	
}