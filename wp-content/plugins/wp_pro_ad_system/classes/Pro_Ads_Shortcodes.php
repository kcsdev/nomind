<?php
class Pro_Ads_Shortcodes {	

	public function __construct() 
	{
		add_shortcode('pro_ad_display_adzone', array($this, 'sc_display_adzone'));
	}
	
	
	
	
	
	/*
	 * Shortcode function - [pro_ad_display_adzone id="1" popup="0"]
	 *
	 * @access public
	 * @return array
	*/
	public function sc_display_adzone( $atts, $content = null ) 
	{	
		global $pro_ads_templates, $pro_ads_adzones, $pro_ads_responsive;
		
		wp_enqueue_style("wp_pro_ads_style", WP_ADS_TPL_URL."/css/wpproads.min.css", false, WP_ADS_VERSION, "all");
		wp_enqueue_script('wp_pro_ads_js_functions', WP_ADS_TPL_URL.'/js/wppas.min.js');
		
		extract( shortcode_atts( array(
			'id'                 => 1,
			'class'              => '',
			'align'              => '',
			'fixed'              => '',
			'border'             => '',
			'border_color'       => '',
			'padding'            => '',
			'background_color'   => '',
			'background_pattern' => '',
			'border_radius'      => '',
			'border'             => '',
			'border_color'       => '',
			'popup'              => 0,
			'popup_bg'           => '',
			'popup_opacity'      => '',
			'flyin'              => 0,
			'flyin_delay'        => 3,
			'background'         => '',
			'corner_curl'        => 0,
			'corner_small'       => 26,
			'corner_big'         => 100,
			'corner_animate'     => 1,
			'container'          => '',
			'repeat'             => '',
			'stretch'            => '',
			'bg_color'           => '',
			'info_text'          => '',
			'info_text_position' => 'above',
			'font_size'          => 11,
			'font_color'         => '#C0C0C0',
			'text_decoration'    => 'none',
			'hide_if_loggedin'   => 0
		), $atts ) );
		
		
		//count($pro_ads_responsive->check_available_adzone_sizes( $id )
		
		$grid_horizontal   = get_post_meta( esc_attr($id), '_adzone_grid_horizontal', true );
		$grid_vertical     = get_post_meta( esc_attr($id), '_adzone_grid_vertical', true );
		
		// Check if adzone is popup
		if( esc_attr($popup) )
		{
			if( !empty($grid_horizontal) && !empty($grid_vertical) )
			{
				// popup grid
				$adzone = $pro_ads_templates->pro_ad_popup_screen( 
					array(
						'html' => $pro_ads_adzones->display_adzone_grid( esc_attr($id), $class ), 
						'adzone_id' => $id,
						'popup_bg'  => $popup_bg,
						'popup_opacity' => $popup_opacity
					) 
				);
			}
			else
			{
				// popup banner
				$adzone = $pro_ads_templates->pro_ad_popup_screen( 
					array(
						'html' => $pro_ads_adzones->display_adzone( esc_attr($id), $class ), 
						'adzone_id' => $id,
						'popup_bg'  => $popup_bg,
						'popup_opacity' => $popup_opacity 
					) 
				);
			}
		}
		// Check if adzone is fly in
		elseif( esc_attr($flyin) )
		{
			if( !empty($grid_horizontal) && !empty($grid_vertical) )
			{
				// fly in grid
				$adzone = $pro_ads_templates->pro_ad_fly_in( 
					array(
						'html' => $pro_ads_adzones->display_adzone_grid( esc_attr($id), $atts ), 
						'adzone_id' => $id,
						'delay' => $flyin_delay
					) 
				);
			}
			else
			{
				// fly in banner
				$adzone = $pro_ads_templates->pro_ad_fly_in( 
					array(
						'html' => $pro_ads_adzones->display_adzone( esc_attr($id), $atts ), 
						'adzone_id' => $id,
						'delay' => $flyin_delay
					) 
				);
			}
		}
		// Check if adzone is background ad
		elseif( esc_attr($background) )
		{
			$adzone = $pro_ads_adzones->display_adzone_as_background( $atts );
		}
		// Check if adzone is corner peeler
		elseif( esc_attr($corner_curl) )
		{
			$adzone = $pro_ads_adzones->display_adzone_as_corner_curl( $atts );
		}
		// Normal adzone
		else
		{
			if( !empty($grid_horizontal) && !empty($grid_vertical) )
			{
				// ad grid
				$adzone = $pro_ads_adzones->display_adzone_grid( esc_attr($id), $atts );
			}
			else
			{
				// normal
				$adzone = $pro_ads_adzones->display_adzone( esc_attr($id), $atts );
			}
		}
		
		return $adzone;
	}
}
?>