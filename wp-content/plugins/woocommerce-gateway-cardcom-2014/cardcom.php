<?php
/*
Plugin Name: WooCommerce CardCom Payment Gateway
Plugin URI: http://www.cardcom.co.il
Description: CardCom Payment gateway for woocommerce
Version: 1.0.0.20
Author: CardCom inc
Author URI: http://www.cardcom.co.il

*/

add_action('plugins_loaded', 'woocommerce_cardcom_init', 0);

function woocommerce_cardcom_init() {

	if ( ! class_exists( 'WC_Payment_Gateway' ) ) { return; }
 
	/**
 	* Gateway class
 	**/
	class WC_Gateway_Cardcom extends WC_Payment_Gateway {
	
		var $terminalnumber;
		var $username;
		var $operation;


		function __construct() { 
		
			$this->id = 'cardcom';
			$this->method_title = __('CardCom', 'woothemes');
			//$this->icon 			= WP_PLUGIN_URL . "/" . plugin_basename( dirname(__FILE__)) . '/images/sc_logo.png';
			$this->has_fields 		= false;
			
			$this->url = "https://secure.cardcom.co.il/external/LowProfileClearing2.aspx";


			// Load the form fields
			$this->init_form_fields();
			
			// Load the settings.
			$this->init_settings();
			
			// Get setting values
			$this->title 			= $this->settings['title'];
			$this->description 		= $this->settings['description'];
			$this->enabled 			= $this->settings['enabled'];
			$this->terminalnumber		= $this->settings['terminalnumber'];
			$this->adminEmail		= $this->settings['adminEmail'];

			$this->username	= $this->settings['username'];
			$this->currency = $this->settings['currency'];

			$this->operation = $this->settings['operation'];
			$this->invoice = $this->settings['invoice'];
			$this->maxpayment = $this->settings['maxpayment'];
			$this->lang = $this->settings['lang'];
			$this->UseIframe = $this->settings['UseIframe'];
				


			add_action( 'woocommerce_api_wc_gateway_cardcom', array( $this, 'check_ipn_response' ) );

			add_action('valid-cardcom-ipn-request', array(&$this, 'ipn_request') );
			add_action('valid-cardcom-successful-request', array(&$this, 'successful_request') );


			add_action('valid-cardcom-cancel-request', array(&$this, 'cancel_request') );

			add_action('woocommerce_receipt_cardcom', array(&$this, 'receipt_page'));
			// Hooks
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
					
		}
		
		//fix shipping by Or
		 function get_shipping_method_fixed($order) 
		 {
	
			$labels = array();
	
			// Backwards compat < 2.1 - get shipping title stored in meta
			if ( $order->shipping_method_title ) {
		
			  $labels[] = $order->shipping_method_title;
			} else {
		
			  // 2.1+ get line items for shipping
			  $shipping_methods = $order->get_shipping_methods();
		
			  foreach ( $shipping_methods as $shipping ) {
				$labels[] = $shipping['name'];
			  }
			}
		
			return implode(',', $labels);
		  }
		
		
		/**
		* Initialize Gateway Settings Form Fields
		*/
		function init_form_fields() {
	    
			$this->form_fields = array(
				'title' => array(
				'title' => __( 'Title', 'woothemes' ),
				'type' => 'text', 
				'description' => __( 'The title which the user sees during the checkout.', 'woothemes'), 
				'default' => __( 'Cardcom', 'woothemes' )
			), 
			'enabled' => array(
				'title' => __( 'Enable/Disable', 'woothemes' ), 
				'label' => __( 'Enable Cardcom', 'woothemes' ), 
				'type' => 'select',
				'options'=>array('yes'=>'Yes','no'=>'No'),
				'description' => '', 
				'default' => 'yes'
			), 
			'description' => array(
				'title' => __( 'Description', 'woothemes' ), 
				'type' => 'text', 
				'description' => __( 'The description which the user sees during the checkout.', 'woothemes'), 
				'default' => 'Pay with Cardcom.'
			), 
			'operation' => array(
				'title' => __( 'Operation', 'woothemes' ), 
				'label' => __( 'Operation', 'woothemes' ), 
				'type' => 'select',
				'options'=>array('1'=>'Charge Only','4'=>'Suspended Deal'),
				'description' => '', 
				'default' => '1'
			), 

			'invoice' => array(
				'title' => __( 'Invoice', 'woothemes' ), 
				'label' => __( 'Invoice', 'woothemes' ), 
				'type' => 'select',
				'options'=>array('1'=>'Yes','0'=>'No'),
				'description' => '', 
				'default' => '1'
			), 
			'terminalnumber' => array(
				'title' => __( 'Terminal Number', 'woothemes' ), 
				'type' => 'text', 
				'description' => __( 'Terminal Number', 'woothemes'), 
				'default' => '1000'
			), 
			'username' => array(
				'title' => __( 'Terminal Name', 'woothemes' ), 
				'type' => 'text', 
				'description' => __( 'Terminal Name', 'woothemes'), 
				'default' => 'yael29'
			),

			'maxpayment' => array(
				'title' => __( 'Max Payment', 'woothemes' ), 
				'type' => 'text', 
				'description' => __( 'Max Payment', 'woothemes'), 
				'default' => '1'
			),
			'currency' => array(
				'title' => __( 'Currency', 'woothemes' ), 
				'type' => 'text', 
				'description' => __( 'Currency: 1 - NIS , 2 - USD , else ISO Currency', 'woothemes'), 
				'default' => '1'
			),
			'lang' => array(
				'title' => __( 'Lang', 'woothemes' ), 
				'type' => 'text', 
				'description' => __( 'Lang', 'woothemes'), 
				'default' => 'en'
			),
			'adminEmail' => array(
				'title' => __( 'Admin Email', 'woothemes' ), 
				'type' => 'text', 
				'description' => __( 'Admin Email', 'woothemes'), 
				'default' => ''
			),
			'UseIframe' => array(
				'title' => __( 'Use Iframe', 'woothemes' ), 
				'label' => __( 'Use Iframe', 'woothemes' ), 
				'type' => 'select',
				'options'=>array('1'=>'Yes','0'=>'No'),
				'description' => '', 
				'default' => '0'
			)
			);
		}
	    
		/**
		* Admin Panel Options 
		* - Options for bits like 'title' and availability on a country-by-country basis
		*/

		function admin_options() {
		?>
			<h3><?php _e( 'CardCom', 'woothemes' ); ?></h3>
			<table class="form-table">
				<?php $this->generate_settings_html(); ?>
			</table><!--/.form-table-->
		<?php
		}
		
		/**
		* Check if this gateway is enabled and available in the user's country
		*/
		function is_available() {
			if ($this->enabled=="yes") :
				return true;
			endif;	
			
			return false;
		}
		
		/**
		* Payment form on checkout page
		*/
		function payment_fields() {
			
			?>

			<?php if ($this->description) : ?><p><?php echo $this->description; ?></p><?php endif; ?>
			<?php
		}
		
		/**
		* Process the payment
		*/
		function process_payment($order_id) {
			global $woocommerce;
			
			$order = new WC_Order( $order_id );
			
			if ($this->UseIframe==1)	
			{
				return array(
					'result' 	=> 'success',
					'redirect'	=> add_query_arg('order', $order->id, add_query_arg('key', $order->order_key, get_permalink(woocommerce_get_page_id('pay'))))
				);
			}
			else
			{
				return array(
					'result' 	=> 'success',
					'redirect'	=> $this->GetRedirectURL($order_id)

				);
			}
		}


			function GetRedirectURL( $order_id ) {
			global $woocommerce;
		

			$order = new WC_Order( $order_id );
			$SumToBill = number_format($order->get_total(), 2, '.', '') ;
			$params = array();
			$params["TerminalNumber"] = $this->terminalnumber ;

			$params["UserName"] = $this->username;
			$params["CodePage"] = "65001";
			$params["APILevel"] = "9";
			$params["SumToBill"] = $SumToBill;

			$params["Operation"] = $this->operation;
			$params["Languge"] = $this->lang;
			$params["CoinID"] = $this->currency;
			// Redirect
			$params["ErrorRedirectUrl"] = trailingslashit(home_url()).'?wc-api=WC_Gateway_Cardcom&'.('cardcomListener=cardcom_cancel&order_id='.$order->id);
			$params["IndicatorUrl"]=trailingslashit(home_url()).'?wc-api=WC_Gateway_Cardcom&'.('cardcomListener=cardcom_IPN&order_id='.$order->id);
			// http://shop.beteavone.com/?wc-api=WC_Gateway_Cardcom&cardcomListener=cardcom_successful&order_id=37
			$params["SuccessRedirectUrl"] = trailingslashit(home_url()).'?wc-api=WC_Gateway_Cardcom&'.('cardcomListener=cardcom_successful&order_id='.$order->id);
			$params["CancelUrl"] = trailingslashit(home_url()).'?wc-api=WC_Gateway_Cardcom&'.('cardcomListener=cardcom_cancel&order_id='.$order->id);

			if ($this->UseIframe==1){

				$params["ErrorRedirectUrl"] = trailingslashit(home_url())."pop.php?url=".urlencode($params["ErrorRedirectUrl"]);
				$params["SuccessRedirectUrl"] = trailingslashit(home_url())."pop.php?url=".urlencode($params["SuccessRedirectUrl"]);
				$params["CancelUrl"] = trailingslashit(home_url())."pop.php?url=".urlencode($params["CancelUrl"]);
			}

			$params["CancelType"] = "2";

			$params["ProductName"] = "Order Id:".$order->id;
			$params["ReturnValue"] = $order->id;

			// Helper Varibale for ansi
			$HebDiscountLocation = "";
			

			if ($this->operation == '4') // Req Params for Suspend Deal
			{
				if ($this->terminalnumber==1000)
				{
					$params['SuspendedDealJValidateType'] = "2";
				}
				else
				{
					$params['SuspendedDealJValidateType'] = "5";
				}

				$params['SuspendedDealGroup'] = "1";

			}

			if(!empty($this->maxpayment)&& $this->maxpayment>="1")
			{
				$params['MaxNumOfPayments']	= $this->maxpayment;
			}

			

			if($this->invoice == '1')
			{
			
 				$params['IsCreateInvoice']			= "true";
				$params['InvoiceHead.CustName']			= $order->billing_first_name." ".$order->billing_last_name;
				$params['InvoiceHead.CustAddresLine1']	= $order->billing_address_1;
				$params['InvoiceHead.CustCity']	= $order->billing_city;
	 	   		$params['InvoiceHead.CustAddresLine2']	= (isset($order->billing_address_2) ? $order->billing_address_2 : '');
				$params['InvoiceHead.CustLinePH']= $order->billing_phone;
				$params['InvoiceHead.Language']	= $this->lang;
				$params['InvoiceHead.Email'] = $order->billing_email;
				$params['InvoiceHead.SendByEmail']= 'true';
				$params['InvoiceHead.CoinID']= $this->currency;
				$params['InvoiceHead.ExtIsVatFree'] = 'false';
				$params['InvoiceHead.Comments'] = $order->id;
		
				$ItemsCount = 0;
				$AddToString  = "";
				$TotalLineCost = 0;
				foreach ($order->get_items() as $item) 
				{
					$ItemTotal =number_format( $order->get_item_total( $item, false ), 2, '.', '');
					$params['InvoiceLines'.$AddToString.'.Description']= substr(strip_tags( preg_replace("/&#\d*;/", " ", $item['name']) ), 0, 200);
					$params['InvoiceLines'.$AddToString.'.Price']=  $ItemTotal;
					$params['InvoiceLines'.$AddToString.'.Quantity']=  $item['qty'];
					$params['InvoiceLines'.$AddToString.'.ProductID']= $item["product_id"];
					$TotalLineCost +=($ItemTotal*$item['qty']);
					$ItemsCount++;
					$AddToString = $ItemsCount;
				}

				// Shipping :
				//if ( version_compare( WOOCOMMERCE_VERSION, '2.1', '>=' ) ) {
					//$shipping_total = $order->get_total_shipping();
				//} else {
					//$shipping_total = $order->get_shipping();
				//}

				$ItemShipping = number_format($order->get_total_shipping() + $order->get_shipping_tax(), 2, '.', '');
				if ($ItemShipping !=0)
				{
					$params['InvoiceLines'.$AddToString.'.Description']= substr(strip_tags( preg_replace("/&#\d*;/", " ", ucwords( $this->get_shipping_method_fixed($order) )) ), 0, 200);
					$params['InvoiceLines'.$AddToString.'.Price']= $ItemShipping;
					$params['InvoiceLines'.$AddToString.'.Quantity']=  1;
					$params['InvoiceLines'.$AddToString.'.ProductID']= "Shipping";
					$TotalLineCost +=$ItemShipping;
					$ItemsCount++;
					$AddToString = $ItemsCount;
				}

				// Discount :
				$order_discount = number_format( $order->get_order_discount(), 2, '.', '');
				if ($order_discount>0)
				{
					if ($this->lang=='he')
					{
						$HebDiscountLocation = $AddToString;
					}
					else
					{
						$params['InvoiceLines'.$AddToString.'.Description']= 'Discount';
					}
					$params['InvoiceLines'.$AddToString.'.Price']= -1*$order_discount;
					$params['InvoiceLines'.$AddToString.'.Quantity']= 1;
					$params['InvoiceLines'.$AddToString.'.ProductID']='Discount';
					$TotalLineCost -=$order_discount;
					$ItemsCount++;
					$AddToString = $ItemsCount;
				}


				if ($SumToBill-$TotalLineCost !=0)
				{
					$params['InvoiceLines'.$AddToString.'.Description']= "מע\"מ";
					$params['InvoiceLines'.$AddToString.'.Price']=  number_format( $SumToBill-$TotalLineCost, 2, '.', '');
					$params['InvoiceLines'.$AddToString.'.Quantity']= '1';
					$params['InvoiceLines'.$AddToString.'.ProductID']= 'Diff';
					$ItemsCount++;
					$AddToString = $ItemsCount;
				}
			}
			$urlencoded = http_build_query($params);
			if ($HebDiscountLocation !="")
			{
				$urlencoded = $urlencoded."&InvoiceLines".$HebDiscountLocation.".Description=%D7%94%D7%A0%D7%97%D7%94";
			}

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'https://secure.cardcom.co.il/BillGoldLowProfile.aspx');
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_FAILONERROR, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $urlencoded );
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curl, CURLOPT_FAILONERROR,true);

	        	$result = curl_exec($curl);

			$exp = explode(';',$result);

			$data = array();

 			if($exp[0] == "0")
			{
				$data['profile'] =$exp[1];
				update_post_meta( (int)$order->id, 'Profile',$data['profile']);

			}else
			{
				if ($this->adminEmail!='')
				{
					 wp_mail($this->adminEmail, 'Bill Gold: Transaction Faild (Bill Gold retrun an error)',
				   "Wordpress Transcation Faild!\n
				   ==== XML Response ====\n
				   Terminal Number:".$this->terminalnumber."\n
				   Error Code:			  ".$exp[0]."\n
				   ==== Transaction Details ====\n
				   Full Response :  ".$result."
				   Info:		  ".$urlencoded."\n"
				   );
				}
			}


			# $woocommerce->add_inline_js('jQuery("#cardcom_payment_form").submit();');
			// Change to On-hold to send email to customer
			//$order->update_status('on-hold', __( 'Awaiting payment result', 'woocommerce' ));
			
			$requestVars = array();
			$requestVars["terminalnumber"] = $this->terminalnumber ;
			$requestVars["Rcode"] = $exp[0] ;
			$requestVars["lowprofilecode"] = $exp[1] ;


			return $this->url."?". http_build_query($requestVars);
		}

		function generate_cardcom_form( $order_id ) {

	
			$URL = $this->GetRedirectURL( $order_id);

			$formstring = '<iframe width="100%" height="1000" frameborder="0" src="'.$URL.'" ></iframe>"';

			
			return $formstring;		

		}


		function receipt_page( $order ) {
			//echo '<p>'.__('Thank you for your order, please click the button below to pay with Cardcom.', 'woocommerce').'</p>';
			echo $this->generate_cardcom_form( $order );
		}


		function check_ipn_response() {
			

			if (isset($_GET['cardcomListener']) && $_GET['cardcomListener'] == 'cardcom_IPN'):
			
				@ob_clean();
			
				$_POST = stripslashes_deep($_REQUEST);

				header('HTTP/1.1 200 OK');
        		
            			do_action("valid-cardcom-ipn-request", $_REQUEST);
			
       			endif;

			if (isset($_GET['cardcomListener']) && $_GET['cardcomListener'] == 'cardcom_successful'):
			
				@ob_clean();
			
				$_POST = stripslashes_deep($_REQUEST);

				header('HTTP/1.1 200 OK');
        		
            			do_action("valid-cardcom-successful-request", $_REQUEST);
			
       			endif;

			if (isset($_GET['cardcomListener']) && $_GET['cardcomListener'] == 'cardcom_cancel'):
			
				@ob_clean();
			
				$_GET= stripslashes_deep($_REQUEST);

				header('HTTP/1.1 200 OK');
        		
            			do_action("valid-cardcom-cancel-request", $_REQUEST);
			
       			endif;

       		
		}

		function cancel_request( $get) {

			$order_id = intval($get["order_id"]);
			global $woocommerce;
		
			$order = new WC_Order( $order_id );

 			if(!empty($order->id))
			{

				wp_redirect($order->get_cancel_order_url());
				die();
			}

		}

		 
		function ipn_request( $posted ) {

			if($posted["DealRespone"] == 0)
			{
				$lowprofilecode = $posted["lowprofilecode"];
				$ResponseCode = $posted["ResponseCode"];
				$orderid = htmlentities($posted["order_id"]);
				
				if ($this->IsLowProfileCodeDealOneOK($lowprofilecode ,$this->terminalnumber,$this->username,$orderid)== '0')
				{
					$order = new WC_Order( $orderid );
					if(!empty($order->id))
					{
						update_post_meta( (int)$orderid , 'Cardcom InternalDealNumber ', $this->InternalDealNumberPro );
	
						$order->add_order_note( __('IPN payment completed OK!', 'woocommerce') );
						$order->update_status("completed");
						//$order->update_status("processing");
						$order->payment_complete();
						return true;
					}
				}
				else
				{
					$order = new WC_Order( $orderid );
					if(!empty($order->id))
					{
						$order->add_order_note( __('IPN payment completed Not OK', 'woocommerce') );
						$order->update_status("failed");
						return true;
					}
				}
			}
		}

		function successful_request( $posted ) {
			
			$orderid = htmlentities($posted["order_id"]);
			$order = new WC_Order( $orderid);

			if(!empty($order->id))
			{

	            $return_url = $this->get_return_url( $order );
 				wp_redirect($return_url);
				return true;
			}
			wp_redirect("/");
			return false;

		}

 	protected $InternalDealNumberPro;
 	protected $DealResponePro;
	function IsLowProfileCodeDealOneOK($lpc,$terminal,$username,$orderid)
	{
		$vars = array( 
			'TerminalNumber'=>$terminal, 
            'LowProfileCode'=>$lpc, 
            'UserName'=>$username 
		);


		
        # encode information
        $urlencoded = http_build_query($vars);
        $CR = curl_init();
        curl_setopt($CR, CURLOPT_URL, 'https://secure.cardcom.co.il/Interface/BillGoldGetLowProfileIndicator.aspx');
        curl_setopt($CR, CURLOPT_POST, 1);
        curl_setopt($CR, CURLOPT_FAILONERROR, true);
        curl_setopt($CR, CURLOPT_POSTFIELDS, $urlencoded );
        curl_setopt($CR, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($CR, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($CR, CURLOPT_FAILONERROR,true);
        $result = curl_exec( $CR );
        curl_close( $CR );
        $responseArray =  array(); 
        $returnvalue = '1';
        parse_str($result,$responseArray);

		$this->InternalDealNumberPro = 0;
		$this->DealResponePro = -1;

		if (isset($responseArray['InternalDealNumber'])){
				$this->InternalDealNumberPro = $responseArray['InternalDealNumber'];
		}

		if (isset($responseArray['DealResponse'])) #  OK!
		{
			$this->DealResponePro = $responseArray['DealResponse'];
		}
		else if (isset($responseArray['SuspendedDealResponseCode'])) #  Suspend Deal
		{
		      $this->DealResponePro = $responseArray['SuspendedDealResponseCode'];
		}


	    if (isset($responseArray['DealResponse'])
	    && $responseArray['DealResponse'] == '0' 
	    && $responseArray['ReturnValue']==$orderid) #  Normal Deal
        {
        	$returnvalue = '0';
        }
        else if (isset($responseArray['SuspendedDealResponseCode'])&& $responseArray['SuspendedDealResponseCode']== '0' && $responseArray['ReturnValue']==$orderid) #  Suspend Deal
		{
			$returnvalue = '0';
		}

		if ($returnvalue =='0'){
			// http://kb.cardcom.co.il/article/AA-00241/0
						add_post_meta( $orderid, 'Payment Gateway','CardCom'); 
						add_post_meta( $orderid, 'cc_number',$responseArray['ExtShvaParams_CardNumber5']); 
						add_post_meta( $orderid, 'cc_holdername',$responseArray['ExtShvaParams_CardOwnerName']); 

						add_post_meta( $orderid, 'cc_numofpayments',1+$responseArray['ExtShvaParams_NumberOfPayments94']); 
						if (1+$responseArray['ExtShvaParams_NumberOfPayments94']==1){
							add_post_meta( $orderid, 'cc_firstpayment',$responseArray['ExtShvaParams_Sum36']); 						}else
						{
							add_post_meta( $orderid, 'cc_firstpayment',$responseArray['ExtShvaParams_FirstPaymentSum78']); 	
						}
						
						add_post_meta( $orderid, 'cc_total',$responseArray['ExtShvaParams_Sum36']); 
			//			add_post_meta( $orderid, 'cc_paymenttype',$responseArray['']); 
						add_post_meta( $orderid, 'cc_cardtype',$responseArray['ExtShvaParams_Sulac25']); 
		}
        
        return $returnvalue;
	} 
	
	} // end woocommerce_sc
	
	/**
 	* Add the Gateway to WooCommerce
 	**/
	function add_cardcom_gateway($methods) {
		$methods[] = 'WC_Gateway_Cardcom';
		return $methods;
	}	
	
	
	add_filter('woocommerce_payment_gateways', 'add_cardcom_gateway' );
} 
// http://www.mrova.com/lets-create-a-payment-gateway-plugin-payu-for-woocommerce/