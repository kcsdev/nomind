<div class="wrap">	
<div id='pnlEdit' class='well'>
	<div id="pnlOptionsByImage" style="margin:10px">
		<div>
			<h2><?php _e("User setting", 'custom-forums'); ?></h2>
		</div>
		<div>
			<p><?php _e("User setting description( Usage: Pick a forum user from list, constituted from wordpress users, belonging to the 'forum users' group, then select a forum from all available forums in another list and click 'add' to assign selected user as a moderator of selected forum.)", 'custom-forums'); ?></p>
			<form method="post" id="forumModForm">
			<table class="wp-list-table widefat" cellspacing="0">
				<thead>
					<tr>
						<th scope="col" class="manage-column column-cb check-column" style="padding:10px;">
							<?php _e("Users", 'custom-forums'); ?>
						</th>	
						<th scope="col" class="manage-column column-cb check-column" style="padding:10px;">
							<?php _e("Forums", 'custom-forums'); ?>
						</th>	
					</tr>
				</thead>
				<tbody>
					<tr class="alternate author-self status-inherit" valign="top">
						<td class="parent column-parent" style="text-align: right;width:20%;padding-top: 9px;" rowspan="2">
							<select name="users" id="selectUsers" style="width: 100%;height: 350px;" size="20">
							<?php
								foreach ($forumUsers as $user) {
							?>
								<option value="<?php echo $user->ID; ?>" <?php if($user_id==$user->ID) { echo 'selected'; } ?>><?php echo $user->user_nicename; ?></option>
							<?php
								}
							?>
							</select> 
						</td>
						<td class="parent column-parent" style="width:80%;">		
							<select name="forums" style="width: 50%;margin-bottom: 5px;">
							<?php
								foreach ($forums as $forum) {
							?>
								<option value="<?php echo $forum->id; ?>"><?php echo $forum->title; ?></option>
							<?php
								}
							?>
							</select> 
							<input type="submit" name="btnAddForum" value="<?php _e("Add", 'custom-forums'); ?>" />					
							<hr style="margin-bottom: 0px;" />
						</td>
					</tr>
					<tr class="alternate author-self status-inherit" valign="top">
						<td class="parent column-parent" style="width:80%;height: 100%;">
							<input type="hidden" name="remove_forum" id="hdnRemoveForum" value="" />
							<table>
							<?php
								foreach ($forummods as $forummod) {
							?>
								<tr><td><?php echo $forummod->title; ?></td><td style="padding: 0px;"><input type="button" name="btnRemoveForum" value="Remove" class="btnRemove" style="float: right;" data-forum="<?php echo $forummod->id; ?>" /></td></tr>
							<?php
								}
							?>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			</form>
		</div>		
	</div>		
</div>
</div>

<script type="text/javascript">	
	jQuery(document).ready(function () {
		jQuery('.btnRemove').click(function() {
			jQuery('#hdnRemoveForum').val(jQuery(this).attr('data-forum'));
			jQuery('#forumModForm').submit();
		});

		jQuery('#selectUsers').change(function() {
			jQuery('#forumModForm').submit();
		});
	});
</script>