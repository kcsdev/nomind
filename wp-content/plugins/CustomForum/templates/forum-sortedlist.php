<style>
	.column6_sub_column1 {
		width: auto;
	}
	.rikuz-age-cols {
		width: 183px;
	}
	.rikuz-box-cols {
		width: 232px;
	}
	.rikuz-box-info {
		width: 93px;
	}
</style>
<div class="column6_block1 clearfix">
	<h2 class=" hdclass heading6"><del><?php _e('Forums', 'custom-forums'); ?></del></h2>
	<div class="rikuz-age-area">
	<?php
	cf_layout_forums_sorted();
	?>
	</div>
                        
	<div class="rikuz-box-area">
		<?php
		cf_layout_most_active_forums_box(); 
		cf_layout_newest_forums_box();
		cf_layout_featured_forums_box();
		?>
		<div class="clear"></div>
	</div>
	<div class="column6_block5">
		<h4 class="hdclass"><?php _e("All forums", 'custom-forums'); ?></h4>
		<?php
		$posts = cf_get_recent_posts($idUser);
		$j = 0;
		foreach($posts as $post) {
			$post_author = get_userdata($post->idUser);
			?>
				<li>
					<div class='postContainer posted_box1 <?php if($j % 2 != 0) {?> posted_box2 <?php }; if(!$post->isApproved) {?> post-unapproved <?php } ?> clearfix'>	
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $post->id; ?>'></input>					
						<div class = 'postHeader clearfix'>
							<a class = 'postSubject'><?php _e_db($post->subject); ?></a>
							<a class='click7 post-author'><?php _e_db($post_author->nickname); ?></a>
							<i><?php _e_db($post->date); ?></i>
							<?php if(strlen($post->attachment)) {
								?>
								<i><a href="<?php echo plugin_dir_url(); ?>CustomForum/attachments/<?php echo $post->attachment; ?>" target="_blank"><img src = '<?php echo plugin_dir_url(  ); ?>/CustomForum/images/attachment-icon.png'/></a></i>
								<?php 
							} ?> 
							<br>
							<div align="left">
	  							<div class = 'viewscounter'><?php _e_db($post->views); ?></div>&nbsp;
	  							<img src = '<?php echo plugin_dir_url(  ); ?>/CustomForum/images/views.png'/>
							</div>
						</div>
						<div class = 'postBody' data-plaintext = '<?php __db($post->text); ?>'>
							<div class = 'non-editable'>
								<?php echo $post->text; ?>
							</div>
							<textarea class="_ckeditor" name="postText" id = "ckeditor_<?php echo $post->id; ?>"><?php echo $post->text; ?></textarea>
						</div>
					</div>
				</li>
			<?php
		$j++;
		}
		?>
	</div>
</div>