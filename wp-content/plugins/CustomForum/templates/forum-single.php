<?php

$posts_per_page = 12;
if(get_option('post_per_page')!=null)
{
    $posts_per_page = get_option('post_per_page');
}
$pages_total = cf_get_pages_count($idForum, $posts_per_page, "forum");

$roleNumber = cf_get_user_role();

$forum = cf_get_forum_by_id($idForum);

$forum_author = get_userdata($forum->idUser);

$moderators = cf_get_moderators_by_forum_id($idForum);

$baseurl = get_permalink().'?cf_action='.$cf_action.'&idForum='.$idForum;

function cf_posts_treeview_layout($idForum, $idPost, $posts_per_page, $nav_page, $roleNumber) {
	
	$flatpostslist = cf_get_root_node_ids($idForum, $idPost, $posts_per_page, $nav_page);
	
	$nodes_info = cf_get_posts_info($idForum);
	
	$all_posts_hierarchy = cf_get_node_hierarchy($nodes_info, $idPost);
	
	$posts_hierarchy = new treeNode();
	foreach($flatpostslist as $post_id) {
		foreach($all_posts_hierarchy->children as $child) {
			if($child->id == $post_id) {
				$posts_hierarchy->addChild($child);
			}
		}
	}
	
	cf_prepare_child_posts_output($posts_hierarchy, $roleNumber, $idForum);
}



function cf_prepare_child_posts_output($treeNode, $roleNumber, $idForum, $depth = 0) {
	if(empty($treeNode->children)) {
		return;
	}
	?>
	<ul<?php if($depth == 0) {?> class = 'treeview-red'<?php }?>>
	<?php 
	$j=0;
	$depth++;
	
	if(count($treeNode->children >= 10)) {
		$ads = cf_get_random_ads(2);
	}
	elseif(count($treeNode->children >= 5)) {
		$ads = cf_get_random_ads(1);
	}
	
	foreach($treeNode->children as $childNode /*$num => $node*/) {
		$post_author = get_userdata($childNode->idUser);
		$idPost = $childNode->id;
		?>
		<li<?php if($childNode->expand) {?> class = 'post-highlighted'<?php } ?>>
			<div class='postContainer posted_box1 <?php if($childNode->isSticky) {?> post-sticky <?php }; if(!$childNode->isApproved) {?> post-unapproved <?php } if(($j+$depth) % 2 != 0) {?> posted_box2 <?php } ?> clearfix'>
				<form  enctype="multipart/form-data" method = 'post' action = '<?php echo get_permalink(); ?>'>
					<input type = 'hidden' name='cf_action' value='post-edit'></input>
					<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
					
					<div class = 'postHeader clearfix'>
						<a class = 'postSubject'><?php _e_db($childNode->subject); ?></a>
						<input type = 'text' name = 'title' class = 'subjEdit postEditControls'></input>
						<a class='click7 post-author'><?php _e_db($post_author->nickname); ?></a>
						<i><?php _e_db($childNode->date); ?></i>
						<?php if(strlen($childNode->attachment)) {
							?>
							<i><a href="/wp-content/uploads/CustomForumAttachments/<?php echo $childNode->attachment; ?>" target="_blank"><img src = '<?php echo plugin_dir_url(  ); ?>/CustomForum/images/attachment-icon.png'/></a></i>
							<?php 
						} ?> 
						<br>
						<div align="left">
  							<div class = 'viewscounter'><?php _e_db($childNode->views); ?></div>&nbsp;
  							<img src = '<?php echo plugin_dir_url(  ); ?>/CustomForum/images/views.png'/>
						</div>
					</div>
					<div class = 'postBody' data-plaintext = '<?php __db($childNode->text); ?>'>
						<div class = 'non-editable'>
							<?php echo $childNode->text; ?>
						</div>
						<textarea class="_ckeditor" name="postText" id = "ckeditor_<?php echo $childNode->id; ?>"><?php echo $childNode->text; ?></textarea>
					</div>
					
					<div class = 'postEditPanel'>
						<div class = 'postEditControls'>
							<input class = 'hiddenfileinput' type="file" name="attachment"/>
							<div class = 'uploaded-file-label'></div><button type = "button" class = 'fakeInputButton click6'><?php _e("Upload file", 'custom-forums'); ?></button><?php _e("Attachment", 'custom-forums'); ?><br>
							<div><?php echo __("Warning: max upload size equals", 'custom-forums')." ".get_option('max_file_size_kb')." KB"; ?></div>
							<br>
						</div>
						<input type = 'submit' class = 'click6 postEditControls' value = '<?php _e("Submit Post Modification", 'custom-forums'); ?>'/>
						<input type='button' class = 'cancelPostEdit click6 postEditControls' value = '<?php _e("Cancel", 'custom-forums'); ?>'></input>
					</div>
				</form>
				<div class = 'postControls'>
					
		<?php 
		if( $roleNumber==1 || ( $roleNumber==0 && (cf_is_mod_forum($idForum) || (get_current_user_id() == $childNode->idUser && count($childNode->children) == 0)))) {
			$fullAccess = false;
			if($roleNumber==1 || ($roleNumber==0 && cf_is_mod_forum($idForum))) 
				$fullAccess = true;
			?>
					<button class = 'EditPostButton click6' 
						data-parentid ='<?php echo $idPost; ?>'>
						<?php _e("Edit Message", 'custom-forums'); ?>
					</button>
			<?php 
			if($fullAccess)	{
				?>
					<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
						<input type = 'hidden' name='cf_action' value='post-delete'></input>
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
						<input type = 'submit' class = 'click6' value = '<?php _e("Delete", 'custom-forums'); ?>'/>
					</form>
				<?php 
			}
			?>
					
					<button type='button' class = 'CollapseThread click6'>
						<?php _e("Collapse", 'custom-forums'); ?>
					</button>
			
			<?php 
			if($fullAccess)	{
				?>
					<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
						<input type = 'hidden' name='cf_action' value='post-<?php echo ($childNode->isApproved == 1)?"dis":""?>approve'></input>
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
						<input type = 'submit' class = 'click6' value = '<?php _e(($childNode->isApproved == 1)?"Disapprove post":"Approve post", 'custom-forums'); ?>'/>
					</form>

					<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
						<input type = 'hidden' name='cf_action' value='post-stick'></input>
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
						<input type = 'hidden' name='stickPost' value ='<?php if(!$childNode->isSticky) echo "on"; ?>'></input>
						<input type = 'submit' class = 'click6'
							value = '<?php if($childNode->isSticky) {_e("Unstick", 'custom-forums'); } else {_e("Stick", 'custom-forums'); } ?>'/> 
					</form>
				<?php 
			}
			
			if(strlen($childNode->attachment)) {
				?>
				<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
					<input type = 'hidden' name='cf_action' value='post-unattach'></input>
					<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
					<input type = 'submit' class = 'click6' value = '<?php _e("Unattach file", 'custom-forums'); ?>'/> 
				</form>
				<?php 
			} 
		}
		else {
			?>
			<button type = 'button' class = 'CollapseThread click6'>
				<?php _e("Cancel", 'custom-forums'); ?>
			</button>
			<?php 
		}
		
		if($roleNumber != -1) {
			?>
					<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
						<input type = 'hidden' name='cf_action' value='post-subscribe'></input>
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
						<input type = 'hidden' name='subscribe' value ='<?php if(!$childNode->current_user_subscribed) echo "on"; ?>'></input>
						<input type = 'submit' class = 'click6' 
							value = '<?php if(!$childNode->current_user_subscribed) {_e("Subscribe", 'custom-forums'); } else {_e("Unsubscribe", 'custom-forums'); } ?>'/>
					</form>
					
					<button class = 'addReplyButton non-root click6'>
						<?php _e("Reply", 'custom-forums'); ?>
					</button>
					
					<div class = 'addReplyFormContainer hasparent' >
						<form enctype="multipart/form-data" action = "<?php echo get_permalink();?>" method = "post">
							<input class="click6" type = 'hidden' name='cf_action' value='post-add'></input>
							<input class="click6" type = 'hidden' name='idForum' value='<?php echo $idForum;?>'></input>
							<input class="click6" type = 'hidden' name='idParent' value='<?php echo $idPost; ?>'></input>
							<div class = 'flr'><?php _e("Subject", 'custom-forums'); ?></div><input type = 'text' name = 'title' class = 'subjEdit'></input><br>
							<br>
							<textarea class="_ckeditor_r" name="postText" id = "ckeditor_<?php echo $idPost; ?>_r"></textarea>
							<?php _e("Subscribe", 'custom-forums'); ?><input type = 'checkbox' name = 'subscribe'></input><br>
							<input class = 'hiddenfileinput' type="file" name="attachment"/>
							<div class = 'uploaded-file-label'></div><button type = "button" class = 'fakeInputButton click6'><?php _e("Upload file", 'custom-forums'); ?></button><?php _e("Attachment", 'custom-forums'); ?><br>
							<div><?php echo __("Warning: max upload size equals", 'custom-forums')." ".get_option('max_file_size_kb')." KB"; ?></div>
							<button type = 'button' class = 'CancelReply click6'>
								<?php _e("Cancel", 'custom-forums'); ?>
							</button>
							<input class="click6" type = 'submit'></input>
						</form>
					</div>
				</div>
			<?php 
		}
		
		?>
			</div><?php
		
		if($depth == 1 && ($j == 5 || $j == 10) && $ads) {
			if($j == 5)
				$ad = $ads[0];
			else
				$ad = $ads[1];
			cf_layout_ad($ad);
		}
		 cf_prepare_child_posts_output($childNode, $roleNumber, $idForum, $depth); ?>
		</li>
		<?php 
		$j++;
	}
	?>
	</ul>
	<?php 
}
?>

<div class="column6_block2 clearfix">
	<h2 class=" hdclass heading6 m-b5">
		<del>
			<a href = "<?php echo get_permalink()."?cf_action=forum-single&idForum=".$forum->id; ?>">
				<?php _e_db($forum->title); ?>
			</a>
		</del>
	</h2>
	<div class="text_box12">
		<?php echo $forum->description; ?>
	</div>
	<?php 
	cf_layout_forum_moderators($idForum);
	cf_layout_tag_list($idForum);
	cf_layout_cat_list($idForum);
	?>
</div>
	
<div class="column6_block3">
	<?php 
	cf_layout_related_forums($forum->id); 
	cf_layout_search();
	?>
	<div class="posted_box">
		<div class="posted_box_heading clearfix">
			<h4 class="hdclass fl-rt"><?php _e_db($forum_author->user_nicename); ?></h4>
			<h4 class="hdclass fl-lt"><?php _e_db($forum->date); ?></h4>   
		</div>
		<div class="posted_box_cont">
			<div class="text_box14"><?php echo $forum->openingpost; ?></div>
		</div>
	</div>

	<div class="column6_block4 clearfix">
		<ul class="cf_column6_block4_list">
			<li>
			
			<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
				<input type = 'hidden' name='cf_action' value='forum-subscribe'></input>
				<input type = 'hidden' name='idForum' value ='<?php echo $idForum; ?>'></input>
				<input type = 'hidden' name='subscribe' value ='<?php if(!cf_is_current_user_subscribed($idForum)) echo "on"; ?>'></input>
				<input type = 'submit' class = 'click6' 
					value = '<?php if(!cf_is_current_user_subscribed($idForum)) {_e("Subscribe", 'custom-forums'); } else {_e("Unsubscribe", 'custom-forums'); } ?>'/>
			</form>
			
			</li>
			<?php 
			cf_layout_auth($roleNumber);
			cf_layout_social();
			?>
			<li class="left1 no-margin"><a class="click6" href="<?php echo get_permalink(); ?>"><?php echo __( 'All forums', 'custom-forums' )?></a></li>
		</ul>
	</div>
	
	<div class="column6_block5">
		<h4 class="hdclass"><?php echo __( 'Recent posts', 'custom-forums' )?></h4>
		<?php  cf_posts_treeview_layout($idForum, $idPost, $posts_per_page, $nav_page, $roleNumber); ?>
		<?php if($roleNumber != -1) { cf_layout_add_new_post($idForum); } ?>
	</div>   
</div>

<?php 
cf_layout_pagination($baseurl, $nav_page, $pages_total);
?>