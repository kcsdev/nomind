<div class="wrap">	
<div id='pnlEdit' class='well'>
	<div id="pnlOptionsByImage" style="margin:10px">
		<div>
			<h2><?php _e("Ad settings", 'custom-forums'); ?></h2>
		</div>
		<div>
			<form method="post" id="forumAdForm" enctype="multipart/form-data">
			<table class="wp-list-table widefat" cellspacing="0">
				<thead>
					<tr>
						<th scope="col" class="manage-column column-cb check-column" style="padding:10px;">
							#
						</th>	
						<th scope="col" class="manage-column column-cb check-column" style="padding:10px;">
							<?php _e("Image", 'custom-forums'); ?>
						</th>
						<th scope="col" class="manage-column column-cb check-column" style="padding:10px;">
							<?php _e("Type", 'custom-forums'); ?>
						</th>
						<th scope="col" class="manage-column column-cb check-column" style="padding:10px;">
							<?php _e("Link", 'custom-forums'); ?>
						</th>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($adImages as $adImage) {
					?>
					<tr class="alternate author-self status-inherit" valign="top">
						<td class="parent column-parent">
							<?php echo $adImage['index']; ?>
						</td>						
						<?php echo true_image_uploader_field_for_ad_setting( 'imagefile'.$adImage['index'], $adImage['image'] ); ?>
						<td class="parent column-parent">
							<select name="linkType[]" value="<?php echo $adImage['link']; ?>" />
							<?php
							$linktypes = array(
								__("Same page", 'custom-forums'),
								__("Popup", 'custom-forums'),
								__("New tab", 'custom-forums'),
								);
							foreach($linktypes as $key => $linktype) {
								?>
								<option <?php if($adImage['type'] == $key) {?> selected = 'selected' <?php } ?> value = '<?php echo $key; ?>'><?php echo $linktype; ?></option>
								<?php 
							}
							?>
							</select>
						</td>
						<td class="parent column-parent">	
							<input type="text" name="imageLink[]" value="<?php echo $adImage['link']; ?>" />
						</td>
					</tr>
					<?php
						}
					?>
				</tbody>
			</table>
			<br />
			<input type="hidden" name="remove_image" id="hdnRemove" value="" />
			<input type="submit" id="btnSaveImage" value="Save" />
			</form>
		</div>		
	</div>		
</div>
</div>

<script type="text/javascript">	
	jQuery(document).ready(function () {
		jQuery('.btnRemove').click(function() {
			jQuery('#hdnRemove').val(jQuery(this).attr('data-index'));
			jQuery('#forumAdForm').submit();
		});

		jQuery('.upload_image_button').click(function(){
			var send_attachment_bkp = wp.media.editor.send.attachment;
			var button = jQuery(this);
			wp.media.editor.send.attachment = function(props, attachment) {
				jQuery('#img'+jQuery(button).prev().attr('id')).attr('src', attachment.url);
				jQuery(button).prev().val(attachment.id);
				wp.media.editor.send.attachment = send_attachment_bkp;
			}
			wp.media.editor.open(button);
			return false;    
		});

		jQuery('.remove_image_button').click(function(){
			var button = jQuery(this);		
			var src = jQuery('#img'+jQuery(button).prev().prev().attr('id')).attr('data-src');
			jQuery('#img'+jQuery(button).prev().prev().attr('id')).attr('src', src);
			jQuery('#'+jQuery(button).prev().prev().attr('id')).val('0');
			return false;
		});

	});
</script>