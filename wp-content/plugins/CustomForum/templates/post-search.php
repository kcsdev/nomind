<?php
$posts_per_page = 12;
if(get_option('post_per_page')!=null)
{
    $posts_per_page = get_option('post_per_page');
}

$roleNumber = cf_get_user_role();

//$baseurl = get_permalink().'?cf_action='.cf_get_action().'&idForum='.$idForum;


list($pages_total, $posts) = cf_search_post($idForum, $posts_per_page, $nav_page, $SearchText, $nicename, $datestart, $dateend, $fordays);

function cf_output_search_result($posts) {
	
	if(empty($posts)) {
		return;
	}
	?>
	<ul>
	<?php 
	$j=0;
	
	foreach($posts as $post) {
		$post_author = get_userdata($post->idUser);
		$idPost = $post->id;
		$idForum = cf_get_forum_by_post_id($idPost)->id;
		?>
		<li>
			<div class='postContainer posted_box1 <?php if(($j+$depth) % 2 != 0) {?> posted_box2 <?php } ?> clearfix'>
				<form method = 'post' action = '<?php echo get_permalink(); ?>'>
					<input type = 'hidden' name='cf_action' value='post-edit'></input>
					<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
					
					<div class = 'postHeader clearfix'>
						<a class = 'postSubject'><?php _e_db($post->subject); ?></a>
						<input type = 'text' name = 'title' class = 'subjEdit postEditControls'></input>
						<a class='click7'><?php _e_db($post_author->nickname); ?></a>
						<i><?php _e_db($post->date); ?></i>
					</div>
					<div class = 'postBody' data-plaintext = '<?php __db($post->text); ?>'>
						<?php 
						wp_editor( __db($post->text), 'my_tmce_'.$idPost, array(
							'textarea_name' => 'postText',
							'tinymce' => array(
								'directionality' => 'rtl',
								//'inline' => true,
								),
							//'teeny' => true,
							'quicktags' => false,
							'media_buttons' => false,
						));
						?>
						</div>
					
					<input name = 'submit_me' type = 'submit' class = 'click6 postEditControls' value = '<?php _e("Submit Post Modification", 'custom-forums'); ?>'/>
					<input type='button' class = 'cancelPostEdit click6 postEditControls' value = '<?php _e("Cancel", 'custom-forums'); ?>'></input>
				</form>
				<div class = 'postControls'>
					
		<?php 
		if($roleNumber==1 || ($roleNumber==0 && cf_is_mod_forum($idForum))) {
			?>
					<button class = 'EditPostButton click6' 
						data-parentid ='<?php echo $idPost; ?>'>
						<?php _e("Edit Message", 'custom-forums'); ?>
					</button>
					
					
					<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
						<input type = 'hidden' name='cf_action' value='post-delete'></input>
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
						<input type = 'submit' class = 'click6' value = '<?php _e("Delete", 'custom-forums'); ?>'/>
					</form>
					<?php
					if(!$post->isApproved) {
						?>
						<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
							<input type = 'hidden' name='cf_action' value='post-approve'></input>
							<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
							<input type = 'submit' class = 'click6' value = '<?php _e("Confirm post", 'custom-forums'); ?>'/>
						</form>
						<?php 
					} 
					?>
					<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
						<input type = 'hidden' name='cf_action' value='post-stick'></input>
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
						<input type = 'hidden' name='stickPost' value ='<?php if(!$post->isSticky) echo "on"; ?>'></input>
						<input type = 'submit' class = 'click6'
							value = '<?php if($post->isSticky) {_e("Unstick", 'custom-forums'); } else {_e("Stick", 'custom-forums'); } ?>'/> 
					</form>
			<?php 
		}
		else {
			?>
			<?php 
		}
		
		if($roleNumber != -1) {
			?>
					<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
						<input type = 'hidden' name='cf_action' value='post-subscribe'></input>
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
						<input type = 'hidden' name='subscribe' value ='<?php if(!$post->current_user_subscribed) echo "on"; ?>'></input>
						<input type = 'submit' class = 'click6' 
							value = '<?php if(!$post->current_user_subscribed) {_e("Subscribe", 'custom-forums'); } else {_e("Unsubscribe", 'custom-forums'); } ?>'/>
					</form>
					<button class = 'addReplyButton  click6'>
						<?php _e("Reply", 'custom-forums'); ?>
					</button>
					
					<div class = 'addReplyFormContainer' >
						<form action = "<?php echo get_permalink();?>" method = "post">
							<input class="click6" type = 'hidden' name='cf_action' value='post-add'></input>
							<input class="click6" type = 'hidden' name='idForum' value='<?php echo $idForum;?>'></input>
							<input class="click6" type = 'hidden' name='idParent' value='<?php echo $idPost; ?>'></input>
							<div class = 'flr'><?php _e("Subject", 'custom-forums'); ?></div><input type = 'text' name = 'title' class = 'subjEdit'></input><br>
							<?php
							wp_editor("", 'my_tmce_r_'.$idPost, array(
								'textarea_name' => 'postText',
								'tinymce' => array(
								'directionality' => 'rtl',
								),
								'quicktags' => false,
								'media_buttons' => false,
							));
							?>
							<?php _e("Subscribe", 'custom-forums'); ?><input type = 'checkbox' name = 'subscribe'></input><br>
							<input class="click6" type = 'submit'></input>
						</form>
					</div>
				</div>
			<?php 
		}
		
		if($j == 5 || $j == 10) {
			?>
				<div class='adsense9'>
					<a href='#'>
						<img src='<?php echo get_bloginfo('template_directory'); ?>/img/ad7.jpg' width='592' height='76' alt=''>
					</a>
				</div>
			<?php 
		}
		?>
			</div>
		</li>
		<?php 
		$j++;
	}
	?>
	</ul>
	<?php 
}
?>
	
	
<div class="column6_block2 clearfix">
	<h2 class=" hdclass heading6 m-b5">
		<del>
			<a href = "<?php echo get_permalink()."?cf_action=forum-single&idForum=".$forum->id; ?>">
				<?php _e_db($forum->title); ?>
			</a>
		</del>
	</h2>
	<div class="text_box12">
		<?php echo $forum->description; ?>
	</div>
	
</div>
	
<div class="column6_block3">
	<?php 
	cf_layout_search();
	?>

	<div class="column6_block4 clearfix">
		<ul class="cf_column6_block4_list">
			<li><a class="click6" href="#"><?php _e( 'Subscribe', 'custom-forums' )?></a></li>
			<?php 
			cf_layout_auth($roleNumber);
			cf_layout_social();
			?>
			<li class="left1 no-margin"><a class="click6" href="<?php echo get_permalink(); ?>"><?php _e( 'All forums', 'custom-forums' )?></a></li>
		</ul>
	</div>
	
	<div class="column6_block5">
		<h4 class="hdclass"><?php _e('Search results', 'custom-forums'); ?></h4>
		<?php  
		
		
		
		cf_output_search_result($posts);
		
		
		
		
		?>
	</div>   
</div>

<?php 
cf_layout_pagination($baseurl, $nav_page, $pages_total);
?>