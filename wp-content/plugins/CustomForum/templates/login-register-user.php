<div id="divOverback" style="width: 100%;height: 100%;background-color: #000;position: fixed;top: 0;left: 0;opacity: 0.5;<?php echo $displayOverback; ?>"></div>

<div id="divRegister" class ="modal-login" style="<?php echo $displaySignUp; ?>">
	<h3 style="margin: 10px;">Sign up</h3>
	<form id="signupForm" method="post" enctype="multipart/form-data">
	<table>		
		<tr>
			<td><?php _e('Nickname', 'custom-forums'); ?></td>
			<td><input type="text" name="nickname" id="sNickname" value="<?php echo $snickname; ?>" /></td>
		</tr>		
		<tr>
			<td><?php _e('Email', 'custom-forums'); ?></td>
			<td><input type="text" name="email" id="sEmail" value="<?php echo $semail; ?>" /></td>
		</tr>
		<tr>
			<td><?php _e('Login', 'custom-forums'); ?></td>
			<td><input type="text" name="slogin" id="sLogin" value="<?php echo $slogin; ?>" /></td>
		</tr>
		<tr>
			<td><?php _e('Password', 'custom-forums'); ?></td>
			<td><input type="password" name="password" id="sPassword" value="<?php echo $spassword; ?>" /></td>
		</tr>
		<tr>
			<td><?php _e('Confirm', 'custom-forums'); ?></td>
			<td><input type="password" name="passwordconfirm" id="sPasswordConfirm" value="<?php echo $spasswordconfirm; ?>" /></td>
		</tr>
		<tr>
			<td><?php _e('Avatar', 'custom-forums'); ?></td>
			<td>
				<input class = 'hiddenfileinput' type="file" id="avatar_image" name="avatar_image"/>
				<div class = 'uploaded-file-label'></div><button type = "button" class = 'fakeInputButton click6'><?php _e("Upload file", 'custom-forums'); ?></button><?php _e("Attachment", 'custom-forums'); ?><br>
				<?php wp_nonce_field( 'avatar_image', 'avatar_image_nonce' ); ?>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input class = 'click6' type="button" name="btnSignUp" value="Sign up" onclick="return clickSingUp();" />
				<input class = 'click6 fade' type="button" name="btnCancel" value="Cancel" onclick="return cancelModal();" />
				<span><?php echo $valid_error_message; ?></span>
			</td>
		</tr>
	</table>
	</form>
</div>

<div id="divLogin" class ="modal-login" style="<?php echo $displayLogIn; ?>">
	<h3 style="margin: 10px;">Log in</h3>
	<form id="loginForm" method="post">
	<table>
		<tr>
			<td><?php _e('Login', 'custom-forums'); ?></td>
			<td><input type="text" name="llogin" id="lLogin" value="<?php echo $llogin; ?>" /></td>
		</tr>
		<tr>
			<td><?php _e('Password', 'custom-forums'); ?></td>
			<td><input type="password" name="password" id="lPassword" value="<?php echo $lpassword; ?>" /></td>
		</tr>
		<tr>
			<td colspan="2">
				<input class = 'click6' type="button" name="btnLogIn" value="Log in" onclick="return clickLogIn();" />
				<input class = 'click6 fade' type="button" name="btnCancel" value="Cancel" onclick="return cancelModal();" />
				<span><?php echo $valid_error_message; ?></span>
			</td>
		</tr>
	</table>
	</form>
</div>

<div id="divMessage" class ="modal-login" style="<?php echo $displayMessage; ?>">
	<p><?php echo $messageRegistration; ?></p>
	<input class = 'click6 fade' type="button" name="btnCancel" value="Cancel" onclick="return cancelModal();" />
</div>


<script type="text/javascript">	
	jQuery(document).ready(function () {
		jQuery('#divOverback').click(cancelModal);
	});

	<?php if($cf_action == "user-confirm") { ?>
		function Redirect() {  
			window.location="<?php echo get_permalink(); ?>"; 
		} 
		setTimeout('Redirect()', 5000);  	
	<?php } ?>

	function clickSingUp()
	{
		var valid = true;
		if(jQuery("#sNickname").val()=="")
		{
			valid = false;	
		}
		if(jQuery("#sEmail").val()=="")
		{
			valid = false;	
		}
		if(jQuery("#sLogin").val()=="")
		{
			valid = false;	
		}
		if(jQuery("#sPassword").val()=="")
		{
			valid = false;	
		}
		if(jQuery("#sPasswordConfirm").val()=="")
		{
			valid = false;	
		}
		if(jQuery("#sPassword").val()!=jQuery("#sPasswordConfirm").val())
		{
			valid = false;	
		}

		if(valid)
		{
			jQuery("#signupForm").submit();			
		}
	}

	function clickLogIn()
	{
		var valid = true;
		if(jQuery("#lLogin").val()=="")
		{
			valid = false;	
		}
		if(jQuery("#lPassword").val()=="")
		{
			valid = false;	
		}

		if(valid)
		{
			jQuery("#loginForm").submit();			
		}
	}

	function openSingUp()
	{
		jQuery("#divOverback").show();
		jQuery("#divRegister").show();
		jQuery("#divLogin").hide();
		jQuery("#divMessage").hide();
	}

	function openLogIn()
	{
		jQuery("#divOverback").show();
		jQuery("#divLogin").show();
		jQuery("#divRegister").hide();
		jQuery("#divMessage").hide();
	}

	function cancelModal()
	{		
		jQuery("#divOverback").hide();
		jQuery("#divRegister").hide();
		jQuery("#divLogin").hide();
		jQuery("#divMessage").hide();
		<?php if($cf_action == "user-confirm") { ?>
			Redirect();
		<?php } ?>
	}
</script>