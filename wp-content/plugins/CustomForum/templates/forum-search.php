<?php
$posts_per_page = 12;
if(get_option('post_per_page')!=null)
{
    $posts_per_page = get_option('post_per_page');
}
$pages_total = cf_get_pages_count($idForum, $posts_per_page, "forum");

$roleNumber = cf_get_user_role();

$forum = cf_get_forum_by_id($idForum);

$forum_author = get_userdata($forum->idUser);

$moderators = cf_get_moderators_by_forum_id($idForum);

$baseurl = get_permalink().'?cf_action='.cf_get_action().'&idForum='.$idForum;

function cf_posts_treeview_layout($idForum, $idPost, $posts_per_page, $nav_page, $roleNumber) {
	
	$flatpostslist = cf_get_root_node_ids($idForum, $idPost, $posts_per_page, $nav_page);
	
	$nodes_info = cf_get_posts_info($idForum);
	
	$all_posts_hierarchy = cf_get_node_hierarchy($nodes_info, $idPost);
	
	$posts_hierarchy = new treeNode();
	foreach($flatpostslist as $post_id) {
		foreach($all_posts_hierarchy->children as $child) {
			if($child->id == $post_id) {
				$posts_hierarchy->addChild($child);
			}
		}
	}
	
	cf_prepare_child_posts_output($posts_hierarchy, $roleNumber, $idForum);
}



function cf_prepare_child_posts_output($treeNode, $roleNumber, $idForum, $depth = 0) {
	if(empty($treeNode->children)) {
		return;
	}
	?>
	<ul<?php if($depth == 0) {?> class = 'treeview-red'<?php }?>>
	<?php 
	$j=0;
	$depth++;
	
	foreach($treeNode->children as $childNode /*$num => $node*/) {
		$post_author = get_userdata($childNode->idUser);
		$idPost = $childNode->id;
		?>
		<li<?php if($childNode->expand) {?> class = 'post-highlighted'<?php } ?>>
			<div class='postContainer posted_box1 <?php if(($j+$depth) % 2 != 0) {?> posted_box2 <?php } ?> clearfix'>
				<form method = 'post' action = '<?php echo get_permalink(); ?>'>
					<input type = 'hidden' name='cf_action' value='post-edit'></input>
					<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
					
					<div class = 'postHeader clearfix'>
						<a class = 'postSubject'><?php _e_db($childNode->subject); ?></a>
						<input 	  name='title'    class = 'subjEdit postEditControls clearfix' type = 'text'></input>
						<a class='click7'><?php _e_db($post_author->nickname); ?></a>
						<i><?php _e_db($childNode->date); ?></i>
					</div>
					<div class = 'postBody' data-plaintext = '<?php __db($childNode->text); ?>'>
						<?php 
						wp_editor( __db($childNode->text), 'my_tmce_'.$childNode->id, array(
							'textarea_name' => 'postText',
							'tinymce' => array(
								'directionality' => 'rtl',
								//'inline' => true,
								),
							//'teeny' => true,
							'quicktags' => false,
							'media_buttons' => false,
						));
						//echo str_replace("\n","<br>",__db($childNode->text));
						//echo $childNode->text;
						?>
						</div>
					
					<input type = 'submit' class = 'click6 postEditControls' value = '<?php _e("Submit Post Modification", 'custom-forums'); ?>'/>
					<input type='button' class = 'cancelPostEdit click6 postEditControls' value = '<?php _e("Cancel", 'custom-forums'); ?>'></input>
				</form>
				<div class = 'postControls'>
					
		<?php 
		if($roleNumber==2 || ($roleNumber==1 && cf_is_mod_forum($idForum))) {
			?>
					<button class = 'EditPostButton click6' 
						data-parentid ='<?php echo $idPost; ?>'>
						<?php _e("Edit Message", 'custom-forums'); ?>
					</button>
					
					
					<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
						<input type = 'hidden' name='cf_action' value='post-delete'></input>
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
						<input type = 'submit' class = 'click6' value = '<?php _e("Delete", 'custom-forums'); ?>'/>
					</form>
					<?php
					if(!$childNode->isApproved) {
						?>
						<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
							<input type = 'hidden' name='cf_action' value='post-approve'></input>
							<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
							<input type = 'submit' class = 'click6' value = '<?php _e("Confirm post", 'custom-forums'); ?>'/>
						</form>
						<?php 
					} 
					?>
					<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
						<input type = 'hidden' name='cf_action' value='post-stick'></input>
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
						<input type = 'hidden' name='stickPost' value ='<?php if(!$childNode->isSticky) echo "on"; ?>'></input>
						<input type = 'submit' class = 'click6'
							value = '<?php if($childNode->isSticky) {_e("Unstick", 'custom-forums'); } else {_e("Stick", 'custom-forums'); } ?>'/> 
					</form>
			<?php 
		}
		else {
			?>
			
			<?php 
		}
		
		if($roleNumber != -1) {
			?>
					<form style = 'display: inline; float: left;' method = 'post' action = '<?php echo get_permalink(); ?>'>
						<input type = 'hidden' name='cf_action' value='post-subscribe'></input>
						<input type = 'hidden' id='hdnIdPost' name='idPost' value ='<?php echo $idPost; ?>'></input>
						<input type = 'hidden' name='subscribe' value ='<?php if(!$childNode->current_user_subscribed) echo "on"; ?>'></input>
						<input type = 'submit' class = 'click6' 
							value = '<?php if(!$childNode->current_user_subscribed) {_e("Subscribe", 'custom-forums'); } else {_e("Unsubscribe", 'custom-forums'); } ?>'/>
					</form>
					<button class = 'addReplyButton click6'
						data-idForum = '<?php echo $idForum; ?>' 
						data-parentid ='<?php echo $idPost; ?>'>
						<?php _e("Reply", 'custom-forums'); ?>
					</button>
				</div>
			<?php 
		}
		
		if($j == 5 || $j == 10) {
			?>
				<div class='adsense9'>
					<a href='#'>
						<img src='<?php echo get_bloginfo('template_directory'); ?>/img/ad7.jpg' width='592' height='76' alt=''>
					</a>
				</div>
			<?php 
		}
		?>
			</div><?php cf_prepare_child_posts_output($childNode, $roleNumber, $idForum, $depth); ?>
		</li>
		<?php 
		$j++;
	}
	?>
	</ul>
	<?php 
}
?>
	
<?php 

?>
	
<div id = 'addReplyFormContainer' style = 'display:none'>
	<form action = "<?php echo get_permalink();?>" method = "post">
		<input class="click6" type = 'hidden' name='cf_action' value='post-add'></input>
		<input class="click6" type = 'hidden' name='idForum' value='<?php echo $idForum;?>'></input>
		<input class="click6" type = 'hidden' name='idParent' value='0'></input>
		<?php _e("Subject", 'custom-forums'); ?><input type = 'text' name = 'title'></input><br>
		<?php _e("Message body", 'custom-forums'); ?><textarea name = 'postText'></textarea><br>
		<?php _e("Subscribe", 'custom-forums'); ?><input type = 'checkbox' name = 'subscribe'></input><br>
		<?php _e("Stick this post", 'custom-forums'); ?><input type = 'checkbox' name = 'stickPost'></input><br>
		<input class="click6" type = 'submit'></input>
	</form>
</div>
	
<div class="column6_block2 clearfix">
	<h2 class=" hdclass heading6 m-b5">
		<del>
			<a href = "<?php echo get_permalink()."?cf_action=forum-single&idForum=".$forum->id; ?>">
				<?php _e_db($forum->title); ?>
			</a>
		</del>
	</h2>
	<div class="text_box12">
		<?php echo $forum->description; ?>
	</div>
	<?php 
	cf_layout_forum_moderators($idForum);
	cf_layout_tag_list($idForum);
	cf_layout_cat_list($idForum);
	?>
</div>
	
<div class="column6_block3">
	<?php 
	cf_layout_related_forums($forum->id); 
	cf_layout_search();
	?>
	<div class="posted_box">
		<div class="posted_box_heading clearfix">
			<h4 class="hdclass fl-rt"><?php _e_db($forum_author->user_nicename); ?></h4>
			<h4 class="hdclass fl-lt"><?php _e_db($forum->date); ?></h4>   
		</div>
		<div class="posted_box_cont">
			<h4 class="hdclass"><?php _e_db($forum->title); ?></h4>	
			<div class="text_box14"><?php _e_db($forum->description); ?></div>
		</div>
	</div>

	<div class="column6_block4 clearfix">
		<ul class="cf_column6_block4_list">
			<li><a class="click6" href="#"><?php echo __( 'Subscribe', 'custom-forums' )?></a></li>
			<?php 
			cf_layout_auth($roleNumber);
			cf_layout_social();
			?>
			<li class="left1 no-margin"><a class="click6" href="<?php echo get_permalink(); ?>"><?php echo __( 'All forums', 'custom-forums' )?></a></li>
		</ul>
	</div>
	
	<div class="column6_block5">
		<h4 class="hdclass"><?php echo __( 'Recent posts', 'custom-forums' )?></h4>
		<?php  cf_posts_treeview_layout($idForum, $idPost, $posts_per_page, $nav_page, $roleNumber);
		if($roleNumber==2 || ($roleNumber==1 && cf_is_mod_forum($idForum))) {?>
			<button class = 'addReplyButton click6' data-idForum = '<?php echo $idForum; ?>'>Add new Post</button>
		<?php } ?>
	</div>   
</div>

<?php 
cf_layout_pagination($baseurl, $nav_page, $pages_total);
?>