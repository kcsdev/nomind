<?php
/*
error_reporting(E_ALL);
ini_set('display_errors', 'true');
*/

$forums_per_page = 12;
if(get_option('forum_per_page')!=null) {
    $forums_per_page = get_option('forum_per_page');
}
$pages_total = cf_get_pages_count($idForum, $forums_per_page, "main", $SearchType, $SearchText);
$forums_info = cf_get_forums_info($forums_per_page, $nav_page, $SearchType, $SearchText);



if($SearchType=='cat') {
	$HeaderForum = __('Forums in category', 'custom-forums').' "'.$SearchText.'"';
}
else if($SearchType=='tag') {
	$HeaderForum = __('Forums with associated tag', 'custom-forums').' "'.$SearchText.'"';
}
else if($SearchType=='firstchar') {
	$HeaderForum = __('Forums starting with the letter', 'custom-forums').' "'.$SearchText.'"';
}
else {
	$HeaderForum = __('All forums', 'custom-forums');
}

$baseurl = get_permalink().('?cf_action='.cf_get_action()).(isset($SearchText)?"&search=".$SearchText:"");

?>

<div class="column6_block3">
<?php if($roleNumber==-1){?>
	<input type="button" class="click6" onclick="return openLogIn();" value="<?php _e("Log in", 'custom-forums'); ?>"/>
	<input type="button" class="click6" onclick="return openSingUp();" value="<?php _e("Register", 'custom-forums'); ?> "/>
<?php } else { ?>
	<form method="post">
		<input type="submit" name="btnLogOut" class="click6" value="<?php _e("Log Out", 'custom-forums'); ?>"/>
	</form>
<?php } ?>
	<h2 class=" hdclass heading6 m-b5"><?php echo $HeaderForum; ?></h2>                                

	<?php 
	
	cf_layout_search();
	
	if(isset($forums_info)) {
		$j = 0;
		
		
		if(count($forums_info >= 10)) {
			$ads = cf_get_random_ads(2);
		}
		elseif(count($forums_info >= 5)) {
			$ads = cf_get_random_ads(1);
		}
		
		foreach($forums_info as $forum) {
			?>
			
			<div class="img_info_box2 clearfix">
				<div class="img_box5_out1 img_box5_out2">
					<div class="img_box9 img_box20">
					<a href="<?php echo get_permalink()."?cf_action=forum-single&idForum=".$forum->id; ?>">
						<img src="<?php echo cf_get_forum_image_path($forum); ?>" alt="<?php plugin_dir_path( __FILE__ ); ?>/images/no-image.jpg">
					</a>
					</div>
				</div>
				<div class="img_box9_cont">
					<h2 class="hdclass f_size1"><?php _e_db($forum->title); ?></h2>
					<div class="text_box9"><?php _e_db($forum->description); ?></div>
				</div>
		<?php if($roleNumber==1 || cf_is_mod_forum($forum->id)) { ?>
			<button class = "addReplyButton click6"><?php _e("Edit Forum", 'custom-forums'); ?></button><br>
				<div class = 'addReplyFormContainer'>
				<form action = "<?php echo get_permalink();?>" method = "post">
					<input type = 'hidden' name='cf_action' value='forum-add'></input>
					<input type = 'hidden' name='idForum' value = <?php _e_db($forum->id); ?>></input>	
					<table>
						<tr>
							<td><?php _e("Title:", 'custom-forums'); ?></td>							
							<td><textarea type = 'text' name = 'title'><?php _e_db($forum->title); ?></textarea></td>
						</tr>
						<tr>
							<td><?php _e("Description:", 'custom-forums'); ?></td>							
							<td><textarea name = 'description'><?php _e_db($forum->description); ?></textarea></td>
						</tr>
						<tr>
							<td><?php _e("Opening Post:", 'custom-forums'); ?></td>							
							<td>
								<textarea class = '_ckeditor_r' id = 'ckeditor_<?php echo $forum->id; ?>_r' name = 'openingpost'>
									<?php echo $forum->openingpost; ?>
								</textarea>
							</td>
						</tr>
						<tr>
							<td><?php _e("Featured:", 'custom-forums'); ?></td>
							<td><input type = 'checkbox' name = 'featured' <?php if($forum->featured) {?> checked <?php }?>></input></td>
						</tr>
						<tr>
							<td><?php _e("Image:", 'custom-forums'); ?></td>
							<td>
							<table>
								<th>
								<?php echo true_image_uploader_field('hdnImages'.$forum->id, $forum->image); ?>
								</th>
							</table>
							</td>
						</tr>
						<tr>
							<td><?php _e("Tags:", 'custom-forums'); ?></td>
							<td>
								<?php 
								$tag_ids = cf_get_tag_ids_by_forum_id($forum->id);
								?>
								<select class = 'cf-inline select-tags' value = '<?php _e_db(implode(",",$tag_ids)); ?>' name = "tags[]" multiple = "multiple">
								<?php
								$tags = cf_get_forum_tags();
								foreach($tags as $tag) {
									?>
									<option<?php if(in_array($tag->ID, $tag_ids)) {?> selected = "selected" <?php } ?> value = "<?php echo $tag->ID; ?>">
										<?php _e_db($tag->post_title); ?>
									</option>
									<?php 
								}
								?>
								</select>
								<div class = 'cf-inline'>
									<input class = 'ajax-add-tag' type = 'text' /><br>
									<button class = 'cf-ajax-button' type = 'button' ><?php _e("Add new tag", 'custom-forums'); ?></button>
								</div>
							</td>
						</tr>
						<tr>
							<td><?php _e("Categories:", 'custom-forums'); ?></td>
							<td>
							<?php 
								$cat_ids = cf_get_cat_ids_by_forum_id($forum->id);
								?>
								<select class = 'cf-inline select-categories' value = '<?php _e_db(implode(",",$cat_ids)); ?>' name = "categories[]" multiple = "multiple">
								<?php
								$categories = cf_get_forum_categories();
								foreach($categories as $category) {
									?>
									<option<?php if(in_array($category->ID, $cat_ids)) {?> selected = "selected" <?php } ?> value = "<?php echo $category->ID; ?>">
										<?php _e_db($category->post_title); ?>
									</option>
									<?php 
								}
								?>
								</select>
								<div class = 'cf-inline'>
									<input class = 'ajax-add-category' type = 'text' /><br>
									<button class = 'cf-ajax-button' type = 'button' ><?php _e("Add new category", 'custom-forums'); ?></button>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">
								<input class="click6" type = 'submit' value = '<?php _e("Submit", 'custom-forums'); ?>'></input>
								<button type = "button" class = 'CancelEditMode click6'>
									<?php _e("Cancel", 'custom-forums'); ?>
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		<?php } ?>
			</div>
			<?php 
			/*
			$buttonForum = "<li><div class='posted_box1 ".(($j % 2 != 0)?" posted_box2 ":"")." clearfix'>";
			if($roleNumber==2 || ($roleNumber==1 && cf_is_mod_forum($forum->id))) {
				$buttonForum .= "<form style = 'display: inline; float: left;' method = 'post' action = '".get_permalink()."'>
						<input type = 'hidden' name='cf_action' value='forum-delete'></input>
						<input type = 'hidden' name='idForum' value ='".$forum->id."'></input>
						<input type = 'submit' class = 'click6' value = '".__("Delete", 'custom-forums')."'/>
					</form>
					<button data-tags='".__db(implode(",",cf_get_tag_names_by_forum_id($forum->id))).
					"' data-cats='".__db(implode(",",cf_get_cat_names_by_forum_id($forum->id))).
					"' data-description='".__db($forum->description).
					"' data-title='".__db($forum->title).
					"' data-idforum ='".$forum->id.
					"' data-featured ='".($forum->featured?1:0).
					"' class = 'addReplyButton click6'>".__("Edit", 'custom-forums')."</button>";
			}
			$buttonForum .= "<a href = '".get_permalink()."?cf_action=forum-single&idForum=".$forum->id."'>".__db($forum->title)."</a></div>";

			echo $buttonForum;
			*/
			
			if(($j == 5 || $j == 10) && $ads) {
				if($j == 5)
					$ad = $ads[0];
				else
					$ad = $ads[1];
				cf_layout_ad($ad);
			}	
			
			$j++;
		}
	} 
	?>	

	<?php if($roleNumber==1) { ?>
	<button class = "addReplyButton click6"><?php _e("Add New Forum", 'custom-forums'); ?></button><br>
	<div class = 'addReplyFormContainer'>
		<form action = "<?php echo get_permalink();?>" method = "post">				
			<input type = 'hidden' name='cf_action' value='forum-add'></input>
			<input type = 'hidden' name='idForum'></input>
			<?php _e("Title:", 'custom-forums'); ?><textarea type = 'text' name = 'title'></textarea><br>
			<?php _e("Description:", 'custom-forums'); ?><textarea name = 'description'></textarea><br>
			<?php _e("Opening Post:", 'custom-forums'); ?><textarea class = '_ckeditor' id = 'ckeditor_0' name = 'openingpost'></textarea><br>
			<?php _e("Featured:", 'custom-forums'); ?><input type = 'checkbox' name = 'featured'></input><br>
			<?php _e("Image:", 'custom-forums'); ?><br><?php echo true_image_uploader_field('hdnImages'); ?><!--<input type="file" name="imagefile" />--><br>
			
			<?php _e("Tags:", 'custom-forums'); ?>
				<select value = '<?php _e_db(implode(",",$tag_ids)); ?>' name = "tags[]" multiple = "multiple">
				<?php
				$tags = cf_get_forum_tags();
				foreach($tags as $tag) {
					?>
					<option value = "<?php echo $tag->ID; ?>">
						<?php _e_db($tag->post_title); ?>
					</option>
					<?php 
					}
				?>
				</select><br>
			
			
			<?php _e("Categories:", 'custom-forums'); ?>
			<select value = '<?php _e_db(implode(",",$cat_ids)); ?>' name = "categories[]" multiple = "multiple">
				<?php
				$categories = cf_get_forum_categories();
				foreach($categories as $category) {
					?>
					<option value = "<?php echo $category->ID; ?>">
						<?php _e_db($category->post_title); ?>
					</option>
					<?php 
				}
				?>
			</select><br>
			

			<input class="click6" type = 'submit'></input>
			<button type = "button" class = 'CancelEditMode click6'>
				<?php _e("Cancel", 'custom-forums'); ?>
			</button>
		</form>
	</div>
	<?php } ?>
</div>
		


<?php 
cf_layout_pagination($baseurl, $nav_page, $pages_total);
?>