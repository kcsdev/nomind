<?php
if($topic->deleted) {
	header("Location: ".get_permalink()."?cf_action=forum-list");
}
$idTopic = $topic->id;
$forum = cf_get_forum_by_topic_id($idTopic);
if($forum->deleted) {
	header("Location: ".get_permalink()."?cf_action=forum-list");
}
$idForum = $forum->id;
$topic_author = get_userdata($topic->idUser);
$roleNumber = cf_get_user_role();

$posts_per_page = 12;
$pages_total = cf_get_pages_count($idTopic, $posts_per_page, "topic");
$nav_page = get_query_var("nav_page")?get_query_var("nav_page"):1;



$moderators = cf_get_moderators_by_forum_id($forum->id);

function cf_posts_treeview_layout($idTopic, $posts_per_page, $nav_page, $roleNumber) {
	
	$flatpostslist = cf_get_root_node_ids($idTopic, $posts_per_page, "topic", $nav_page);
	
	$nodes_info = cf_get_posts_info($idTopic);
	$all_posts_hierarchy = cf_get_node_hierarchy($nodes_info);
	
	foreach($all_posts_hierarchy as $nodeid => $children) {
		if(in_array($nodeid, $flatpostslist)) {
			$posts_hierarchy[$nodeid] = $all_posts_hierarchy[$nodeid];
		}
	} 
	echo cf_prepare_child_posts_output($nodes_info, $posts_hierarchy, $roleNumber, $idForum);
}

function cf_prepare_child_posts_output($nodes_info, $posts_branch, $roleNumber, $idForum, $depth = 0) {
	if(empty($posts_branch)) {
		return "";
	}
	$ret = "";
	if($depth == 0) {
		$ret.= "<ul class = 'treeview-red'>";
	}
	else {
		$ret.= "<ul>";	
	}
	$j=0;
	$depth++;
	foreach($posts_branch as $idPost => $children) {
		$post_author = get_userdata(cf_get_node_from_list_by_id($nodes_info, $idPost)->idUser);
		
		$ret.= "<li><div class='posted_box1 ".((($j+$depth) % 2 != 0)?" posted_box2 ":"")." clearfix'>";
		if($roleNumber==2 || ($roleNumber==1 && cf_is_mod_forum($idForum))) {
			$ret.= "<form style = 'display: inline; float: left;' method = 'post' action = '".get_permalink()."'>
					<input type = 'hidden' name='cf_action' value='post-delete'></input>
					<input type = 'hidden' name='idPost' value ='".$idPost."'></input>
					<input type = 'submit' class = 'click6' value = 'Delete'/>
				</form>
				<button class = 'addReplyButton click6' data-parentid ='".$idPost."'>Reply</button>";
		}
		$ret.= "<a href='#'>".htmlentities(cf_get_node_from_list_by_id($nodes_info, $idPost)->text, ENT_QUOTES , "UTF-8")."</a>
				<a class='click7'>".htmlentities($post_author->nickname, ENT_QUOTES , "UTF-8")."</a>
				<i>".cf_get_node_from_list_by_id($nodes_info, $idPost)->date."</i>";
		if($j == 5 || $j == 10) {
			$ret.= "<div class='adsense9'>
				<a href='#'><img src='".get_bloginfo('template_directory')."/img/ad7.jpg' width='592' height='76' alt=''></a>
			</div>";			 
		}
		$ret.= "</div>".cf_prepare_child_posts_output($nodes_info, $children, $idPost, $roleNumber, $idForum, $depth)."</li>";
		$j++;
	}
	$ret.= "</ul>";
	
	return $ret;
}

?>

<div id = 'addReplyFormContainer' style = 'display:none'>
	<form action = "<?php echo get_permalink();?>" method = "post">
		<input class="click6" type = 'hidden' name='cf_action' value='post-add'></input>
		<input class="click6" type = 'hidden' name='idTopic' value='<?php echo $idTopic;?>'></input>
		<input class="click6" type = 'hidden' name='idParent' value='0'></input>
		Message:<textarea name = 'postText'></textarea><br>
		<input class="click6" type = 'submit'></input>
	</form>
</div>

							<div class="column6_block2 clearfix">
                            	<h2 class=" hdclass heading6 m-b5">
                            		<del>
                            			<a href = "<?php echo get_permalink()."?cf_action=forum-single&idForum=".$forum->id; ?>">
                            				<?php echo htmlentities($forum->title, ENT_QUOTES , "UTF-8"); ?>
                            			</a>
                            		</del>
                            	</h2>
                                <div class="text_box12">
                                	<?php echo $topic->description; ?>
                                </div>
                                <?php 
                                cf_layout_forum_moderators($moderators);
                                cf_layout_tag_list($idForum);
                                cf_layout_cat_list($idForum);
								?>
                            </div>	
                        	<div class="column6_block3">
                            	<?php 
                            	cf_layout_related_forums($forum->id); 
                            	cf_layout_search();
                            	?>
                            <div class="posted_box">
                            	<div class="posted_box_heading clearfix">
                                	<h4 class="hdclass fl-rt"><?php echo htmlentities($topic_author->nickname, ENT_QUOTES , "UTF-8"); ?></h4>
                                	<h4 class="hdclass fl-lt"><?php echo htmlentities($topic->date, ENT_QUOTES , "UTF-8"); ?></h4>   
                                </div>
                                <div class="posted_box_cont">
                                	<h4 class="hdclass"><?php echo htmlentities($topic->title, ENT_QUOTES , "UTF-8"); ?></h4>	
                                	<div class="text_box14"><?php echo htmlentities($topic->opPostText, ENT_QUOTES , "UTF-8"); ?></div>
                                </div>
                            </div>
                            <div class="column6_block4 clearfix">
                            	<ul class="column6_block4_list">
                                	<li><a class="click6" href="#"><?php echo __( 'Subscribe', 'custom-forums' )?></a></li>
                                	<?php 
                                    cf_layout_auth();
									cf_layout_social();
									?>
                                    <li class="left1 no-margin"><a class="click6" href="<?php echo get_permalink(); ?>"><?php echo __( 'All forums', 'custom-forums' )?></a></li>
                                </ul>
                            </div>
                            <div class="column6_block5">
                            	<h4 class="hdclass"><?php echo __( 'Recent posts', 'custom-forums' )?></h4>
                            	
                            	<?php  cf_posts_treeview_layout($idTopic, $posts_per_page, $nav_page, $roleNumber);?>
                            	
                            </div>   
                            </div>
                            <div class="more_view_list clearfix">
                        	<ul class="uli">
                        		<?php
                        		for($i = 1; $i <= $pages_total; $i++) {
                        			echo '<li class="uli'.($i==$nav_page?' current':'').'"><a href="'.get_permalink().'?cf_action=topic-single&idTopic='.$idTopic.'&nav_page='.$i.'">'.$i.'</a></li>';	
                        		} 
                        		?>
                            </ul>
                            <a class="click4" href="#"><?php echo __( 'Abe', 'custom-forums' )?></a>
                        </div>
                        