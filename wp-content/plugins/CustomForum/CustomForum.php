<?php
/*
* Plugin name: Custom Forum
* Description: Forum special for nomind site
* Text Domain: custom-forums
*/

ob_start();

error_reporting(E_ALL);
ini_set('display_errors', 'true');
/**/

//include_once('../../../wp-load.php');
defined('ABSPATH') or die("");

register_activation_hook(__FILE__,'cf_install');

$main_page = "custom-forum";

function true_include_myuploadscript() {
	wp_enqueue_script('jquery');
	if ( ! did_action( 'wp_enqueue_media' ) ) {
		wp_enqueue_media();
	}
 	//wp_enqueue_script( 'myuploadscript', plugins_url('/js/customforum.js', __FILE__), array('jquery'), null, false );
}
 
add_action( 'wp_enqueue_scripts', 'true_include_myuploadscript' );

function true_include_myuploadscript_admin() {
	wp_enqueue_script('jquery');
	if ( ! did_action( 'wp_enqueue_media' ) ) {
		wp_enqueue_media();
	}
}
 
add_action( 'admin_enqueue_scripts', 'true_include_myuploadscript_admin' );

add_filter( 'posts_where', 'devplus_attachments_wpquery_where' );
function devplus_attachments_wpquery_where( $where ){
	global $current_user;
	$user_info = get_userdata($current_user->ID);
	if($user_info->roles[0]=='administrator') {
		return $where;
	}
	if( is_user_logged_in() ){
		if( isset( $_POST['action'] ) ){
			if( $_POST['action'] == 'query-attachments' ){
				$where .= ' AND post_author='.$current_user->data->ID;
			}
		}
	}
	return $where;
}


// Register hooks
//add_action('wp_enqueue_scripts', 'add_scripts');
add_action('init', 'add_scripts');

function add_scripts(){
	//wp_enqueue_script('suggest');
	wp_enqueue_script('tiny_mce');

	wp_localize_script( 'jquery', 'myajax', 
	array(
   		'url' => admin_url('admin-ajax.php')
	));

	$labels = array(
		'name' => __('Forum Tags', 'custom-forums'),
		'singular_name' => __('Forum Tag Item', 'custom-forums'),
		'add_new' => __('Add New', 'custom-forums'),
		'add_new_item' => __('Add New Forum Tag Item', 'custom-forums'),
		'edit_item' => __('Edit Forum Tag Item', 'custom-forums'),
		'new_item' => __('New Forum Tag Item', 'custom-forums'),
		'view_item' => __('View Forum Tag Item', 'custom-forums'),
		'search_items' => __('Search Forum Tag', 'custom-forums'),
		'not_found' =>  __('Nothing found', 'custom-forums'),
		'not_found_in_trash' => __('Nothing found in Trash', 'custom-forums'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => false,
		'publicly_queryable' => false,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'show_ui' => true,
		'menu_position' => null,
		'supports' => array('title','editor', 'revisions', 'page-attributes')
	); 
	register_post_type( 'forum_tag' , $args );

	$labels = array(
		'name' => __('Forum Categories', 'custom-forums'),
		'singular_name' => __('Forum Category Item', 'custom-forums'),
		'add_new' => __('Add New', 'custom-forums'),
		'add_new_item' => __('Add New Forum Category Item', 'custom-forums'),
		'edit_item' => __('Edit Forum Category Item', 'custom-forums'),
		'new_item' => __('New Forum Category Item', 'custom-forums'),
		'view_item' => __('View Forum Category Item', 'custom-forums'),
		'search_items' => __('Search Forum Category', 'custom-forums'),
		'not_found' =>  __('Nothing found', 'custom-forums'),
		'not_found_in_trash' => __('Nothing found in Trash', 'custom-forums'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => false,
		'publicly_queryable' => false,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'show_ui' => true,
		'menu_position' => null,
		'supports' => array('title','editor', 'revisions', 'page-attributes')
	); 
	register_post_type( 'forum_categories' , $args ); 
}

add_action( 'plugins_loaded', 'cf_load_textdomain' );

/*if ( function_exists( 'add_image_size' ) ) {
	add_action('init', 'image_attachments_define_image_sizes');
	function image_attachments_define_image_sizes() {
		add_image_size( 'cf-ads', 592, 76 );
	}
}*/

//Hide Custom Tags and categories from main menu

function toggle_custom_menu_order(){
	return true;
}

add_filter( 'custom_menu_order', 'toggle_custom_menu_order' );

function remove_those_menu_items( $menu_order ){
	global $menu;
	foreach ( $menu as $mkey => $m ) {
		$key1 = array_search( 'edit.php?post_type=forum_categories', $m );
		$key2 = array_search( 'edit.php?post_type=forum_tag', $m );
		if ( $key1 || $key2 )
			unset( $menu[$mkey] );
	}

	return $menu_order;
}
add_filter( 'menu_order', 'remove_those_menu_items' );

//----Hide Custom Tags and categories from main menu

function cf_load_textdomain() {
	load_plugin_textdomain('custom-forums', false, dirname( plugin_basename( __FILE__ ) ) . '/languages');
}

add_action("admin_menu", "cf_add_pages");

add_action("wp_add_post", "cf_edit_post");
add_action("wp_alter_post", "cf_alter_post");
add_action("wp_set_user_privileges", "cf_set_user_privileges");


function add_query_vars_filter( $vars ){
	$vars = array_merge($vars, array(
		"idForum",
		"idPost",
		"cf_action",
		"title",
		"description",
		"tags",
		"categories",
		"idParent",
		"postText",
		"nav_page",
		"search",
		"featured",
		"openingpost",
		"subscribe",
		"stickPost",
		"nicename", 
		"datestart", 
		"dateend", 
		"fordays",
		"popupMessage"
	));
	return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

function cf_add_post_view_callback($idPost){
	global $wpdb;
	$update = $wpdb->prepare("Update ".$wpdb->prefix . "cf_post SET views = views + 1 WHERE id = '%d'", $idPost); 
	$wpdb->query( $update );
	ob_end_clean();
	echo $idPost;
	die();
}

function cf_layout_search($idForum=0) {
	?>
	<div class="form_box1 clearfix">
		<h2 class="hdclass"><?php echo __( 'Search1', 'custom-forums' )?></h2>
		<form id = 'postSearchForm' action = "<?php echo get_permalink();?>" method = "post">
		<input type = 'hidden' name='cf_action' value='post-search'></input>
		<input type = 'hidden' name='idForum' value='<?php echo $idForum; ?>'></input>
		<input class="input_text1 input_text9"  type="text" name="search" value="<?php echo __( 'Post text', 'custom-forums' )?>" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
		<input class="input_text1 input_text10" type="text" name="nicename" value="<?php echo __( 'Nickname', 'custom-forums' )?>" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
		<input class="input_text1 input_text10 search-date" type="text" name="datestart" value="<?php echo __( 'Date from', 'custom-forums' )?>" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
		<input class="input_text1 input_text10 search-date" type="text" name="dateend" value="<?php echo __( 'Date to', 'custom-forums' )?>" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
		<input class="input_btn1  input_btn2" type="submit"  value="<?php echo __( 'Search2', 'custom-forums' )?>">
		</form>
	</div>
	<?php 
}

function cf_layout_most_active_forums_box() {
	global $wpdb;
	$forums  = $wpdb->get_results("select ff.id, ff.title, pp.cnt from
 (select id, title from ".$wpdb->prefix."cf_forum where deleted = 0) as ff 
join 
	(select count(*) as cnt, idForum from ".$wpdb->prefix."cf_post where deleted = 0 group by idForum) as pp 
on ff.id = pp.idForum order by pp.cnt desc  limit 0, 10");
	?>
	<div class="rikuz-box-cols">
		<h3><?php _e('Most active forums', 'custom-forums'); ?></h3>
		<div class="rikuz-box-info-bg">
			<ul class="rikuz-box-info">
			<?php
			for($i = 0; $i < count($forums); $i++) {
				if($i == 5) {
					?>
					</ul>
					<ul class="rikuz-box-info nomrgn">
					<?php 
				}
				?>
				<li><a class = gray-link href="<?php echo get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$forums[$i]->id; ?>"><?php echo $forums[$i]->title; ?></a></li>
				<?php 
			}
			?>
			</ul>
		<div class="clear"></div>
		</div>
	</div>
	<?php 
}

function cf_layout_newest_forums_box() {
	global $wpdb;
	$forums  = $wpdb->get_results("select id, title from ".$wpdb->prefix."cf_forum where deleted = 0 order by date desc limit 0, 10");
	?>
	<div class="rikuz-box-cols">
		<h3><?php _e('New Forums', 'custom-forums'); ?></h3>
		<div class="rikuz-box-info-bg">
			<ul class="rikuz-box-info">
			<?php
			for($i = 0; $i < count($forums); $i++) {
				if($i == 5) {
					?>
					</ul>
					<ul class="rikuz-box-info nomrgn">
					<?php 
				}
				?>
				<li><a class = gray-link href="<?php echo get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$forums[$i]->id; ?>"><?php echo $forums[$i]->title; ?></a></li>
				<?php 
			}
			?>
			</ul>
		<div class="clear"></div>
		</div>
	</div>
	<?php 
}

function cf_layout_featured_forums_box() {
	global $wpdb;
	$forums  = $wpdb->get_results("select id, title from ".$wpdb->prefix."cf_forum where deleted = 0 and featured = 1 order by rand() limit 0, 10");
	?>
	<div class="rikuz-box-cols nomrgn">
		<h3><?php _e('Featured Forums', 'custom-forums'); ?></h3>
		<div class="rikuz-box-info-bg">
			<ul class="rikuz-box-info">
			<?php
			for($i = 0; $i < count($forums); $i++) {
				if($i == 5) {
					?>
					</ul>
					<ul class="rikuz-box-info nomrgn">
					<?php 
				}
				?>
				<li><a class = gray-link href="<?php echo get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$forums[$i]->id; ?>"><?php echo $forums[$i]->title; ?></a></li>
				<?php 
			}
			?>
			</ul>
		<div class="clear"></div>
		</div>
	</div>
	<?php 
}


function cf_layout_forums_sorted() {
	$all_categories = cf_get_forum_categories();
	shuffle($all_categories);
	$categories = array_slice($all_categories, 0, 4);
	foreach($categories as $category) {
		$forums = cf_get_forums_by_cat_id($category->ID, 9);
		?>
		<div class="rikuz-age-cols">
			<h3><?php echo $category->post_title; ?></h3>
			<ul class="rikuz-cols-list">
				<?php
				foreach($forums as $forum) {
					?>
					<li><a href="<?php echo get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$forum->id; ?>"><?php echo $forum->title; ?></a></li>
					<?php 
				} 
				?>
			</ul>
		</div>
		<?php 
	}
	?>
	<div class="clear"></div>
	<?php 
}

function cf_layout_pagination($baseurl, $nav_page, $pages_total) {
	if($pages_total == 1)
			return;
	?>
	<div class="more_view_list clearfix">
		<ul class="uli">
		<?php
		for($i = 1; $i <= $pages_total; $i++) {
			?>
			<li class="uli<?php if($i==$nav_page) {?> current<?php } ?>">
				<a href="<?php echo $baseurl; ?>&nav_page=<?php echo $i; ?>">
					<?php echo $i; ?>
				</a>
			</li>
			<?php 	
		} 
		?>
		</ul>
		<?php
		if($nav_page < $pages_total) {
			?>
			<a class="click4" href="<?php echo $baseurl.'&nav_page='.($nav_page+1); ?>"><?php echo __( 'Next page', 'custom-forums' )?></a>
			<?php
		} 
		?>
		<?php
		if($nav_page > 1) {
			?>
			<a class="click4" href="<?php echo $baseurl.'&nav_page='.($nav_page-1); ?>"><?php echo __( 'Previous page', 'custom-forums' )?></a>
			<?php
		} 
		?>
	</div>
	<?php 
}

function cf_wp_editor($text, $id) {
	wp_editor( $text, 'my_ckedit_'.$id, array(
		'textarea_name' => 'postText',
		'tinymce' => array(
			'filebrowserBrowseUrl' => '/kcfinder/browse.php?type=files'
			)
		/*'tinymce' => array(
			//'readonly' => 1,
			'directionality' => 'rtl',
		),
	'quicktags' => false,
	'media_buttons' => false,*/
	));
}

function cf_get_recent_posts($idUser) {
	global $wpdb;
	$user_info = get_userdata($idUser);
	if($user_info->roles[0]=='administrator') {
		$sql = $wpdb->prepare("Select * from ".$wpdb->prefix."cf_post where deleted = 0 order by date desc limit 0, 6");
	}
	else {
		$sql = $wpdb->prepare("Select * from ".$wpdb->prefix."cf_post where deleted = 0 order by date desc limit 0, 6");
	}
	return $wpdb->get_results($sql);
}

function cf_layout_forum_moderators($idForum) {
	$moderators = cf_get_moderators_by_forum_id($idForum);
	$moderators_count = count($moderators);
	?>
	<div class="img_info_block1 clearfix">
	<?php 
	for($i = 0; $i < $moderators_count; $i++) {
		?>
		<div class="img_info_box3 no-bdr <?php if($i!=$moderators_count-1){echo "clearfix"; } else echo "no-padding no-margin"?>">
			<div class="img_box11">
				<?php echo get_avatar($moderators[$i]->ID, 120);?>
			</div>
			<div class="img_box11_info">
				<h2 class="hdclass"><?php echo $moderators[$i]->display_name; ?></h2>
				<div class="text_box13">
					<?php echo get_user_meta($moderators[$i]->ID, 'description', true); ?>
				</div>
				<div class="img_box11_info_link clearfix">			
					<a href="#"><?php echo __( 'Continue', 'custom-forums' )?></a>
						<img src="<?php bloginfo('template_directory'); ?>/img/social_icon26.png" width="20" height="20" alt="">
					<a href="#"><?php echo __( 'Moderator box bottom link', 'custom-forums' )?></a>
				</div>
			</div>
		</div>
		<?php
	}
	?>
	</div>
	<?php	
}

function cf_layout_related_forums($idForum) {
	$related_forums = cf_get_related_forums($idForum);
	$related_forums_count = count($related_forums);
	?>
	<h2 class=" hdclass heading6 m-b5"><?php echo __( 'Related forums', 'custom-forums' )?></h2>
	<div class="others_nav1 clearfix">
		<ul class="others_nav1_box">
		<?php 		
		for($i = 0; $i < $related_forums_count; $i++) {
			?>
			<a class = gray-link href="<?php echo get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$related_forums[$i]->id; ?>">
				<?php _e_db($related_forums[$i]->title); ?>
			</a>
			<?php 
			if($i != $related_forums_count-1) {
				?>&nbsp;|&nbsp;<?php
			}
			/*
			?>
			<div class="img_info_box1">
				<div class="img_box7">
					<a href="<?php echo get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$related_forums[$i]->id; ?>">
						<img src="<?php echo cf_get_forum_image_path($related_forums[$i]); ?>" width="122" height="70" alt="<?php plugin_dir_path( __FILE__ ); ?>/images/no-image.jpg">
					</a>
				</div>
				<h2 class="hdclass"><?php _e_db($related_forums[$i]->title); ?></h2>
				<div class="text_box7"><?php _e_db($related_forums[$i]->description); ?></div>
			</div>
			<?php
			*/ 
		}
		?>
		</ul>
	</div>
	<?php 
}

function cf_layout_tag_list($idForum) {
	$tag_names = cf_get_tag_names_by_forum_id($idForum);
	$tags_count = count($tag_names);
	if($tags_count > 0) {
		?>
		<div><?php _e('Tags:', 'custom-forums'); 
		for($i = 0; $i < $tags_count; $i++) {
			?>
			<div class = 'custom-forum-list-item'>
				<a href="<?php echo get_site_url()."/custom-forum/?cf_action=forum-search-tag&search=".$tag_names[$i]; ?>"><?php _e_db($tag_names[$i]); ?></a>
			</div>
			<?php
		}
		?>
		</div>
		<?php
	} 
}

function cf_layout_cat_list($idForum) {
	$cat_names = cf_get_cat_names_by_forum_id($idForum);
	$cats_count = count($cat_names);
	if($cats_count > 0) {
		?>
		<div><?php _e('Categories:', 'custom-forums');
		for($i = 0; $i < $cats_count; $i++) {
			?>
			<div class = 'custom-forum-list-item'>
				<a href="<?php echo get_site_url()."/custom-forum/?cf_action=forum-search-cat&search=".$cat_names[$i]; ?>"><?php _e_db($cat_names[$i]); ?></a>
			</div>
			<?php
		}
		?>
		</div>
		<?php 
	}
}

function cf_get_forum_image_path($forum) {
	if($forum->image != 0) {
		$image_attributes = wp_get_attachment_image_src( $forum->image );
		return $image_attributes[0];
	}
	return plugin_dir_url( __FILE__ )."images/no-image.jpg";
}

function cf_get_random_featured_forums() {
	global $wpdb;
	$sql = "Select * from ".$wpdb->prefix."cf_forum where featured = 1 order by rand() limit 0,5";
	return $wpdb->get_results($sql);
}

function cf_get_related_forums($idForum) {
	global $wpdb;
	$tag_ids = cf_get_tag_ids_by_forum_id($idForum);
	$cat_ids = cf_get_cat_ids_by_forum_id($idForum);
	
	$tagsInClause = count($tag_ids)?$wpdb->prepare(implode(", ", array_fill(0, count($tag_ids), "%d")), $tag_ids):"''";
	$catsInClause = count($cat_ids)?$wpdb->prepare(implode(", ", array_fill(0, count($cat_ids), "%d")), $cat_ids):"''";
	
	$sql = $wpdb->prepare("Select * from 
			(Select ff.* from (select * from ".$wpdb->prefix."cf_forumtag where idTag in (".$tagsInClause.")) as fc 
			join (select * from ".$wpdb->prefix."cf_forum where deleted = 0 and id <> %d) as ff on ff.id = fc.idForum
	union
			Select ff.* from (select * from ".$wpdb->prefix."cf_forumcat where idCat in (".$catsInClause.")) as fc 
			join (select * from ".$wpdb->prefix."cf_forum where deleted = 0 and id <> %d) as ff on ff.id = fc.idForum) as unioned 
			order by featured, rand() limit 0,8",
			$idForum, $idForum);
	return $wpdb->get_results($sql);
}

function cf_get_forums_by_cat_id($cid, $limit) {
	global $wpdb;
	$sql = $wpdb->prepare("select * from
			(select * from ".$wpdb->prefix."cf_forumcat where idCat = '%d') as fc 
		join 
			(select * from ".$wpdb->prefix."cf_forum where deleted = 0 ) as ff 
		on ff.id = fc.idForum order by rand() limit 0, %d",
	$cid, $limit);
	return $wpdb->get_results($sql);
}

function cf_layout_add_new_post($idForum) {
	?>
	<button class = 'addReplyButton click6'><?php echo __( 'Add new Post', 'custom-forums' )?></button>
	<div class = 'addReplyFormContainer'>
		<form enctype="multipart/form-data" action = "<?php echo get_permalink();?>" method = "post">
			<input class="click6" type = 'hidden' name='cf_action' value='post-add'></input>
			<input class="click6" type = 'hidden' name='idForum' value='<?php echo $idForum;?>'></input>
			<input class="click6" type = 'hidden' name='idParent' value='0'></input>
			<div class = 'flr'><?php _e("Subject", 'custom-forums'); ?></div>
			<input type = 'text' name = 'title' class = 'subjEdit hidable'></input>
			<br><br>
			<textarea class="_ckeditor_r" name="postText" id = "ckeditor_0_r"></textarea>
			<?php _e("Subscribe", 'custom-forums'); ?><input type = 'checkbox' name = 'subscribe'></input><br>
			<?php
			$role = cf_get_user_role();
			if($role == 1 || ($role == 0 && cf_is_mod_forum($idForum))) {
				_e("Stick this post", 'custom-forums');
				?>
				<input type = 'checkbox' name = 'stickPost'></input><br>
				<?php 
			}
			?>
			<input class = 'hiddenfileinput' type="file" name="attachment"/>
			<div class = 'uploaded-file-label'></div><button type = "button" class = 'fakeInputButton click6'><?php _e("Upload file", 'custom-forums'); ?></button><?php _e("Attachment", 'custom-forums'); ?><br>
			<div><?php echo __("Warning: max upload size equals", 'custom-forums')." ".get_option('max_file_size_kb')." KB"; ?></div>
			<button type = "button" class = 'CancelEditMode click6'>
				<?php _e("Cancel", 'custom-forums'); ?>
			</button>
			<input class="click6" type = 'submit'></input>
		</form>
	</div>
<?php 
}

function cf_before_content($isrikuz) {
	?>
						<div class="column6">
							<div class="column6_block1 clearfix">
								<h2 class=" hdclass heading6"><del><a href="/custom-forum"><?php echo __( 'Custom Forums', 'custom-forums' )?></a></del></h2>
                    			<div class="forums_list clearfix">
                        			<h3 class="hdclass"><?php echo __( 'Forums list', 'custom-forums' )?></h3>
                        			<ul class="forums_list_box">
                        	<?php
                        	foreach (array_diff(range(0x05D0, 0x05EA), array(0x05DA, 0x05DD, 0x05DF, 0x05E3, 0x05E5)) as $char) {
    							$char = html_entity_decode("&#$char;", ENT_COMPAT, "UTF-8");
                        		echo "<li><a href='".get_site_url()."/custom-forum/?cf_action=forum-search-char&search=".$char."'>".$char."</a></li>";
     							//$alphabets[$char] = ++$i;
							}  
                        	?>
                            		</ul>
                        		</div>
                    		</div>
                    	<?php
                    	if($isrikuz) {
                    		cf_layout_search();
                    	}
                    	?>
                    		<div class="column5_block3 clearfix no-bdr no-margin">
                    			<div class="info_heading info_heading1 clearfix">
                            		<h4 class="hdclass"><?php echo __( 'Featured Forums', 'custom-forums' ); ?></h4>
                            		<h4 class="hdclass fll"><a href = '/custom-forum-rikuz'><?php echo __( 'All Forums', 'custom-forums' )?></a></h4>
                        		</div>
                        <?php 
                        $featured_forums = cf_get_random_featured_forums();
                        $featured_forums_count = count($featured_forums);
                        for($i = 0; $i < $featured_forums_count; $i++) {
                        	?>
                        		<div class="img_info_box1">
	                        		<div class="img_box7">
                            			<a href="<?php echo get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$featured_forums[$i]->id; ?>">
                            				<img src="<?php echo cf_get_forum_image_path($featured_forums[$i]); ?>" width="122" height="70" alt="<?php plugin_dir_path( __FILE__ ); ?>/images/no-image.jpg">
                            			</a>
                            		</div>
                            		<h2 class="hdclass"><?php _e_db($featured_forums[$i]->title); ?></h2>
                            		<div class="text_box7"><?php _e_db($featured_forums[$i]->description); ?></div>
                        		</div>
                        	<?php 
                        }
                        ?>
                    		</div>
                    	<?php
						if(!$isrikuz) { 
                    		?>
                    		<div class="adsense8">
                    			<!-- Middle Ads area -->		
                    			<a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/ad6.jpg" width="775" height="96" alt=""></a>
                    		</div>
                    		<?php
                   		}
						?>
							<div class="clearfix">
								<div class="column6_sub_column1">
	<?php 
}

function cf_upload_image() {
	$imageFileName = "";
	if(isset($_FILES['imagefile'])) {
		$whitelist = array(".jpeg", ".jpg", ".png", ".gif");
		$filetmpNme=$_FILES['imagefile']['name'];
		foreach  ($whitelist as  $item) {
			if(preg_match("/$item\$/i",$_FILES['imagefile']['name'])) {
				if($item==".jpeg") {
					$filetmpNme= substr($filetmpNme, 0, strlen($filetmpNme)-5).'_'.time().substr($filetmpNme, strlen($filetmpNme)-5);
				}
				else if($item==".jpg" or $item==".png" or $item==".gif") {
					$filetmpNme= substr($filetmpNme, 0, strlen($filetmpNme)-4).'_'.time().substr($filetmpNme, strlen($filetmpNme)-4);
				}
			}
		}
		$_FILES['imagefile']['name']=$filetmpNme;
		$uploadfile = cf_get_upload_dir().basename($_FILES['imagefile']['name']);
		if(is_uploaded_file($_FILES['imagefile']['tmp_name'])){
			if(move_uploaded_file($_FILES['imagefile']['tmp_name'],$uploadfile)){
				$imageFileName = $_FILES['imagefile']['name'];
			}
		}
	}
	return $imageFileName;
}

/*function cf_layout_single_forum($idForum) {
	cf_before_content();
	include plugin_dir_path( __FILE__ ).'/templates/forum-single.php';
}*/

function cf_get_tag_ids_by_forum_id($idForum) {
	global $wpdb;
	$sqlGetTags = $wpdb->prepare("Select idTag from " . $wpdb->prefix . "cf_forumtag where idForum = '%d'", $idForum);
	$tag_ids = array();
	$tagIdResults = $wpdb->get_results($sqlGetTags);
	foreach($tagIdResults as $res) {
		$tag_ids[]= $res->idTag;
	}
	return $tag_ids;
}

function cf_get_cat_ids_by_forum_id($idForum) {
	global $wpdb;
	$sqlGetCats = $wpdb->prepare("Select idCat from " . $wpdb->prefix . "cf_forumcat where idForum = '%d'", $idForum);
	$cat_ids = array();
	$catIdResults = $wpdb->get_results($sqlGetCats);
	foreach($catIdResults as $res) {
		$cat_ids[]= $res->idCat;
	}
	return $cat_ids;
}

function cf_get_tag_names_by_forum_id($idForum) {
	$tag_ids = cf_get_tag_ids_by_forum_id($idForum);
	$tag_names = array();
	
	$tags = cf_get_forum_tags();

	foreach($tag_ids as $tag_id) {
		foreach($tags as $tag) {
			if($tag->ID == $tag_id)
				$tag_names[]= $tag->post_title;
		}
	}
	return $tag_names;
}

function cf_get_forum_categories() {
	return get_posts(array('posts_per_page' => 0,
		 'orderby' => 'title',
		 'post_type' => 'forum_categories'));
}

function cf_get_forum_tags() {
	return get_posts(array('posts_per_page' => 0,
		 'orderby' => 'title',
		 'post_type' => 'forum_tag'));
}

function cf_get_cat_names_by_forum_id($idForum) {
	$cat_ids = cf_get_cat_ids_by_forum_id($idForum);
	$cat_names = array();
	
	$categories = cf_get_forum_categories();
	
	foreach($cat_ids as $cat_id) {
		foreach($categories as $category) {
			if($category->ID == $cat_id)
				$cat_names[]= $category->post_title;
		}
	}
	return $cat_names;
}

function cf_get_forum_by_id($idForum) {
	global $wpdb;
	$sqlGetForum = $wpdb->prepare("Select * from " . $wpdb->prefix . "cf_forum where id = '%d'", $idForum);
	$res = $wpdb->get_results($sqlGetForum);
	if(empty($res))
		return false;

	/*$update = $wpdb->prepare("Update ".$wpdb->prefix . "cf_forum SET views = views + 1 WHERE id = '%d'", $idForum); 
	$wpdb->query( $update );*/

	return $res[0];
}

function cf_get_post_by_id($idPost) {
	global $wpdb;
	$sqlGetPost = $wpdb->prepare("Select * from " . $wpdb->prefix . "cf_post where id = '%d'", $idPost);
	$res = $wpdb->get_results($sqlGetPost);
	if(empty($res))
		return false;
	return $res[0];
}

function cf_get_forum_by_post_id($idPost) {
	global $wpdb;
	$sql = $wpdb->prepare("Select ff.* from (select * from ".$wpdb->prefix."cf_post where id = %d and deleted = 0 ) as tt
	left join (select * from ".$wpdb->prefix."cf_forum where deleted = 0 ) as ff on ff.id = tt.idForum", $idPost);
	return $wpdb->get_row($sql);
}

function cf_get_tag_id_by_name($tag_name) {
	$tags = cf_get_forum_tags();
	foreach($tags as $tag) {
		if($tag->post_title == $tag_name) {
			return $tag->ID;
		}
	}
	return 0;
}

function cf_get_cat_id_by_name($cat_name) {
	$cats = cf_get_forum_categories();
	foreach($cats as $cat) {
		if($cat->post_title == $cat_name) {
			return $cat->ID;
		}
	}
	return 0;
}

function cf_get_upload_dir() {	return dirname(__FILE__).'/images/'; }

function cf_get_node_from_list_by_id($list, $id) {
	foreach($list as $node) {
		if($node->id == $id)
			return $node;
	}
}

function cf_is_mod_forum($idForum) {
	global $wpdb;
	global $current_user;
	$table_name = $wpdb->prefix . "cf_forummod";
	
	$sql = "Select * from ".$table_name." where idUser = ".($current_user->ID)." and idForum = ".$idForum;
	
	$result = $wpdb->get_results($sql);
	
	return count($result)>0;

}

function cf_get_forums_info($forums_per_page = 12, $nav_page = 1, $type = null, $search = null) {
	global $wpdb;
	
	if(get_option('forum_per_page')!=null)
	{
	    $forums_per_page = get_option('forum_per_page');
	}
	
	$table_name = $wpdb->prefix . "cf_forum as forum";
	$where = "";
	if($type != null) {
		if($type=='cat') {
			$category_id = cf_get_cat_id_by_name($search);
			if($category_id==0) {
				return array();
			}
			$table_name .= " join ".$wpdb->prefix ."cf_forumcat as cat on forum.id = cat.idForum ";
			$where = " and cat.idCat = ".$category_id." ";
		}
		elseif($type=='tag') {
			$tag_id = cf_get_tag_id_by_name($search);
			if($tag_id==0) {
				return array();
			}
			$table_name .= " join ".$wpdb->prefix ."cf_forumtag as tag on forum.id = tag.idForum ";
			$where = " and tag.idTag = ".$tag_id." ";
		}
		elseif($type=='firstchar') {
			$whereLike = $wpdb->prepare("%s", $search);
			$whereLike = substr($whereLike,1,-1);
			$where = " and forum.title like '".$whereLike."%' ";
		}
		else {
			$whereLike = $wpdb->prepare("%s", $search);
			$whereLike = substr($whereLike,1,-1);
			$where = " and forum.title like '%".$whereLike."%' ";
		}
	}
	$sqlGetForums = "Select forum.* from ".$table_name." where deleted = 0 ".$where." order by forum.title asc limit ".($forums_per_page * ($nav_page-1)).", ".$forums_per_page;
	return $wpdb->get_results($sqlGetForums);
}

function cf_get_posts_info($idForum, $activeOnly = false) {
	global $wpdb;
	global $roleNumber;
	$UserId = get_current_user_id();
	$table_name = $wpdb->prefix . "cf_post as Post ";
	$where_sub = "";
	$select_sub = ", 0 as current_user_subscribed ";
	if($UserId)
	{
		$table_name	.= " left join ".$wpdb->prefix."cf_subscription as Subscription on Post.id = Subscription.idPost ";
		//$where_sub = " and (Subscription.idUser IS NULL OR Subscription.idUser = ".$UserId.")";
		$select_sub = ", Subscription.idUser IS NOT NULL AS current_user_subscribed ";
	}

	$whereActive = " and Post.isApproved = 1 ";
	if($roleNumber==1 || ($roleNumber==0 && cf_is_mod_forum($idForum))) {
		$whereActive = "";
	}

	$sqlGetPostsTree = $wpdb->prepare("Select Post.* ".$select_sub." from " . $table_name. "		
		where Post.idForum = '%d' and Post.deleted = 0 ".$whereActive.$where_sub." order by date desc", $idForum);	
	
	return $wpdb->get_results($sqlGetPostsTree);
}

function cf_layout_auth($roleNumber) {
	if($roleNumber==-1){?>
		<li><input type="button" class="click6" onclick="return openLogIn();" value="Log In"/></li>
		<li><input type="button" class="click6" onclick="return openSingUp();" value="Register"/></li>
		<?php } else { ?>
		<li><form method="post"><input type="submit" name="btnLogOut" class="click6" value="<?php echo __( 'Log Out', 'custom-forums' )?>"/></form></li>
		<!--li><a class="addReplyButton click6" data-parentid="0"><?php echo __( 'Add a message', 'custom-forums' )?></a></li-->
		<?php }
}

function cf_layout_social() {
	?>
	<li class="social_link3"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/social_icon25.jpg" width="86" height="20" alt=""></a></li>
	<li class="social_link3"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/social_icon2.jpg" width="90" height="20" alt=""></a></li>
	<li class="social_link4"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/social_icon4.jpg" width="16" height="16" alt=""></a></li>
	<li class="social_link4"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/social_icon5.jpg" width="16" height="16" alt=""></a>&nbsp;&nbsp;&nbsp;<?php echo __( 'Like on Facebook', 'custom-forums' )?></li>
	<?php 
}

function cf_user_post_subscription($idPost, $subscribe = true) {
	
}

function cf_get_action() {
	if(!strlen(get_query_var("cf_action")))
		return "forum-list"; 
	return get_query_var("cf_action");
}

function cf_get_user_role(){
	
	if(!is_user_logged_in())
		return -1;
	
	global $current_user;

	$user_info = get_userdata($current_user->ID);
	
	if($user_info->roles[0]=='administrator')
		return 1;
	
	if(!is_plugin_active('user-access-manager/user-access-manager.php')) 
		return -1;
	
	global $oUserAccessManager;
	if(!isset($oUserAccessManager)) 
		return -1;
	
	$oUamUserGroups = $oUserAccessManager->getAccessHandler()->getUserGroups();
	foreach($oUamUserGroups as $oUamUserGroup) {
		if($oUamUserGroup->getGroupName() == "Forum users") {
			foreach($oUamUserGroup->getFullUsers() as $user) {
				if($user->id == $current_user->ID) {
					return 0;
				} 
			}
			return -1;
		}
	} 
	return -1;
}

function __db($data) {
	return htmlentities($data, ENT_QUOTES, "UTF-8");
}

function _e_db($data) {
	echo __db($data);
}

function cf_search_post($idForum, $posts_per_page, $nav_page, $searchText=null, $nicename=null, $dateStart=null, $dateEnd=null, $forDays=null) {
	global $wpdb;
	
	if($posts_per_page == 0)
		$posts_per_page = 12;
	$where = "";
	$results ="";
	if($dateStart==null and $dateEnd==null)
	{
		if($forDays!=null)
		{
			$time = strtotime("-".$forDays." day");
			$fordate = date("Y-m-d", $time);
			$where = " date >= '".$fordate."' ";
		}
	}
	else
	{
		if($dateStart!=null and $dateEnd!=null)
		{
			$where = $wpdb->prepare(" date BETWEEN %s AND %s ", $dateStart, $dateEnd);
		}
		else
		{
			if($dateStart!=null)
			{
				$where = $wpdb->prepare(" date >= %s ", $dateStart);
			}		
			if($dateEnd!=null)
			{
				$where = $wpdb->prepare(" date <= %s", $dateEnd);
			}
		}	
	}

	$table_name = $wpdb->prefix . "cf_post";
	
	$wherePost = array();
	
	
	
	if($idForum)
		$wherePost[]= $wpdb->prepare("post.idForum = %d ", $idForum);
	if($searchText!=null)
		$wherePost[]= "text like '%".trim($wpdb->prepare("%s", $searchText), "'")."%' ";
	if($nicename!=null)
		$wherePost[]= "users.user_nicename like '%".trim($wpdb->prepare("%s", $nicename), "'")."%' ";
	$wherePost = implode(" and ", $wherePost);
	
	$limit = " limit ".($posts_per_page * ($nav_page-1)).", ".$posts_per_page;
	
	$where_final = (strlen($where)?(" and ".$where):"").(strlen($wherePost)?(" and ".$wherePost):""); 
	
	$search = "SELECT post.*, users.user_nicename FROM ".$table_name." as post left join ".$wpdb->prefix ."users as users on post.idUser = users.ID WHERE deleted = 0 ".$where_final." order by date desc ".$limit;
	
	$count = "SELECT count(*) FROM ".$table_name." as post left join ".$wpdb->prefix ."users as users on post.idUser = users.ID WHERE deleted = 0 ".$where_final; 
	
	$count_row = $wpdb->get_row($count, ARRAY_N);
	
	return array(ceil($count_row[0]/$posts_per_page), $wpdb->get_results($search));
}

function cf_delete_entry($id, $type) {
	if(!in_array($type, array('post', 'forum')))
		return;
	global $wpdb;
	$delete = $wpdb->prepare("Update ".$wpdb->prefix . "cf_".$type." SET deleted = 1 WHERE id = '%d'", $id); 
	$wpdb->query( $delete );
}

function cf_edit_forum($title, $description, $tag_ids, $cat_ids, $image, $featured, $openingpost, $idForum = null) {
	global $wpdb;
	$featured = ($featured=="on")?1:0;
	$table_name = $wpdb->prefix . "cf_forum";
	if(!($idForum)) {
		$insert = $wpdb->prepare("INSERT INTO ".$table_name." (title, description, image, idUser, featured, openingpost) VALUES ('%s', '%s', %d, '%d', ".$featured.", '%s')",
			$title,
			$description,
			$image,
			get_current_user_id(),
			$openingpost);
		$results = $wpdb->query( $insert );
		$idForum = $wpdb->insert_id;
	}
	else {
		$update = $wpdb->prepare("UPDATE ".$table_name." set title = '%s', description = '%s', image = %d, featured = ".$featured.", openingpost = '%s' where id = '%d'",
				$title,
				$description,
				$image,
				$openingpost,
				$idForum);
		$results = $wpdb->query( $update );
	}
	
	
	/*$tagidsArray = explode(",",trim($tags, ", "));
	$catidsArray = explode(",",trim($categories, ", "));*/
	
	$table_name = $wpdb->prefix . "cf_forumtag";
	$deleteTags = $wpdb->prepare("Delete from ".$table_name." where idForum = '%d'", $idForum);
	$results = $wpdb->query( $deleteTags );
	
	$table_name = $wpdb->prefix . "cf_forumcat";
	$deleteCats = $wpdb->prepare("Delete from ".$table_name." where idForum = '%d'", $idForum);
	$results = $wpdb->query( $deleteCats );
	
	foreach ($tag_ids as $tag_id) {

		$post_tags =  cf_get_forum_tags();
		foreach($post_tags as $legal_tag) {
			if($legal_tag->ID == $tag_id) {
				$table_name = $wpdb->prefix . "cf_forumtag";
				$insert = $wpdb->prepare("INSERT INTO ".$table_name." (idForum, idTag) VALUES ('%d', '%d')", $idForum, $legal_tag->ID);
				$results = $wpdb->query( $insert );
				break;
			}
		}
	}
	foreach ($cat_ids as $cat_id) {
		$categories = cf_get_forum_categories();
		foreach($categories as $legal_cat) {
			if($legal_cat->ID == $cat_id) {
				$table_name = $wpdb->prefix . "cf_forumcat";
				$insert = $wpdb->prepare("INSERT INTO ".$table_name." (idForum, idCat) VALUES ('%d', '%d')", $idForum, $legal_cat->ID);
				$results = $wpdb->query( $insert );
				break;
			}
		}
	}
	
	return $idForum;
}

function cf_get_node_hierarchy($nodes_info, $idPost) {
	$treeNodes = array();
	$treeNodes[0] = new treeNode();
	
	$hierarchy = $treeNodes[0];
	
	foreach($nodes_info as $post) {
		$treeNodes[$post->id] = new treeNode($post);
		if($post->id == $idPost)
			$treeNodes[$post->id]->expand = true;
	}
	
	foreach($treeNodes as $id => $treeNode) {
		if($id == 0)
			continue;
		if(isset($treeNodes[$treeNode->idParent]))
			$treeNodes[$treeNode->idParent]->addChild($treeNode);
	}
	
	return $hierarchy;
}

Class treeNode {
	public $children = array();
	public $expand = false;
	private $_post;
	public function __get($val) {
		return $this->_post->$val;
	}
	function __construct($post = null) {
		if(isset($post))
			$this->_post = $post;
	} 
	function addChild($post) {
		$this->children[] = $post;
	}
}

function cf_get_pages_count($id, $posts_per_page, $scope, $type = null, $search = null) {
	global $wpdb;
	
	if($scope == 'forum') {
		$hideUnApproved = true;	
		$role = cf_get_user_role();
		if($role == 1) {
			$hideUnApproved = false;
		}
		elseif($role == 0) {
			if(cf_is_mod_forum($id)) {
				$hideUnApproved = false;
			}
		}
	}
	
	if(get_option('post_per_page')!=null) {
	    $posts_per_page = get_option('post_per_page');
	}

	if(!in_array($scope, array('forum', 'main'))) {
		return false;
	}
	
	$scopeVars = array(
		'forum' => array(
			'id' => 'idForum',
			'table' => 'post'
			),
		'main' => array(
			'table' => 'forum'
			)
	);
	
	$table_name = $wpdb->prefix . "cf_".$scopeVars[$scope]['table']." as ".$scopeVars[$scope]['table'];
	
	if(isset($scopeVars[$scope]['id'])) {
		$where = $wpdb->prepare($scopeVars[$scope]['id']." = '%d' and idParent = 0 and", 
			$id);
	}
	else {
		$where = "";
	}
	
	$where.= " deleted = 0";
	
	if($scope == 'forum' && $hideUnApproved) {
		$where.= " and isApproved = 1";
	}
	
	if($type != null) {
		if($type=='cat') {
			$category_id = cf_get_cat_id_by_name($search);
			if($category_id==0) {
				return 0;
			}
			$table_name .= " join ".$wpdb->prefix ."cf_forumcat as cat on forum.id = cat.idForum ";
			$where.= " and cat.idCat = ".$category_id." ";
		}
		elseif($type=='tag') {
			$tag_id = cf_get_tag_id_by_name($search);
			if($tag_id==0) {
				return 0;
			}
			$table_name .= " join ".$wpdb->prefix ."cf_forumtag as tag on forum.id = tag.idForum ";
			$where.= " and tag.idTag = ".$tag_id." ";
		}
		elseif($type=='firstchar') {
			$whereLike = $wpdb->prepare("%s", $search);
			$whereLike = substr($whereLike,1,-1);
			$where.= " and forum.title like '".$whereLike."%' ";
		}
		else {
			$whereLike = $wpdb->prepare("%s", $search);
			$whereLike = substr($whereLike,1,-1);
			$where.= " and forum.title like '%".$whereLike."%' ";
		}
	}
	
	$count = "Select count(*) from ".$table_name." where ".$where;
	$count_row = $wpdb->get_row($count, ARRAY_N);
	return ceil($count_row[0]/$posts_per_page);
}

function cf_get_root_node_ids($idForum, $idPost, $nodes_per_page, $nav_page=0) {
	global $wpdb;

	if(get_option('post_per_page')!=null)
	{
	    $nodes_per_page = get_option('post_per_page');
	}
	
	if($idPost) {
		$root_post = get_root_post_by_post_id($idPost);
	}
		
	if($root_post) {
		if($root_post->isSticky) {
			$minshift_row = $wpdb->get_row($wpdb->prepare(
				"Select count(*) from " . $wpdb->prefix."cf_post where idForum = '%d' and idParent = 0 and deleted = 0 and isSticky = 1 and date > %s",
				$idForum,
				$root_post->date
			), ARRAY_N);

			$minshift = $minshift_row[0];
		}
		else {
			$minshift_row1 = $wpdb->get_row($wpdb->prepare(
				"Select count(*) from " . $wpdb->prefix."cf_post where idForum = '%d' and idParent = 0 and deleted = 0 and isSticky = 1",
				$idForum
			), ARRAY_N);
			$minshift_row2 = $wpdb->get_row($wpdb->prepare(
				"Select count(*) from " . $wpdb->prefix."cf_post where idForum = '%d' and idParent = 0 and deleted = 0 and isSticky = 0 and date > %s",
				$idForum,
				$root_post->date
			), ARRAY_N);

			$minshift = $minshift_row1[0] + $minshift_row2[0];
		}
		
		$nav_page = 1+floor($minshift/$nodes_per_page);

	}

		
	$flatlist = $wpdb->prepare("Select id from " . $wpdb->prefix."cf_post where idForum = '%d' and idParent = 0 and deleted = 0 order by isSticky desc, date desc limit %d, %d",
		$idForum,
		($nodes_per_page * ($nav_page-1)),
		$nodes_per_page);

	$results = $wpdb->get_results( $flatlist );
	$ret = array();
	foreach($results as $resobj) {
		$ret[]= $resobj->id;
	}
	
	return $ret;
}

function get_parent_post($idPost) {
	global $wpdb;
	$post = cf_get_post_by_id($idPost);
	if($post)
		return cf_get_post_by_id($post->idParent);
}

function get_root_post_by_post_id($idPost) {
	$post = cf_get_post_by_id($idPost);
	if(!$post)
		return false;
	while($parent_post = get_parent_post($post->id)) {
		$post = $parent_post;
	}
	return $post;
}

function cf_edit_post($postText, $subject, $stickPost, $idForum, $idParent, $PostId=null) {
	global $wpdb;
	
	$stickPost = $stickPost?1:0;
	
	$table_name = $wpdb->prefix . "cf_post";
	$idUser = get_current_user_id();
	if(!isset($PostId)) {
		$insert = $wpdb->prepare("INSERT INTO ".$table_name." (text, subject, isSticky, idForum, idParent, idUser) VALUES ('%s', '%s', ".$stickPost.", '%d', '%d', '%d')",
			$postText,
			$subject,
			$idForum,
			$idParent,
			$idUser);

		$results = $wpdb->query( $insert );
		$PostId = $wpdb->insert_id;
	}
	else {
		$update = $wpdb->prepare("UPDATE ".$table_name." SET text = %s, subject = %s where id = %d",
			$postText,
			$subject,	
			$PostId);

		$results = $wpdb->query( $update );
	}	
	

	
	
	if(isset($_FILES['attachment'])) {
		if($_FILES['attachment']['error'] != 4) {
			
			function wpse_141088_upload_dir( $dir ) {
				$mydir = '/CustomForumAttachments';
				return array(
						'path'   => $dir['basedir'] . $mydir,
						'url'    => $dir['baseurl'] . $mydir,
						'subdir' => $mydir
				) + $dir;
			}
	
			function wpse47580_check_upload_limits( $file ) {		
				if ( $file['size'] > 1024*get_option('max_file_size_kb'))
					$file['error'] = __( 'Exceeded filesize limit', 'custom-forums' );
				return $file;
			}
		
			add_filter( 'wp_handle_upload_prefilter', 'wpse47580_check_upload_limits' );
			add_filter( 'upload_dir', 'wpse_141088_upload_dir' );
			
			if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . '/wp-admin/includes/file.php' );
			
			$upload_results = wp_handle_upload( $_FILES['attachment'], array( 'test_form' => false ) );
			
			remove_filter( 'wp_handle_upload_prefilter', 'wpse47580_check_upload_limits' );
			remove_filter( 'upload_dir', 'wpse_141088_upload_dir' );
		
			if(is_array($upload_results)) {
				if(isset($upload_results['error'])) {
					global $fileUploadError;
					$fileUploadError = $upload_results['error'];
					return $PostId;
				}
			}
		
			$updateAttachment = $wpdb->prepare("UPDATE ".$table_name." SET attachment = %s where id = %d", basename($upload_results['url']), $PostId);
			$wpdb->query( $updateAttachment );
		}
	}
	
	return $PostId;
}

function cf_approve_post($PostId, $approve = true) { 
	global $wpdb;
	$table_name = $wpdb->prefix . "cf_post";	
	
	$sql = $wpdb->prepare("UPDATE ".$table_name." SET isApproved = '".($approve?1:0)."' where id = %d",$PostId);
	$results = $wpdb->query( $sql );
	
	return true;
}

function cf_post_set_sticky($PostId, $sticky) { 
	global $wpdb;
	$sticky = $sticky?1:0;
	$table_name = $wpdb->prefix . "cf_post";	
	$sql = $wpdb->prepare("UPDATE ".$table_name." SET isSticky = ".$sticky." where id = %d", $PostId);
	$results = $wpdb->query( $sql );
	return true;
}

function cf_post_unattach($PostId) {
	global $wpdb;
	$table_name = $wpdb->prefix . "cf_post";
	$sql = $wpdb->prepare("UPDATE ".$table_name." SET attachment = '' where id = %d", $PostId);
	$wpdb->query( $sql );
	return true;
}

add_filter('pre_option_default_role', function($default_role) {
		return 'forum_user';
	});

function cf_install() {
	global $wpdb;  
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	
	//create user group
	
	if(is_plugin_active('user-access-manager/user-access-manager.php')) {
		global $oUserAccessManager;
		if(isset($oUserAccessManager)) {
			$forumUsersGroupExists = false;
			$oUamUserGroups = $oUserAccessManager->getAccessHandler()->getUserGroups();
			foreach($oUamUserGroups as $oUamUserGroup) {
				if($oUamUserGroup->getGroupName() == "Forum users")
					$forumUsersGroupExists = true;
			}
			if(!$forumUsersGroupExists) {
				$newoUamUserGroup = new UamUserGroup($oUserAccessManager->getAccessHandler(), null);
				$newoUamUserGroup->setGroupName("Forum users");
				$newoUamUserGroup->setGroupDesc("Allowed to use custom forums");
				$newoUamUserGroup->save();
			}
		}
	}
	
	
	//table forum
	$table_name = $wpdb->prefix . "cf_forum";
	$sqlCreateTable = "CREATE TABLE " . $table_name . "
				 (
				  id int(11) NOT NULL AUTO_INCREMENT,
				  title varchar(255) NOT NULL,
				  openingpost text NOT NULL,
				  description text NOT NULL,
				  image int(11) NOT NULL default 0,
				  idUser int(11) NOT NULL,
				  date timestamp DEFAULT CURRENT_TIMESTAMP,		
				  featured tinyint(4) NOT NULL default 0,	  
				  deleted tinyint(4) NOT NULL default 0,
				  views int(11) NOT NULL default 0,
				  UNIQUE KEY id (id)
				)
				character set utf8 collate utf8_general_ci;";
	dbDelta( $sqlCreateTable );

	//table forumtag
	$table_name = $wpdb->prefix . "cf_forumtag";
	$sqlCreateTable = "CREATE TABLE " . $table_name . " (
				  id int(11) NOT NULL AUTO_INCREMENT,
				  idForum int(11) NOT NULL,
				  idTag int(11) NOT NULL,
				  UNIQUE KEY id (id)
				)
				character set utf8 collate utf8_general_ci;";
	dbDelta( $sqlCreateTable );

	//table forumcategory
	$table_name = $wpdb->prefix . "cf_forumcat";
	$sqlCreateTable = "CREATE TABLE " . $table_name . " (
				  id int(11) NOT NULL AUTO_INCREMENT,
				  idForum int(11) NOT NULL,
				  idCat int(11) NOT NULL,
				  UNIQUE KEY id (id)
				)
				character set utf8 collate utf8_general_ci;";
	dbDelta( $sqlCreateTable );

	//table forumcategory
	$table_name = $wpdb->prefix . "cf_forummod";
	$sqlCreateTable = "CREATE TABLE " . $table_name . " (
				  id int(11) NOT NULL AUTO_INCREMENT,
				  idForum int(11) NOT NULL,
				  idUser int(11) NOT NULL,
				  UNIQUE KEY id (id)
				)
				character set utf8 collate utf8_general_ci;";
	dbDelta( $sqlCreateTable );
	
	//table post
	$table_name = $wpdb->prefix . "cf_post";
	$sqlCreateTable = "CREATE TABLE " . $table_name . " (
				  id int(11) NOT NULL AUTO_INCREMENT,
				  text text NOT NULL,
				  subject text NOT NULL,
				  idForum int(11) NOT NULL,				  
				  idParent int(11) NOT NULL,
				  idUser int(11) NOT NULL,
				  date timestamp DEFAULT CURRENT_TIMESTAMP,
				  attachment text NOT NULL,
				  isApproved tinyint(4) NOT NULL default 0,
				  isSticky tinyint(4) NOT NULL default 0,
				  deleted tinyint(4) NOT NULL default 0,
				  views int(11) NOT NULL default 0,
				  UNIQUE KEY id (id)
				)
				character set utf8 collate utf8_general_ci;";
	dbDelta( $sqlCreateTable );

	//table adimage
	$table_name = $wpdb->prefix . "cf_adimage";
	$sqlCreateTable = "CREATE TABLE " . $table_name . " (
				  id int(11) NOT NULL,				  
				  image int(11) NOT NULL default 0,
				  ltype tinyint(4) NOT NULL default 0,	  
				  link text NOT NULL,
				  UNIQUE KEY id (id)
				)
				character set utf8 collate utf8_general_ci;";
	dbDelta( $sqlCreateTable );

	//table subscription
	$table_name = $wpdb->prefix . "cf_subscription";
	$sqlCreateTable = "CREATE TABLE " . $table_name . " (
				  id int(11) NOT NULL AUTO_INCREMENT,			  
				  idPost int(11) NOT NULL,			
				  idforum int(11) NOT NULL,
				  idUser int(11) NOT NULL,
				  UNIQUE KEY id (id)
				)
				character set utf8 collate utf8_general_ci;";
	dbDelta( $sqlCreateTable );
	
	cf_add_page('Custom forum', '[custom_forum]');
	cf_add_page('Custom forum rikuz', '[custom_forum_rikuz]');

	if(get_option('forum_per_page')==null) {
	    add_option('forum_per_page', '10');
	}

	if(get_option('post_per_page')==null) {
	    add_option('post_per_page', '10');
	}
	
	if(get_option('max_file_size_kb')==null) {
		add_option('max_file_size_kb', '300');
	}
}

function cf_add_page($new_page_title, $new_page_content) {
	//basic page
	$new_page_template = '';

	$page_check = get_page_by_title($new_page_title);
	$new_page = array(
			'post_type' => 'page',
			'post_title' => $new_page_title,
			'post_content' => $new_page_content,
			'post_status' => 'publish',
			'post_author' => 1,
	);
	if(!isset($page_check->ID)){
		$new_page_id = wp_insert_post($new_page);
		if(isset($new_page_template)){
			update_post_meta($new_page_id, '_wp_page_template', $new_page_template);
		}
	}
}

function cf_send_subscription($idPost, $user_id_array = array()) {
	global $wpdb;	
	$table_name = $wpdb->prefix . "cf_subscription";
	
	$sql = $wpdb->prepare("select * from ".$table_name." where idPost = '%d' and not idUser = %d", $idPost, get_current_user_id());
	
	$message = "<h3>".__( 'Email subject (new message)', 'custom-forums' )."</h3>".
		"<div>".__( 'Email body (new message)', 'custom-forums' )."</div>".
		"<div>".__( 'Follow this link to view new message:', 'custom-forums' )." ".
		get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".cf_get_forum_by_post_id($idPost)->id."&idPost=".$idPost."</div>";
	
	$subscriptions = $wpdb->get_results( $sql );
		
	
	$post_subscriber_ids = array(); 
	
	foreach ($subscriptions as $subscription) {
		$post_subscriber_ids[]= $subscription->idUser;
	}
	
	$all_user_ids = array_unique(array_merge($user_id_array, $post_subscriber_ids));
	
	$table_name = $wpdb->prefix . "cf_post";
	$sql = $wpdb->prepare("select idParent from ".$table_name." where id = '%d'", $idPost);
	$ParentID = $wpdb->get_row( $sql, ARRAY_N );
	if(count($ParentID)) {
		if($ParentID[0] == 0) {
			
			if(!($forum = cf_get_forum_by_post_id($idPost)))
				return;
			
			$idForum = $forum->id;			
			
			$table_name = $wpdb->prefix . "cf_subscription";
			
			$sql = $wpdb->prepare("select * from ".$table_name." where idForum = '%d' and not idUser = '%d'", $idForum, get_current_user_id());
			$forum_subscriptions = $wpdb->get_results( $sql );
			
			$forum_subscriber_ids = array();
			
			foreach ($forum_subscriptions as $subscription) {
				$forum_subscriber_ids[]= $subscription->idUser;
			}		
			
			$all_user_ids = array_unique(array_merge($all_user_ids, $forum_subscriber_ids));
			
			foreach ($all_user_ids as $idUser) {
				$user = get_user_by( 'id', $idUser );
				$headers = "Content-Type: text/html; charset=utf-8";
				wp_mail( $user->user_email, 'Subscription', $message, $headers);
			}
		}
		else {
			cf_send_subscription($ParentID[0], $all_user_ids);
		}
	}
}


function cf_add_subscription ($idPost, $idUser) {
	global $wpdb;
	$table_name = $wpdb->prefix . "cf_subscription";
	
	$sql = $wpdb->prepare("select * from ".$table_name." where idPost = '%d' and idUser = '%d'",
				$idPost,
				$idUser);

	$subscription = $wpdb->get_results( $sql );
	if(count($subscription)==0)
	{
		$sql = $wpdb->prepare("INSERT INTO ".$table_name." (idPost, idUser) VALUES ('%d', '%d')",
				$idPost,
				$idUser);

		$results = $wpdb->query( $sql ); 
	}
}

function cf_add_forum_subscription ($idForum, $idUser) {
	global $wpdb;
	$table_name = $wpdb->prefix . "cf_subscription";

	$sql = $wpdb->prepare("select * from ".$table_name." where idForum = '%d' and idUser = '%d'",
			$idForum,
			$idUser);

	$subscription = $wpdb->get_results( $sql );
	if(count($subscription)==0)
	{
		$sql = $wpdb->prepare("INSERT INTO ".$table_name." (idForum, idUser) VALUES ('%d', '%d')",
				$idForum,
				$idUser);

		$results = $wpdb->query( $sql );
	}
}

function cf_remove_subscription ($idPost, $idUser) {
	global $wpdb;
	$table_name = $wpdb->prefix . "cf_subscription";

	$sql = $wpdb->prepare("DELETE FROM ".$table_name." where idPost = '%d' and idUser = '%d'",
			$idPost,
			$idUser);

	$results = $wpdb->query( $sql ); 
}

function cf_remove_forum_subscription ($idForum, $idUser) {
	global $wpdb;
	$table_name = $wpdb->prefix . "cf_subscription";

	$sql = $wpdb->prepare("DELETE FROM ".$table_name." where idForum = '%d' and idUser = '%d'",
			$idForum,
			$idUser);

	$results = $wpdb->query( $sql );
}

function cf_is_current_user_subscribed($idForum) {
	global $wpdb;
	$idUser = get_current_user_id();
	
	$table_name = $wpdb->prefix . "cf_subscription";
	
	$sql = $wpdb->prepare("select * from ".$table_name." where idForum = '%d' and idUser = '%d'",
		$idForum,
		$idUser);
	
	return count($wpdb->get_results( $sql ));
}


function cf_add_pages () {
    add_menu_page (__("Forum settings", 'custom-forums'), __("Forum settings", 'custom-forums'), 8, __FILE__, 'cf_forumsetting_page');
	add_submenu_page(__FILE__, __("Ad settings", 'custom-forums'), __("Ad settings", 'custom-forums'), 8, 'ad_settings', 'cf_adsetting_page');
	add_submenu_page(__FILE__, __("User settings", 'custom-forums'), __("User settings", 'custom-forums'), 8, 'user_settings', 'cf_usersetting_page');
	add_submenu_page(__FILE__, __("Custom Forum Tags", 'custom-forums'), __("Custom Forum Tags", 'custom-forums'), 8, 'edit.php?post_type=forum_tag');
	add_submenu_page(__FILE__, __("Custom Forum Category", 'custom-forums'), __("Custom Forum Category", 'custom-forums'), 8, 'edit.php?post_type=forum_categories');
}

function cf_forumsetting_page () {
	global $wpdb;

	if(isset($_POST['btnSaveAdminSettingForum']))
	{
		update_option('forum_per_page', $_POST['txtForumPerPage']);
		update_option('post_per_page', $_POST['txtPostPerPage']);
		update_option('max_file_size_kb', $_POST['txtMaxUploadSizeKB']);
		
	}

	$forum_per_page = 1;
	if(get_option('forum_per_page')!=null)
	{
	    $forum_per_page = get_option('forum_per_page');
	}
	$post_per_page = 1;
	if(get_option('post_per_page')!=null)
	{
	    $post_per_page = get_option('post_per_page');
	}
	$max_file_size_kb = 1024;
	if(get_option('max_file_size_kb')!=null)
	{
		$max_file_size_kb = get_option('max_file_size_kb');
	}

	?> <h2><?php _e('Forum settings', 'custom-forums'); ?></h2>
			<a href='?page=ad_settings'><?php _e('Ad settings', 'custom-forums'); ?></a><br />
			<a href='?page=user_settings'><?php _e('User settings', 'custom-forums'); ?></a>
			<br />
			<br />
			<form method='post'>
				<table>
					<tr>
						<td><?php _e('Forums per page', 'custom-forums'); ?></td>
						<td><input type='text' name='txtForumPerPage' value='<?php echo $forum_per_page; ?>' /></td>
					</tr>
					<tr>
						<td><?php _e('Posts per page', 'custom-forums'); ?></td>
						<td><input type='text' name='txtPostPerPage' value='<?php echo $post_per_page; ?>' /></td>
					</tr>
					<tr>
						<td><?php _e('Max File Upload size(KB)', 'custom-forums'); ?></td>
						<td><input type='text' name='txtMaxUploadSizeKB' value='<?php echo $max_file_size_kb; ?>' /></td>
					</tr>
					<tr>
						<td colspan='2'><input type='submit' name='btnSaveAdminSettingForum' value='Save' /></td>
					</tr>
				</table>
			</form>
	<?php 
}

function reArrayFiles(&$file_post) {

    $file_ary = array();
    $file_count = count($file_post['name']);
    $file_keys = array_keys($file_post);

    for ($i=0; $i<$file_count; $i++) {
        foreach ($file_keys as $key) {
            $file_ary[$i][$key] = $file_post[$key][$i];
        }
    }

    return $file_ary;
}

function cf_adsetting_page () {
	global $wpdb;
	$table_name = $wpdb->prefix . "cf_adimage";
	$adImages = array();
	
	for($i=0;$i<10;$i++)
	{
		$index = $i+1;
		$adImages[$index]['index'] = $index;
		$adImages[$index]['image'] = 0;
		$adImages[$index]['link'] = '';
		$adImages[$index]['type'] = 0;

		if(isset($_POST['imagefile'.$index]))
		{
			$imageFileName = $_POST['imagefile'.$index];
			if($imageFileName>0)
			{
				
				$linkType = $_POST['linkType'][$i];
				
				$linkAd = "";
			    if($_POST['imageLink'][$i]!="")
			    {
		    		$linkAd = (stripos($_POST['imageLink'][$i], "http://")===0)?$_POST['imageLink'][$i]:"http://".$_POST['imageLink'][$i]; 
			    }
				$sql = "select * from ".$table_name." where id=".$index;
				$adExist = $wpdb->get_results( $sql );
				if(count($adExist)>0)
				{
					$imageFileName = ($imageFileName=="")?$adExist[0]->image:$imageFileName;

		    		$sql = $wpdb->prepare("UPDATE ".$table_name." SET image = '%s', link = '%s', ltype = '%d' where id = '%d'",						
						$imageFileName,
						$linkAd,
						$linkType,
						$index);
				}
				else
				{
					$sql = $wpdb->prepare("INSERT INTO ".$table_name." (id, image, link, ltype) VALUES ('%d', '%s', '%s', %d)",
						$index,
						$imageFileName,
						$linkAd,
						$linkType
						);
				}
			}
			else
			{
				$sql = $wpdb->prepare("DELETE FROM ".$table_name." WHERE id='%d'", $index);
			}
			$results = $wpdb->query( $sql ); 
		}
	}

	//$sql = "select * from ".$table_name." order by id";
	//$adimage_list = $wpdb->get_results( $sql );
	$adimage_list = cf_get_ads();
	if(count($adimage_list)>0)
	{
		foreach ($adimage_list as $asImage) {
			$adImages[$asImage->id]['index'] = $asImage->id;
			$adImages[$asImage->id]['image'] = $asImage->image;
			$adImages[$asImage->id]['link'] = $asImage->link;
			$adImages[$asImage->id]['type'] = $asImage->ltype;
		} 
	}
	include plugin_dir_path( __FILE__ ).'/templates/ad-settings.php';
}

function cf_layout_ad($ad) {
	$image = wp_get_attachment_image_src($ad->image, 'cf-ads');
	?>
	<div class='adsense9'>
		<a <?php if($ad->ltype != 1) {?>href='<?php echo $ad->link;?>'<?php } ?> <?php if($ad->ltype == 2){?> target="_blank" <?php }
		if($ad->ltype == 1) {?> onclick="window.open('<?php echo $ad->link; ?>','','width=600,height=400')" <?php } ?>>
			<img src='<?php echo $image[0]; ?>' class = 'adimg' alt=''>
		</a>
	</div>
	<?php 
}

function cf_get_moderators_by_forum_id($idForum) {
	global $wpdb;
	$sql = $wpdb->prepare("select idUser from ". $wpdb->prefix . "cf_forummod where idForum = %d", $idForum);
	$results = $wpdb->get_results( $sql ); 
	$moderators = array();
	foreach($results as $result) {
		$moderators[]= get_user_by('id', $result->idUser);
	}
	return $moderators;
}

function cf_get_ads() {
	global $wpdb;
	$table_name = $wpdb->prefix . "cf_adimage";
	$sql = "select * from ".$table_name." order by id";
	return $wpdb->get_results( $sql );
}

function cf_get_random_ads($amount) {
	$adimage_list = cf_get_ads();
	$adimage_list_count = count($adimage_list);
	
	if($adimage_list_count == 0) {
		return false;
		}
		
	$ad_ids = range(0, $adimage_list_count-1);
		
	if($adimage_list_count < $amount) {
		$random_ad_ids = array();
		for($i = 0; $i < $amount; $i++) {
			shuffle($ad_ids);
			$random_ad_ids[] = $ad_ids[0];
		}
	}
	else {
		$random_ad_ids = array();
		shuffle($ad_ids);
		for($i = 0; $i < $amount; $i++) {
			$random_ad_ids[] =  $ad_ids[$i];
		}
	}
	
	$ads = array();
	foreach($random_ad_ids as $ad_id) {
		$ads[]= $adimage_list[$ad_id];
	} 
	return $ads;
}

function cf_usersetting_page () {

	$forumUsersGroupExists = false;
	
	if(is_plugin_active('user-access-manager/user-access-manager.php')) {
		global $oUserAccessManager;
		if(isset($oUserAccessManager)) {
			$oUamUserGroups = $oUserAccessManager->getAccessHandler()->getUserGroups();
			foreach($oUamUserGroups as $oUamUserGroup) {
				if($oUamUserGroup->getGroupName() == "Forum users") {
					$forumUsersGroupExists = true;
					$forumUsers = array();
					foreach($oUamUserGroup->getFullUsers() as $user) 
						$forumUsers[] = get_user_by( 'id', $user->id );
				}
			}
		}
	}
	
	if(!$forumUsersGroupExists) {
		?>
		<p>"Forum users" group doesn't exist. Make sure UAM plugin is installed and try reactivating custom forums plugin.</p>
		<?php
		return; 
	}

	global $wpdb;
	$user_id = -1;
	$forum_id = -1;
	$table_forum = $wpdb->prefix . "cf_forum";
	$table_forummod = $wpdb->prefix . "cf_forummod";

	if(isset($_POST["users"]))
		$user_id = $_POST["users"];

	if(isset($_POST["btnAddForum"]) && isset($_POST["forums"]) && $user_id!=-1)
	{
		$forum_id = $_POST["forums"];
		$insert = $wpdb->prepare("INSERT INTO ".$table_forummod." (idForum, idUser) VALUES ('%d', '%d')",
			$forum_id,
			$user_id);
		
		$results = $wpdb->query( $insert );
		
		if($user = get_user_by('id', $user_id)) {
			$user->add_cap( 'upload_files' );
			$user->add_cap( 'edit_others_pages' );
			$user->add_cap( 'edit_published_pages' );
		}
	}

	if(isset($_POST["remove_forum"]) && $user_id!=-1)
	{
		$remove_forum = intval($_POST["remove_forum"]);
		if($remove_forum > 0)
		{
			$delete = $wpdb->prepare("DELETE FROM ".$table_forummod." WHERE id='%d'", $remove_forum);
			$results = $wpdb->query( $delete );
		}
	}
	
	
	if($user_id==-1 && count($forumUsers)>0)
		$user_id = $forumUsers[0]->id;

	$whereForums = "";
	if($user_id!=-1)
	{
		$sql = $wpdb->prepare("select forummod.id, forum.title, forum.id as forumId from ".$table_forum." as forum join ".$table_forummod." as forummod on forum.id = forummod.idForum where forum.deleted=0 and forummod.idUser = '%d' order by forum.title", $user_id);
		$forummods = $wpdb->get_results( $sql );
		foreach ($forummods as $forummod) {
			$whereForums .= $forummod->forumId.",";
		} 
		if($whereForums != "")
		{
			$whereForums = substr($whereForums,0,-1);
			$whereForums = " and id NOT IN (".$whereForums.") ";
		}
	}

	$sql = "select id, title from ".$table_forum." where deleted=0 ".$whereForums." order by title";
	$forums = $wpdb->get_results( $sql );

	include plugin_dir_path( __FILE__ ).'/templates/user-settings.php';
}

function true_image_uploader_field( $name, $value = '', $w = 592, $h = 76) {	
	$default = plugins_url( 'images/no-image.jpg', __FILE__ );
	if( $value ) {
		$image_attributes = wp_get_attachment_image_src( $value, 'thumbnail'/*, array($w, $h)*/ );
		$src = $image_attributes[0];
	} else {
		$src = $default;
	}
	return '<td>
				<img id="img'.$name.'" data-src="' . $default . '" src="' . $src . '" />
		  	</td>
		  	<td>
				<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
				<input type="button" class="upload_image_button button" value="'. __( 'Add image', 'custom-forums' ).'" />
				<input type="button" class="remove_image_button button" value="&times;" />
			</td>';
}

function true_image_uploader_field_for_ad_setting( $name, $value = '') {
	$default = plugins_url( 'images/no-image.jpg', __FILE__ );
	if( $value ) {
		//$image_attributes = wp_get_attachment_image_src( $value, array($w, $h) );
		$image_attributes = wp_get_attachment_image_src( $value, 'large' );
		$src = $image_attributes[0];
	} else {
		$src = $default;
	}
	return '<td>
				<img id="img'.$name.'" data-src="' . $default . '" src="' . $src . '" />
				<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />
				<input type="button" class="upload_image_button button" value="'. __( 'Add image', 'custom-forums' ).'" />
				<input type="button" class="remove_image_button button" value="&times;" />
			</td>';
}

function cf_main_layout($isrikuz = false) {

$upload_dir = wp_upload_dir();
if(!file_exists($upload_dir['basedir'].'/CustomForumAttachments')) {
	wp_mkdir_p($upload_dir['basedir'].'/CustomForumAttachments');
}

echo "<link rel='stylesheet' type='text/css' href='".plugins_url('/css/CustomForum.css', __FILE__)."' />";
echo "<link rel='stylesheet' type='text/css' href='".plugins_url('/css/rtl-treeview/jquery.treeview.rtl.css', __FILE__)."' />";
echo "<link rel='stylesheet' type='text/css' href='".plugins_url('/css/suggest.css', __FILE__)."' />";
echo '<script type="text/javascript" src="'.plugins_url('/js/jquery.treeview.js', __FILE__).'"></script>';
echo '<script type="text/javascript" src="'.plugins_url('/js/jquery-ui-1.11.2/jquery-ui.min.js', __FILE__).'"></script>';
echo '<script type="text/javascript" src="'.plugins_url('/js/jquery.ui.datepicker-he.js', __FILE__).'"></script>';
echo '<script type="text/javascript" src="'.plugins_url().'/ckeditor-for-wordpress/ckeditor/ckeditor.js"></script>';
echo '<script type="text/javascript" src="/wp-includes/js/jquery/suggest.js"></script>';
echo '<script type="text/javascript" src="'.plugins_url('/js/customforum.js', __FILE__).'"></script>';
//echo '<script type="text/javascript" src="/wp-includes/js/tinymce/tinymce.min.js"></script>';

//	echo '<script type="text/javascript" src="'.plugins_url().'/ckeditor-for-wordpress/ckeditor.config.js"></script>';

//echo '<script type="text/javascript" src="'.plugins_url('/js/customforum.js', __FILE__).'"></script>';

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );


if(isset($_POST['btnLogOut'])) {
	wp_logout();
}

//registration and login
$displayOverback = $displaySignUp = $displayLogIn = $displayMessage = "display:none;";
$snickname = "";
$semail = "";
$slogin = "";
$spassword = "";
$spasswordconfirm = "";
$llogin = "";
$lpassword = "";
$valid_error_message = "";
$messageRegistration = get_query_var("popupMessage");
if(strlen($messageRegistration)) {
	$displayOverback = $displayMessage = "";
}
if(isset($_POST['llogin'])) {
	$valid=true;
	$llogin = $_POST['llogin'];
	$lpassword = $_POST['password'];
	if($llogin=="") {
		$valid = false;
		$valid_error_message = _e("Please enter Login", 'custom-forums');
	}
	if($lpassword=="") {
		$valid = false;
		$valid_error_message = _e("Please enter Password", 'custom-forums');
	}

	if($valid) {
		$creds = array();
		$creds['user_login'] = $llogin;
		$creds['user_password'] = $lpassword;
		$creds['remember'] = true;
		$user = wp_signon( $creds, false );
		if ( is_wp_error($user) ) {
			$valid_error_message = $user->get_error_message();
			$valid = false;
		}
		else {
			wp_redirect(get_site_url()."/custom-forum/?cf_action=".cf_get_action()); exit();
			/*$llogin = "";
				$lpassword = "";*/
		}
	}

	if(!$valid) {
		$displayOverback = $displayLogIn = "";
	}
}
if(isset($_POST['slogin'])) {
	$valid=true;
	$snickname = $_POST['nickname'];
	$semail = $_POST['email'];
	$slogin = $_POST['slogin'];
	$spassword = $_POST['password'];
	$spasswordconfirm = $_POST['passwordconfirm'];
	if($snickname=="") {
		$valid = false;
		$valid_error_message = _e("Please enter Nickname", 'custom-forums');
	}
	if($semail=="") {
		$valid = false;
		$valid_error_message = _e("Please enter Email", 'custom-forums');
	}
	if(!is_email($semail)) {
		$valid = false;
		$valid_error_message = _e("Please enter correct Email", 'custom-forums');
	}
	if ( email_exists($semail) ) {
		$valid = false;
		$valid_error_message = _e("Email existing! Please enter other Email", 'custom-forums');
	}
	if($slogin=="") {
		$valid = false;
		$valid_error_message = _e("Please enter Login", 'custom-forums');
	}
	if(username_exists($slogin)!=null) {
		$valid = false;
		$valid_error_message = _e("Login existing! Please enter other Login", 'custom-forums');
	}
	if($spassword=="") {
		$valid = false;
		$valid_error_message = _e("Please enter Password", 'custom-forums');
	}
	if($spasswordconfirm=="") {
		$valid = false;
		$valid_error_message = _e("Please enter Confirm Password", 'custom-forums');
	}
	if($spassword!=$spasswordconfirm) {
		$valid = false;
		$valid_error_message = _e("Password != Confirm Password", 'custom-forums');
	}
	if($valid) {
		$userdata = array(
				'user_login' => $slogin,
				'user_pass'  => $spassword,
				'user_email' => $semail,
				'role' => 'pending',
				'nickname'   => $snickname,
		);

		$user_id = wp_insert_user( $userdata );
			
		if( is_wp_error( $user_id ) ) {
			$valid_error_message = $user_id->get_error_message();
			$valid = false;
		} else {

			if ( isset( $_POST['avatar_image_nonce'] ) && wp_verify_nonce( $_POST['avatar_image_nonce'], 'avatar_image' )) {
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
				require_once( ABSPATH . 'wp-admin/includes/media.php' );
					
				$attachment_id = media_handle_upload( 'avatar_image', 0 );
					
				if ( !is_wp_error( $attachment_id ) ) {
					global $blog_id, $wpdb;
					$wpua_metakey = $wpdb->get_blog_prefix($blog_id).'user_avatar';
					update_user_meta($user_id, $wpua_metakey, $attachment_id);
				}

			}

			$code = sha1( $user_id . time() );
			$activation_link = add_query_arg( array( 'key' => $code, 'user' => $user_id ), get_site_url()."/custom-forum/?cf_action=user-confirm");
			add_user_meta( $user_id, 'has_to_be_activated', $code, true );
			$headers = "Content-Type: text/html; charset=utf-8";
			wp_mail(
			$userdata['user_email'],
			__('ACTIVATION ON NOMIND CUSTOM FORUMS LETTER SUBJECT', 'custom-forums'),
			__('ACTIVATION ON NOMIND CUSTOM FORUMS LETTER BODY, follow this link:', 'custom-forums').$activation_link,
			$headers
			);

			$messageRegistration = __('We sent you an email with a link to activate your account. Please check the mail: ','custom-forums').$semail;
			$displayOverback = $displayMessage = "";
			$snickname = "";
			$semail = "";
			$slogin = "";
			$spassword = "";
			$spasswordconfirm = "";
		}
	}

	if(!$valid) {
		$displayOverback = $displaySignUp = "";
	}
}
//end
global $UserId, $cf_action, $roleNumber, $idForum, $idPost, $title, $description, $tags, $categories, $featured, $text, $idParent, $nicename, $datestart,
$dateend, $fordays, $SearchText, $nav_page, $main_page;

$UserId = get_current_user_id();
$cf_action = 	$isrikuz?'forum-sortedlist':cf_get_action();
$roleNumber = 	cf_get_user_role();
$idForum = 		filter_var(get_query_var("idForum"), FILTER_VALIDATE_INT)?(get_query_var("idForum")>0?get_query_var("idForum"):0):0;
$idPost = 		filter_var(get_query_var("idPost"), FILTER_VALIDATE_INT)?(get_query_var("idPost")>0?get_query_var("idPost"):0):0;
$nav_page = 	filter_var(get_query_var("nav_page"), FILTER_VALIDATE_INT)?(get_query_var("nav_page")>0?get_query_var("nav_page"):1):1;
$subscribe = 	(get_query_var("subscribe")=="on")?1:0;
$stickPost = 	(get_query_var("stickPost")=="on")?1:0;
$title = 		stripslashes(get_query_var("title"));
$description = 	stripslashes(get_query_var("description"));
$tags = 		is_array($_POST["tags"])?$_POST["tags"]:array();
$categories = 	is_array($_POST["categories"])?$_POST["categories"]:array();
$featured = 	get_query_var("featured");
$openingpost = 	get_query_var("openingpost");
$postText = 	stripslashes(get_query_var("postText"));
$idParent = 	get_query_var("idParent");
$nicename = 	strlen(get_query_var("nicename"))?stripslashes(get_query_var("nicename")):null;

$datestart =	filter_var(get_query_var("datestart"),FILTER_VALIDATE_REGEXP,array("options"=>array("regexp"=> "/^\d{4}-\d{2}-\d{2}$/")))?get_query_var("datestart"):null;
$dateend = 		filter_var(get_query_var("dateend"),FILTER_VALIDATE_REGEXP,array("options"=>array("regexp"=> "/^\d{4}-\d{2}-\d{2}$/")))?get_query_var("dateend"):null;

$fordays = 		strlen(get_query_var("fordays"))?get_query_var("fordays"):null;
$SearchText = 	strlen(get_query_var("search"))?stripslashes(get_query_var("search")):null;

cf_before_content($isrikuz);

if($cf_action == "forum-single") {
	ob_end_flush();
	include plugin_dir_path( __FILE__ ).'/templates/forum-single.php';
}
elseif($cf_action == "forum-sortedlist") {
	ob_end_flush();
	include plugin_dir_path( __FILE__ ).'/templates/forum-sortedlist.php';
}
elseif($cf_action == "forum-list") {
	$SearchType=null;
	ob_end_flush();
	include plugin_dir_path( __FILE__ ).'/templates/forum-list.php';
}
elseif($cf_action == "forum-delete") {
	cf_delete_entry($idForum, 'forum');
	wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-list"); exit();
}
elseif($cf_action == "post-delete") {
	$idForum = cf_get_forum_by_post_id($idPost)->id;
	cf_delete_entry($idPost, 'post');
	wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$idForum); exit();
}
elseif($cf_action == "forum-add") {
	//$imageFileName = cf_upload_image();
	$imageFileName = 0;
	if($idForum==0){
		$imageFileName = $_POST["hdnImages"];
	}
	else {
		$nameHdnImages = "hdnImages".$idForum;
		$imageFileName =  $_POST[$nameHdnImages];
	}

	$idForum = cf_edit_forum($title, $description, $tags, $categories, $imageFileName, $featured, $openingpost, $idForum);
	wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$idForum); exit();
}
elseif($cf_action == "post-add") {
	$idPost = cf_edit_post($postText, $title, $stickPost, $idForum, $idParent);
	cf_send_subscription($idPost);
	if($subscribe) {
		cf_add_subscription($idPost);
	}
	global $fileUploadError;
	if(isset($fileUploadError)) {
		wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$idForum."&idPost=".$idPost."&popupMessage=".urlencode($fileUploadError)); exit();
	}
	else {
		wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$idForum."&idPost=".$idPost); exit();
	}
}
elseif($cf_action == "post-edit") {
	$idForum = cf_get_forum_by_post_id($idPost)->id;
	cf_edit_post($postText, $title, $stickPost, $idForum, $idParent, $idPost);
	global $fileUploadError;
	if(isset($fileUploadError)) {
		wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$idForum."&idPost=".$idPost."&popupMessage=".urlencode($fileUploadError)); exit();
	}
	else {
		wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$idForum."&idPost=".$idPost); exit();
	}
}
elseif($cf_action == "post-approve") {
	cf_approve_post($idPost);
	//die();
	wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".cf_get_forum_by_post_id($idPost)->id."&idPost=".$idPost); exit();
}
elseif($cf_action == "post-disapprove") {
	cf_approve_post($idPost, false);
	//die();
	wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".cf_get_forum_by_post_id($idPost)->id."&idPost=".$idPost); exit();
}
elseif($cf_action == "post-subscribe") {
	if($subscribe) {
		cf_add_subscription($idPost, $UserId);
	}
	else {
		cf_remove_subscription($idPost, $UserId);
	}
	wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".cf_get_forum_by_post_id($idPost)->id."&idPost=".$idPost); exit();
}
elseif($cf_action == "forum-subscribe") {
	if($subscribe) {
		cf_add_forum_subscription($idForum, $UserId);
	}
	else {
		cf_remove_forum_subscription($idForum, $UserId);
	}
	wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".$idForum); exit();
}
elseif($cf_action == "tag-add") {
	ob_end_clean();
		
	$idTag = wp_insert_post(array(
			'post_title' => $title,
			'post_status' => 'publish',
			'post_type' => 'forum_tag'
	));
		
	if(filter_var($idTag, FILTER_VALIDATE_INT)) {
		die(json_encode(array(
				'tagid' => $idTag
		)));
	}
}
elseif($cf_action == "category-add") {
	ob_end_clean();
		
	$idCat = wp_insert_post(array(
			'post_title' => $title,
			'post_status' => 'publish',
			'post_type' => 'forum_categories'
	));
		
	if(filter_var($idCat, FILTER_VALIDATE_INT)) {
		die(json_encode(array(
				'catid' => $idCat
		)));
	}
}
elseif($cf_action == "post-stick") {
	cf_post_set_sticky($idPost, $stickPost);
	wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".cf_get_forum_by_post_id($idPost)->id."&idPost=".$idPost); exit();
}
elseif($cf_action == "post-unattach") {
	cf_post_unattach($idPost);
	wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-single&idForum=".cf_get_forum_by_post_id($idPost)->id."&idPost=".$idPost); exit();
}
elseif($cf_action == "forum-search-char") {
	$SearchType="firstchar";
	ob_end_flush();
	include plugin_dir_path( __FILE__ ).'/templates/forum-list.php';
}
elseif($cf_action == "forum-search-cat") {
	$SearchType="cat";
	ob_end_flush();
	include plugin_dir_path( __FILE__ ).'/templates/forum-list.php';
}
elseif($cf_action == "forum-search-tag") {
	$SearchType="tag";
	ob_end_flush();
	include plugin_dir_path( __FILE__ ).'/templates/forum-list.php';
}
elseif($cf_action == "add_post_view") {
	cf_add_post_view_callback($idPost);
}
elseif($cf_action == "forum-search") {
	$SearchType="search";
	ob_end_flush();
	include plugin_dir_path( __FILE__ ).'/templates/forum-list.php';
}
elseif($cf_action == "user-confirm") {
	$user_id = filter_input( INPUT_GET, 'user', FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 1 ) ) );
	if ( $user_id ) {
		$code = get_user_meta( $user_id, 'has_to_be_activated', true );
		if ( $code == filter_input( INPUT_GET, 'key' ) ) {
			if($user = get_user_by('id', $user_id)) {
				$user->add_cap( 'upload_files' );
			}
			wp_update_user( array( 'ID' => $user_id, 'role' => 'subscriber' ) );
			delete_user_meta( $user_id, 'has_to_be_activated' );
			if(is_plugin_active('user-access-manager/user-access-manager.php')) {
				global $oUserAccessManager;
				if(isset($oUserAccessManager)) {
					$oUamUserGroups = $oUserAccessManager->getAccessHandler()->getUserGroups();
					foreach($oUamUserGroups as $oUamUserGroup) {
						if($oUamUserGroup->getGroupName() == "Forum users") {
							$oUamUserGroup->addObject('user', $user_id);
							$oUamUserGroup->save();
						}
					}
				}
			}
		}
	}

	$messageRegistration = __('Your account has been activated successfully. Please log in with your account. You will be redirected in 5 seconds','custom-forums');
	$displayOverback = $displayMessage = "";
	ob_end_flush();
	//wp_redirect(get_site_url()."/custom-forum/?cf_action=forum-list");
}
elseif($cf_action == "post-search") {
	ob_end_flush();
	include plugin_dir_path( __FILE__ ).'/templates/post-search.php';
}


ob_end_flush();
include plugin_dir_path( __FILE__ ).'/templates/login-register-user.php';
}

function cf_main_layout_rikuz() {
	cf_main_layout(true);
}

add_shortcode('custom_forum', 'cf_main_layout');
add_shortcode('custom_forum_rikuz', 'cf_main_layout_rikuz');
?>