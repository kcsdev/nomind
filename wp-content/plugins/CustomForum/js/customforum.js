jQuery(document).ready(function() {
	
	var PostsViewed = new Array();
	
	$.datepicker.setDefaults($.datepicker.regional["he"]);
	
	jQuery('.search-date').datepicker({
        dateFormat : 'yy-mm-dd'
    });
	
	jQuery('.cf-ajax-button').click(function() {
		
		function onAjaxResponse($textinput, $select, tagval) {
			return function(data, textStatus, jqXHR) {
				response = jQuery.parseJSON(jqXHR.responseText);
				console.log(response);
				$textinput.val('');
				if(response.tagid) {
					$select.append($("<option></option>")
						.attr("value",response.tagid)
							.attr("selected", "selected")
								.text(tagval));
					$select.attr('size', $select.find('option').length);
				}
		    };
		}
		
		if(jQuery(this).siblings('input.ajax-add-tag').length) {
			var tagval = jQuery(this).siblings('input.ajax-add-tag').val();
			if(!tagval.length)
				return;
			jQuery.ajax({
				type: "POST",
				url: window.location.pathname,
				data: {
					'cf_action' : 'tag-add',
					'title' : tagval
				},
				success : onAjaxResponse(jQuery(this).siblings('input.ajax-add-tag'), jQuery(this).parent().parent().find('select.select-tags'), tagval)
			});
		}
		if(jQuery(this).siblings('input.ajax-add-category').length) {
			var catval = jQuery(this).siblings('input.ajax-add-category').val();
			if(!catval.length)
				return;
			jQuery.ajax({
				type: "POST",
				url: window.location.pathname,
				data: {
					'cf_action' : 'category-add',
					'title' : catval
				},
				success : onAjaxResponse(jQuery(this).siblings('input.ajax-add-cat'), jQuery(this).parent().parent().find('select.select-categories'), catval)
			});
		
		}
		
	})
	
	jQuery('#postSearchForm').submit(function(e){
		e.preventDefault();
		jQuery(this).find('input[type=text]').each(function(){
			if(jQuery(this).val() == jQuery(this)[0].defaultValue) {
				jQuery(this).val("");
			}
		});
		jQuery(this).unbind("submit").submit();
	})
	
	jQuery('.upload_image_button').click(function(){
		var send_attachment_bkp = wp.media.editor.send.attachment;
		var button = jQuery(this);
		wp.media.editor.send.attachment = function(props, attachment) {
			jQuery('#img'+jQuery(button).prev().attr('id')).attr('src', attachment.url);
			jQuery(button).prev().val(attachment.id);
			wp.media.editor.send.attachment = send_attachment_bkp;
		}
		wp.media.editor.open(button);
		return false;    
	});
	
	jQuery('.fakeInputButton').click(function(){
		jQuery(this).siblings('.hiddenfileinput').click();
	});
	
	jQuery('.hiddenfileinput').change(function(e){
		var fullPath = jQuery(this).val();
		if (fullPath) {
			var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
			var filename = fullPath.substring(startIndex);
			if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
				filename = filename.substring(1);
			}
			jQuery(this).siblings('.uploaded-file-label').html(filename);
		}
	});
	
	jQuery('.remove_image_button').click(function(){
		var button = jQuery(this);			
		var src = jQuery('#img'+jQuery(button).prev().prev().attr('id')).attr('data-src');
		jQuery('#img'+jQuery(button).prev().prev().attr('id')).attr('src', src);
		jQuery('#'+jQuery(button).prev().prev().attr('id')).val('0');
		return false;
	});

	//jQuery('#topics-treeview').bonsai();
	jQuery(".treeview-red").treeview({
	    animated: "fast",
	    collapsed: true,
	    //unique: true,
//	    persist: "cookie",
	    toggle: function() {
	      //window.console && console.log("%o was toggled", this);
	    }
	  });
	
	//editMode = false;
	
	$curPost = jQuery('.post-highlighted').closest('li.expandable');
	while($curPost.length) {
		$curPost.children('.hitarea.expandable-hitarea').click();
		$curPost = $curPost.closest('li.expandable');
	}
	
	jQuery('.CancelEditMode').click(function(e) {
		$postContainer = jQuery(this).closest('.postContainer');
		cancelPostEdit($postContainer);
		$thisPostBody = $postContainer.find('.postBody').removeClass('expanded').slideUp();
		jQuery(this).closest('.addReplyFormContainer:not(.hasparent)').slideUp();
		$thisPostControls = $postContainer.find('.postControls').removeClass('expanded').slideUp();
		//editMode = false;
	});
	
	jQuery('.CancelReply').click(function(e) {
		jQuery(this).closest('.addReplyFormContainer').slideUp();
	});
	
	jQuery(document).on("click",".CollapseThread",function() {
		$postContainer = jQuery(this).closest('.postContainer');
		$postContainer.find('.addReplyFormContainer,.postBody,.postControls,.postEditPanel').slideUp(function(){
			jQuery(this).closest('.postContainer').find('.postSubject').removeClass('CollapseThread');
		});
	});
	
	jQuery(".postContainer input[name='attachment'],.addReplyFormContainer input[name='attachment']").bind('change', function() {
			
			//alert(this.files[0].size);
		});
	
	jQuery('.postContainer').click(function(e) {
		var target = e.target;
		/*if(editMode) {
			return;
		}*/
		if(jQuery(target).is('.CollapseThread') || jQuery(target).closest('.CollapseThread').length) {
			return;
		}
		//editMode = true;
		
			$postContainer = jQuery(target).closest('.postContainer');
			$thisPostBody = $postContainer.find('.postBody');
			$thisPostControls = $postContainer.find('.postControls');
			jQuery('.postBody').not($thisPostBody).removeClass('expanded');
			jQuery('.postControls').not($thisPostControls).removeClass('expanded');
			
			if(!$thisPostBody.hasClass('expanded')) {
				$thisHdnIdPostControls = $postContainer.find('#hdnIdPost');
				_idPost = $thisHdnIdPostControls.val();
				
				if(PostsViewed.indexOf(_idPost) == -1) {
					PostsViewed.push(_idPost);
					var data = {
							cf_action: 'add_post_view',
							idPost: _idPost
						};
						
					if (!location.origin)
					   location.origin = location.protocol + "//" + location.host;
					
						jQuery.post(location.origin+'/custom-forum', data, function(response) {
							console.log(response);
							$viewscounter = $postContainer.find('.viewscounter')
							$viewscounter.html(parseInt($viewscounter.html())+1);
					});
				}
			}
			
			$thisPostBody.addClass('expanded');
			$thisPostControls.addClass('expanded');
			
			jQuery('.postBody.expanded,.postControls.expanded').slideDown(function(){
				jQuery(this).closest('.postContainer').find('.postSubject').addClass('CollapseThread');
			});
			//jQuery('.postBody:not(.expanded),.postControls:not(.expanded)').slideUp();

	});
	
	
	function cancelEdit() {
		editMode = false;
	}
	
	
	jQuery('.EditPostButton').click(function() {
		$postContainer = jQuery(this).closest('.postContainer');
		$postSubject = $postContainer.find('.postSubject');
		$postBody = $postContainer.find('.postBody');
		$postSubject.hide();
		$postContainer.find(jQuery('input[name=title]')).val($postSubject.html());
		
		$postContainer.find('.postEditControls').show();
		$postContainer.find('.postControls').slideUp();
		//editMode = true;
		
		_ckeditor = $postContainer.find(jQuery('._ckeditor'));
		
		if(!$postContainer.find(jQuery('.cke.cke_reset')).length) {
			CKEDITOR.replace( 
				_ckeditor.attr('id'),
				{
					toolbar: 'WordPressBasic',
					customConfig: '/wp-content/plugins/ckeditor-for-wordpress/ckeditor.config.js' 
				}
			);
		}
		else {
			$postContainer.find(jQuery('.cke.cke_reset')).slideDown();
		}
		
		$postContainer.find(jQuery('.non-editable')).hide();
		
	});
	
	function cancelPostEdit($postContainer) {
		$postSubject = $postContainer.find('.postSubject');
		$postBody = $postContainer.find('.postBody');
		$postContainer.find('.postEditControls').hide();
		$postSubject.show();
		$postContainer.find('.hidable').hide();
		$postContainer.find('.postControls').slideDown();
		$postContainer.find(jQuery('.cke.cke_reset')).hide();
		$postContainer.find(jQuery('.non-editable')).slideDown();
	}
	
	jQuery('.cancelPostEdit').click(function() {
		$postContainer = jQuery(this).closest('.postContainer');
		cancelPostEdit($postContainer);
	});
	
	jQuery(document).click(function(event) {/*
	    var target = event.target;
	    var isOpenMediaModal = false;
	    if(jQuery('div').is('.media-modal'))
	    {
	    	var mediaModalId = jQuery('.media-modal').parent().attr('id');	  
	    	isOpenMediaModal = jQuery(event.target).closest("#"+mediaModalId).length != 0;
		}
	    if(!(
	    		jQuery(target).parents().is('.addReplyButton') || 
	    		jQuery(target).is('.addReplyButton') || 
	    		jQuery(target).parents().is('.addReplyFormContainer') || 
	    		jQuery(target).is('.addReplyFormContainer')
	    		)) {
	    	if(!isOpenMediaModal){
		    	jQuery('.addReplyFormContainer').slideUp();
		    	jQuery('.addReplyButton').show();
	    	}
	    }*/
	});
	
	jQuery(document).on('click', '.posted_box1:not(.menuExpanded)', function(){
		jQuery(this).addClass('menuExpanding');
		setTimeout(function(){
			jQuery('.menuExpanding').addClass('menuExpanded').removeClass('menuExpanding');
			jQuery("#posted_menu").remove();
			jQuery('.menuExpanded').after(jQuery('#posted_menu'));
		}, 100)
	})
	
	jQuery('.EditPostButton').click(function(){
		$postContainer = jQuery(this).closest('.postContainer');
		
	});
	
	
	
	jQuery('.addReplyButton').click(function(){
		if(jQuery(this).hasClass('non-root')) {
			jQuery('.postEditControls').not(jQuery(this).closest('.postContainer')).slideUp();
		}
		/*else {
			if(editMode)
				return;
		}
		editMode = true;*/
		//jQuery('.addReplyButton').not(jQuery(this)).show();
		//jQuery(this).hide();
		//jQuery('.addReplyFormContainer').not(jQuery(this).siblings('.addReplyFormContainer')).hide();

		$ReplyFormContainer = jQuery(this).siblings('.addReplyFormContainer'); 
		$ReplyFormContainer.slideDown();
		if(!$ReplyFormContainer.find(jQuery('.cke.cke_reset')).length) {
			CKEDITOR.replace( 
					$ReplyFormContainer.find('._ckeditor_r').attr('id'),
					{
						toolbar: 'WordPressBasic',
						customConfig: '/wp-content/plugins/ckeditor-for-wordpress/ckeditor.config.js' 
					}
					);
		}
		
	});

	jQuery('.autosuggest').each(function(){
		jQuery(this).suggest(jQuery(this).data('url'), {multiple:true, multipleSep: ","});
	})
	
})
