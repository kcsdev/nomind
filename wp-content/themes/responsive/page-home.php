<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Indexim Template (custom)
 *
Template Name: Home Page template
 *
 * @file           page-home.php
 * @package        Responsive
 * @author         Arkady
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/page-activities.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */



get_header();
?>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/js/flexslider/flexslider.css" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/flexslider/jquery.flexslider.js"></script>

<?php
	
    
    get_template_part( 'template-parts/home/header-search' );

	get_template_part( 'template-parts/home/home-block1' );
    
    get_template_part( 'template-parts/home/home-block2' );
    
    get_template_part( 'template-parts/home/home-block3' );
    
    get_template_part( 'template-parts/home/home-block4' );
    
    get_template_part( 'template-parts/home/home-block5' );

	//get_sidebar( 'home' );
    
?>

</div><!-- end of #main content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>