
<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Footer Template
 *
 *
 * @file           footer.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.2
 * @filesource     wp-content/themes/responsive/footer.php
 * @link           http://codex.wordpress.org/Theme_Development#Footer_.28footer.php.29
 * @since          available since Release 1.0
 */

/*
 * Globalize Theme options
 */
global $responsive_options;
$responsive_options = responsive_get_options();
?>
<?php responsive_wrapper_bottom(); // after wrapper content hook ?>
</div><!-- end of #wrapper -->
<?php responsive_wrapper_end(); // after wrapper hook ?>


<div class="adsense1 adsense11"><?php echo do_shortcode("[pro_ad_display_adzone id=741]"); ?></div>

<div class="footer clearfix">
	<?php responsive_footer_top(); ?>
	
		<?php get_sidebar( 'footer' ); ?>
	
	<?php responsive_footer_bottom(); ?>
</div><!-- end #footer -->
<?php responsive_footer_after(); ?>
</div><!-- end of #container -->
<?php responsive_container_end(); // after container hook ?>
<?php wp_footer(); ?>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-smooth-scroll/jquery.smooth-scroll.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
<script type="text/javascript">
string0day = '';
string0hour = '';
string0minute = '';
string0second = ' 0 שניות';
string1day = ' 1 יום';
string1hour = ' 1 שעה';
string1minute = ' 1 דקה';
string1second = ' 1 שניות';
stringNdays = ' [N] ימים';
stringNhours = ' [N] שעות';
stringNminutes = ' [N] דקות';
stringNseconds = ' [N] שניות';
stringmonth = ["DECEMBER","JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"];
stringweekday = ["SUNDAY","MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY","SATURDAY","SUNDAY"];
</script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/easy-timer.js"></script>

<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#wpfepp-form-7-event_start_date-field').datepicker({
        dateFormat : 'dd-mm-yy'
    });

});

</script>




</body>
</html>