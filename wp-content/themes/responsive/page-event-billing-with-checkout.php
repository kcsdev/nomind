<?php
/** Template Name: Event billing with checkout page */
get_header();
?>

<?php get_template_part( 'loop-header' ); ?>

<?php if(isset($_GET['dealId'])): ?>
<div class="main_content clearfix">
    <div class="block1 block2 block8 clearfix">
        <div class="order-block-title big"><h2><?php the_title(); ?></h2></div>
        <div class="payment-right">
            <div class="payment-top">
                <!-- Event details template part -->
                <?php $EM_Event = em_get_event($_GET['dealId'],'post_id'); ?>
                <h3><?= $EM_Event->post_title; ?></h3>
                <p>
                    <b>תאריך:</b>
                    <?=date('d.m.Y', $EM_Event->start)?>  - <?=date('d.m.Y', $EM_Event->end)?>
                </p>
                <p>&nbsp;</p>
                <p><b>פרטים:</b></p>
                <?= $EM_Event->post_content; ?>
                <p></p>
                <?php get_template_part("template-parts/event-billing/billing-checkout"); ?>
            </div>
        </div>
        <div class="payment-support m-l1">
            <div class="payment-support-pic"><img src="http://nomind.c14.co.il/wp-content/themes/responsive/img/payment-support.jpg" alt=""></div>
            <p class="payment-support-a">טלפון: 6244115 - 03 </p>
        </div>
        <div class="clear"></div>

    </div>

    <?php get_template_part( 'template-parts/event-billing/billing-info-form' ); ?>
</div>
<?php endif; ?>

<?php get_footer(); ?>