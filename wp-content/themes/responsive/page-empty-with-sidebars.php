<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * New Page With Sidbars Template
 * 
 * Template Name: New Page With Sidbars
 *
 */

get_header(); ?>



<?php get_template_part( 'loop-header' ); ?>

    <div class="main_content clearfix">

	<?php if( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			<?php responsive_entry_before(); ?>
            
			<?php get_template_part( 'template-parts/empty-page/empty-page-with-sidebars' ); ?>
            
			<?php responsive_entry_after(); ?>

			<?php responsive_comments_before(); ?>
			<?php //comments_template( '', true ); ?>
			<?php responsive_comments_after(); ?>

		<?php
		endwhile;

		get_template_part( 'loop-nav' );

	else :

		get_template_part( 'loop-no-posts' );

	endif;
	?>

    </div><!-- end of #content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
