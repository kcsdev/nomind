<?php
/**
 * Plugin Name: Writers_Widget
 * Description: Show writers
 * Version: 0.1
 * Author: Erez Baumann
 * Author Skype: guver81
 */


add_action( 'widgets_init', 'writers_widget' );


function writers_widget() {
	register_widget( 'Writers_Widget' );
}

class Writers_Widget extends WP_Widget {

	function Writers_Widget() {
		$widget_ops = array( 'classname' => 'example', 'description' => __('A widget that displays writers ', 'example') );		
		$this->WP_Widget( 'writers-widget', __('Writers Widget', 'example'), $widget_ops );
	}
	
	function widget( $args, $instance ) {
		/**
		 * Filter the widget title.
		 *
		 * @since 2.6.0
		 *
		 * @param string $title    The widget title. Default 'Pages'.
		 * @param array  $instance An array of the widget's settings.
		 * @param mixed  $id_base  The widget ID.
		 */
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Writers' ) : $instance['title'], $instance, $this->id_base );

		$sortby = empty( $instance['sortby'] ) ? '' : $instance['sortby'];
		$exclude = empty( $instance['exclude'] ) ? '' : $instance['exclude'];
        $limit = empty( $instance['limit'] ) ? 12 : $instance['limit'];

		if ( $sortby == 'menu_order' )
			$sortby = 'menu_order, post_title';

        $writers = get_posts( array ( 'post_type' => 'writers',
                                      'exclude'   => $exclude,
                                      'sort_order'=> $sortby,
                                      'number'    => $limit ));
        foreach ( $writers as $w ) {
            
            //$writers->the_post();

            $out .= '<div class="img_box3"><a href="#"><img src="'.wp_get_attachment_url(get_post_thumbnail_id($w->ID)).'" width="75" height="68" alt=""></a>';
            $out .= '<h5 class="hdclass">'.$w->post_title.'</h5></div>';
        }
		
        
        echo $args['before_widget'];      
        echo $args['before_title'] . $title . $args['after_title']; 
		echo $out; 
		echo $args['after_widget'];
		
	}

	//Update the widget 
	 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		if ( in_array( $new_instance['sortby'], array( 'post_title', 'menu_order', 'ID' ) ) ) {
			$instance['sortby'] = $new_instance['sortby'];
		} else {
			$instance['sortby'] = 'menu_order';
		}

		$instance['exclude'] = strip_tags( $new_instance['exclude'] );
        $instance['limit'] = intval( $new_instance['limit'] );

		return $instance;
	}

	public function form( $instance ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'sortby' => 'post_title', 'title' => '', 'exclude' => '', 'limit' => 12) );
		$title = esc_attr( $instance['title'] );
		$exclude = esc_attr( $instance['exclude'] );
        $limit = esc_attr( $instance['limit'] );
	?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
		<p>
			<label for="<?php echo $this->get_field_id('sortby'); ?>"><?php _e( 'Sort by:' ); ?></label>
			<select name="<?php echo $this->get_field_name('sortby'); ?>" id="<?php echo $this->get_field_id('sortby'); ?>" class="widefat">
				<option value="post_title"<?php selected( $instance['sortby'], 'post_title' ); ?>><?php _e('Page title'); ?></option>
				<option value="menu_order"<?php selected( $instance['sortby'], 'menu_order' ); ?>><?php _e('Page order'); ?></option>
				<option value="ID"<?php selected( $instance['sortby'], 'ID' ); ?>><?php _e( 'Page ID' ); ?></option>
			</select>
		</p>
        <p><label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('Limit:'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo $limit; ?>" /></p>
		<p>
			<label for="<?php echo $this->get_field_id('exclude'); ?>"><?php _e( 'Exclude:' ); ?></label> <input type="text" value="<?php echo $exclude; ?>" name="<?php echo $this->get_field_name('exclude'); ?>" id="<?php echo $this->get_field_id('exclude'); ?>" class="widefat" />
			<br />
			<small><?php _e( 'Page IDs, separated by commas.' ); ?></small>
		</p>
<?php
	}
}

?>