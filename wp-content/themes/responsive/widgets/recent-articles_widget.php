<?php
/**
 * Plugin Name: Recent_Posts_Widget
 * Description: Show recent posts from selected category
 * Version: 0.1
 * Author: Erez Baumann
 * Author Skype: guver81
 */


add_action( 'widgets_init', 'recent_articles_widget' );


function recent_articles_widget() {
	register_widget( 'Recent_Articles_Widget' );
}

class Recent_Articles_Widget extends WP_Widget {

	function Recent_Articles_Widget() {
		$widget_ops = array( 'classname' => 'example', 'description' => __('Show recent posts from selected category ', 'example') );		
		//$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'example-widget' );		
		$this->WP_Widget( 'recent-articles-widget', __('Recent Articles Widget', 'example'), $widget_ops );
	}
	
	function widget( $args, $instance ) {
		/**
		 * Filter the widget title.
		 *
		 * @since 2.6.0
		 *
		 * @param string $title    The widget title. Default 'Pages'.
		 * @param array  $instance An array of the widget's settings.
		 * @param mixed  $id_base  The widget ID.
		**/
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Articles' ) : $instance['title'], $instance, $this->id_base );

		$sortby = empty( $instance['sortby'] ) ? '' : $instance['sortby'];
		$exclude = empty( $instance['exclude'] ) ? '' : $instance['exclude'];
        $limit = empty( $instance['limit'] ) ? 12 : $instance['limit'];

		if ( $sortby == 'menu_order' )
			$sortby = 'menu_order, post_title';

		/**
		 * Filter the arguments for the Pages widget.
		 *
		 * @since 2.8.0
		 *
		 * @see wp_list_pages()
		 *
		 * @param array $args An array of arguments to retrieve the pages list.
		 */
		?>
        <div class="middle_column_box1">
            <div class="info_heading info_heading6  clearfix">
                <h4 class="hdclass">מאמרים</h4>
                <ul class="uli">
                    <li class="uli no-bdr"><a href="#">חדשים</a></li>
                    <li class="uli"><a href="#">נצפים ביותר</a></li>
                    <li class="uli"><a href="#">לכל האנדקסים</a></li>
                </ul>
            </div>
        
        <?php
        
        
        
        $posts = wp_get_recent_posts( array ( 'exclude' => $exclude,
                                              'sort_order' => $sortby,
                                              'numberposts' => $limit,
                                              'post_type' => 'articles', ));
        $i = 0;
        //die(var_dump($posts));
        foreach($posts as $p) {   ?>
            <div class="info_box1_part info_box1_part2 clearfix"> 
                <div class="img_box13"><img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($p->ID)); ?>" width="99" height="65" alt=""></div>
                <div class="img_info">
                    <h5 class="hdclass"><?php echo $p->post_title; ?></h5>
                    <ul>
                        <li><?php echo get_the_author_meta( 'nickname', $p->post_author ); ?></li>
                        <li class="no-bdr"><?php echo date('d.m.Y', strtotime($p->post_date)); ?></li>  
                    </ul>
                    <div class="text_box1 text_box2"><?php echo $p->post_excerpt; ?></div>
                </div>
            </div>
        
        <?php } ?>
        </div>
        
		<?php
			echo $args['after_widget'];
		
	}

	//Update the widget 
	 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		if ( in_array( $new_instance['sortby'], array( 'post_title', 'menu_order', 'ID' ) ) ) {
			$instance['sortby'] = $new_instance['sortby'];
		} else {
			$instance['sortby'] = 'menu_order';
		}

		$instance['exclude'] = strip_tags( $new_instance['exclude'] );
        $instance['limit'] = intval( $new_instance['limit'] );

		return $instance;
	}

	public function form( $instance ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'sortby' => 'post_title', 'title' => '', 'exclude' => '', 'limit' => 12) );
		$title = esc_attr( $instance['title'] );
		$exclude = esc_attr( $instance['exclude'] );
        $limit = esc_attr( $instance['limit'] );
	?>
		<!--<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>-->
		<p>
			<label for="<?php echo $this->get_field_id('sortby'); ?>"><?php _e( 'Sort by:' ); ?></label>
			<select name="<?php echo $this->get_field_name('sortby'); ?>" id="<?php echo $this->get_field_id('sortby'); ?>" class="widefat">
				<option value="post_title"<?php selected( $instance['sortby'], 'post_title' ); ?>><?php _e('Page title'); ?></option>
				<option value="menu_order"<?php selected( $instance['sortby'], 'menu_order' ); ?>><?php _e('Page order'); ?></option>
				<option value="ID"<?php selected( $instance['sortby'], 'ID' ); ?>><?php _e( 'Page ID' ); ?></option>
			</select>
		</p>
        <p><label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('Limit:'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo $limit; ?>" /></p>
		<p>
			<label for="<?php echo $this->get_field_id('exclude'); ?>"><?php _e( 'Exclude:' ); ?></label> <input type="text" value="<?php echo $exclude; ?>" name="<?php echo $this->get_field_name('exclude'); ?>" id="<?php echo $this->get_field_id('exclude'); ?>" class="widefat" />
			<br />
			<small><?php _e( 'Page IDs, separated by commas.' ); ?></small>
		</p>
<?php
	}
}

?>