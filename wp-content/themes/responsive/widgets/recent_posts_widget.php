<?php
/**
 * Plugin Name: Recent_Posts_Widget
 * Description: Show recent posts from selected category
 * Version: 0.1
 * Author: Erez Baumann
 * Author Skype: guver81
 */


add_action( 'widgets_init', 'recent_posts_widget' );


function recent_posts_widget() {
	register_widget( 'Recent_Posts_Widget' );
}

class Recent_Posts_Widget extends WP_Widget {

	function Recent_Posts_Widget() {
		$widget_ops = array( 'classname' => 'example', 'description' => __('Show recent posts from selected category ', 'example') );		
		//$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'example-widget' );		
		$this->WP_Widget( 'recent-posts-widget', __('Recent Posts Widget', 'example'), $widget_ops );
	}
	
	function widget( $args, $instance ) {
		/**
		 * Filter the widget title.
		 *
		 * @since 2.6.0
		 *
		 * @param string $title    The widget title. Default 'Pages'.
		 * @param array  $instance An array of the widget's settings.
		 * @param mixed  $id_base  The widget ID.
		**/
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Pages' ) : $instance['title'], $instance, $this->id_base );

		$sortby = empty( $instance['sortby'] ) ? '' : $instance['sortby'];
		$exclude = empty( $instance['exclude'] ) ? '' : $instance['exclude'];
        $limit = empty( $instance['limit'] ) ? 12 : $instance['limit'];

		if ( $sortby == 'menu_order' )
			$sortby = 'menu_order, post_title';

		/**
		 * Filter the arguments for the Pages widget.
		 *
		 * @since 2.8.0
		 *
		 * @see wp_list_pages()
		 *
		 * @param array $args An array of arguments to retrieve the pages list.
		 */
		$pages = get_pages( array ( 'exclude' => $exclude,
                                    'sort_order' => $sortby,
                                    'number' => $limit ));
        $i = 0;
        foreach($pages as $p) {
            if($i % 4 == 3) $out .= '<li class="uli no-bg">';
            else $out .= '<li class="uli">';             
            $out .= '<img src="'.wp_get_attachment_url(get_post_thumbnail_id($p->ID)).'" width="34" height="35" alt="">';
            $out .= '<h4 class="hdclass f-size11">'.$p->post_title.'</h4></li>';
            $i++;
        }
        if ( ! empty( $out ) ) {
			echo $args['before_widget'];
			if ( $title ) {
			    //echo $args['before_title'] . $title . $args['after_title'];
			}
        
		?>
		<ul class="uli">
			<?php   //custom
                    
                    echo $out; ?>
		</ul>
        <div class="message_box clearfix">
            <h2 class="hdclass">הצטרפו למועדון החברים של נומיינד</h2>
            <input class="input_text1 input_text2" type="text" name="" value="שם" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" >
            <input class="input_text1 input_text2 no-margin" type="text" name="" value="משפחה" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" >
            <input class="input_text1 input_text3" type="text" name="" value="דוא”ל" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" >
            <input class="input_btn1" type="submit" name="" value="שלח">
        </div> 
		<?php
			echo $args['after_widget'];
		}
	}

	//Update the widget 
	 
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		if ( in_array( $new_instance['sortby'], array( 'post_title', 'menu_order', 'ID' ) ) ) {
			$instance['sortby'] = $new_instance['sortby'];
		} else {
			$instance['sortby'] = 'menu_order';
		}

		$instance['exclude'] = strip_tags( $new_instance['exclude'] );
        $instance['limit'] = intval( $new_instance['limit'] );

		return $instance;
	}

	public function form( $instance ) {
		//Defaults
		$instance = wp_parse_args( (array) $instance, array( 'sortby' => 'post_title', 'title' => '', 'exclude' => '', 'limit' => 12) );
		$title = esc_attr( $instance['title'] );
		$exclude = esc_attr( $instance['exclude'] );
        $limit = esc_attr( $instance['limit'] );
	?>
		<!--<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>-->
		<p>
			<label for="<?php echo $this->get_field_id('sortby'); ?>"><?php _e( 'Sort by:' ); ?></label>
			<select name="<?php echo $this->get_field_name('sortby'); ?>" id="<?php echo $this->get_field_id('sortby'); ?>" class="widefat">
				<option value="post_title"<?php selected( $instance['sortby'], 'post_title' ); ?>><?php _e('Page title'); ?></option>
				<option value="menu_order"<?php selected( $instance['sortby'], 'menu_order' ); ?>><?php _e('Page order'); ?></option>
				<option value="ID"<?php selected( $instance['sortby'], 'ID' ); ?>><?php _e( 'Page ID' ); ?></option>
			</select>
		</p>
        <p><label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('Limit:'); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo $limit; ?>" /></p>
		<p>
			<label for="<?php echo $this->get_field_id('exclude'); ?>"><?php _e( 'Exclude:' ); ?></label> <input type="text" value="<?php echo $exclude; ?>" name="<?php echo $this->get_field_name('exclude'); ?>" id="<?php echo $this->get_field_id('exclude'); ?>" class="widefat" />
			<br />
			<small><?php _e( 'Page IDs, separated by commas.' ); ?></small>
		</p>
<?php
	}
}

?>