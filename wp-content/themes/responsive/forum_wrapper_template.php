<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}


/**
 *
Template Name: forum wrapper
 *
 */

get_header(); ?>

<div id="content" class="<?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">

	<?php if( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'loop-header' ); ?>

			<?php responsive_entry_before(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php responsive_entry_top(); ?>
				
				<div class="post-entry">
					<div class="block1 block2 clearfix">
				
				<?php //get_template_part( 'adscolumn', 'right' ); ?>
				
                <?php include get_stylesheet_directory() .'/template-parts/blogim/blogim-sidebar.php' ; ?> 
                
				<?php the_content(); ?>
				
				</div>
				<?php get_template_part( 'adscolumn', 'left' ); ?>
                    	</div>
                	</div>
             	</div>
             	<div class="block1 block5">
             		<h2 class="heading6 m-b1">המומחים של נומיינד</h2>
               		<ul class="photo_galary clearfix">
                   		<li class="first"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic2.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   		<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic3.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   		<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic4.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   		<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic5.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   		<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic6.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   		<li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic7.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   		<li class=" last"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic8.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   	</ul>
               <!--<div class="adsense1"><img src="<?php bloginfo('template_directory'); ?>/img/ad1.png" width="964" height="91" alt=""></div>-->
               
	            </div>
					
				</div><!-- end of .post-entry -->
				
				<?php get_template_part( 'post-data' ); ?>

				<?php responsive_entry_bottom(); ?>
			</div><!-- end of #post-<?php the_ID(); ?> -->
			<?php responsive_entry_after(); ?>

		<?php
		endwhile;

		get_template_part( 'loop-nav' );

	else :

		get_template_part( 'loop-no-posts' );

	endif;
	?>

</div><!-- end of #content -->

<?php get_footer(); ?>
