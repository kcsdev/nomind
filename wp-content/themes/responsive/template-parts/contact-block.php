	<div class="block1 block2 block8 clearfix">
			<div class="order-block-title big"><h2><?the_title();?></h2></div>
			<div class="payment-right">
				<div class="payment-top">
					<h3><?the_title();?></h3>
					<p><?the_content();?></p>
					<?php echo do_shortcode( '[contact-form-7 id="415" title="צור קשר"]' ); ?>
				</div>
			</div>
			<div class="payment-support m-l1">
				<div class="payment-support-pic"><img src="<?php echo get_template_directory_uri(); ?>/img/payment-support.jpg" alt=""></div>
				<p class="payment-support-a">טלפון: 6244115 - 03 </p>
			</div>
			<div class="clear"></div>
			
	</div>