   <div class="block1 block2 clearfix">
                
               <?php include get_stylesheet_directory() .'/template-parts/activities/activities-right-sidebar.php' ; ?>

                <div class="column5 no-bdr">
                    <div class="column5_block5 no-bdr">
                    <div class="peiluyot-title">
                    	<h2><?
                        global $wp_query;
                        
                        if(is_tax(EM_TAXONOMY_CATEGORY) )
                        {
                           $term_slug = get_query_var( 'term' );
                           
                            $taxonomyName = get_query_var( 'taxonomy' );
                            $current_term = get_term_by( 'slug', $term_slug, $taxonomyName );
                            $current_term_id =    $current_term->term_id;
                            
                        }
                       
                        ?>
                        <a style="color: #ff7300;" href="<?= get_permalink(383);?>">פעילויות</a> : <i style="color: blue;"><?= $current_term->name; ?></i></h2>
                        
                        <ul class="f_size1">
                        	<li><a href="?date=this-week">רק השבוע</a></li>
                            <li class="nobrdr"><a href="?type=recurring">פעילויות שבועיות</a></li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="peiluyot-top-link-bg">
                    	<div class="peiluyot-top-link">
                        	<ul>
                                <? 
                                
                                 if(isset($_GET['type']) && $_GET['type'] == 'recurring')
                                    $type = 'event-recurring';
                                 else
                                    $type = EM_POST_TYPE_EVENT;   
                                
                                 if(isset($_GET['date']) && $_GET['date'] == 'this-week')   
                                    $this_week = true;
                                 else
                                    $this_week = false;   
                                
                                 if(isset($_GET['category']))
                                 {
                                    $parent_cat =  get_term_by('slug', $_GET['category'], EM_TAXONOMY_CATEGORY);
                                    
                                    $current_term_id =    $parent_cat->term_id;
   
                                        $args = array(
                                            'orderby'           => 'name', 
                                            'order'             => 'ASC',
                                            'hide_empty'        => false,
                                            'hierarchical'      => true,
                                            'parent'          => $parent_cat->term_id
                                        ); 
                                    
                                  }
                                 
                                  else
                                  {
                                        $args = array(
                                            'orderby'           => 'name', 
                                            'order'             => 'ASC',
                                            'hide_empty'        => false,
                                            'parent'          => 0
                                        ); 
                                  }
                                     $taxonomies = array( 
                                        EM_TAXONOMY_CATEGORY
                                    );
                                  $terms = get_terms($taxonomies, $args );
                               
                                ?>
                                <?foreach($terms as $category) : ?>
                                    <li><a href="<?=get_term_link($category->slug, EM_TAXONOMY_CATEGORY);?>"><?= $category->name;?></a></li>
                                <? endforeach; ?>
                            </ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <style>
                    .img_box5_out2 a
                    {
                        margin-left: 0;
                    }
                    </style>
                        
            <?php
            
             $search_in_progress = false;
             
             if(isset($_GET['search']))
                $search_in_progress = true;
             
             $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
               
             $posts_per_page = 4;
                
                if($search_in_progress)
                {
                    
                    $args = array(
                                'post_type' => array('event-recurring', EM_POST_TYPE_EVENT ),
                                'posts_per_page' => $posts_per_page,
                                'paged' =>$paged,
                                'orderby' => 'date',
                                'order' => 'DESC');
                                
                                                                
                    $args['meta_query'] = array();
                    
                    //for search
                    #category
                    if(isset($_GET['category']) && $_GET['category'] !='')
                        $searched_category = $_GET['category'];
                    
                    if(isset($_GET['subcategory']) && $_GET['subcategory'] !='')
                        $searched_category = $_GET['subcategory'];
                        
                     if(isset($searched_category))
                     {
                        $searched_term = get_term_by( 'slug', $searched_category, EM_TAXONOMY_CATEGORY );
                        
                        if(isset($searched_term))
                        {
                            $args['tax_query'] = array(
                                    array(
                                        'taxonomy' => EM_TAXONOMY_CATEGORY,
                                        'terms' => array( $searched_term->term_id )
                                    )
                                );
                         }
                     }
                     
                     #date
                     if((isset($_GET['dateFrom']) && $_GET['dateFrom'] !='') && (isset($_GET['dateTo']) && $_GET['dateTo'] !=''))
                     {
                        $args['meta_query'][] =
                                array(
                                    'key' => '_event_start_date',
                                    'value' => array(date('Ymd', strtotime($_GET['dateFrom'])),date('Ymd', strtotime($_GET['dateTo']))),
                                    'compare' => 'BETWEEN',
                                    'type' => 'DATE'
                                )
                             ;   
                     }
                     elseif(isset($_GET['dateFrom']) && $_GET['dateFrom'] !='') 
                     {
                        $args['meta_query'][] =
                                array(
                                    'key' => '_event_start_date',
                                    'value' => date('Ymd', strtotime($_GET['dateFrom'])),
                                    'compare' => '>=',
                                    'type' => 'DATE'
                               
                             );   
                     }
                     elseif(isset($_GET['dateTo']) && $_GET['dateTo'] !='') 
                     {
                        $args['meta_query'][] =
                                array(
                                    'key' => '_event_start_date',
                                    'value' => date('Ymd', strtotime($_GET['dateTo'])),
                                    'compare' => '<',
                                    'type' => 'DATE'
                                
                             );   
                     }
                     
                     #area
                     if(isset($_GET['area']) && $_GET['area'] !='') 
                     {
                        $args['meta_query'][] =
                                array(
                                    'key' => 'event_area',
                                    'value' => $_GET['area'],
                                    'compare' => '='
                             );   
                     }
                     
                     #free_text
                     if(isset($_GET['text']) && $_GET['text'] !='') 
                        $args['s'] = trim($_GET['text']);
                                                
                         
                }
                else
                {
                        if(isset($current_term_id))
                        {
                            $args = array(
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => EM_TAXONOMY_CATEGORY,
                                        'terms' => array( $current_term_id )
                                    )
                                ),
                                'post_type' => $type,
                                'posts_per_page' => $posts_per_page,
                                'paged' =>$paged,
                                'orderby' => 'date',
                                'order' => 'DESC'
                                
                            );
                        }
                        else
                        {
                             $args = array(
                                'post_type' => $type,
                                'posts_per_page' => $posts_per_page,
                                'paged' =>$paged,
                                'orderby' => 'date',
                                'order' => 'DESC'
                            );
                        }
                        
                        if($this_week)
                        {   
                              $ts = strtotime("now");
                              $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
                              $start_date = date('Ymd', $start);
                              $end_date = date('Ymd', strtotime('next saturday', $start));
                            
                            $args['meta_query'] = array(
                                array(
                                    'key' => '_event_start_date',
                                    'value' => array($start_date,$end_date),
                                    'compare' => 'BETWEEN',
                                    'type' => 'DATE'
                                )
                             );   
                        }
                }
                
                global $EM_Event;
                $wp_query = new WP_Query($args);
                $max_page = $wp_query->max_num_pages;
                
                if ($wp_query->have_posts()) :
                    while ($wp_query->have_posts()): $wp_query->the_post(); 
                    
                    if (has_post_thumbnail()) {
                        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb');
                        $index_img =  $image_url[0];
                    }  
                    else
                    {
                        $index_img = get_template_directory_uri().'img/info9_pic3.jpg';
                    }
                        $EM_Event = em_get_event(get_the_ID() ,'post_id');
                        //var_dump($EM_Event); die();
                    ?>
                        
                    <div class="img_info_box2 dbl-brdr clearfix">
                        <div class="img_box5_out1 img_box5_out2">
                            <div class="img_box9 img_box20"><a href="<?the_permalink();?>" style="padding: 0;border: 0;margin: 0;"><img src="<?= $index_img; ?>" width="107" height="62" alt=""></a></div>
                            
                            	<?php wpfp_link() ?>
                            
                            <span><?=$EM_Event->output('#_LOCATION');?></span>
                        </div>
                        <div class="peiluyot-pic-dtl">
                            <h2><a href="<?the_permalink();?>"><?= the_title();?></a></h2>
                            <h3><?=date('d.m.Y', $EM_Event->start)?>  - <?=date('d.m.Y', $EM_Event->end)?></h3>
                            <p></p>
                            <?=$EM_Event->output('#_EXCERPT');?>
                            <ul class="peiluyot-cont">
                                <li class="msg"><a href="#">צור קשר</a></li>
                                <li class="nobrdr call"><a id="show-phone-<?=get_the_ID();?>" href="#" onclick="showPhone('<?=get_the_ID();?>'); return false;">הצג טלפון</a></li>
                            </ul>
                            <? if(get_field('deal_number') && get_field('deal_number') != 0) : ?>
                                <a href="<?= get_permalink(get_field('deal_number'));?>"><div class="peiluyot-cont-tag">קופון</div></a>
                            <? endif; ?>    
                            <div class="clear"></div>
                            <div class="peiluyot-cont-btm"><?php the_terms( get_the_ID(), EM_TAXONOMY_TAG, '<strong>מילות מפתח:</strong>', ' , ' ); ?></div>
                        </div>
                    </div>
                    
                      <?php    
                         endwhile;
                      ?>
                                   
                        <script>
                        function showPhone(eventID)
                        {
                             $.post( "", { event_id: eventID  }, function( data ) {
                                console.log( data ); 
                                $('a.#show-phone-'+eventID).html(data);
                                                                
                              });
                        }
                        
                        </script>
                       
                    </div>
                    <div class="more_view_list clearfix">
                                               
                            
                            <?php 
                            
                            $big = 999999999;
                            $args = array(
                                	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                	'format'             => '/פעילויות/page/%#%',
                                	'total'              => $max_page,
                                	'current'            => $paged,
                                	'show_all'           => False,
                                	'end_size'           => 1,
                                	'mid_size'           => 2,
                                	'prev_next'          => False,
                                	'prev_text'          => 'קודם',
                                	'next_text'          => 'הבא',
                                	'type'               => 'array',
                                	'add_args'           => True,
                                	'add_fragment'       => '',
                                	'before_page_number' => '',
                                	'after_page_number'  => ''
                                ); ?>
                            
                             
                            
                            <? $links = paginate_links( $args );
                             //echo get_next_posts_link( 'אבה', $the_query->max_num_pages ); 
                            
                            //echo get_previous_posts_link( 'Newer Entries' );
                            
                             ?>
                             
                             <ul class="uli">
                                <? foreach($links as $link) : ?>
                                    <li class="uli">
                                    <? echo $link; ?>
                                    </li>
                                <? endforeach; ?>
                             </ul>
                             
                              <?= get_next_posts_link( 'הבא', $the_query->max_num_pages ); ?>
                              
                              <?= get_previous_posts_link( 'קודם' ); ?>
                        </div>
                         
                        
                         <? else : ?>
                                <div class="post_block">
                                	<div class="post_heading clearfix">
                                    	<h2 class="hdclass">לא נמצאו תוצאות</h2>
                                    </div>
                                </div>
                                </div>
                          <?    
                            endif;    
 
                            wp_reset_query();
                          ?>
                    
                </div>
               
               
             <?php include get_stylesheet_directory() .'/template-parts/activities/activities-left-sidebar.php' ; ?>
                 
             </div>
             <div class="block1 block5">
                <h2 class="heading6 m-b1">המומחים של נומיינד</h2>
               <ul class="photo_galary clearfix">
                   <li class="first"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic2.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic3.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic4.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic5.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic6.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic7.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li class=" last"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic8.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
               </ul>
              
               
            </div>
        