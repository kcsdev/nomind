

                  <?php if (has_post_thumbnail()) {
                        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb');
                        $index_img =  $image_url[0];
                        }  
                        else
                        {
                            $index_img = get_template_directory_uri().'img/info9_pic3.jpg';
                        }
                            $EM_Event = em_get_event(get_the_ID() ,'post_id');
                            
                    ?>
           <script>
            function showPhone(eventID)
            {
                 $.post( "", { event_id: eventID  }, function( data ) {
                    console.log( data ); 
                    $('a.#show-phone-'+eventID).html(data);
                                                    
                  });
            }
            
            function sendMessage()
            {
                 $('#send-to-event-organizer').modal('show');
            }
            
               
            
            </script>
  
  <div class="block1 block2 clearfix">
               <?php include get_stylesheet_directory() .'/template-parts/activities/activities-right-sidebar.php' ; ?>
               <?php include get_stylesheet_directory() .'/template-parts/activities/activities-left-sidebar.php' ; ?>

               <div class="column5 no-bdr">
                    <div class="column5_block5 no-bdr">
                    <div class="peiluyot-title">
                    	<h2>פעילויות</h2>
                        <ul class="f_size1">
                        	<li><a href="#">רק השבוע</a></li>
                            <li class="nobrdr"><a href="#">פעילויות שבועיות</a></li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                    <div class="peiluyot-top-link-bg m-b1">
                    	<div class="peiluyot-top-link m-b1"	>

                        	<ul>
                            	<li><a href="#">תחזית שנתית ל-2012</a></li>
                                <li><a href="#">אסטרולוגיה</a></li>
                                <li><a href="#">מודעות והעצמה</a></li>
                                <li><a href="#">קורס אסטרולוגיה</a></li>
                                <li class="nobrdr"><a href="#">גוף ונפש</a></li>
                                <li><a href="#">אייג'</a></li>
                                <li><a href="#">בעד, נגד</a></li>
                                <li><a href="#">קבלה טורים ומדורים</a></li>
                                <li><a href="#">מחשבוני נומרולוגיה</a></li>
                                <li><a href="#">אסטרולוגיה סינית</a></li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <div class="column5_block1 no-margin clearfix">
                            <h4 class="hdclass"><?= the_title();?></h4>
                            <div class="small_text1"><?=date('d.m.Y', $EM_Event->start)?>  - <?=date('d.m.Y', $EM_Event->end)?></div>
                            <div class="text_box5 no-margin"><?=$EM_Event->output('#_EXCERPT');?></div>
                        </div>
                    </div>
                     <div class="social_block2 m-b3 clearfix">
                        	<ul class="uli social_link1">
                            	<li class="uli"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon1.jpg" width="85" height="20" alt=""></a></li>
                                <li class="uli"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon2.jpg" width="90" height="20" alt=""></a></li>
                                <li class="uli"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon3.jpg" width="16" height="20" alt=""></a>&nbsp; &nbsp;שלח לחבר </li>
                            </ul>
                        	<ul class="uli social_link2">
                            	<li class="uli"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon4.jpg" width="16" height="16" alt=""></a></li>
                                <li class="uli"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon5.jpg" width="16" height="16" alt=""></a></li>
                                <li class="uli"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon6.jpg" width="16" height="16" alt=""></a></li>
                                <li class="uli"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon7.jpg" width="16" height="16" alt=""></a></li>
                                <li class="uli"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon8.jpg" width="18" height="17" alt=""></a></li>
                            </ul>
                        </div>
                    <div class="img_info_block2 clearfix">
                            	<div class="img_info_block2_right">
                                	<div class="text_box7">
                                    	<?=$EM_Event->output('#_EXCERPT');?>
                                    </div>
                                   
                                </div>
                                <div class="img_cont1">
                                	<div class="img_cont_pic1">
                                    	<img src="<?= $index_img; ?>" width="263" height="171" alt="">
                                    </div>
                                    <div class="img_cont_pic1_pager_block">
                                    <!--	<div class="pager_box clearfix">
                                       	<a class="prev3" href="#"></a>
                                            <ul class="pager1">
                                            	<li><a href="#">10</a></li>
                                                <li><a href="#">9</a></li>
                                                <li><a href="#">8</a></li>
                                                <li><a href="#">7</a></li>
                                                <li><a href="#">6</a></li>
                                                <li><a href="#">5</a></li>
                                                <li><a href="#">4</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a class="active" href="#">1</a></li>
                                            </ul>
                                            <a class="next3" href="#"></a>
                                        </div> -->
                                        <a class="search_icon1" href="#"></a>
                                    </div>
                                    <div class="social_link clearfix">
                                        <ul class="uli">
                                        	
                                            <?//check if user is logged in
                                            if(is_user_logged_in()) : ?>                                            
                                                <li><img src="<?php echo get_template_directory_uri(); ?>/img/s_link2.png" width="20" height="20" alt=""><a href="#" onclick="sendMessage(); return false;" >שלח הודעה</a></li>
                                            <?endif;?>
                                            
                                            <li><img src="<?php echo get_template_directory_uri(); ?>/img/s_link1.png" width="20" height="20" alt=""><a id="show-phone-<?=get_the_ID();?>" href="#" onclick="showPhone('<?=get_the_ID();?>'); return false;">הצג טלפון</a></li>
                                            <li><img src="<?php echo get_template_directory_uri(); ?>/img/s_link3.png" width="20" height="20" alt=""><a target="_blank" href="<?=$EM_Event->output('#_CONTACTURL');?>">האתר שלי</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                    <div class="text_box7">
                      <?= the_content(); ?>
                    </div>
                    
                    <div class="video_block2"> 
                    <!-- get the latest video -->
                    <?
                     $args = array(
                                 
                                'post_type' => 'video',
                                'posts_per_page' => 1,
                                'orderby' => 'date',
                                'order' => 'DESC'
                         
                            );
                                    $search_posts = new WP_Query( $args );
                                    
                                    $video_post = $search_posts->get_posts();
                                    
                                    $videoid = getVideoId($video_post[0]->ID);
                    ?>
                            	<div class="video_heading1 clearfix">
                                	<img src="<?php echo get_template_directory_uri(); ?>/img/box_pic3.png" width="42" height="42" alt="">
                                    
                                	<h4 class="hdclass">NoMind Tv</h4>
                                    <a href="/nomind-tv/">לספריה המלאה</a>	
                                </div>
                                <div class="video2_outer">
                                	<div class="video_box2">
                                    <iframe width="413" height="232" src="https://www.youtube.com/embed/<?=$videoid;?>" frameborder="0" allowfullscreen></iframe>
                                   </div>
                                    <div class="video_heading_bottom">
                                        <h4 class="hdclass"><?=$video_post[0]->post_title;?></h4>
                                    </div>
                                </div>
                     </div>
                    <div class="column5_block6">
                        <div class="heading18 lima_bdr clearfix">
                            <h4 class="hdclass indigo">מידע על הפעילויות</h4>
                        </div>
                        <ul class="list_box2">
                            <li><i> איש קשר:</i> אבי לוי </li>
                            <li><i>כתובת:</i> מנחם בגין 5 </li>
                            <li><i>עיר:</i> יהוד </li>
                            <li><i>איזור:</i> המרכז </li>
                            <li><i>רמה:</i> בינוני </li>
                            <li><i>שעות:</i> 14:00 - 8:00</li>
                            <li><i>מחיר:</i> 1500 </li>
                            <li><i>מחיר קופון הנחה:</i> 1100 </li>
                        </ul>
                        <a class="click8" href="#">הוסף למזוודה שלי</a>
                        </br>
                             <div >
                                 <?php $field_key = "field_56530c90d6148"; 
                                    $post_id = (get_the_ID()); 
                                    $fields = get_field_object($field_key);
                                 if(isset($fields)){ ?>

                                <div class="heading18 lima_bdr clearfix">
                                      <h4 class="prod_head">הזמנות</h4>
                                </div>
                                 <form  class = "add_to_cart_active"  method="post"><?php
                                                 foreach($fields['value'] as $field):  ?>   
                                                <label class = "prod-label">
                                                    <input value = "<?php echo ($field->ID) ?>" type="checkbox" name="product<?php echo ($field->ID) ?>" class = "checkbox-product" />
                                                        <?php  echo $field->post_title; ?>
                                                        <?php echo get_the_post_thumbnail($field->ID, 'thumbnail' );  ?>  </br>                                                                  <span>מחיר רגיל:</span> <?php echo(get_post_meta($field->ID,'_regular_price',true));?>   </br>                        
                                                        <span>מחיר אחרי הנחה:</span> <?php echo(get_post_meta($field->ID,'_sale_price',true));?>
                                                </label> 
                                        <?php endforeach; ?>
                                         <input type="submit" value="הוסף לעגלה" id="act-add-to-cart">
                                        </form>
                                    <br>
                                 <?php } ?>
                            </div>  
                    </div>  
                
                    <div class="column5_block2 clearfix" style = "clear:both;" >
                                <ul class="uli">
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon9.jpg" width="20" height="23" alt=""><a id="show-phone-<?=get_the_ID();?>" href="#" onclick="showPhone('<?=get_the_ID();?>'); return false;">הצג טלפון</a></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon10.jpg" width="20" height="23" alt=""><a href="#" onclick="sendMessage(); return false;" >צור קשר</a></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon11.jpg" width="22" height="23" alt=""><a target="_blank" href="<?=$EM_Event->output('#_CONTACTURL');?>">לאתר</a></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon12.jpg" width="22" height="23" alt=""><a href="#">המאמרים שלי</a></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon13.jpg" width="27" height="23" alt=""><a href="#">הבלוגים שלי</a></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon14.jpg" width="19" height="23" alt=""><a href="#">הפעילויות שלי</a></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon15.jpg" width="21" height="23" alt=""><a href="#">הוידאו שלי</a></li>
                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon16.jpg" width="21" height="23" alt=""><a href="#">הורוסקופים שלי</a></li>
                                </ul>
                            </div>
                    <div class="click_info1">
                        <a href="#">רוצה לקבל את הניוזלטר שלנו ולהיות מעודכן לפני כולם!  <i>לחץ כאן</i></a>
                    </div>
                    <div class="social_block3 clearfix">
                        <div class="column5_social_link clearfix">
                            <ul class="uli fl-rt">
                                <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon19.jpg" width="16" height="18" alt="">&nbsp; הדפסה</a></li>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon20.jpg" width="16" height="18" alt="">&nbsp; כתוב לעורך</a></li>
                                <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon21.jpg" width="16" height="18" alt="">&nbsp; שלח לחבר</a></li>
                            </ul>
                            <ul class="uli fl-lt">
                                <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon22.jpg" width="16" height="18" alt=""></a></li>
                                <li class="no-margin"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/social_icon23.jpg" width="16" height="18" alt=""></a></li>
                                <li>&nbsp;&nbsp;&nbsp;שתף</li>	
                            </ul>
                        </div>
                    </div>
                    <div class="column5_block3 no-bdr clearfix">
                    	<div class="heading18 lima_bdr">
                        	<h4 class="hdclass indigo">פעילויות נוספות שעשויות לעניין אותך</h4>
                        </div>
                    	<div class="img_info_box1">
                        	<div class="img_box7">
                            	<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/info4_img1.jpg" width="122" height="70" alt=""></a>
                            </div>
                            <h2 class="hdclass no-margin">משחקים שח-מט</h2>
                            <div class="text_box7">ליזמים של להבריא עסקיהם 
 .חוסר ידע ,משאבים קשות</div>
                        </div>
                        <div class="img_info_box1">
                        	<div class="img_box7">
                            	<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/info4_img2.jpg" width="122" height="70" alt=""></a>
                            </div>
                            <h2 class="hdclass no-margin">משחקים שח-מט</h2>
                            <div class="text_box7">ליזמים של להבריא עסקיהם
 .חוסר ידע ,משאבים קשות</div>
                        </div> 
                        <div class="img_info_box1 no-margin no-bdr no-padding">
                        	<div class="img_box7">
                            	<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/info4_img3.jpg" width="122" height="70" alt=""></a>
                            </div>
                            <h2 class="hdclass no-margin">משחקים שח-מט</h2>
                            <div class="text_box7">ליזמים של להבריא עסקיהם
 .חוסר ידע ,משאבים קשות</div>
                        </div>
                    </div>
                    
                    
                   
                    </div>
                    
                </div>
                

             </div>
             <div class="block1 block5">
             <h2 class="heading6  m-b1">המומחים של נומיינד</h2>
               <ul class="photo_galary clearfix">
                   <li class="first"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic2.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic3.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic4.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic5.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic6.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic7.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
                   <li class=" last"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic8.jpg" width="111" height="75" alt=""></a>שם המומחה</li>
               </ul>
               
               
            </div>
            
<script>
jQuery(document).ready(function($){
    $("#act-add-to-cart").click(function(){ 
        $(".checkbox-product").each(function(){ 
            var product = {};
            if($(this).is(":checked")){ 
                product.ID = $(this).val();
                console.log(product);
                $.ajax({
                    type:"get",
                    url:"/?add-to-cart="+product.ID,
                    success:function(){
                        console.log("added to cart successfuly!")
                    }
                });
            }
        });
        
    });
});
</script>


