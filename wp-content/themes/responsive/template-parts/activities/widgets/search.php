   <div class="select_block clearfix">
        	<h4 class="hdclass">מצא פעילות</h4>	
            <form action="/events" method="get">
            <div class="form_box">
            
            <?php
                  $args = array(
                                            'orderby'           => 'name', 
                                            'order'             => 'ASC',
                                            'hide_empty'        => false,
                                            'parent'          => 0
                                        ); 
                                
                     $taxonomies = array( 
                        EM_TAXONOMY_CATEGORY
                    );
                  $terms = get_terms($taxonomies, $args );
                               
                ?>
                                
           
            
        	<select class="select_box" name="category">
                <option value="">נושא</option>
               <?foreach($terms as $category) : ?>
                   <option value="<?= $category->slug?>"><?= $category->name;?></option>
                <? endforeach; ?>
            </select>
            <select class="select_box" name="subcategory">
                <option value="">תת נושא</option>
            </select>
            <select class="select_box" name="area">
                <option value="">אזור</option>
               <?php include get_stylesheet_directory() .'/template-parts/home/widgets/areas.php' ; ?>
            </select>
            <input placeholder="מתאריך" name="dateFrom" id="dateFrom" style="width: 60px;float: right;height: 19px;margin: 0 5px;" class="input_text1"/>
            <input placeholder="עד תאריך" name="dateTo" class="input_text1" style="width: 60px;float: right;height: 19px;"/>
            <input name="text" class="input_text1 input_text16" style="width: 190px !important;float: right;height: 19px;margin: 0 5px;" type="text"  placeholder="טקסט חופשי" >
            <input type="hidden" name="search" value="1" />
            <input class="input_btn1 input_btn10" type="submit" value="חיפוש">
            </div>
            </form>
        </div>
        
        <script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>;
        <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() .'/js/jquery.ui.datepicker-he.js'; ?>"></script>;

        <script>
        $(document).ready(function() {
            $.datepicker.setDefaults($.datepicker.regional["he"]);
            $('input[name=dateFrom],input[name=dateTo]').datepicker();

            var category = $("select[name='category']");
            var subcategory = $("select[name='subcategory']");
                
                category.change(function() {
                     
                     var category_slug = $(this).val();   
                        
                     $.ajax({
                        url: "/ajax/activity-subcategories.php" + "?&category=" + category_slug,
                        type: "GET",
                        dataType: "text",
                        cache: false,
                        success: function(data) {
                            if (data != '') {
                                subcategory.find('option').remove().end().append('<option value="">תת נושא</option>');
                                subcategory.append(data);
                            }
                            else
                            {
                                subcategory.find('option').remove().end().append('<option value="">תת נושא</option>');
                            }
                         
                        }
                    });
                });
                
               
         });
        
        </script>
       