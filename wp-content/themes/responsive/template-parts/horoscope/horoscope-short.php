<?php
$post = get_post(get_the_ID(), 'OBJECT');
$terms = wp_get_post_terms( get_the_ID(), 'signs' );

$signs = apply_filters( 'taxonomy-images-get-terms', '', array('taxonomy' => 'signs') );


foreach($signs as $s) {
    if($s->term_id == $terms[0]->term_id) {
        $url = wp_get_attachment_image_src( $s->image_id, 'full' );
        break;
    }
}

//die(print_r($url));
?>

<div class="astro_box">
    <a href="<?php echo get_term_link($terms[0], 'signs'); ?>">
    <div class="box_area">
        <img src="<?php echo $url[0]; ?>" alt="" width="36" height="40">
        <h2 class="hdclass"><?php echo $terms[0]->name; ?></h2>
    </div>
    </a>
    
    <div class="astro_box_text">
        <a href="<?php echo get_term_link($terms[0], 'signs'); ?>">
            <h2 class="hdclass"><?php echo $terms[0]->name; ?></h2>
        </a>
        <div class="time"><?php echo $terms[0]->description; ?></div>
            <div class="text_area2">
            <?php the_content(); ?>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    
<?php    
    // Get the custom fields
    $ids = $terms[0]->term_id;
    $sign_custom_fields = get_option( "taxonomy_term_$ids" );  
      
       
?>
    
    <div class="box_nav_outer clearfix">
        <ul class="uli box_nav ">
            <li><a href="<?= isset($sign_custom_fields['sign_properties']) ? $sign_custom_fields['sign_properties'] : '#'; ?>">מאפייני המזל</a></li>
            <li><a href="<?= isset($sign_custom_fields['astrology_forum']) ? $sign_custom_fields['astrology_forum'] : '#'; ?>">פורום אסטרולוגיה</a></li>
            <li><a href="<?= isset($sign_custom_fields['year_forecast']) ? $sign_custom_fields['year_forecast'] : '#'; ?>">תחזית הורוסקופ שנתית</a></li>
        </ul>
    </div>