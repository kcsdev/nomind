<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Home Widgets Template
 *
 *
 * @file           sidebar-home.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/sidebar-home.php
 * @link           http://codex.wordpress.org/Theme_Development#Widgets_.28sidebar.php.29
 * @since          available since Release 1.0
 */
?>

<div class="adsense1"><?php echo do_shortcode("[pro_ad_display_adzone id=733]"); ?></div>
<div class="breadcrumb_area clearfix">

</div>

<div class="main_content clearfix">
<div class="block1 clearfix" style="margin-bottom: 0px;">
    
    <div class="slide_block">

   <?php dynamic_content_gallery(); ?>
   
    </div>
    <?php responsive_widgets_before(); // above widgets container hook ?>
	
    <div id="widgets" class="home-widgets">
		<div id="home_widget_1" class="grid col-220"  style="margin-left: 0; margin-bottom: 0;">
			
            
            <?php responsive_widgets(); // above widgets hook ?>

			<?php if( !dynamic_sidebar( 'home-widget-1' ) ) : ?>  <!-- NEWS -->
				<div class="widget-wrapper">

					<div class="widget-title-home"><h3><?php _e( 'Home Widget 1', 'responsive' ); ?></h3></div>
					
				</div><!-- end of .widget-wrapper -->
			<?php endif; //end of home-widget-1 ?>

			<?php responsive_widgets_end(); // responsive after widgets hook ?>
		</div><!-- end of .col-300 -->

		<div id="home_widget_2" class="grid col-220">
		
              
               
                <div class="cont_box1">
                  
                   <?php include get_stylesheet_directory() .'/inc/sidebar/site-areas.php' ; ?>
                  
                    <div class="message_box clearfix">
                        <h2 class="hdclass">הצטרפו למועדון החברים של נומיינד</h2>
                         <?php get_mail_chimp_frm();?>
                    </div>
                </div>
            
		</div><!-- end of .col-300 -->

		<div id="home_widget_3" class="grid col-220 fit">
                
			<?php responsive_widgets(); // above widgets hook ?>

			<?php if( !dynamic_sidebar( 'home-widget-3' ) ) : ?>
				<!--<div class="widget-wrapper">
                    
                     
                    
					<div class="widget-title-home"><h3><?php _e( 'Home Widget 3', 'responsive' ); ?></h3></div>
					<div
						class="textwidget"><?php _e( 'This is your third home widget box. To edit please go to Appearance > Widgets and choose 8th widget from the top in area 8 called Home Widget 3. Title is also manageable from widgets as well.', 'responsive' ); ?></div>
                    
				</div><!-- end of .widget-wrapper -->
			<?php endif; //end of home-widget-3 ?>

			<?php responsive_widgets_end(); // after widgets hook ?>
		</div><!-- end of .col-300 fit -->
	</div><!-- end of #widgets -->
<?php responsive_widgets_after(); // after widgets container hook ?>
    

 </div>
