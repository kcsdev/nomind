<style>
.hdclass-right
{
    text-align: right;
}
</style>

<div class="block1 clearfix">
    <div class="adsense2"><?php echo do_shortcode("[pro_ad_display_adzone id=742]"); ?></div>
    <div class="info_box1">
    <?php 
    $category1 =  get_post_meta( 575, 'category1', true  ); 
    $category2 =  get_post_meta( 575, 'category2', true  );
    $category3 =  get_post_meta( 575, 'category3', true  );
    $category4 =  get_post_meta( 575, 'category4', true  ); 
    
    $categoryObj1 = get_category( $category1 );
    $categoryObj2 = get_category( $category2 );
    $categoryObj3 = get_category( $category3 );
    $categoryObj4 = get_category( $category4 );    
    ?>
                
                
                <h4 class="hdclass-right"><i><?= get_the_category_by_ID( $category1 ); ?></i></h4>
                <div class="info_list clearfix">
                    <ul class="uli">
                        <li class="uli"><a href="<?= site_url().'/category/allarticles/'. $categoryObj1->slug?>">מאמרים</a></li>
                        <li class="uli"><a href="<?=site_url().'/custom-forum/?cf_action=forum-search-cat&search='.urlencode($categoryObj1->name); ?>">פורומים</a></li>
                        <li class="uli"><a href="<?= site_url().'/category/'. $categoryObj1->slug?>">בלוגים</a></li>
                        <li class="uli"><a href="#">היכרויות</a></li>
                        <li class="uli"><a href="<?=site_url().'/category_recipes/'.$categoryObj1->slug?>">מתכונים</a></li>
                        <li class="uli no-bdr"><a href="<?= get_permalink(419);?>">תמונות</a></li>
                    </ul>
                </div>
                <? //get 2 recent articles (posts) 
                     $new_loop = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page' => 2,
                        'orderby' => 'rand',
                        'cat' => $category1
                        
                    ));
                    
                  $counter = 0;
                ?>
                    <? if ($new_loop->have_posts()) : while ($new_loop->have_posts()) : $new_loop->the_post(); ?>
        
                    <?
                        $image_url = array();
                        if (has_post_thumbnail()) {
                            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                        } else
                            $image_url[0] = get_template_directory_uri().'/img/info_pic9.jpg'; //default image
                            
                   ?>
                   
                    
                    <div class="info_box1_part clearfix" <?  if($counter == 0) echo 'style="border-bottom: 1px solid #d1c6c1;"'; else echo 'style="border-bottom: none;"';?>>  
                        <div class="img_box"><a href="<?php the_permalink() ?>"> <img src="<?=$image_url[0]?>" width="112" height="74" alt=""></a></div>
                        <div class="img_info">
                            <h5 class="hdclass-right"><a href="<?php the_permalink() ?>"><?the_title();?></a></h5>
                            <ul>
                                <li><?the_author();?></li>
                                <li class="no-bdr"><? the_time('d.m.Y'); ?></li>  
                            </ul>
                            <div class="text_box1 text_box2">
                                לפרטים המלאים, כולל סרטונים, לחצו על הקישור לאתר בעברית של המכון לתזונה אינטגרטיבית המופיע כאן בתחתי...
                            </div>
                        </div>
                    </div>
                    
                     <?php $counter++; endwhile;
                     endif; 
                     wp_reset_query(); ?>

                   
            </div>
            
            
    <div class="info_box1 info_box2">
        <h4 class="hdclass-right"><i><?= get_the_category_by_ID( $category2 ); ?></i></h4>
        <div class="info_list clearfix">
            <ul class="uli">
                <li class="uli"><a href="<?= site_url().'/category/allarticles/'. $categoryObj1->slug?>">מאמרים</a></li>
                <li class="uli"><a href="<?=site_url().'/custom-forum/?cf_action=forum-search-cat&search='.urlencode($categoryObj1->name); ?>">פורומים</a></li>
                <li class="uli"><a href="<?= site_url().'/category/'. $categoryObj1->slug?>">בלוגים</a></li>
                <li class="uli"><a href="#">היכרויות</a></li>
                <li class="uli"><a href="<?=site_url().'/category_recipes/'.$categoryObj1->slug?>">מתכונים</a></li>
                <li class="uli no-bdr"><a href="<?= get_permalink(419);?>">תמונות</a></li>
            </ul>
        </div>
        
        <? //get 2 recent articles (posts) 
                     $new_loop = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page' => 2,
                        'orderby' => 'rand',
                        'cat' => $category2
                        
                    ));
                    
                  $counter = 0;
                ?>
                    <? if ($new_loop->have_posts()) : while ($new_loop->have_posts()) : $new_loop->the_post(); ?>
        
                    <?
                        $image_url = array();
                        if (has_post_thumbnail()) {
                            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                        } else
                            $image_url[0] = get_template_directory_uri().'/img/info_pic9.jpg'; //default image
                            
                   ?>
        <div class="info_box1_part clearfix" <?  if($counter == 0) echo 'style="border-bottom: 1px solid #d1c6c1;"'; else echo 'style="border-bottom: none;"';?>>  
                        <div class="img_box"><a href="<?php the_permalink() ?>"> <img src="<?=$image_url[0]?>" width="112" height="74" alt=""></a></div>
                        <div class="img_info">
                            <h5 class="hdclass-right"><a href="<?php the_permalink() ?>"><?the_title();?></a></h5>
                            <ul>
                                <li><?the_author();?></li>
                                <li class="no-bdr"><? the_time('d.m.Y'); ?></li>  
                            </ul>
                            <div class="text_box1 text_box2">
                                    
                                
                                     לפרטים המלאים, כולל סרטונים, לחצו על הקישור לאתר בעברית של המכון לתזונה אינטגרטיבית המופיע כאן בתחתי...
                                
                               
                            </div>
                            
                        </div>
                    </div>
          <?php $counter++; endwhile;
                     endif; 
                     wp_reset_query(); ?>

       
    </div>
    <div class="info_box1 info_box3 no-pad-b" style="margin-top: 20px;">
        <h4 class="hdclass-right-right"><i><?= get_the_category_by_ID( $category3 ); ?> </i></h4>
        <div class="info_list clearfix">
            <ul class="uli">
                <li class="uli"><a href="<?= site_url().'/category/allarticles/'. $categoryObj1->slug?>">מאמרים</a></li>
                <li class="uli"><a href="<?=site_url().'/custom-forum/?cf_action=forum-search-cat&search='.urlencode($categoryObj1->name); ?>">פורומים</a></li>
                <li class="uli"><a href="<?= site_url().'/category/'. $categoryObj1->slug?>">בלוגים</a></li>
                <li class="uli"><a href="#">היכרויות</a></li>
                <li class="uli"><a href="<?=site_url().'/category_recipes/'.$categoryObj1->slug?>">מתכונים</a></li>
                <li class="uli no-bdr"><a href="<?= get_permalink(419);?>">תמונות</a></li>
            </ul>
        </div>

        <? //get 2 recent articles (posts) 
                     $new_loop = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page' => 2,
                        'orderby' => 'rand',
                        'cat' => $category3
                        
                    ));
                    
                  $counter = 0;
                ?>
                    <? if ($new_loop->have_posts()) : while ($new_loop->have_posts()) : $new_loop->the_post(); ?>
        
                    <?
                        $image_url = array();
                        if (has_post_thumbnail()) {
                            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                        } else
                            $image_url[0] = get_template_directory_uri().'/img/info_pic9.jpg'; //default image
                            
                   ?>
        <div class="info_box1_part clearfix" <?  if($counter == 0) echo 'style="border-bottom: 1px solid #d1c6c1;"'; else echo 'style="border-bottom: none;"';?>>  
                        <div class="img_box"><a href="<?php the_permalink() ?>"> <img src="<?=$image_url[0]?>" width="112" height="74" alt=""></a></div>
                        <div class="img_info">
                            <h5 class="hdclass-right"><a href="<?php the_permalink() ?>"><?the_title();?></a></h5>
                            <ul>
                                <li><?the_author();?></li>
                                <li class="no-bdr"><? the_time('d.m.Y'); ?></li>  
                            </ul>
                            <div class="text_box1 text_box2">
                                    
                                לפרטים המלאים, כולל סרטונים, לחצו על הקישור לאתר בעברית של המכון לתזונה אינטגרטיבית המופיע כאן בתחתי...
                               
                            </div>
                            
                        </div>
                    </div>
          <?php $counter++; endwhile;
                     endif; 
                     wp_reset_query(); ?>

       
    </div>
    <div class="info_box1 info_box4"   style="margin-top: 20px;">
        <h4 class="hdclass-right"><i><?= get_the_category_by_ID( $category4 ); ?></i></h4>
        <div class="info_list clearfix">
            <ul class="uli">
                <li class="uli"><a href="<?= site_url().'/category/allarticles/'. $categoryObj1->slug?>">מאמרים</a></li>
                <li class="uli"><a href="<?=site_url().'/custom-forum/?cf_action=forum-search-cat&search='.urlencode($categoryObj1->name); ?>">פורומים</a></li>
                <li class="uli"><a href="<?= site_url().'/category/'. $categoryObj1->slug?>">בלוגים</a></li>
                <li class="uli"><a href="#">היכרויות</a></li>
                <li class="uli"><a href="<?=site_url().'/category_recipes/'.$categoryObj1->slug?>">מתכונים</a></li>
                <li class="uli no-bdr"><a href="<?= get_permalink(419);?>">תמונות</a></li>
            </ul>
        </div>
        <? //get 2 recent articles (posts) 
                     $new_loop = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page' => 2,
                        'orderby' => 'rand',
                        'cat' => $category4
                        
                    ));
                    
                  $counter = 0;
                ?>
                    <? if ($new_loop->have_posts()) : while ($new_loop->have_posts()) : $new_loop->the_post(); ?>
        
                    <?
                        $image_url = array();
                        if (has_post_thumbnail()) {
                            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                        } else
                            $image_url[0] = get_template_directory_uri().'/img/info_pic9.jpg'; //default image
                            
                   ?>
        <div class="info_box1_part clearfix" <?  if($counter == 0) echo 'style="border-bottom: 1px solid #d1c6c1;"'; else echo 'style="border-bottom: none;"';?>>  
                        <div class="img_box"><a href="<?php the_permalink() ?>"> <img src="<?=$image_url[0]?>" width="112" height="74" alt=""></a></div>
                        <div class="img_info">
                            <h5 class="hdclass-right"><a href="<?php the_permalink() ?>"><?the_title();?></a></h5>
                            <ul>
                                <li><?the_author();?></li>
                                <li class="no-bdr"><? the_time('d.m.Y'); ?></li>  
                            </ul>
                            <div class="text_box1 text_box2">
                                    
                                לפרטים המלאים, כולל סרטונים, לחצו על הקישור לאתר בעברית של המכון לתזונה אינטגרטיבית המופיע כאן בתחתי...
                               
                            </div>
                            
                        </div>
                    </div>
          <?php $counter++; endwhile;
                     endif; 
                     wp_reset_query(); ?>

    </div>
</div> 