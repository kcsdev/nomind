<div class="link_list">
            	<div class="info_heading info_heading4 clearfix">
                    <h4 class="hdclass">שיתופי פעולה</h4>
                </div>
                <div class="link_list_box1"	>
                	<ul class="uli">
                    <?php $args = array();
                    $args = array('post_type' => 'partnership',
                                              'posts_per_page' => 10,
                                               'orderby' => 'rand');
                                $partnerships = get_posts($args);
                        
                                $counter = 0;
                        
                            ?>
                                    
                
                        <?foreach($partnerships as $partner) : ?>
                            <? $counter++;
                                $class = '';
                             ?>
                            
                            <? if($counter % 2 == 0) { $class = 'margin-b1';}?>
                      		    <li class="uli <?=$class;?>"><a rel="nofollow" target="_blank" href="<?=get_field('link_address', $partner->ID);?>"><img src="<?=get_field('partnership_image', $partner->ID);?>" width="108" height="29" alt=""></a></li>
                            
                         <?endforeach;?>
                        
                        
                     </ul>
                </div>
            </div>	