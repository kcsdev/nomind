<style>
    
    
    
     .slide_cont
     {
        width: 172px;
     }
     
    
</style>

<div class="slider_box">
                <div class="info_heading info_heading2 clearfix">
                    <h4 class="hdclass">אינדקסים</h4>
                    <ul class="uli">
                        <li class="uli"><a href="/business-directory/?action=search&dosrch=1&new=1">חדשים</a></li>
                        <li class="uli"><a href="/business-directory/?action=search&dosrch=1&popular=1">נצפים ביותר</a></li>
                        <li class="uli"><a href="/%d7%90%d7%99%d7%a0%d7%93%d7%a7%d7%a1%d7%99%d7%9d/">כל האינדקסים</a></li>
                    </ul>
                </div>
                <a class="next1" href="#"></a>
                <a class="prev1" href="#"></a> 
                
                <?php   
                $term_args = array('hide_empty' => 0);
                
                $catIndeximImgs = apply_filters( 'taxonomy-images-get-terms', '', array('taxonomy' => WPBDP_CATEGORY_TAX, 'having_images' => ture ), $term_args );
                
                ?>
               
                <div class="slider_part">
                <ul class="slides">
                 <?php for($i=0; $i < count($catIndeximImgs); $i=$i+3) : ?>
                 <li>
                   <?php for($j=$i; $j<$i+3; $j++) : ?> 
                     <?php $link = get_term_link($catIndeximImgs[$j]->slug, WPBDP_CATEGORY_TAX); 
                           $url = wp_get_attachment_image_src( $catIndeximImgs[$j]->image_id, 'medium' );
                      ?>      
                         <?php if(isset($catIndeximImgs[$j])) : ?>
                           
                         
                     <div class="slide_cont" >
                                    <div class="slide_img_area">
                                <a href="<?= $link;?>">
                                    <h4 class="hdclass"> <?= $catIndeximImgs[$j]->name; ?></h4>
                                   
                                   <?php 
                                     $img_url = $url[0];
            
                                        if($img_url == '')
                                             $img_url = get_stylesheet_directory_uri().'/img/slide1_pic3.jpg';
                                        
                                        ?> 
                                   <img src="<?= $img_url;?>" width="170" height="133" alt="<?= $catIndeximImgs[$j]->name; ?>">
    
          
                                </a>    	
                                </div>
                                <h2 class="hdclass"><a href="/business-directory/?action=submitlisting"><img src="<?php echo get_template_directory_uri(); ?>/img/click_bg3.png" width="18" height="18" alt=""></a>הוסף מודעה</h2>
                               <?php
                                         $sub_categories = get_terms( WPBDP_CATEGORY_TAX, array(
                                                    'hide_empty' => 0,
                                                    'parent' => $catIndeximImgs[$j]->term_id,
                                                )); 
                                    ?>
                                    <select class="select_box1 sub_category">
                                        <option value="">תת קטגוריה</option>
                                        <?php foreach($sub_categories as $sub) : ?>
                                        <? $link = get_term_link($sub->slug, WPBDP_CATEGORY_TAX); ?> 
                                            <option value="<?php echo $link; ?>"><?php echo $sub->name;?></option>
                                        <?php endforeach; ?>
                                    </select>
                            </div> 
                         
                         <?php endif; ?> 
                   <?php endfor; ?>
                    </li>
                    <?php endfor; ?>
                 </ul>
                </div>
                
               
    </div>
    
      <script>
     
       $(window).load(function() {
            $('.slider_part').flexslider( {
                controlNav: false,
                animationLoop: false,
                slideshow: false
              }
            );
            
            $('a.prev1').click(function(e){
                e.preventDefault();
                $('.slider_part').flexslider('prev')
                return false;
            });
            
            $('a.next1').click(function(e){
                e.preventDefault();
                $('.slider_part').flexslider('next')
                return false;
            });
            
             $('.sub_category').bind('change', function () {
                  var url = $(this).val(); // get selected value
                  if (url) { // require a URL
                      window.location = url; // redirect
                  }
                  return false;
              });  
          });       
            
            
       
      
     
      </script>      