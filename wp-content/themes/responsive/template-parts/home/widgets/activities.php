<div class="info_box5">
                <div class="info_heading">
                    <h4 class="hdclass">פעילויות</h4>
                </div>
                
                <?
                $args = array(
                                'post_type' => EM_POST_TYPE_EVENT,
                                'posts_per_page' => 2,
                                'orderby' => 'rand'
                            );
            
                 $counter = 1;
                global $EM_Event;
                $wp_query = new WP_Query($args);
                
                
                
                if ($wp_query->have_posts()) :
                    while ($wp_query->have_posts()): $wp_query->the_post(); 
                    
                    if (has_post_thumbnail()) {
                        $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb');
                        $index_img =  $image_url[0];
                    }  
                    else
                    {
                        $index_img = get_template_directory_uri().'img/info9_pic3.jpg';
                    }
                        $EM_Event = em_get_event(get_the_ID() ,'post_id');
                       
                    ?>
                    <div class="info_box1_part1 <? if($counter == 2) echo ' no-margin no-bdr no-padding'?>">
                        <div class="img_box1 ">
                        <img src="<?=$index_img?>" width="138" height="72" alt=""></div>
                        <div class="img_info">
                            <h5 class="hdclass"><a href="<?the_permalink();?>"><?the_title();?></a> </h5>
                            <ul>
                                <li><?the_author();?></li>
                                <li class="no-bdr"><?=date('d.m.Y', $EM_Event->start)?></li>
                            </ul>
                            <div class="text_box1 text_box2">
                                 <?the_excerpt_max_charlength(80);?>
                            </div>
                        </div>
                   </div>
                 <?php    
                    $counter++;
                    endwhile;
                    endif;
                    wp_reset_query();
                    
                 ?>
</div>