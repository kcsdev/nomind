<div class="info_box5 no-margin"> 
        <div class="info_heading info_heading1 clearfix">
        
        <? $blog_categories = get_blogs_categories(); ?>
        
            <h4 class="hdclass">בלוגים</h4>
            <ul class="uli">
                <li class="uli"><a href="<?=get_permalink(41);?>">כל הבלוכים</a></li>
                <li class="uli"><a href="#">מדוברים</a></li>
                <li class="uli"><a href="#">חדשים</a></li>
            </ul>
        </div>
        
          <?
            $new_loop = new WP_Query(array(
                    'post_type' => 'post',
                    'category__in' => $blog_categories,
                    'posts_per_page' => 2,
                    'orderby' => 'rand'
                ));
                $counter = 0;
        ?>        
        
        <?php 
        
        if ($new_loop->have_posts()) : while ($new_loop->have_posts()) : $new_loop->the_post(); ?>

        <?php

        $post_id = get_the_ID();
        if (has_post_thumbnail()) {
            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
        } else
            $image_url[0] = ''; //default image?
        ?>                        
        <div class="info_box1_part1 <? if($counter == 1) echo ' no-margin no-bdr no-padding'?>" >
          <div class="img_box1">
          <a href="<?php the_permalink(); ?>">
          <img src="<?php echo get_template_directory_uri(); ?>/img/info1_pic1.jpg" width="138" height="72" alt="" scale="0">
          </a>
          </div>
            <div class="img_info">
                <h5 class="hdclass"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                <ul>
                    <li><?php the_author(); ?></li>
                    <li class="no-bdr"><?php the_time('d.m.Y'); ?></li>
                </ul>
                <div class="text_box1 text_box2">
                     <?the_excerpt_max_charlength(80);?>
                </div>
            </div>
        </div>
        <?php  $counter++; endwhile;
    else: ?>


    <?php endif; ?>
    <?php wp_reset_query(); ?>                
       
    </div>