<div class="link_list">
            	<div class="info_heading info_heading4 clearfix">
                    <h4 class="hdclass">קישורים</h4>
                </div>
                <div class="link_list_box clearfix">
                	<ul class="uli">
                    	<!-- max 32 links (random) -->
                        <? /* $bookmarks = get_bookmarks( array(
                            	'orderby'        => 'rand',
                                'limit' => 32
                            )); */ ?>
                            
                            <?  
                                $args = array('post_type' => 'links',
                                              'posts_per_page' => 32,
                                               'orderby' => 'rand');
                                $bookmarks = get_posts($args);
                                 // var_dump($bookmarks);
                            ?>
                            
                        <?foreach($bookmarks as $bookmark) : ?>
                            <li class="uli"><a rel="nofollow" target="_blank" href="<?=get_field('link_address', $bookmark->ID);?>"><?=$bookmark->post_title;?></a></li>
                        <?endforeach;?>
                    </ul>
                </div>	
            </div>