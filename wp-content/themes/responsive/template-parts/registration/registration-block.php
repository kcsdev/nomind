<div class="block1 block2 block8 clearfix">
            	<div class="order-block-title big"><h2>טופס הרשמה</h2>asdfsdf</div>
                <div class="payment-right">
                    <?= the_content(); ?>					
					<p class="y12">* מציינת שדה חובה למילוי</p>
					<p class="y11">סמן מתוך הרשימה - את איזה תחום אתה מעוניין להפעיל בפורטל נומיינד (<a href="#" class="linkk">לא ברור - לחץ כאן להסבר</a>)</p>
                    
                  
                    <ul style="color:red;">
                    <?
                    global $valid_error_message;
                    if(isset($valid_error_message)) {
                        foreach($valid_error_message as $message){
                            echo '<li style="color:red;">'.$message.'</li>';
                        }

                    } ?>
                    </ul>
                    <form id="registerForm" name="registerForm" method="post" enctype="multipart/form-data">
                    <div class="payment-right-row y13">
						<div class="y14">
							<label>
								<input type="checkbox" name="chk_group[]" value="publishers"/>
								מערכת מפרסמים
							</label>
							<label>
								<input type="checkbox" name="chk_group[]" value="article_writers"/>
								כתיבת מאמרים
							</label>
							<label>
								<input type="checkbox" name="chk_group[]" value="blog_writers"/>
								בלוגים
							</label>
							<label>
								<input type="checkbox" name="chk_group[]" value="index_writers"/>
								אינדקסים
							</label>
							<label>
								<input type="checkbox" name="chk_group[]" value="video_adders"/>
								הוספת סרטים
							</label>
							<label>
								<input type="checkbox" name="chk_group[]" value="Forum users"/>
								פורומים
							</label>
						</div>
                        <div class="have-acc-right payment">
                            <label class="p_r2"><sup>*</sup> שם משפחה: </label>
                            <input name="last_name" type="text" class="acc-field pay" />
                            <div class="clear"></div>
                            
                            <label class="p_r2"><sup>*</sup> דוא"ל: </label>
                            <input name="email" type="text" class="acc-field pay" />
                            <div class="clear"></div>
                            
                            <label class="p_r2"><sup>*</sup> טלפון: </label>
                            <input name="phone1" type="text" class="acc-field pay y15" />
							<select name="phone1_area_code" class="acc-field y16">
									<option value="02">02</option>
                                	<option value="03">03</option>
                                	<option value="04">04</option>
                                	<option value="08">08</option>
                                	<option value="09">09</option>
                                	<option value="050">050</option>
                                	<option value="052">052</option>
                                	<option value="053">053</option>
                                	<option value="054">054</option>
                                	<option value="055">055</option>
                                	<option value="058">058</option>
                                	<option value="072">072</option>
                                	<option value="073">073</option>
                                	<option value="074">074</option>
                                	<option value="076">076</option>
                                	<option value="077">077</option>
							</select>
                            <div class="clear"></div>
                            
                            <label class="p_r2">שם משתמש: </label>
                            <input name="username" type="text" class="acc-field pay" />
                            <div class="clear"></div>
                            
                            
                            <label class="p_r2">כינוי לפורומים: </label>
                            <input name="nickname" type="text" class="acc-field pay" />
                            <div class="clear"></div>
                            
                      
                       </div>
                        
                        <div class="have-acc-right payment2 payment3">
                            <label><sup>*</sup> שם פרטי: </label>
                            <input name="first_name" type="text" class="acc-field pay" value="<?php echo isset($_POST['first_name']) ? $_POST['first_name'] : ''; ?>"/>
                            <div class="clear"></div>
                            
                            <label><sup>*</sup> דוא"ל שוב: </label>
                            <input name="email_confirm" type="text" class="acc-field pay" />
                            <div class="clear"></div>
                            
                            <label><sup>*</sup> טלפון נוסף:</label>
                            <input name="" type="text" class="acc-field pay y15" />
							<select name="phone2_area_code" class="acc-field y16">
									<option value="02">02</option>
                                	<option value="03">03</option>
                                	<option value="04">04</option>
                                	<option value="08">08</option>
                                	<option value="09">09</option>
                                	<option value="050">050</option>
                                	<option value="052">052</option>
                                	<option value="053">053</option>
                                	<option value="054">054</option>
                                	<option value="055">055</option>
                                	<option value="058">058</option>
                                	<option value="072">072</option>
                                	<option value="073">073</option>
                                	<option value="074">074</option>
                                	<option value="076">076</option>
                                	<option value="077">077</option>
							</select>
                            <div class="clear"></div>
                            
                            <label>שם העסק: </label>
                            <input name="company_name" type="text" class="acc-field pay" value="<?php echo isset($_POST['company_name']) ? $_POST['company_name'] : ''; ?>"/>
                            <div class="clear"></div>
							<input type="submit" value="בצע הרשמה" class="send" />
                            </form>
                        </div>
                        <div class="clear"></div>
                        
                         
                    </div>
                <?= the_excerpt(); ?>
                </div>
                <div class="payment-support m-l1">
                	<div class="payment-support-pic"><img src="<?php echo get_template_directory_uri(); ?>/img/payment-support.jpg" alt=""></div>
                    <a href="#">טלפון: 6244115 - 03 </a>
                </div>
                <div class="clear"></div>
            </div>