<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.bxslider.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.bxslider.js"></script>
<link href='<?php bloginfo('template_directory'); ?>/css/jquery.bxslider.css' rel='stylesheet' type='text/css'>
<link href='<?php bloginfo('template_directory'); ?>/css/vit.css' rel='stylesheet' type='text/css'>
<?php
//find parent
/**
 *   $categories = get_the_category();
 *         $cats = array();
 *
 *         foreach($categories as $category)
 *         {
 *             $cats[] = $category->cat_ID;
 *         }
 *
 *         $id = min($cats); //cat_name
 *         $cat_name = get_cat_name( $id );
 * 
 * 
 */

//$id = 40; //blogs category

$blogs = get_term_by('slug', 'blogs');
$cat_name = get_cat_name($id);


$all_categories = array(
    41,
    73,
    61,
    81,
    69,
    77,
    53,
    57,
    51,
    65);

?>

<div class="block1 block2 clearfix">
<div class="column3">
    <?php echo getUserCart(); ?>
    <div class="right_list_bar1">
        <h4 class="hdclass" style="text-align: center;">קטגוריות <?php echo
get_cat_name($id); ?></h4>

        <div class="right_list_bar1_cont">
            <ul class="right_list_bar1_cont_box">
                <?php

            $args = array('orderby' => 'name', 'parent' => $id);
            $categories = get_categories($args);
            
            
            foreach ($categories as $category) {
                echo '<li><a href="' . get_category_link($category->term_id) . '">' . $category->
                    name . '</a></li>';
} ?>
            </ul>

        </div>
    </div>
    <div class="adsense5"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/ad4.png" width="159"
                                           height="212" alt=""></a></div>

    <!-- login control -->
    <?php include (get_template_directory() . '/inc/login-control.php'); ?>

    <div class="adsense2 adsense6">
        <a href="#"><img height="601" width="160" alt="" src="<?php bloginfo('template_directory'); ?>/img/ad2.jpg"></a>
    </div>
    <div class="adsense4 adsense6">
        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/ad3.jpg" width="160" height="600" alt=""></a>
    </div>
</div>

<div class="column4">
    <?php include get_stylesheet_directory() .'/template-parts/blogim/blogim-author.php' ; ?>
<div class="middle_column_box2 m-b1 clearfix">

    <?php

/*   $idObj = get_category_by_slug('blogs');
$id = $idObj->term_id; */


$args = array(
    'orderby' => 'name',
    'parent' => $id,
    'hide_empty' => 0);

$categories = get_categories($args);

$counter = 0;

foreach ($categories as $category) {


    /*$categories = apply_filters( 'taxonomy-images-get-terms', '', array(
    'term_args' => '&include='.$category->term_id.'orderby=count&hide_empty=0'
    ) ); */

    $images_raw = get_option('taxonomy_image_plugin');
    if (array_key_exists($category->term_id, $images_raw)) {
        $category_image = wp_get_attachment_image_src($images_raw[$category->term_id],
            'thumbnail');
    }

?>

        <div class="img_box5 img_box10 <?php echo ($counter % 3 == 0) ?
'no-mg-r' : ''; ?>">
            <a href="<?php echo get_category_link($category->term_id); ?>"><img src="<?php echo
$category_image[0]; ?>"
                                                                                width="86" height="52"
                                                                                alt="<?php echo
$category->name; ?>"></a>
              <h4 class="hdclass"><a href="<?php echo get_category_link($category->
term_id); ?>"><?php echo
$category->name; ?></a></h4>
        </div>

        <?php
    $counter++;
} ?>

</div>
<div class="cont_box1 cont_box2">
    <ul class="uli m-b5">
        <li class="uli">
            <img src="<?php bloginfo('template_directory'); ?>/img/list1_pic11.png" width="33" height="35" alt="">
            <h4 class="hdclass">רדיו</h4>
        </li>
        <li class="uli">
            <img src="<?php bloginfo('template_directory'); ?>/img/list1_pic1.png" width="43" height="35" alt="">
            <h4 class="hdclass">בלוגים</h4>
        </li>
        <li class="uli">
            <img src="<?php bloginfo('template_directory'); ?>/img/list1_pic2.png" width="34" height="35" alt="">
            <h4 class="hdclass">הורוסקופ</h4>
        </li>
        <li class="uli">
            <img src="<?php bloginfo('template_directory'); ?>/img/list1_pic3.png" width="38" height="35" alt="">
            <h4 class="hdclass">מאמרים</h4>
        </li>
        <li class="uli no-bg">
            <img src="<?php bloginfo('template_directory'); ?>/img/list1_pic4.png" width="37" height="35" alt="">
            <h4 class="hdclass">טיול טקסט</h4>
        </li>
        <li class="uli">
            <img src="<?php bloginfo('template_directory'); ?>/img/list1_pic10.png" width="23" height="35" alt="">
            <h4 class="hdclass no-bg">מחשבונים</h4>
        </li>
        <li class="uli">
            <img src="<?php bloginfo('template_directory'); ?>/img/list1_pic5.png" width="41" height="35" alt="">
            <h4 class="hdclass no-bg">מתכונים</h4>
        </li>
        <li class="uli">
            <img src="<?php bloginfo('template_directory'); ?>/img/list1_pic6.png" width="33" height="35" alt="">
            <h4 class="hdclass no-bg">רק השבוע</h4>
        </li>
        <li class="uli">
            <img src="<?php bloginfo('template_directory'); ?>/img/list1_pic7.png" width="38" height="35" alt="">
            <h4 class="hdclass no-bg">פורומים</h4>
        </li>
        <li class="uli no-bg">
            <img src="<?php bloginfo('template_directory'); ?>/img/list1_pic8.png" width="39" height="35" alt="">
            <h4 class="hdclass no-bg">קופונים</h4>
        </li>
    </ul>
    <div class="message_box message_box1 no-margin clearfix">
        <h2 class="hdclass">הצטרפו למועדון החברים של נומיינד</h2>
        <?php get_mail_chimp_frm(); ?>
    </div>
</div>
<div class="adsense7">
    <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/ad5.jpg" width="300" height="250" alt=""></a>
</div>
<div class="column4_block1 m-b1 clearfix">
    <div class="info_heading info_heading1 m-b3  clearfix">
        <h4 class="hdclass">באנרים</h4>
    </div>
    <?php
global $wpdb;
$selected_category = $id; // Put your parent category id here
$children = get_categories("child_of=$selected_category");
$inlist = "$selected_category";
$i = 0;
foreach ($children as $cat) {
    $inlist .= ($i ? ',' . $cat->cat_ID : $cat->cat_ID);
    $i++;
}

$sql = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID,
                            comment_author, comment_date, comment_approved, comment_type,comment_author_url,
                            SUBSTRING(comment_content,1,100) AS com_excerpt
                        FROM $wpdb->term_taxonomy as t1, $wpdb->posts, $wpdb->term_relationships as r1, $wpdb->comments
                        WHERE comment_approved = '1'
                           AND comment_type = ''
                           AND ID = comment_post_ID
                           AND post_password = ''
                           AND ID = r1.object_id
                           AND r1.term_taxonomy_id = t1.term_taxonomy_id
                           AND t1.taxonomy = 'category'
                           AND t1.term_id IN ($inlist)
                        ORDER BY comment_date DESC LIMIT 3";
$comments = $wpdb->get_results($sql);

foreach ($comments as $comment) {

    //$output .= "« ID"."#comments" . "\" title=\"Comment on ".$comment->post_title . "\">" . strip_tags($comment->com_excerpt)."... » Comment on ". strip_tags($comment->comment_author) ." , ". strip_tags($comment->comment_date) ."";

?>

        <div class="comments_box1">
            <h2 class="hdclass"><?php echo $comment->post_title; ?></h2>

            <div class="">
                <div class="text_box11">
                    <?php echo mb_substr(strip_tags($comment->com_excerpt), 0,
60); ?>
                    ...
                </div>
                <br/>
                <a href="<?php echo get_permalink($comment->comment_post_ID); ?>">להמשך</a>
            </div>
        </div>


    <?php } ?>


</div>
<div class="middle_column_box4">
    <div class="box_left">
        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/box_pic2.jpg" alt=""></a>
    </div>
    <div class="box_right">
        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/box_pic1.jpg" alt=""></a>
    </div>
    <div class="clear"></div>
</div>


<div class="middle_column_box1">
    <div class="info_heading info_heading1 no-margin clearfix">
        <h4 class="hdclass">פוסטים חמים</h4>
    </div>

    <!-- all the categories -->
    <?php
$new_loop = new WP_Query(array(
    'post_type' => 'post',
    'category__in' => $all_categories,
    'posts_per_page' => 2,
    'orderby' => 'comment_count'));



?>

    <?php if ($new_loop->have_posts()):
    while ($new_loop->have_posts()):
        $new_loop->the_post(); ?>

        <?php

        $post_id = get_the_ID();
        if (has_post_thumbnail()) {
            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
        } else
            $image_url[0] = ''; //default image?

?>
        
        <div class="info_box1_part info_box1_part2 clearfix">
            <div class="img_box info_pic1"><a href="<?php the_permalink(); ?>"><img src="<?php echo
$image_url[0]; ?>"
                                                                                    width="122" height="70" alt=""></a>
            </div>
            <div class="img_info">
                <h5 class="hdclass"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                <ul>
                    <li><a href="<?= get_blog_author_page_url(get_the_author_meta
('ID')); ?>"><?php the_author(); ?></a></li>
                    <li class="no-bdr"><?php the_time('d.m.Y'); ?></li>
                </ul>
                <div class="text_box1 text_box2">
                    <?php echo get_post_excerpt(get_the_ID()); ?>
                </div>
            </div>
        </div>


    <?php endwhile;
    else: ?>


    <?php endif; ?>
    <?php wp_reset_query(); ?>

</div>

 <?php include(get_template_directory() . '/inc/forums/featured-forums.php'); ?>   
 
<div class="social_block1 margin_bottom">
    <iframe
        src="http://www.facebook.com/plugins/likebox.php?locale=he_IL&href=https%3A%2F%2Fwww.facebook.com%2FNomind.israel&amp;width&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=false"
        scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:290px;"
        allowTransparency="true"></iframe>

</div>
 <?php include(get_template_directory() . '/inc/blogim/tags.php'); ?>   
</div>
<script>
 $(document).ready(function() {

      $('li.uli a').smoothScroll();
});
</script>
<div class="column5">
    <div class="column5_block1 clearfix">
        <h2 class=" hdclass heading6 heading7">
            <del><?php echo single_cat_title(); ?></del>
        </h2>
        <div class="info_list info_list1 clearfix">
            <ul class="uli">
                <li class="uli"><a href="#hotPosts">פוסטים חמים</a></li>
                <li class="uli"><a href="#newPosts">פוסטים חדשים</a></li>
                <li class="uli no-bdr"><a href="#updatedPosts">בלוגים חדשים</a></li>
            </ul>

        </div>

        <!-- latest post image -->
        <div class="img_box6 no-margin">


            <?php
        /*   $categories = get_categories( array(
        'child_of'=>$id
        ) );

        $subcategories = array();

        foreach ( $categories as $category ) {
        $subcategories[] = $category->cat_ID;
        }
        */
        $category = get_the_category();

        $main_category = get_category($category[0]);

        /*  if($main_category->parent != 0)
        $main_category = get_category($main_category->parent) */


?>

            <?php
        $category = get_the_category();
        $main_category = get_category($category[0]);
        $new_loop = new WP_Query(array(
            'post_type' => 'post',
            'category__in' => $main_category,
            'posts_per_page' => 1));
?>

            <?php if ($new_loop->have_posts()):
            while ($new_loop->have_posts()):
                $new_loop->the_post(); ?>

            <?php
                if (has_post_thumbnail()) {
                    $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                    echo '<a id="view_im_a" href="' . get_permalink() . '" title="' .
                        the_title_attribute('echo=0') . '">';
                    echo '<img id="view_im" src="' . $image_url[0] .
                        '" width="435" height="258" alt="">';
                    echo '</a>';
                }
?>

        </div>

        <div class="img_box6_cont" id="view_text">
            <a href="<?php the_permalink('echo=0'); ?>"
               title="<?php echo the_title_attribute('echo=0'); ?> "><?php the_title(); ?></a>
        </div>

        <?php endwhile;
            else: ?>
    </div>

    <div class="img_box6_cont ">
        שם קותרת התמונה
    </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?>


    <!-- end of latest post image -->
    <?php
                $new_loop = new WP_Query(array(
                    'post_type' => 'post',
                    'category__in' => $main_category,
                    'posts_per_page' => 10));
?>

    <div class="slide_pager clearfix">
        <ul class="uli2">
            <?php if ($new_loop->have_posts()):
                    while ($new_loop->have_posts()):
                        $new_loop->the_post(); ?>
                <?php
                        if (has_post_thumbnail()) {
                            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                            echo '<li class="uli"><a href="javascript:void(0);" title="' .
                                the_title_attribute('echo=0') . '">';
                            echo '<img onclick="'; ?>changeIt('<?php echo $image_url[0];?>','<?php the_permalink(); ?>','<?php the_title(); ?>');" <?php echo
'data-link="" data-title="' . get_the_title() . '" src="' .get_stylesheet_directory_uri().'/lib/timthumb.php?w=66&h=66&src='. urlencode($image_url[0]) .
'" alt="">';
                            echo '</a></li>';
                        }
?>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
        </ul>


    </div>
</div>
<div class="column5_block3 clearfix">
    <div class="info_heading info_heading1 clearfix">
        <h4 id="hotPosts" class="hdclass">פוסטים חמים</h4>
    </div>


    <?php
                $new_loop = new WP_Query(array(
                    'post_type' => 'post',
                    'category__in' => $main_category,
                    'posts_per_page' => 3,
                    'orderby' => 'comment_count'));
?>

    <?php if ($new_loop->have_posts()):
                    while ($new_loop->have_posts()):
                        $new_loop->the_post(); ?>

        <?php

                        $post_id = get_the_ID();
                        if (has_post_thumbnail()) {
                            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                        } else
                            $image_url[0] = ''; //default image?

?>

        <div class="img_info_box1">
            <div class="img_box7">
                <a href="<?php the_permalink(); ?>"><img src="<?php echo $image_url[0]; ?>" width="122" height="70"
                                                         alt=""></a>
            </div>
            <h2 class="hdclass"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

            <div class="text_box7"><?php echo get_post_excerpt(get_the_ID(), 40); ?></div>
            <ul class="uli">
                <li class="uli"><?php the_time('d/m/Y'); ?></li>
                <li class="uli no-margin no-bdr no-padding">תגובות <?php echo
get_comments_number($post_id); ?></li>
            </ul>
           <iframe style="width: 100px;height: 50px;" src="//www.facebook.com/plugins/like.php?locale=he_IL&href=<?php echo
urlencode(the_permalink()); ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
        </div>


    <?php endwhile;
                    else: ?>


    <?php endif; ?>
    <?php wp_reset_query(); ?>


</div>
<div class="column5_block5 no-bdr no-padding">
    <div class="info_heading info_heading1 clearfix">
        <h4 id="newPosts"  class="hdclass">פוסטים חדשים</h4>
    </div>

    <?php
                        $new_loop = new WP_Query(array(
                            'post_type' => 'post',
                            'category__in' => $main_category,
                            'posts_per_page' => 3));
?>

    <?php if ($new_loop->have_posts()):
                            while ($new_loop->have_posts()):
                                $new_loop->the_post(); ?>

        <?php

                                $post_id = get_the_ID();
                                if (has_post_thumbnail()) {
                                    $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                                    //print_r($image_url);
                                } else
                                    $image_url[0] = ''; //default image?

?>

        <div class="img_info_box2 clearfix">
            <div class="img_box5_out1">
                <div class="img_box9"><a href="<?php the_permalink(); ?>"><img src="<?php echo
$image_url[0]; ?>"
                                                                               width="123" height="85" alt=""></a></div>
            </div>
            <div class="img_box9_cont">
                <a href="<?php the_permalink(); ?>"><h2 class="hdclass"><?php the_title(); ?></h2></a>

                <div class="text_box9"><?php echo get_post_excerpt(get_the_ID(),
140); ?></div>
                <ul class="uli">
                    <li class="uli"><a href="<?php the_permalink(); ?>"> פורסם ב<?php the_time('d ב F, Y'); ?> </a></li>
                    <li class="uli no-bdr"><a
                            href="<?php the_permalink(); ?>">תגובות <?php echo
get_comments_number($post_id); ?></a>
                    </li>
                    <li class="uli no-bdr">
                    <!-- facebook -->
                    <iframe style="width: 100px;height: 50px;" src="//www.facebook.com/plugins/like.php?locale=he_IL&href=<?php echo
urlencode(the_permalink()); ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
                    </li>
                </ul>
            </div>
        </div>


    <?php endwhile;
                            else: ?>


    <?php endif; ?>
    <?php wp_reset_query(); ?>



    <div class="img_info_box2 clearfix">
        <img src="<?php bloginfo('template_directory'); ?>/img/pic9.png" width="430" height="87" alt="">
    </div>

</div>
<div class="column5_block3 clearfix">
    <div class="info_heading info_heading1 clearfix">
        <h4 id="updatedPosts" class="hdclass">בלוגים חדשים</h4>
    </div>

    <?php
                                $new_loop = new WP_Query(array(
                                    'post_type' => 'post',
                                    'category__in' => $main_category,
                                    'posts_per_page' => 5,
                                    'orderby' => 'comment_count'));

                                $counter = 0;

?>

    <?php if ($new_loop->have_posts()):
                                    while ($new_loop->have_posts()):
                                        $new_loop->the_post(); ?>

        <?php
                                        $counter++;
                                        $post_id = get_the_ID();
                                        if (has_post_thumbnail()) {
                                            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                                        } else
                                            $image_url[0] = ''; //default image?

?>

        <div class="img_info_box1">
            <div class="img_box7">
                <a href="<?php the_permalink(); ?>"><img src="<?php echo $image_url[0]; ?>" width="122" height="70"
                                                         alt=""></a>
            </div>
            <h2 class="hdclass"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

            <div class="text_box7"><?php echo get_post_excerpt(get_the_ID(), 40); ?></div>
            <ul class="uli">
                <li class="uli"><?php the_time('d/m/Y'); ?></li>
                <li class="uli no-margin no-bdr no-padding"> תגובות <?php echo
get_comments_number($post_id); ?></li>
            </ul>
           <iframe style="width: 100px;height: 50px;" src="//www.facebook.com/plugins/like.php?locale=he_IL&href=<?php echo
urlencode(the_permalink()); ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
        </div>

        <?php if ($counter == 3)
                                            break; ?>

    <?php endwhile;
                                    else: ?>


    <?php endif; ?>

    <?php //wp_reset_query(); ?>

</div>


<div class="column5_block5 no-bdr">
    <?php if ($new_loop->have_posts()):
                                            while ($new_loop->have_posts()):
                                                $new_loop->the_post(); ?>

        <?php
                                                $counter++;
                                                $post_id = get_the_ID();
                                                if (has_post_thumbnail()) {
                                                    $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                                                } else
                                                    $image_url[0] = ''; //default image?

?>
        <div class="img_info_box2 clearfix">
            <div class="img_box5_out1">
                <div class="img_box9"><a href="<?php the_permalink(); ?>"><img src="<?php echo
$image_url[0]; ?>"
                                                                               width="122" height="70" alt=""></a></div>
            </div>
            <div class="img_box9_cont">
                <h2 class="hdclass"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                <div class="text_box9"><?php echo get_post_excerpt(get_the_ID(),
140); ?></div>
                <ul class="uli">
                    <li class="uli"><a href="<?php the_permalink(); ?>"><?php the_time('d/m/Y'); ?></a></li>
                    <li class="uli no-bdr"><a href="<?php the_permalink(); ?>">
                            תגובות <?php echo get_comments_number($post_id); ?></a></li>
                    <li class="uli no-bdr"><iframe style="width: 100px;height: 50px;" src="//www.facebook.com/plugins/like.php?locale=he_IL&href=<?php echo
urlencode(the_permalink()); ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe></li>
                </ul>
            </div>
        </div>


    <?php endwhile;
                                            else: ?>


    <?php endif; ?>

    <?php wp_reset_query(); ?>


</div>

<div class="click_info1 no-bdr no-margin">
    <a href="#">רוצה לקבל את הניוזלטר שלנו ולהיות מעודכן לפני כולם! <i>לחץ כאן</i></a>
</div>

</div>
</div>
<div class="block1 block5">
    <h2 class="heading6 m-b1">המומחים של נומיינד</h2>
    <ul class="uli photo_galary clearfix">
        <li class="uli first"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic2.jpg" width="111"
                                               height="75" alt=""></a>החמומה םש
        </li>
        <li class="uli"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic3.jpg" width="111"
                                         height="75" alt=""></a>החמומה םש
        </li>
        <li class="uli"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic4.jpg" width="111"
                                         height="75" alt=""></a>החמומה םש
        </li>
        <li class="uli"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic5.jpg" width="111"
                                         height="75" alt=""></a>החמומה םש
        </li>
        <li class="uli"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic6.jpg" width="111"
                                         height="75" alt=""></a>החמומה םש
        </li>
        <li class="uli"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic7.jpg" width="111"
                                         height="75" alt=""></a>החמומה םש
        </li>
        <li class="uli last"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic8.jpg" width="111"
                                              height="75" alt=""></a>החמומה םש
        </li>
    </ul>
    <!--<div class="adsense1"><img src="<?php bloginfo('template_directory'); ?>/img/ad1.png" width="964" height="91" alt=""></div>-->

</div>
<script>
    $('.uli2').bxSlider({
        minSlides: 3,
        maxSlides: 4,
        slideWidth: 86,
        slideMargin: 10,
        //wrapperClass: 'slide_pager'
    });
    function changeIt(n, lin, tit) {
        document.getElementById("view_im").src = n;
        $("#view_im_a").attr("href", lin);
        $("#view_im_a").attr("title", tit);
        $("#view_text a").attr("href", lin);
        $("#view_text a").attr("title", tit);
        $("#view_text a").html(tit);
        // $('#view_im_a')= $('.slide_pager ul li a img').data('link');
        //$('#view_im_a')= $('.slide_pager ul li a img').data('title');
    }
    ;


</script>