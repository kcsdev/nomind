
                    <div class="column4_block1 clearfix">
                    	<div class="info_heading info_heading3  clearfix">
                            <h4 class="hdclass">עלה השבוע</h4>
                        </div>
                        
                        <?php foreach($recent_posts as $rp) { ?>
                        
                        
                        <div class="comments_box1">
                        	<a href="<?php echo get_permalink($rp->ID); ?>"><h2 class="hdclass"><?php echo $rp->post_title; ?></h2></a>
                        	<div class="">
                            	<a style="text-decoration: none !important;" href="<?php echo get_permalink($rp->ID); ?>">
                                <div class="text_box11" style="float: none;"><?php echo $rp->post_excerpt; ?></div></a>
                                <a href="<?php echo get_permalink($rp->ID); ?>">להמשך</a>
                            </div>
                        </div>
                        
                        
                        <?php } ?>

                    </div>
                    
                    <div class="writers_info writers_info1 nowidth clearfix no-margin">
                        <div class="info_heading info_heading3 m-b3  clearfix">
                            <h4 class="hdclass">כותבי הבית</h4>
                        </div>
                        <?php 
                        $i=1;
                        foreach($authors as $a) { 
                                $user = get_userdata($a);
                                if($i % 3 == 1) $margin = 'no-mg-r'; else $margin = '';
                            ?>
                        <div class="img_box3 margin1 <?php echo $margin; ?>">
                            <a href="#"><img src="<?php echo get_cupp_meta( $user->id, 86 ); ?>" width="86" height="68" alt=""></a>
                            <h5 class="hdclass"><?php echo $user->display_name; ?></h5>
                        </div>		
                        <?php $i++; } ?>	
                    </div>
            		
                    <div class="middle_column_box4">
                        <div class="box_left">
                        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/box_pic2.jpg" alt=""></a>
                        </div>
                        <div class="box_right">
                        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/box_pic1.jpg" alt=""></a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    
                    <!-- forums -->
                     <?php include get_stylesheet_directory() .'/inc/forums/featured-forums.php' ; ?>
                    
                 <!-- blogim -->
                    <?php include get_stylesheet_directory() .'/template-parts/blogim/widgets/blogim.php' ; ?>
                    
                   <div class="middle_column_box5">
                        	    <?php include(get_template_directory() . '/inc/nomind-tv/nomind-tv-widget.php'); ?>
                   </div>
                    <div class="social_block1 no-margin">
                    	<iframe src="http://www.facebook.com/plugins/likebox.php?locale=he_IL&href=https%3A%2F%2Fwww.facebook.com%2FNomind.israel&amp;width&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:290px;" allowTransparency="true"></iframe>
						
                    </div>
                    <div class="">
                    	<div class="info_heading info_heading3 clearfix">
                            <h4 class="hdclass">תגיות</h4>
                        </div>
                        <?php
                        $args= array(
                            'words_number' => '20',
                        'words_color' => '#000',
                        'hover_color' => '#ccc',
                        'smallest_font' => 7 ,
                        'largest_font' => 14);
                       echo categorized_tag_cloud($args);?>
                    </div>
