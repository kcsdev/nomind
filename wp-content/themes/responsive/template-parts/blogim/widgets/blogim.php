
<div class="middle_column_box2 middle_column_box3">
                    	<div class="info_heading info_heading1 clearfix">
                            <h4 class="hdclass">בלוגים</h4>
                            <ul class="uli">
                                <li class="uli no-bdr"><a href="<?=get_site_url().'/category/blogs/';?>">חדשים</a></li>
                                <li class="uli"><a href="<?=get_site_url().'/category/blogs/';?>">נצפים ביותר</a></li>
                                <li class="uli"><a href="<?=get_site_url().'/category/blogs/';?>">לכל הבלוגים</a></li>
                            </ul>
                        </div>
                        
        <? 
        wp_reset_query();
        $blog_query = new WP_Query( array( 'post_type' => 'post', 'posts_per_page '=>3, 'order' => 'rand', 'post_status' => 'publish' ) );
	       $counter = 0;
        	//die(var_dump($blog_query->have_posts()));
        	if( $blog_query->have_posts() ) :
        
    		while( $blog_query->have_posts() ) : $blog_query->the_post();
                $authorID = get_the_author_ID();
                $author = get_userdata($authorID);
                $counter++;
                
    			?>
    
    		       <div class="img_box5_out <?= $counter == 1 ? 'no-mg-r' : '' ?>">
                            <div class="img_box5">
                            	<a href="<?=get_blog_author_page_url($authorID)?>"><img src="<?php echo get_cupp_meta( $authorID, 86 ); ?>" width="86" height="68" alt="" scale="0"></a>
                            	<h4 class="hdclass"><?= $author->first_name.' '.$author->last_name; ?></h4>
                            </div>
                            	<h3 class="hdclass"><?php the_time('d.m.Y'); ?></h3>
                                <div class="text_box4"><?the_title();?></div>
                            </div>
    
        		<?php
                    if($counter == 3) break;
    		endwhile;
    	
    		endif;

	
	wp_reset_postdata();
    wp_reset_query();
	?>
    
  </div>