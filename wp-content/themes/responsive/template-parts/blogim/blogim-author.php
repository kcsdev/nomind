            <div class="blogim-user-author">
                <?php
                    $user_id = get_the_author_meta('ID');
                    $author_display_name = get_the_author_meta('display_name');
                    $author_desc = get_the_author_meta('description');
                    $author_phone = get_the_author_meta('dbem_phone');
                    $author_email = get_the_author_meta('user_email');
                    $author_url = get_the_author_meta('user_url');
                ?>
                <div class="blogim-user-avatar">
                    <?php echo get_avatar($user_id); ?>
                </div>
                <h4 class="blogim-user-name">
                    <?php echo $author_display_name; ?>
                </h4>
                <p class="blogim-user-desc">
                    <?php echo $author_desc; ?>
                </p>
                <div class="blogim-user-contact">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/author_phone.png" alt="" />
                    <a href="tel:<?php echo $author_phone; ?>">הצג מספר</a>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/author_email.png" alt="" />
                    <a href="mailto:<?php echo $author_email; ?>">צור קשר</a>
                    <img src="<?php echo get_template_directory_uri(); ?>/img/author_website.png" alt="" />
                    <a href="<?php echo $author_url; ?>" target="_blank">לאתר</a>
                </div>
            </div>