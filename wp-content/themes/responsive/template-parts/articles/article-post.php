            <div class="block1 block2 clearfix">
                 <?php include get_stylesheet_directory() .'/template-parts/blogim/blogim-sidebar.php' ; ?> 
                <div class="column5  no-bdr">
                	<div class="column5_block1 no-margin clearfix">
                    <?php $category = get_the_category(); ?>
                    	<h2 class=" hdclass heading8 f_size20"><del><?php echo $main_category->name ?></del></h2>
                        <div class="text_block1">
                        	<h4 class="hdclass"><?php the_title(); ?></h4>
                        	<div class="date_text"><?php the_date('d.m.Y', '', '', TRUE); ?></div>
                            <div class="text_box17"><?php echo get_the_excerpt(); ?></div>
                        </div>
                    </div>
                    <div class="social_block2 clearfix">
                        <ul class="social_link2">
			    <li style="height: 20px; margin:0"><iframe style="width: 85px;height: 20px;" src="//www.facebook.com/plugins/like.php?locale=he_IL&href=<?php echo urlencode(get_permalink());?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe></li>
                            <li style="width: 65px;" ><div class="g-plusone" data-size="medium"></div></li>
                            <?php include get_stylesheet_directory().'/inc/post-buttons-upper.php';?>
                        </ul>
                        <div class="social_block2_right_text">
                        	<i> <?php the_author(); ?> |&nbsp;</i><?php the_date('d.m.Y', '', '', TRUE); ?>
                        </div>
                    </div>
                    <div class="social_block2 m-b6 clearfix">
                        <div class="social_block2_right_text">
                        	<?php the_tags('<i>מילות מפתח:</i> '); ?>
                        </div>
                    </div>
                    <div class="img_box6_view">
                        <div class="img_box6 no-margin"><?php the_post_thumbnail('medium'); ?>
                        <!--<img src="<?php bloginfo('template_directory'); ?>/img/info3_pic3.jpg" width="435" height="258" alt="">--></div>
                        <div class="img_box6_cont"><?php the_post_thumbnail_caption(); ?></div>
                    </div>
                    <div class="text_box3"><?php the_content(); ?>
                    </div>
                    <div class="spacer"></div>	
                    <div class="text_block2">
                    	<h4 class="hdclass">שם הכותב: <del><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>?id=<?php echo get_the_author_meta( 'ID' );?>"><?php the_author_meta( 'display_name' ); ?></a></del></h4>
                    	<h4 class="hdclass">אודות הכותב:<i> </i></h4>
                        <div class="text_box3"><?php echo get_the_author_meta( 'description' ); ?></div>
                    </div>
                    <div class="column5_block2 clearfix">
                    	<div class="text_block2">
                            <a href="<?= get_blog_author_page_url(get_the_author_meta('ID'));?>"><h4 class="hdclass">מידע נוסף על כותב המאמר</h4></a>
                        </div>
                    </div>
                    <div class="column5_block2 clearfix">
                    	<ul class="uli">
                        	<li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon9.jpg" width="20" height="23" alt=""><a href="#">הצג טלפון</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon10.jpg" width="20" height="23" alt=""><a href="#">צור קשר</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon11.jpg" width="22" height="23" alt=""><a href="#">לאתר</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon12.jpg" width="22" height="23" alt=""><a href="#">המאמרים שלי</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon13.jpg" width="27" height="23" alt=""><a href="#">הבלוגים שלי</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon14.jpg" width="19" height="23" alt=""><a href="#">הפעילויות שלי</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon15.jpg" width="21" height="23" alt=""><a href="#">הוידאו שלי</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon16.jpg" width="21" height="23" alt=""><a href="#">הקופונים שלי</a></li>
                        </ul>
                    </div>
                    
                    
                    
                    <div class="click_info1">
                    	<a href="#">רוצה לקבל את הניוזלטר שלנו ולהיות מעודכן לפני כולם!&nbsp;&nbsp;<i> לחץ כאן</i></a>
                    </div>

                    <div class="column5_social_link clearfix">
                    	<ul class="uli">
                            <?php include get_stylesheet_directory().'/inc/post-buttons.php';?>
                            
                        </ul>
                    </div>
                    <?php echo do_shortcode('[fbcomments]'); ?>
                    <div class="spacer"></div>
                    <h2 class=" hdclass heading8 heading19 f_size2">כתבות נוספות שעשויות לעניין אותך</h2>
                    <div class="spacer"></div>
                    <div class="column5_block3 clearfix">
                        <?php $args1 = array(
                            'numberposts' => 3,
                            'offset' => 0,
                            'orderby' => 'rand',
                            'order' => 'DESC',
                            'exclude' => get_the_ID(),
                            'post_type' => 'articles',
                            'post_status' => 'publish',
                            'parent' => $main_category->term_id,
                            'suppress_filters' => true );
                            
                            $recent_posts = wp_get_recent_posts( $args1, OBJECT );
                        ?>  

                        <?php foreach($recent_posts as $rp) { 
                                    $authors[] = $rp->post_author;
                                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($rp->ID), 'thumbnail' );
                                    $url = $thumb['0']; ?>

                    	<div class="img_info_box1">
                        	<div class="img_box7">
                            	<a href="<?php echo get_permalink($rp->ID); ?>">
                                    <img src="<?php echo $url; ?>" width="122" height="70" alt=""></a>
                            </div>
                            <a href="<?php echo get_permalink($rp->ID); ?>"><h2 class="hdclass no-margin"><?php echo short_title($rp->post_title, '...', 8); ?></h2>
                            <div class="text_box7"><?php echo $rp->post_excerpt; ?></div></a>
 							<ul class="uli">
                            	<li><?php echo date('d.m.Y', strtotime($rp->post_date)); ?></li>
                                <li class="no-margin no-bdr no-padding">תגובות <?php echo $rp->comment_count; ?></li>
                            </ul>
                            
                            <iframe style="width: 100px;height: 50px;" src="//www.facebook.com/plugins/like.php?locale=he_IL&href=<?php echo urlencode(get_permalink($rp->ID));?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
                        </div>
                        
                        <?php } ?>

                    </div>
                    <div class="column5_block4">
                    	<a class="click2 click9" href="#"><img src="<?php bloginfo('template_directory'); ?>/img/anchor_icon1.png" width="14" height="14" alt="">לקריאת כל התגובות ברצף</a>
                        <?php $comments = get_comments( array ( 'post_id' => get_the_ID(),  'status' => 'approve', 'post_type' => 'articles'  ) );
                              $i=1;
                              //die(var_dump(get_the_ID()));
                              foreach($comments as $c) { ?>
                        
                        <h4 class="hdclass"><?php echo $i.'. '.$c->comment_content; ?></h4>
                        <div class="text_box8"><?php echo $c->comment_author.', '.date('d/m/Y H:i', strtotime($c->comment_date)); ?> </div>
                                                
                        <?php } ?>
                    </div>
                </div>
                
                    <?php include get_stylesheet_directory() .'/template-parts/articles/articles-left-sidebar.php' ; ?>
                    
             </div>
             <div class="block1 block5">
             <h2 class="heading6  m-b1">המומחים של נומיינד</h2>
               <ul class="uli photo_galary clearfix">
                   <li class="first"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic2.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic3.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic4.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic5.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic6.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic7.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li class="last"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic8.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
               </ul>
               <!--<div class="adsense1"><img src="<?php bloginfo('template_directory'); ?>/img/ad1.png" width="964" height="91" alt=""></div>-->
               
            </div>