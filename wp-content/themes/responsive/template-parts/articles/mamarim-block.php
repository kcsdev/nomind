<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.bxslider.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.bxslider.js"></script>
<link href='<?php bloginfo('template_directory'); ?>/css/jquery.bxslider.css' rel='stylesheet' type='text/css'>
<link href='<?php bloginfo('template_directory'); ?>/css/vit.css' rel='stylesheet' type='text/css'>            
<?php
$cat_name = get_cat_name($id);

function do_comment_modal($post_id)
{
    include(get_template_directory() . '/inc/comment-link.php' );  // note: $title will be a local variable
}
?>

            <div class="block1 block2 clearfix">
             
              <?  include(get_template_directory() . '/template-parts/articles/articles-right-sidebar.php' ); ?>
           
           
                 
                <div class="column5  no-bdr">
                	<div class="column5_block1 img_info_box2 clearfix">
                    	<h2 class=" hdclass heading8 heading7"><del>מאמרים</del></h2>
   						<div class="img_box6 no-margin">
                           <!-- main image -->  
                            <?php
                        $category = get_the_category();
                        $main_category = get_category($category[0]);
                        $new_loop = new WP_Query(array(
                          'numberposts' => 1,
                            'offset' => 0,
                            'category' => $id,
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'exclude' => get_the_ID(),
                            'post_type' => 'articles',
                            'post_status' => 'publish',
            ));
            ?>

            <?php if ($new_loop->have_posts()) : while ($new_loop->have_posts()) :
            $new_loop->the_post(); ?>

            <?php
            if (has_post_thumbnail()) {
                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                $main_img =  '<a id="view_im_a" href="' . get_permalink() . '" title="' . the_title_attribute('echo=0').'"><img id="view_im" src="'. $image_url[0] . '" width="435" height="258" alt="'.the_title_attribute('echo=0').'"></a>';
            }
            ?>
            
             <?php endwhile;
        else: ?>
            <?php $main_img = ''; ?>
    <?php endif; ?>
    
    <?php wp_reset_query(); ?>
                           
       <?php echo $main_img;?>
       
       </div>
                
         <div class="img_box6_cont" id="view_text">
                <a href="<?php the_permalink('echo=0'); ?>"
                   title="<?php echo the_title_attribute('echo=0'); ?> "><?php the_title(); ?></a>
            </div>
        <div class="slide_pager clearfix">
         <ul class="uli2">
                   <?php if ($new_loop->have_posts()) : while ($new_loop->have_posts()) : $new_loop->the_post(); ?>
                <?php
                if (has_post_thumbnail()) {
                    $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                    echo '<li class="uli"><a href="javascript:void(0);" title="' . the_title_attribute('echo=0') . '">';
                    echo '<img onclick="';?>changeIt(this.src,'<?php the_permalink(); ?>','<?php the_title(); ?>');" <?php echo 'data-link="" data-title="' . get_the_title() . '" src="' . $image_url[0] . '" alt="">';
                    echo '</a></li>';
                }
                ?>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            </ul>
            </div>
        </div>
        
        <script>
            $('.uli2').bxSlider({
                minSlides: 3,
                maxSlides: 4,
                slideWidth: 170,
                slideMargin: 10,
                //wrapperClass: 'slide_pager'
            });
            function changeIt(n, lin, tit) {
                document.getElementById("view_im").src = n;
                $("#view_im_a").attr("href", lin);
                $("#view_im_a").attr("title", tit);
                $("#view_text a").attr("href", lin);
                $("#view_text a").attr("title", tit);
                $("#view_text a").html(tit);
                // $('#view_im_a')= $('.slide_pager ul li a img').data('link');
                //$('#view_im_a')= $('.slide_pager ul li a img').data('title');
            }
            ;
        </script>
                    
	<?php
	global $wp_query, $paged;
    
    
 if(isset($_GET['search']))
                $search_in_progress = true;
             
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
             
    if(isset($_GET['new']))
    {   
        $args = array( 'post_type' => 'articles', 
                                        'posts_per_page' => 2,  
                                        'paged' =>$paged,
                                        'orderby' => 'date',
                                        'order' => 'DESC');

    }
    elseif(isset($_GET['popular']))
    {
        $args = array( 'post_type' => 'articles', 
                                        'posts_per_page' => 2,  
                                        'paged' =>$paged,
                                        'orderby' => 'date',
                                        'order' => 'DESC');    
    }
    else
    {
        $args = array( 'post_type' => 'articles', 
                                        'posts_per_page' => 2,  
                                        'paged' =>$paged,
                                        'orderby' => 'date',
                                        'order' => 'DESC');
    }
    
                                        
                                                     
             
	$blog_query = new WP_Query( $args );
                                        
    $max_page = $blog_query->max_num_pages;
 
	$wp_query = null;
	$wp_query = $blog_query;
    //die(var_dump($blog_query->have_posts()));
    
	if( $blog_query->have_posts() ) :

		while( $blog_query->have_posts() ) : $blog_query->the_post();
            $authors[] = get_the_author_ID();
			?>

	    <?php if (has_post_thumbnail()) {
            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
        } else
            $image_url[0] = ''; //default image?
        ?>

        <div class="column5_block5 spacer1">
	<div class="img_info_box2 no-bdr no-pad-b column5_block5 clearfix">
    	<div class="img_box5_out1 img_box5_out3">
        	<div class="img_box9 info_pic"><a href="<?php the_permalink() ?>" class="nostyle"><img src="<?php echo $image_url[0]; ?>" width="100" height="100"
                                                         alt=""></a></a></div>
            <?php if ( is_user_logged_in() ) { ?>
            	<?php wpfp_link() ?>
            <?php } ?>
        </div>
    	<div class="img_box9_cont">
        	<h2 class="hd2 no-margin"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
            <div class="small_text3"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>?id=<?php echo get_the_author_meta( 'ID' );?>"><?php the_author_meta( 'display_name' ); ?></a></div>
        	<div class="text_box9 bottom_border"><?php the_excerpt(); ?></div>
            <div class="small_text3">
            	<?php the_tags('<i>מילות מפתח:</i> '); ?>
            </div>
            
            <ul class="uli">
                <li class="uli no-bdr" style="width: 150px;"><a class="color1" href="<?php the_permalink(); ?>"> פורסם ב <?php the_time('d ב F, Y'); ?> </a></li>
                <li class="uli no-bdr"><a class="color1" href="<?php comments_link(); ?> ">&nbsp; &gt;&gt; תובוגת <?php wp_count_comments(); ?> &nbsp;</a></li>
                <li class="uli no-bdr" style="width: 100px;"><a class="color1" href="#comment-modal-<?php echo get_the_ID(); ?>" data-toggle="modal"> &gt;&gt; הוסף תגובה</a></li>
            </ul> 
        </div>
    </div>
</div>
<?php do_comment_modal(get_the_ID()); ?>

		
		<?php
		endwhile; ?>

	   <div class="spacer"></div>
           <div class="more_view_list clearfix">
                                               
                            
        <?php 
        
        $big = 999999999;
        $args = array(
            	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            	'format'             => '/page/%#%',
            	'total'              => $max_page,
            	'current'            => $paged,
            	'show_all'           => False,
            	'end_size'           => 1,
            	'mid_size'           => 2,
            	'prev_next'          => False,
            	'prev_text'          => 'קודם',
            	'next_text'          => 'הבא',
            	'type'               => 'array',
            	'add_args'           => True,
            	'add_fragment'       => '',
            	'before_page_number' => '',
            	'after_page_number'  => ''
            ); ?>
        
         
        
        <? $links = paginate_links( $args );
         //echo get_next_posts_link( 'אבה', $the_query->max_num_pages ); 
        
        //echo get_previous_posts_link( 'Newer Entries' );
        
         ?>
         
         <ul class="uli">
            <? foreach($links as $link) : ?>
                <li class="uli">
                <? echo $link; ?>
                </li>
            <? endforeach; ?>
         </ul>
         
          <?= get_next_posts_link( 'הבא', $blog_query->max_num_pages ); ?>
          
          <?= get_previous_posts_link( 'קודם' ); ?>
    </div>
     
 
    	
	<?

	else : ?>
        <div class="post_block">
        	<div class="post_heading clearfix">
            	<h2 class="hdclass">לא נמצאו תוצאות</h2>
            </div>
        </div>
	<?endif;
	$wp_query = $temp_query;
	wp_reset_postdata();
	?>
                    


                    

                    <div class="spacer"></div>
                    <div class="click_info1 no-bdr no-margin">
                    	<a href="#">רוצה לקבל את הניוזלטר שלנו ולהיות מעודכן לפני כולם!&nbsp;&nbsp;<i> לחץ כאן</i></a>
                    </div>
                    <div class="spacer"></div>
                    <h2 class=" hdclass heading8">כתבות נוספות שעשויות לעניין אותך</h2>
                    <div class="spacer"></div>
                    <div class="column5_block3 no-bdr no-margin clearfix">
                        <?php $args1 = array(
                            'numberposts' => 3,
                            'offset' => 0,
                            'orderby' => 'rand',
                            'order' => 'DESC',
                            'post_type' => 'articles',
                            'post_status' => 'publish',
                            'suppress_filters' => true );
                            
                            $recent_posts = wp_get_recent_posts( $args1, OBJECT );
                        ?>                    	
                        
                        <?php foreach($recent_posts as $rp) { 
                                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($rp->ID), 'thumbnail' );
                                    $url = $thumb['0']; ?>                        
                        <div class="img_info_box1">
                        	<div class="img_box7">
                            	<a href="<?php echo get_permalink( $rp->ID ); ?>">
                                <img src="<?php echo $url; ?>" width="122" height="70" alt=""></a>
                            </div>
                            <a href="<?php echo get_permalink( $rp->ID ); ?>">
                            <h2 class="hdclass no-margin"><?php echo short_title($rp->post_title, '...', 8); ?></h2>
                            <div class="text_box7"><?php echo $rp->post_excerpt; ?></div></a>
                        </div>

                        <?php } ?>

                    </div>
                </div>
                
                <?php include get_stylesheet_directory() .'/template-parts/articles/articles-left-sidebar.php' ; ?>                    
                    
                    
             </div>
             <div class="block1 block5">
             <h2 class="heading6  m-b1">המומחים של נומיינד</h2>
               <ul class="uli photo_galary clearfix">
                   <li class="first"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic2.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic3.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic4.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic5.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic6.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic7.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li class="last"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic8.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
               </ul>
               <!--<div class="adsense1"><img src="<?php bloginfo('template_directory'); ?>/img/ad1.png" width="964" height="91" alt=""></div>-->
               
             </div>
             
             