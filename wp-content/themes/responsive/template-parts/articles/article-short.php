


 <?php if (has_post_thumbnail()) {
            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail');
        } else
            $image_url[0] = ''; //default image?
        ?>

<div class="column5_block5 spacer1">
	<div class="img_info_box2 no-bdr no-pad-b column5_block5 clearfix">
    	<div class="img_box5_out1 img_box5_out3">
        	<div class="img_box9 info_pic"><a href="<?php the_permalink() ?>" class="nostyle"><img src="<?php echo $image_url[0]; ?>" width="100" height="100"
                                                         alt=""></a></a></div>
            <?php if ( is_user_logged_in() ) { ?>
            <a class="bttn1" href="#">הוסף למזוודה שלי</a>
            <?php } ?>
        </div>
    	<div class="img_box9_cont">
        	<h2 class="hdclass hd2 no-margin"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
            <div class="small_text3"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>?id=<?php echo get_the_author_meta( 'ID' );?>"><?php the_author_meta( 'display_name' ); ?></a></div>
        	<div class="text_box9 bottom_border"><?php the_excerpt(); ?></div>
            <div class="small_text3">
            	<?php the_tags('<i>מילות מפתח:</i> '); ?>
            </div>
            
            <ul class="uli">
                <li class="uli no-bdr" style="width: 150px;"><a class="color1" href="<?php the_permalink(); ?>"> פורסם ב <?php the_time('d ב F, Y'); ?> </a></li>
                <li class="uli no-bdr"><a class="color1" href="<?php comments_link(); ?> ">&nbsp; &gt;&gt; תובוגת <?php wp_count_comments(); ?> &nbsp;</a></li>
                <li class="uli no-bdr" style="width: 100px;"><a class="color1" href="#comment-modal-<?php echo get_the_ID(); ?>" data-toggle="modal"> &gt;&gt; הוסף תגובה</a></li>
            </ul> 
        </div>
    </div>
</div>
<?php do_comment_modal(get_the_ID()); ?>

