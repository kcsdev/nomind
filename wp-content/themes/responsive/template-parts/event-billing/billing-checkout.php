<h3 class="big">מחירים לפסטיבל נומיינד אביב בים המלח - בחר מלון</h3>
<ul class="checkout-items">
    <li class="checkout-single-item">
        <img class="checkout-item-img" src="<?php echo get_stylesheet_directory_uri(); ?>/img/checkout-test.jpg" alt="" />
        <span class="checkout-item-area blue-text">ים המלח</span>
        <b class="gray">מלון צל הרים</b>
        <span class="checkout-item-details">
            <span class="checkout-item-date">24.04.2013 - 20.04.2013</span>
            <span class="checkout-item-detail">2 לילות</span>
            <span class="checkout-item-detail">חצי פנסיון</span>
        </span>
        <span class="checkout-item-price blue-text">1,00 ש"ח לבנאדם</span>
        <button class="billing-green-btn">הזמן</button>
    </li>
    <li class="checkout-single-item">
        <img class="checkout-item-img" src="<?php echo get_stylesheet_directory_uri(); ?>/img/checkout-test.jpg" alt="" />
        <span class="checkout-item-area blue-text">ים המלח</span>
        <b class="gray">מלון צל הרים</b>
        <span class="checkout-item-details">
            <span class="checkout-item-date">24.04.2013 - 20.04.2013</span>
            <span class="checkout-item-detail">2 לילות</span>
            <span class="checkout-item-detail">חצי פנסיון</span>
        </span>
        <span class="checkout-item-price blue-text">1,00 ש"ח לבנאדם</span>
        <button class="billing-green-btn">הזמן</button>
    </li>
    <li class="checkout-single-item">
        <img class="checkout-item-img" src="<?php echo get_stylesheet_directory_uri(); ?>/img/checkout-test.jpg" alt="" />
        <span class="checkout-item-area blue-text">ים המלח</span>
        <b class="gray">מלון צל הרים</b>
        <span class="checkout-item-details">
            <span class="checkout-item-date">24.04.2013 - 20.04.2013</span>
            <span class="checkout-item-detail">2 לילות</span>
            <span class="checkout-item-detail">חצי פנסיון</span>
        </span>
        <span class="checkout-item-price blue-text">1,00 ש"ח לבנאדם</span>
        <button class="billing-green-btn">הזמן</button>
    </li>
</ul>