<?php $sum = 100; ?>
<form action="/" method="post" class="billing-info-form">
    <div class="block1 block2 block8 clearfix">
        <div class="order-block-title"><h2>פרטי המזמין</h2></div>
        <div class="billing-summary">
            הסכום הכולל לתשלום -
            <?php echo $sum; ?> &#8362;
        </div>
        <div class="have-acc-right payment">
            <label class="p_r2"><sup>*</sup> שם פרטי: </label>
            <input name="firstName" id="firstName" type="text" class="acc-field pay" value="">
            <div class="clear"></div>

            <label class="p_r2"><sup>*</sup> שם משפחה:</label>
            <input name="lastName" id="lastName" type="text" class="acc-field pay" value="">
            <div class="clear"></div>

            <label class="p_r2"><sup>*</sup> רחוב/מספר:</label>
            <input name="street" id="street" type="text" class="acc-field pay" value="">
            <div class="clear"></div>

            <label class="p_r2"><sup>*</sup> עיר/מיקוד:</label>
            <input name="city" id="city" type="text" class="acc-field pay" value="">
            <div class="clear"></div>

            <label class="p_r2"><sup>*</sup> אימייל:</label>
            <input name="email" id="email" type="text" class="acc-field pay" value="">
            <div class="clear"></div>
        </div>
        <div class="have-acc-right payment">
            <label class="p_r2"> טלפון/נייד:</label>
            <input name="phone" id="phone" type="text" class="acc-field pay" value="">
            <div class="clear"></div>

            <label class="p_r2"> תאריך לידה:</label>
            <select>
                <!--<input name="d-->
            </select>
            <div class="clear"></div>

            <label class="p_r2">סוכן מטפל:</label>
            <input name="agent" id="agent" type="text" class="acc-field pay" value="">
            <div class="clear"></div>

            <label class="p_r2">שם על החשבונית:</label>
            <input name="billingName" id="billingName" type="text" class="acc-field pay" value="">
            <div class="clear"></div>

            <label class="p_r2">הערות לקוח:</label>
            <textarea name="clientComments" id="clientComments" class="acc-textarea pay"></textarea>
            <div class="clear"></div>
        </div>
    </div>

    <div class="block1 block2 block8 clearfix">
        <div class="billing-info-bottom">
            <button class="billing-green-btn" type="submit">בצע את ההזמנה</button>
            <div class="billing-info-agreements">
                <p>
                    <input name="terms-of-use" type="checkbox" value="1" />
                    <span>הנני מאשר את תנאי ההזמנהו - תנאי דמי ביטול/שינוי הכלולים בעסקה זו
    </span>
                </p>
                <p>
                    <input name="mailing-agreement" type="checkbox" value="1" />
                    <span>הנני מעוניין לקבל דיוורים בדואר אלקטרוני בנושא מבצעים, הטבות ומידע</span>
                </p>
            </div>
        </div>
    </div>
</form>