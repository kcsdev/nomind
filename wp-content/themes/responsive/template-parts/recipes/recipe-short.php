<div class="column5_block5 spacer1">
	<div class="img_info_box2 no-bdr no-pad-b column5_block5 clearfix">
    	<div class="img_box5_out1 img_box5_out3">
        	<div class="img_box9 info_pic"><a href="<?php the_permalink() ?>" class="nostyle"><?php the_post_thumbnail(); ?></a></div>
            <?php if ( is_user_logged_in() ) { ?>
            <a class="bttn1" href="#">הוסף למזוודה שלי</a>
            <?php } ?>
        </div>
    	<div class="img_box9_cont">
        	<h2 class="hdclass hd2 no-margin"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
            <div class="small_text3"><a href="<?php the_author_link(); ?>"><?php the_author(); ?></a></div>
        	<div class="text_box9 bottom_border"><?php the_excerpt(); ?></div>
            <div class="small_text3">
            	<?php the_tags('<i>מילות מפתח:</i> '); ?>
            </div>
            
            <ul class="uli">
                <li class="uli no-bdr"><a class="color1" href="#">פורסם ביום ראשון 9 בדצמבר 2013</a></li>
                <li class="uli no-bdr"><a class="color1" href="#">&nbsp; &gt;&gt; תובוגת <?php wp_count_comments(); ?> &nbsp;</a></li>
                <li class="uli no-bdr"><a class="color1" href="#"> &gt;&gt; הוסף תגובה</a></li>
            </ul> 
        </div>
    </div>
</div>