            <div class="block1 block2 clearfix">
             	<div class="column3">
                  <?php echo getUserCart(); ?>
                     <!-- login control -->
                    <?php include(get_template_directory() . '/inc/login-control.php'); ?>
                    <?php $category = get_the_category();
                                                     
                            $main_category = get_category($category[0]);
                            
                            if($main_category->parent != 0)
                                $main_category = get_category($main_category->parent)
                            
                            ?>
                            
                    <div class="right_list_bar1">
                    	<h4 class="hdclass hd1"><?php echo $main_category->name ?></h4>
                    	<div class="right_list_bar1_cont">
                        	<ul class="right_list_bar1_cont_box">	
                                <?php
                                $args = array(
                                  'orderby' => 'rand',
                                  'parent' => $main_category->term_id
                                  );
                                $catRecipes = get_terms( 'category', $args );
                                //die(var_dump($signs[0]));
                                foreach ( $catRecipes as $cr ) {
                                    echo '<li><a href="' . get_term_link( $cr, 'category_recipes' ) . '">' . $cr->name . '</a></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="adsense5"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/ad4.png" width="159" height="212" alt=""></a></div>
                    <div class="adsense2 adsense5 adsense6">
                        <a href="#"><img height="601" width="160" alt="" src="<?php bloginfo('template_directory'); ?>/img/ad2.jpg"></a>
                    </div>
                    <div class="adsense5"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/ad4.png" width="159" height="212" alt=""></a></div>
                    <div class="adsense5 adsense6">
                        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/ad3.jpg" width="160" height="600" alt=""></a>
                    </div>
                	<div class="adsense5"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/ad4.png" width="159" height="212" alt=""></a></div>
                </div> 
                <div class="column5  no-bdr">
                	<div class="column5_block1 no-margin clearfix">
                    <?php $category = get_the_category(); ?>
                    	<h2 class=" hdclass heading8 f_size20"><del><?php echo $main_category->name ?></del></h2>
                        <div class="text_block1">
                        	<h4 class="hdclass"><?php the_title(); ?></h4>
                        	<div class="date_text"><?php the_date('d.m.Y', '', '', TRUE); ?></div>
                            <div class="text_box17"><?php echo get_the_excerpt(); ?></div>
                        </div>
                    </div>
                    <div class="social_block2 clearfix">
                        <ul class="social_link2">
							<li style="height: 20px; margin:0"><iframe style="width: 85px;height: 20px;" src="//www.facebook.com/plugins/like.php?locale=he_IL&href=<?php echo urlencode(get_permalink());?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe></li>
                            <li style="width: 65px;" ><div class="g-plusone" data-size="medium"></div></li>
                             <?php include get_stylesheet_directory().'/inc/post-buttons-upper.php';?>
                        </ul>
                        <div class="social_block2_right_text">
                        	<i> <?php the_author(); ?> |&nbsp;</i><?php the_date('d.m.Y', '', '', TRUE); ?>
                        </div>
                    </div>
                    <div class="social_block2 m-b6 clearfix">
                        <div class="social_block2_right_text">
                        	<?php the_tags('<i>מילות מפתח:</i> '); ?>
                        </div>
                    </div>
                    <div class="img_box6_view">
                        <div class="img_box6 no-margin"><?php the_post_thumbnail('medium'); ?>
                        <!--<img src="<?php bloginfo('template_directory'); ?>/img/info3_pic3.jpg" width="435" height="258" alt="">--></div>
                        <div class="img_box6_cont"><?php the_post_thumbnail_caption(); ?></div>
                    </div>
                    <div class="text_box3"><?php the_content(); ?>
                    </div>
                    <div class="spacer"></div>	
                    <div class="text_block2">
                    	<h4 class="hdclass">שם הכותב: <del><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>?id=<?php echo get_the_author_meta( 'ID' );?>"><?php the_author_meta( 'display_name' ); ?></a></del></h4>
                    	<h4 class="hdclass">אודות הכותב:<i> </i></h4>
                        <div class="text_box3"><?php echo get_the_author_meta( 'description' ); ?></div>
                    </div>
                    <div class="column5_block2 clearfix">
                    	<div class="text_block2">
                            <a href="<?php the_author_link(); ?>"><h4 class="hdclass">מידע נוסף על כותב המאמר</h4></a>
                        </div>
                    </div>
                    <div class="column5_block2 clearfix">
                    	<ul class="uli">
                        	<li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon9.jpg" width="20" height="23" alt=""><a href="#">הצג טלפון</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon10.jpg" width="20" height="23" alt=""><a href="#">צור קשר</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon11.jpg" width="22" height="23" alt=""><a href="#">לאתר</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon12.jpg" width="22" height="23" alt=""><a href="#">המאמרים שלי</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon13.jpg" width="27" height="23" alt=""><a href="#">הבלוגים שלי</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon14.jpg" width="19" height="23" alt=""><a href="#">הפעילויות שלי</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon15.jpg" width="21" height="23" alt=""><a href="#">הוידאו שלי</a></li>
                            <li><img src="<?php bloginfo('template_directory'); ?>/img/social_icon16.jpg" width="21" height="23" alt=""><a href="#">הקופונים שלי</a></li>
                        </ul>
                    </div>
                    
                    
                    
                    <div class="click_info1">
                    	<a href="#">רוצה לקבל את הניוזלטר שלנו ולהיות מעודכן לפני כולם!&nbsp;&nbsp;<i> לחץ כאן</i></a>
                    </div>
                    <div class="column5_social_link clearfix">
                    	<ul class="uli">
                            <?php include get_stylesheet_directory().'/inc/post-buttons.php';?>
                            
                        </ul>
                    </div>
                    <div class="spacer"></div>
                    <h2 class=" hdclass heading8 heading19 f_size2">כתבות נוספות שעשויות לעניין אותך</h2>
                    <div class="spacer"></div>
                    <div class="column5_block3 clearfix">
                        <?php $args1 = array(
                            'numberposts' => 3,
                            'offset' => 0,
                            'orderby' => 'rand',
                            'order' => 'DESC',
                            'exclude' => get_the_ID(),
                            'post_type' => 'recipes',
                            'post_status' => 'publish',
                            'parent' => $main_category->term_id,
                            'suppress_filters' => true );
                            
                            $recent_posts = wp_get_recent_posts( $args1, OBJECT );
                        ?>  

                        <?php foreach($recent_posts as $rp) { 
                                    $authors[] = $rp->post_author;
                                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($rp->ID), 'thumbnail' );
                                    $url = $thumb['0']; ?>

                    	<div class="img_info_box1">
                        	<div class="img_box7">
                            	<a href="<?php echo get_permalink($rp->ID); ?>">
                                    <img src="<?php echo $url; ?>" width="122" height="70" alt=""></a>
                            </div>
                            <a href="<?php echo get_permalink($rp->ID); ?>"><h2 class="hdclass no-margin"><?php echo short_title($rp->post_title, '...', 8); ?></h2>
                            <div class="text_box7"><?php echo $rp->post_excerpt; ?></div></a>
 							<ul class="uli">
                            	<li><?php echo date('d.m.Y', strtotime($rp->post_date)); ?></li>
                                <li class="no-margin no-bdr no-padding">תגובות <?php echo $rp->comment_count; ?></li>
                            </ul>
                            
                            <iframe style="width: 100px;height: 50px;" src="//www.facebook.com/plugins/like.php?locale=he_IL&href=<?php echo urlencode(get_permalink($rp->ID));?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
                        </div>
                        
                        <?php } ?>

                    </div>
                    <div class="column5_block4">
                    	<a class="click2 click9" href="#"><img src="<?php bloginfo('template_directory'); ?>/img/anchor_icon1.png" width="14" height="14" alt="">לקריאת כל התגובות ברצף</a>
                        <?php $comments = get_comments( array ( 'post_id' => get_the_ID(),  'status' => 'approve', 'post_type' => 'recipes'  ) );
                              $i=1;
                              //die(var_dump(get_the_ID()));
                              foreach($comments as $c) { ?>
                        
                        <h4 class="hdclass"><?php echo $i.'. '.$c->comment_content; ?></h4>
                        <div class="text_box8"><?php echo $c->comment_author.', '.date('d/m/Y H:i', strtotime($c->comment_date)); ?> </div>
                                                
                        <?php } ?>
                    </div>
                </div>
                <div class="column4 border_rght">
                	<div class="middle_column_box2 middle_column_box4 clearfix">

                        <?php   $i=1;
                                $catRecipesImgs = apply_filters( 'taxonomy-images-get-terms', '', array('taxonomy' => 'category_recipes') );
                                //die(var_dump($catRecipesImgs ));
                    	        foreach ( $catRecipesImgs as $catRec ) {
                    	           if($i % 3 == 1) $margin = 'no-mg-r'; else $margin = '';
                    	           $url = wp_get_attachment_image_src( $catRec->image_id, 'full' );
                                   ?>
                                <div class="img_box5 img_box10 <?php echo $margin; ?>">
                                    <a href="<?php echo get_term_link($catRec, 'category_recipes'); ?>">
                                        <img src="<?php echo $url[0]; ?>" width="86" height="52" alt="">
                                    <h4 class="hdclass"><?php echo $catRec->name; ?></h4></a>
                                </div>
                        <?php $i++; } ?>                 	
                                             	
                    </div>
                    <div class="block_box m-b6 clearfix">
                        <div class="block_box_img"><img src="<?php bloginfo('template_directory'); ?>/img/icon4.png" alt="" ></div>
                        <div class="block_box_text">
                            <h2>פרסם מאמרים</h2>
                            <h5>הרשמו עכשיו ושתפו אותנו במה שרק אתם יודעים! זה בחינם! </h5>
                        </div>
                    </div>
                	<div class="adsense7">
                    	<a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/ad5.jpg" width="298" height="250" alt=""></a>
                    </div>
                        
                    <div class="column4_block1 clearfix">
                    	<div class="info_heading info_heading3  clearfix">
                            <h4 class="hdclass">עלה השבוע</h4>
                        </div>
                        
                        <?php foreach($recent_posts as $rp) { ?>
                        
                        
                        <div class="comments_box1">
                        	<a href="<?php echo get_permalink($rp->ID); ?>"><h2 class="hdclass"><?php echo $rp->post_title; ?></h2></a>
                        	<div class="">
                            	<a style="text-decoration: none !important;" href="<?php echo get_permalink($rp->ID); ?>">
                                <div class="text_box11" style="float: none;"><?php echo $rp->post_excerpt; ?></div></a>
                                <a href="<?php echo get_permalink($rp->ID); ?>">להמשך</a>
                            </div>
                        </div>
                        
                        
                        <?php } ?>

                    </div>
                    
                    <div class="writers_info writers_info1 nowidth clearfix no-margin">
                        <div class="info_heading info_heading3 m-b3  clearfix">
                            <h4 class="hdclass">כותבי הבית</h4>
                        </div>
                        <?php 
                        $i=1;
                        foreach($authors as $a) { 
                                $user = get_userdata($a);
                                if($i % 3 == 1) $margin = 'no-mg-r'; else $margin = '';
                            ?>
                        <div class="img_box3 margin1 <?php echo $margin; ?>">
                            <a href="#"><img src="<?php echo get_cupp_meta( $user->id, 86 ); ?>" width="86" height="68" alt=""></a>
                            <h5 class="hdclass"><?php echo $user->display_name; ?></h5>
                        </div>		
                        <?php $i++; } ?>	
                    </div>
            		
                    <div class="middle_column_box4">
                        <div class="box_left">
                        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/box_pic2.jpg" alt=""></a>
                        </div>
                        <div class="box_right">
                        <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/box_pic1.jpg" alt=""></a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    
                    <div class="middle_column_box2 adsense5 clearfix">
                    	<div class="info_heading info_heading1 clearfix">
                            <h4 class="hdclass">פורומים</h4>
                            
                        </div>
                        <div class="img_box5 no-mg-r">
                        	<a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/info2_pic2.jpg" width="86" height="58" alt=""></a>
                        	<h4 class="hdclass"> םורופ היגולורטסא</h4>
                        </div>
                        <div class="img_box5">
                        	<a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/info2_pic3.jpg" width="86" height="58" alt=""></a>
                        	<h4 class="hdclass"> םורופ היגולורטסא</h4>
                        </div>
                        <div class="img_box5 ">
                        	<a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/info2_pic4.jpg" width="86" height="58" alt=""></a>
                        	<h4 class="hdclass"> םורופ היגולורטסא</h4>
                        </div>                        	
                    </div>
                    
                    <div class="middle_column_box2 middle_column_box3">
                    	<div class="info_heading info_heading1 clearfix">
                            <h4 class="hdclass">בלוגים</h4>
                            <ul class="uli">
                                <li class="uli no-bdr"><a href="#">חדשים</a></li>
                                <li class="uli"><a href="#">נצפים ביותר</a></li>
                                <li class="uli"><a href="#">לכל האנדקסים</a></li>
                            </ul>
                        </div>
                        <div class="img_box5_out no-mg-r">
                        <div class="img_box5">
                        	<a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/info2_pic5.jpg" width="86" height="58" alt=""></a>
                        	<h4 class="hdclass">היסנלו לגיס</h4>
                        </div>
                        	<h3 class="hdclass">12.10.2012</h3>
                            <div class="text_box4">ךילהת ךל םג עייסל לכונו הנימאו .יבקע</div>
                        </div>
                        <div class="img_box5_out">
                        <div class="img_box5">
                        	<a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/info2_pic6.jpg" width="86" height="58" alt=""></a>
                        	<h4 class="hdclass">רבו לכימ</h4>
                        </div>
                        	<h3 class="hdclass">12.10.2012</h3>
                            <div class="text_box4">ךילהת ךל םג עייסל לכונו הנימאו .יבקע</div>
                        </div>
                        <div class="img_box5_out">
                        <div class="img_box5 ">
                        	<a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/info2_pic7.jpg" width="86" height="58" alt=""></a>
                        	<h4 class="hdclass">ןהכ הננר</h4>
                        </div>
                        	<h3 class="hdclass">12.10.2012</h3>
                            <div class="text_box4">ךילהת ךל םג עייסל לכונו הנימאו .יבקע</div>
                        </div>
                        	
                    </div>
                    
                    <div class="middle_column_box5">
                        	<div class="top_section clearfix">
                            <div class="top_section_left">
                        	<a href="#">האלמה הירפסל</a>
                        	</div>
                            <div class="top_section_right">
                            	<img src="<?php bloginfo('template_directory'); ?>/img/video_icon.png" width="38" height="38" alt="" class="icon">
                            <h2>NoMind Tv</h2>
                            </div>
                        	</div>
                            <div class="middle_section">
                            <img src="<?php bloginfo('template_directory'); ?>/img/pic9.jpg" alt="">
                            	<div class="arrow">
                                <a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/play_icon.png" alt=""></a>
                                </div>
                            </div>
                            <div class="last_section">
                            טמ-חש םיקחשמ ןטשה
                            </div>
                            <div class="clear"></div>
                        </div>
                    <div class="social_block1 no-margin">
                    	<iframe src="http://www.facebook.com/plugins/likebox.php?locale=he_IL&href=https%3A%2F%2Fwww.facebook.com%2FNomind.israel&amp;width&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:290px;" allowTransparency="true"></iframe>
						
                    </div>
                    <div class="">
                    	<div class="info_heading info_heading3 clearfix">
                            <h4 class="hdclass">תגיות</h4>
                        </div>
                        <?php
                        $args= array(
                            'words_number' => '20',
                        'words_color' => '#577bc9',
                        'hover_color' => '#ccc',
                        'smallest_font' => 7 ,
                        'largest_font' => 14);
                       echo categorized_tag_cloud($args);?>
                    </div>
                </div>  
             </div>
             <div class="block1 block5">
             <h2 class="heading6  m-b1">המומחים של נומיינד</h2>
               <ul class="uli photo_galary clearfix">
                   <li class="first"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic2.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic3.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic4.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic5.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic6.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic7.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li class="last"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/img/pic8.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
               </ul>
               <!--<div class="adsense1"><img src="<?php bloginfo('template_directory'); ?>/img/ad1.png" width="964" height="91" alt=""></div>-->
               
            </div>