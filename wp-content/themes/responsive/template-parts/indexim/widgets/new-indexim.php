<?php
                       
                        $new_loop = new WP_Query(array(
                            'posts_per_page ' => 2,
                            'offset' => 0,
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'post_type' => WPBDP_POST_TYPE,
                            'post_status' => 'publish'
            ));
            ?>
            
            <div class="info_box5 info_box12 clearfix"> 
                                <div class="heading18 oracle_bdr clearfix">
                                    <h4 class="hdclass oracle">אינדקסים חדשים</h4>
                                </div>
                            

            <?php 
            $counter = 0;
            if ($new_loop->have_posts()) : while ($new_loop->have_posts()) :
            $new_loop->the_post(); ?>

            <?php
            if (has_post_thumbnail()) {
                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb');
                $index_img =  $image_url[0];
            }
            ?>
                    <div class="info_box1_part1 width130 <?php echo $counter == 1 ? 'no-margin no-bdr no-padding' : '';?>">
                        <div class="img_box1"><a href="<?=get_the_permalink(); ?>"><img src="<?php echo $index_img; ?>" width="122" height="70" alt=""/></a></div>
                        <div class="img_info">
                            <a href="<?=get_the_permalink(); ?>"><h5 class="hdclass"><?php echo get_the_title();?></h5></a>
                            <div class="text_box1 text_box2">
                                 <?php echo mb_substr(get_the_excerpt(), 0, 80);?>... 
                            </div>
                        </div>
                    </div>
             <?php 
                 $counter++; 
                 if($counter==2)
                    break;
                
             endwhile;
             wp_reset_query();             
        else: ?>
            <?php $main_img = ''; ?>
    <?php endif; ?>
       
     </div>    
    
    <?php wp_reset_query(); wp_reset_postdata();?>