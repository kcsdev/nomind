  <?php $queried_object = get_queried_object();
         $current_term_id = $queried_object->term_id;
    
                     ?>
                	<div class="column6_block1 clearfix">
                    	<div class="info_heading info_heading7 clearfix">
                            <h2 class="hdclass">אינדקסים</h2>
                            <ul class="uli f_size1">
                                <li class="no-bdr"><a href="/business-directory/?action=search&dosrch=1&new=1">אינדקסים חדשים</a></li>
                                <li class=""><a href="/business-directory/?action=search&dosrch=1&popular=1">הנצפים ביותר</a></li>
                                <li class=""><a href="/business-directory/?action=search&dosrch=1&featured=1">מבצעים</a></li>
                            </ul>
                        </div>
                        <div class="forums_list1 clearfix">
                       
                        	<ul class="forums_list1_box"> 
                            	
                                <?php
                                 $categories = get_terms(WPBDP_CATEGORY_TAX, 'hide_empty=0&parent=0');
                                         
                                          $i=0;        
                                  foreach ($categories as $category) {
                                            $class = "";
                                      if($i%7 == 0)
                                      {
                                         $class = "margin-l-n ";
                                      } 
                                      
                                      if($category->term_id == $current_term_id)
                                                 $class .= "active";
                                      
                                       $link = get_term_link($category->slug, WPBDP_CATEGORY_TAX); 
                                       echo '<li><a class="'.$class.'" href="'.$link.'">'.$category->name.'</a></li>';
                                       $i++;
                                  }     
                                ?>
                             
                               
                            </ul>
                        </div>
                    	<div class="forums_list clearfix">
                        	<h3 class="hdclass">עסקים לפי:</h3>
                        	<ul class="forums_list_box">
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=א">א</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ב">ב</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ג">ג</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ד">ד</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ה">ה</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ו">ו</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ז">ז</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ח">ח</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ט">ט</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=י">י</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=כ">כ</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ל">ל</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=מ">מ</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=נ">נ</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ס">ס</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ע">ע</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=פ">פ</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=צ">צ</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ק">ק</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ר">ר</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ש">ש</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&letter=ת">ת</a></li>
                            </ul>
                        </div>
                        <div class="forums_list clearfix">
                        	<h3 class="hdclass">לפי ישוב:</h3>
                        	<ul class="forums_list_box">
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=א">א</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ב">ב</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ג">ג</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ד">ד</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ה">ה</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ו">ו</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ז">ז</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ח">ח</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ט">ט</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=י">י</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=כ">כ</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ל">ל</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=מ">מ</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=נ">נ</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ס">ס</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ע">ע</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=פ">פ</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=צ">צ</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ק">ק</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ר">ר</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ש">ש</a></li>
                            	<li><a href="/business-directory/?action=search&dosrch=1&area=ת">ת</a></li>
                            </ul>
                        </div>
                    </div>


 <div class="select_block select_block1 select_block2 clearfix">
                      <h4 class="hdclass">מצא עסק</h4>
                      <form action="" method="post">
                          <div class="form_box">
                              <select class="select_box select_box3">
                                  <option value="">נושא</option>
                                  <?php foreach ($categories as $category) {

                                      echo '<option value="'.$category->slug.'">'.$category->name.'</option>';
                                  }
                                  ?>
                              </select>
                              <select class="select_box select_box3">
                                  <option value="">תת נושא</option>
                              </select>
                              <select class="select_box select_box3">
                                  <option value="">אזור</option>
                              </select>
                              <input class="input_text1" type="text" name="" placeholder="טקסט חופשי" >
                              <input class="input_btn1 input_btn3" type="submit" name="" value="חיפוש ">
                          </div>
                      </form>
                    </div>
                    <div class="column5_block3 clearfix">
                        <!-- 5 recent lisitngs -->
                        
                        
                        
                         <?php include get_stylesheet_directory().'/inc/indexim/recent-listings.php';?>
                                     
                    </div>
                   