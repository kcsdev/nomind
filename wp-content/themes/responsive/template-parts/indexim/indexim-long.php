     <div class="block1 block2 clearfix">
     
             <?php get_template_part( 'sidebar-right' ); ?>
            
                <div class="column6">
                
                    <?php include get_stylesheet_directory() .'/template-parts/indexim/indexim-top.php' ; ?>
                    
                    
                    <?
                     $search_in_progress = false;
                     $area_search_in_progress = false;
                     $recents_search_in_progress = false;
                     
                     if(isset($_GET['letter']) && (isset($_GET['action']) && $_GET['action'] == 'search'))
                     {
                        $search_in_progress = true;
                        $title = 'תוצאות החיפוש לפי אות '.$_GET['letter'];
                     }
                     elseif(isset($_GET['area']) && (isset($_GET['action']) && $_GET['action'] == 'search'))
                     {
                        $area_search_in_progress = true;
                        $title = 'אינדקסים באיזור שמתחיל באות '.$_GET['area'];
                     }
                     elseif(isset($_GET['new']) && (isset($_GET['action']) && $_GET['action'] == 'search'))
                     {
                        $recents_search_in_progress = true;
                         $title = 'אינדקסים אחרונים';
                     }
                     elseif(isset($_GET['popular']) && (isset($_GET['action']) && $_GET['action'] == 'search'))
                     {
                        $recents_search_in_progress = true;
                         $title = 'הנצפים ביותר';
                     }
                      elseif(isset($_GET['featured']) && (isset($_GET['action']) && $_GET['action'] == 'search'))
                     {
                        $recents_search_in_progress = true;
                         $title = 'מבצעים';
                     }        
                    ?>
                    <div class="clearfix">
                    	<div class="column5 border-left no-padding_r">
                        	<div class="heading18 oracle_bdr clearfix">
                            <?  if(isset($title)) : ?>
                                <h5 class="hdclass oracle"><?= $title; ?></h5>
                            <? else: ?>
                                <h5 class="hdclass oracle"><?php echo $queried_object->name; ?></h5>
                            <? endif; ?>    
                            </div>
                            
                            <?php
                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                           
                            $posts_per_page = 4;
                            
                            if($search_in_progress)
                            {
                                $postids=$wpdb->get_col($wpdb->prepare("
                                SELECT      ID
                                FROM        $wpdb->posts AS posts
                                WHERE       SUBSTR(posts.post_title,1,1) = %s AND posts.post_type =  %s
                                ORDER BY    posts.post_title",$_GET['letter'],WPBDP_POST_TYPE));
                                
                                
                                if ($postids) {
                                    $args=array(
                                      'post__in' => $postids,
                                      'post_type' => WPBDP_POST_TYPE,
                                      'post_status' => 'publish',
                                      'posts_per_page' => $posts_per_page,
                                      'paged' => $paged
                                    );
                                }
                                else
                                {
                                    $args=array(
                                      'post__in' => array(9999999999999999)  //just nothing
                                    ); 
                                }    
                                
                            }
                            elseif($area_search_in_progress)
                            {
                                $args = array(
                                      
                                        'post_type' => WPBDP_POST_TYPE,
                                        'posts_per_page' => $posts_per_page,
                                        'paged' =>$paged,
                                        'orderby' => 'date',
                                        'order' => 'DESC',
                                        'meta_key'     => 'listing_area',
                                    	'meta_value'   => '^'.$_GET['area'],
                                    	'meta_compare' => 'REGEXP'
                                        
                                    );
                            }
                            elseif($recents_search_in_progress)
                            {
                                $args = array(
                                      
                                        'post_type' => WPBDP_POST_TYPE,
                                        'posts_per_page' => $posts_per_page,
                                        'paged' =>$paged,
                                        'orderby' => 'date',
                                        'order' => 'DESC'
                                    );
                            }
                            else
                            {
                                    if(isset($current_term_id))
                                    {
                                        $args = array(
                                            'tax_query' => array(
                                                array(
                                                    'taxonomy' => WPBDP_CATEGORY_TAX,
                                                    'terms' => array( $current_term_id )
                                                )
                                            ),
                                            'post_type' => WPBDP_POST_TYPE,
                                            'posts_per_page' => $posts_per_page,
                                            'paged' =>$paged,
                                            'orderby' => 'date',
                                            'order' => 'DESC'
                                            
                                        );
                                    }
                                    else
                                    {
                                         $args = array(
                                            'post_type' => WPBDP_POST_TYPE,
                                            'posts_per_page' => $posts_per_page,
                                            'paged' =>$paged,
                                            'orderby' => 'date',
                                            'order' => 'DESC'
                                        );
                                    }
                            }
                            
                        
                            $wp_query = new WP_Query($args);
                            $max_page = $wp_query->max_num_pages;
                            
                            if ($wp_query->have_posts()) :
                                while ($wp_query->have_posts()): $wp_query->the_post(); 
                                
                                if (has_post_thumbnail()) {
                                    $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb');
                                    $index_img =  $image_url[0];
                                }  
                                else
                                {
                                    $index_img = get_template_directory_uri().'img/info9_pic3.jpg';
                                }
                                ?>
                                
                               
                                <?php 
                                $listing = WPBDP_Listing::get( get_the_ID() );
                                $listing_url = $listing->get_field_value(5);
                                
                               
                                ?>
                               
                                 <div class="post_block">
                            	<div class="post_heading clearfix">
                                	<h2 class="hdclass"><a href="<?php echo get_the_permalink();?>"><?php the_title();?></a></h2>
                                </div>
                                <div class="post_cont clearfix">
                                	<div class="post_cont_right">
                                    	<div class="post_cont_right_img"><a href="<?php echo get_the_permalink();?>"><img src="<?php echo $index_img; ?>" width="107" height="75" alt=""></a></div>
                                    	 <?php wpfp_link() ?>
                                    </div>
                                  <div class="post_cont_left">
                                    	<div class="text_box16"><?php echo mb_substr(get_the_excerpt(), 0, 180);?>...</div>
                                     <!--   <del>כניסות: 8013</del> -->
                                    	<div class="social_link social_link5 no-bdr no-margin clearfix">
                                            <ul class="uli">
                                                <li><img src="<?php echo get_template_directory_uri(); ?>/img/s_link2.png" width="20" height="20" alt=""><a href="#">צור קשר</a></li>
                                                
                                                <?php if(isset($listing_url) && $listing_url != '') : ?>
                                                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/s_link3.png" width="20" height="20" alt=""><?php echo $listing_url; ?></li>
                                                <?php endif; ?>
                                                
                                                <li><img src="<?php echo get_template_directory_uri(); ?>/img/s_link4.png" width="25" height="18" alt=""><a href="#">כרטיס</a></li>
                                                <li><iframe style="width: 85px;height: 20px;" src="//www.facebook.com/plugins/like.php?locale=he_IL&href=<?php echo urlencode(get_permalink());?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe></li>
                                            </ul>
                                        </div>
                                    </div>
                                
                                </div>	
                            </div>
                          <?php    
                                endwhile;
                         ?>
      
                        
                            <div class="click_info1">
                                <a href="#">רוצה לקבל את הניוזלטר שלנו ולהיות מעודכן לפני כולם!  <i>לחץ כאן</i></a>
                            </div>
                            <div class="more_view_list clearfix">
                        
                           
                            
                            <?php 
                            
                            $big = 999999999;
                            $args = array(
                                	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                	'format'             => '/business-directory/page/%#%',
                                	'total'              => $max_page,
                                	'current'            => $paged,
                                	'show_all'           => False,
                                	'end_size'           => 1,
                                	'mid_size'           => 2,
                                	'prev_next'          => False,
                                	'prev_text'          => 'קודם',
                                	'next_text'          => 'הבא',
                                	'type'               => 'array',
                                	'add_args'           => True,
                                	'add_fragment'       => '',
                                	'before_page_number' => '',
                                	'after_page_number'  => ''
                                ); ?>
                            
                             
                            
                            <? $links = paginate_links( $args );
                             //echo get_next_posts_link( 'אבה', $the_query->max_num_pages ); 
                            
                            //echo get_previous_posts_link( 'Newer Entries' );
                            
                             ?>
                             
                             <ul class="uli">
                                <? foreach($links as $link) : ?>
                                    <li class="uli">
                                    <? echo $link; ?>
                                    </li>
                                <? endforeach; ?>
                             </ul>
                             
                              <?= get_next_posts_link( 'אבה', $the_query->max_num_pages ); ?>
                              
                              <?= get_previous_posts_link( 'קודם' ); ?>
                        </div>
                         
                        
                         <? else : ?>
                                <div class="post_block">
                                	<div class="post_heading clearfix">
                                    	<h2 class="hdclass">לא נמצאו תוצאות</h2>
                                    </div>
                                </div>
                          <?    
                            endif;    
 
                            wp_reset_query();
                          ?>
                        </div>
                         <?php include get_stylesheet_directory() .'/template-parts/indexim/indexim-left-sidebar.php' ; ?>
                    </div>
                </div>
                  
             </div>
             <div class="block1 block5">
             <h2 class="heading6 m-b1">המומחים של נומיינד</h2>
               <ul class="photo_galary clearfix">
                   <li class="first"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic2.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic3.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic4.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic5.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic6.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic7.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
                   <li class=" last"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/pic8.jpg" width="111" height="75" alt=""></a>החמומה םש</li>
               </ul>
              <!-- <div class="adsense1"><img src="<?php echo get_template_directory_uri(); ?>/img/ad1.png" width="964" height="91" alt=""></div>-->
               
            </div>