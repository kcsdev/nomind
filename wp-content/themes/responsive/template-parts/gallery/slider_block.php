<?php

//$pic_object = get_field("gal_repeater", $current_nm_pic);

$post = get_post($current_nm_pic);
setup_postdata($post);

?>


    <div class="column8">
        <div class="gallery-details">
            <h3 class="heading6"><?php the_title(); ?></h3>
            <div class="gallery-desc">
                <?php the_content(); ?>
            </div>
        </div>

        <div class="gallery-slider">
            <div class="gallery-browser">
                <?php
                $rows = get_field('gal_repeater',$current_nm_pic); // get all the rows
                $first_row = $rows[0];
                ?>
                <div class="gallery-current-img">
                    <img src="<?php echo $first_row['gal_repeater_img']; ?>" alt="<?php echo $first_row['gal_repeater_title']; ?>" />
                </div>
                <div class="gallery-navigation">
                    <button class="gallery-navigation-next"></button>
                    <button class="gallery-navigation-prev"></button>
                </div>
            </div>
            <div class="gallery-thumbnail">
                <ul class="gallery-thumbnail-list">
                    <?php foreach($rows as $row): ?>
                        <li>
                            <img src="<?php echo $row['gal_repeater_img']; ?>" alt="<?php echo $row['gal_repeater_title']; ?>" />
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

    </div>
