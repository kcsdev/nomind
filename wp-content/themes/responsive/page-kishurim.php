<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Kishurim Template (custom)
 *
Template Name: Kishurim main
 *
 * @file           page-kishurim.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/blog-excerpt.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */

get_header();
?>
    <?php get_template_part( 'loop-header' ); ?>
        
    <div class="main_content clearfix">
    <!--<div class="block1 clearfix <?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">-->

	<?php
	global $wp_query, $paged;

	?>

<?php
	get_template_part( 'template-parts/kishurim/kishurim-block' );
?>

</div><!-- end of #main content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
