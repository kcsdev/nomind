<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Indexim Template (custom)
 *
Template Name: Activities main
 *
 * @file           page-activities.php
 * @package        Responsive
 * @author         Arkady
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/page-activities.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */

if(isset($_POST['event_id']))
{
    $EM_Event = em_get_event($_POST['event_id'] ,'post_id');
    
    echo $EM_Event->output('#_CONTACTPHONE');   
    die();     
}

get_header();
?>
    <?php get_template_part( 'loop-header' ); ?>
        
    <div class="main_content clearfix">
    <!--<div class="block1 clearfix <?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">-->

	<?php
	global $wp_query, $paged;
	if( get_query_var( 'paged' ) ) {
		$paged = get_query_var( 'paged' );
	}elseif( get_query_var( 'page' ) ) {
		$paged = get_query_var( 'page' );
	}
	else {
		$paged = 1;
	}
	$blog_query = new WP_Query( array( 'post_type' => 'event', 'paged' => $paged ) );
	$temp_query = $wp_query;
	$wp_query = null;
	$wp_query = $blog_query;

	?>

<?php
	get_template_part( 'template-parts/activities/activities-block' );
?>

</div><!-- end of #main content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
