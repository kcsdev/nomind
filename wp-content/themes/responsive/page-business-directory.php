<?php
 /*Template Name: Business directory
 *
 * @file           page-business-directory.php
 * @package        Responsive
 * @author         Arkady
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/blog-excerpt.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */

get_header();
?>
    <?php get_template_part( 'loop-header' ); ?>
        
    <div class="main_content clearfix">
   

	<?php if( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

					<?php the_content(); ?>
		<?php
		endwhile;


	endif;
	?>

</div><!-- end of #main content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>



