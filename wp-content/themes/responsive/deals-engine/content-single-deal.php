<?php

/**
 * The template for displaying product content in the single-deal.php template
 *
 * Override this template by copying it to yourtheme/deals-engine/content-single-deal.php
 *
 * @author 		Social Deals Engine
 * @package 	Deals-Engine/Includes/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $wps_deals_options, $post;
global $wps_deals_price; 
$prefix = WPS_DEALS_META_PREFIX;
                        
$expired = '';

// get the size
$deal_size = isset( $wps_deals_options['deals_size_single'] ) ? $wps_deals_options['deals_size_single'] : '';

?>

<?php
	/**
	 * wps_deals_before_single_deal hook
	 */
	 do_action( 'wps_deals_before_single_deal' );

	 if( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="http://schema.org/Product" id="deal-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * wps_deals_before_single_deal_content hook
		 */
		do_action( 'wps_deals_before_single_deal_content' );
	?>

    
                     <div class="top_block clearfix"> 
                      
                          <div class="top_heading">נומיינד קופונים - <div class="top_heading1"><?= get_the_title(); ?></div></div>
                     
                          
                          <h3 class="block_heading"><?= get_the_title();?></h3>
                          
                          <div class="right_column1 right_column3 fl-rt">
                              <div class="date_timebox">
                                   <div class="button_box1">
    
                                <div class="deals-product-btn  deals-clearfix">
		
	
                                	<a href="javascript:void(0);" class="deals-add-to-cart-button ">
                                		לקנייה	</a>
                                				
                                	<input type="hidden" id="deals_id" name="wps_deals_id" class="deals-id" value="<?the_ID();?>">
                                				
                                	<div class="deals-cart-process-show">
                                		<img class="deals-cart-loader" src="<?=site_url();?>/wp-content/plugins/deals-engine/includes/images/cart-loader.gif" scale="0">
                                	</div>
                                				
                                	
                                </div>
   
                                       	<? $productprice = $wps_deals_price->wps_deals_get_price( $post->ID ); 
                                           // get the discount 
		                                   $discount = $wps_deals_price->wps_deals_get_discount( $post->ID );
		
                                    		// get the normal price
                                    		$normalprice = get_post_meta( $post->ID, $prefix . 'normal_price', true );
                                        ?>
                                        
                                        <div class="arrow_button" style="direction: initial;">₪ <?= $productprice;?> :מחיר</div>
                                   </div>
                                   <div class="button_box2">
                                        <div class="button_box2_right fl-rt">הנחה<br><?= $discount; ?></div>
                                        <div class="button_box2_left fl-rt">חיסכון<br>₪ <?= ($normalprice - $productprice)?></div>
                                   </div>
                                    <?$enddate = get_post_meta( get_the_ID(), WPS_DEALS_META_PREFIX . 'end_date', true );?>
                                    <?$startdate = get_post_meta( get_the_ID(), WPS_DEALS_META_PREFIX . 'start_date', true );?>
                                  <div class="button_box2 button_box3">
                                        <div class="button_box2_right fl-rt"><img src="<?php bloginfo('template_directory'); ?>/img/watch.png" width="26" height="26" alt=""></div>
                                        <div class="button_box2_left fl-rt" style="direction: rtl;">הזמן שנותר לקנייה:<br><div style="font-size: 11pt !important;"><? echo do_shortcode( '[countdown date='.$enddate.'][hmstimer][/countdown]' );?></div>
                                        <br />
                                       
                                                                        
                                        
                                        </div>
                                   </div>
                                   
                        <?php             	// get today's date and time
            			$today	= wps_deals_current_date();
            			
            			// get the end date & time
            			$enddate = get_post_meta( $post->ID, $prefix . 'end_date', true );
            			
            			$boughts = get_post_meta( $post->ID, $prefix . 'boughts', true );
		
                        $bought = isset( $boughts ) && !empty( $boughts ) ? $boughts : '0';
                        
                         if (has_post_thumbnail()) {
                            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb');
                            $index_img =  $image_url[0];
                        }  
                        else
                        {
                            $index_img = get_template_directory_uri().'img/info9_pic3.jpg';
                        }
                         
            			
            			// check if the deal is still active
            			 
            			?> 
                           <div class="button_box2 button_box3 button_box4">
                                <div class="button_box2_right fl-rt"><img src="<?php bloginfo('template_directory'); ?>/img/right.png" width="26" height="26" alt=""></div>
                                <? if( $enddate >= $today ) : ?>
                                    <div class="button_box2_left fl-rt" style="direction: initial;"><?= $bought; ?> נרכשו<br>העיסקה בתוקף</div>
                                <? else : ?>
                                    <div class="button_box2_left fl-rt" style="direction: initial;"><?= $bought;?> נרכשו<br>פג תוקף</div>
                                <? endif; ?>     
                           </div>
                              </div>
                              
                              <div class="date_info">
                                  <h4 class="heading8 heading19">ההצעה שלנו</h4>
                                  <div class="info_text"></div>
                              </div>
                              
                              <div class="date_info">
                                  <h4 class="heading8 heading19">שעות פתיחה</h4>
                                  <div class="info_text">
                                  <? $business_address = get_post_meta( $post->ID, $prefix . 'business_address', true );?>
                                  <?= $business_address; ?>
                                  </div>
                              </div>
                             
                          </div>
                          
                          <div class="right_column2 clearfix fl-rt">
                               <div class="festival_img"><img src="<?=$index_img?>" width="396" height="270" alt=""></div>
                               <div class="heading9"><? the_title();?></div>
                               <?
                               	$enddate = date('d.m.Y', strtotime( $enddate ));
                                $startdate = date('d.m.Y', strtotime( $startdate ));
                               ?>
                               <div class="date"><?= $enddate?> - <?= $startdate;?></div>
                               <div class="date">מספר קופון: <span class="festiv_text"><b><?=the_ID();?></b></span></div> 
                               <div class="festiv_text" style="padding: 10px;"> 
                               <?= get_the_content(); ?> ...
                               </div>
                             <div class="deals-product-btn  deals-clearfix">
    
    
                            	<a href="javascript:void(0);" class="deals-add-to-cart-button ">
                            		לקנייה	</a>
                            				
                            	<input type="hidden" id="deals_id" name="wps_deals_id" class="deals-id" value="<?the_ID();?>">
                            				
                            	<div class="deals-cart-process-show">
                            		<img class="deals-cart-loader" src="<?=site_url();?>/wp-content/plugins/deals-engine/includes/images/cart-loader.gif" scale="0">
                            	</div>
                            				
                            	
                            </div>
                          </div>
                          
                           <? include get_stylesheet_directory() .'/template-parts/kuponim/kuponim-sidebar.php' ; ?>  
                          
                        
                      </div>
          </div>
    
    

	<?php
		/**
		 * wps_deals_after_single_deal_content hook
		 */
		do_action( 'wps_deals_after_single_deal_content' );
	?>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #deal-<?php the_ID(); ?> -->

<?php do_action( 'wps_deals_after_single_deal' ); ?>
 