<?php 

/**
 * Image Template
 * 
 * Override this template by copying it to <ourtheme/deals-engine/single-deal/image.php
 * 
 * @author 		Social Deals Engine
 * @package 	Deals-Engine/Includes/Templates
 * @version     1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

?>

<div class="deals-single-image">

	<?php echo $dealimg; ?>
	
</div>