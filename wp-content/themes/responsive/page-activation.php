<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Activation Page Template (custom)
 *
Template Name: Activation page
 *
 * @file           page-activation.php
 * @package        Responsive
 * @author         Arkady
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/blog-excerpt.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */


 $user_id = filter_input( INPUT_GET, 'user', FILTER_VALIDATE_INT, array( 'options' => array( 'min_range' => 1 ) ) );
   
    $success = false;
    if ( $user_id ) {
          
        $code = get_user_meta( $user_id, 'has_to_be_activated', true );
        if ( $code == filter_input( INPUT_GET, 'key' ) ) {
            delete_user_meta( $user_id, 'has_to_be_activated' );
            $success= true;
            
            
            
        }
    }
  
   
get_header();
?>
    <?php get_template_part( 'loop-header' ); ?>
        
    <div class="main_content clearfix">
    <!--<div class="block1 clearfix <?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">-->

	<?php
	global $wp_query, $paged;
	if( get_query_var( 'paged' ) ) {
		$paged = get_query_var( 'paged' );
	}elseif( get_query_var( 'page' ) ) {
		$paged = get_query_var( 'page' );
	}
	else {
		$paged = 1;
	}
	$blog_query = new WP_Query( array( 'post_type' => 'post', 'paged' => $paged ) );
	$temp_query = $wp_query;
	$wp_query = null;
	$wp_query = $blog_query;

	?>

<div class="block1 block2 block8 clearfix">
	<div class="order-block-title big"><h2>אקטיבציה של החשבון</h2></div>
    <div class="payment-right">
    <?if($success) : ?>
		<p class="y11">סיימת בהצלחה אקטיבציה של החשבון. לחץ <a href="<?= get_permalink(584); ?>">כאן</a> כדי להיכנס לאתר</p>
        <p>פרטי ההתחברות שלך:</p>
        <br />
        <? $user_info = get_userdata($user_id); ?>
        <p>שם משתמש: <span><?=$user_info->user_login;?></span></p>
        <p>סיסמא: <span><?=$user_info->user_pass;?></span></p>
        
    <? else: ?>
        <p class="y11">האקטיבציה לא הצליחה.</p>
    <? endif; ?>    
        
<?php ?>
</div>
</div>
</div><!-- end of #main content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
