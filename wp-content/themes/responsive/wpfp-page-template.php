   <div class="block1 block2 clearfix">
     
             <?php get_template_part( 'sidebar-right' ); ?>
            
                <div class="column6">
                <div class="top_heading top_heading2 top_heading4 no-margin no-padding clearfix">
                        <h2 class="hdclass">המזוודה שלי</h2>
                        <i>הדברים שאהבת באתר שלנו</i>
                    </div><br />
                    <p>
                         <?echo '<p>'.wpfp_clear_list_link().'</p>';?>
                    </p><p>&nbsp;</p>
<?php

    $wpfp_before = "";
    echo "<div class='wpfp-span'>";
   /* if (!empty($user)) {
        if (wpfp_is_user_favlist_public($user)) {
            $wpfp_before = "$user's Favorite Posts.";
        } else {
            $wpfp_before = "$user's list is not public.";
        }
    }
    */
    
    if ($wpfp_before):
        echo '<div class="wpfp-page-before">'.$wpfp_before.'</div>';
    endif;
    
        
   
    if ($favorite_post_ids) {
		$favorite_post_ids = array_reverse($favorite_post_ids);
        $post_per_page = wpfp_get_option("post_per_page");
        $page = intval(get_query_var('paged'));
      ?>
      
      <?php
      
       $post_types = array('אינדקסים'=>WPBDP_POST_TYPE, 'פעילויות'=>EM_POST_TYPE_EVENT, 'כתבות'=>'post', 'מתכונים'=>'recipes', 'מאמרים'=> 'articles' );
        $counter = 0;
        foreach($post_types as $title=>$type)
        {
            $counter++;
            $qry = array('post_type' => $type, 'post__in' => $favorite_post_ids, 'posts_per_page'=> $post_per_page, 'orderby' => 'post__in', 'paged' => $page);
            // custom post type support can easily be added with a line of code like below.
            // $qry['post_type'] = array('post','page');
            query_posts($qry);
            
            if(have_posts()) :?>
                <div class="info_heading info_heading7 clearfix" style="margin-top: 20px;background: url(<?= get_template_directory_uri()?>/img/heading_border<?=$counter < 6 && $counter != 3   ? $counter : 1 ;?>.png) right bottom no-repeat;">
                    <h2 class="hdclass"><?=$title?></h2>
                </div>      
                <ul>
                <?while ( have_posts() ) : the_post();
                    echo "<li><a href='".get_permalink()."' title='". get_the_title() ."'>" . get_the_title() . "</a> ";
                    wpfp_remove_favorite_link(get_the_ID());
                    echo "</li>";
                endwhile;
                echo "</ul>";
             endif;
                ?>
                
             
             
                <?php 
                    wp_reset_query();
                
            
           }
        
            echo '</div>';
        
    } else {
        echo "</ul>";
        $wpfp_options = wpfp_get_options();
        echo "<li>";
        echo $wpfp_options['favorites_empty'];
        echo "</li>";
        echo "</ul>";
    }
    

   
    echo "</div>";
    wpfp_cookie_warning();
    ?>
    </div>
    </div>
    