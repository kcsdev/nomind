<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Admin Panel Page Template (custom)
 *
Template Name: Admin Panel page
 *
 * @file           page-admin-panel.php
 * @package        Responsive
 * @author         Arkady
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/blog-excerpt.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */


get_header();
?>
    <?php get_template_part( 'loop-header' ); ?>
        
    <div class="main_content clearfix">
    <!--<div class="block1 clearfix <?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">-->

	<?php
	global $wp_query, $paged;
	if( get_query_var( 'paged' ) ) {
		$paged = get_query_var( 'paged' );
	}elseif( get_query_var( 'page' ) ) {
		$paged = get_query_var( 'page' );
	}
	else {
		$paged = 1;
	}
	$blog_query = new WP_Query( array( 'post_type' => 'post', 'paged' => $paged ) );
	$temp_query = $wp_query;
	$wp_query = null;
	$wp_query = $blog_query;

	?>

<?php
	get_template_part( 'template-parts/admin-panel/admin-block' );
    
?>


   `<?php// if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
 

	<h1><?php the_title(); ?></h1>

        <div id="entry"></div>

<div id="873" class="my_tabs">
<?php
$id=873;
$post = get_post($id);
$title = apply_filters('the_title', $post->post_title);
echo $title;
$content = apply_filters('the_content', $post->post_content);
echo $content;
?>
</div>
                          
<div id="884" class="my_tabs">
<?php
$id=884;
$post = get_post($id);
$title = apply_filters('the_title', $post->post_title);
echo $title;
$content = apply_filters('the_content', $post->post_content);
echo $content;
?>
</div>

<div id="882" class="my_tabs">
<?php
$id=882;
$post = get_post($id);
$title = apply_filters('the_title', $post->post_title);
echo $title;
$content = apply_filters('the_content', $post->post_content);
echo $content;
?>
</div>

<div id="651" class="my_tabs">
<?php
$id=651;
$post = get_post($id);
$title = apply_filters('the_title', $post->post_title);
echo $title;
$content = apply_filters('the_content', $post->post_content);
echo $content;
?>
</div>

<div id="886" class="my_tabs">
<?php
$id=886;
$post = get_post($id);
$title = apply_filters('the_title', $post->post_title);
echo $title;
$content = apply_filters('the_content', $post->post_content);
echo $content;
?>
</div>
   <?php// endwhile; ?>
</div>


<!--</div> end of #main content -->

<?php //get_sidebar(); ?>



<!--
<script>
//get posts with ajax
//------------------    
    
jQuery('ul.forums_list1_box li a').on('click', function(){
var id = $(this).attr('class');
    
 $.ajax({
            url:"/wp-admin/admin-ajax.php",
            type:'POST',
            data:'action=getvisualform_action&post=' + id, 
            success:function(results)
            {
                jQuery('a#activitylist').removeAttr('id');
                jQuery('div#entry').html(results);
                jQuery('a.' + id).attr('id', 'activitylist');

            }
        }); 
});
</script>
-->


<!--
<script>
jQuery('.wpfepp-form').submit(ajaxSubmit);
function ajaxSubmit(){

var newVideoForm = jQuery(this).serialize();

jQuery.ajax({
type:"POST",
url: "/wp-admin/admin-ajax.php",
data: newVideoForm,
success:function(data){
jQuery("#feedback").html(data);
}
});

return false;
}
    
    
</script>
-->




<script>
//      
//     localStorage.setItem(873, $('div#873').html());
//     localStorage.setItem(884, $('div#884').html());
//     localStorage.setItem(882, $('div#882').html());
//     localStorage.setItem(651, $('div#651').html());
//     localStorage.setItem(886, $('div#886').html());
//    $('div.my_tabs').remove();
//          
//    $('div#entry').append(localStorage['873']);
////    $(document).ready(function(){
//////  var d = jQuery('a[href="#1437564034879-f7b4e6ca-aaf1"]');
//////$(d[0]).trigger('click');
////});
//    jQuery('ul.forums_list1_box a').on('click',function(){
////        $.getScript("/wp-content/themes/responsive/js/custom.js");
//
//        
//        jQuery('ul.forums_list1_box a').removeAttr('id','activitylist');
//    jQuery(this).attr('id', 'activitylist'); 
//    var id = $(this).attr('class');
//     jQuery('div#entry').empty();
//     jQuery('div#entry').append(localStorage[id]);
//    });

    
    
    //storage.setItem(837, $('div#873').html());
 jQuery('div.my_tabs').css('display', 'none');
  var id = $('#activitylist').attr('class');
  jQuery('div#' + id).css('display', 'block');
  jQuery('ul.forums_list1_box a').on('click',function(){
    jQuery('ul.forums_list1_box a').removeAttr('id','activitylist');
    $(this).attr('id', 'activitylist'); 
    jQuery('div.my_tabs').css('display', 'none');
   var id = $(this).attr('class');
  jQuery('div#' + id).css('display', 'block');
  
  });      
        
        </script>
<div style="display:none" id="dvloader"><img src="http://nomind.c14.co.il/wp-content/themes/responsive/img/ajax-loader.gif" /></div>





<?php get_footer(); ?>
