<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

get_header();
?>
    <?php get_template_part( 'loop-header' ); ?>
        
    <div class="main_content clearfix">
    <!--<div class="block1 clearfix <?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">-->

	<?php
	global $wp_query, $paged;
	if( get_query_var( 'paged' ) ) {
		$paged = get_query_var( 'paged' );
	}elseif( get_query_var( 'page' ) ) {
		$paged = get_query_var( 'page' );
	}
	else {
		$paged = 1;
	}
	$blog_query = new WP_Query( array( 'post_type' => 'recipes', 'paged' => $paged, 	'tax_query' => array(
		array(
			'taxonomy' => 'category_recipes',
			'field'    => 'slug',
			'terms'    => $_GET['category_recipes'],
		),
	), ) );
	$temp_query = $wp_query;
	$wp_query = null;
	$wp_query = $blog_query;

	?>

<?php
	get_template_part( 'template-parts/recipes-block' );
?>

</div><!-- end of #main content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>