<?php
/**
 * Gallery Template (custom)
 *
Template Name: Gallery Page Template
 *
 * @file           page-gallery.php
 * @package        Responsive
 * @author         Arkady
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/page-activities.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */

get_header();
the_post();

$all_galleries = get_posts(array(
    'post_type' => 'nm_pics'
));

if(isset($_GET['img_id'])){
    $current_nm_pic = $_GET['img_id'];
} else {
    $current_nm_pic = $all_galleries[0]->ID;
}

?>

<div class="block1 block2 clearfix">

    <div class="column3">
        <img src="<?php echo get_template_directory_uri() . '/img/ad4.png'; ?>" alt="" />
    </div>

    <div class="column8">
        <h2 class="heading1 heading6">
            <del>תמונות</del>
        </h2>
        <div class="block1 block2 clearfix">
            <div class="column3">
                <div class="right_list_bar1">
                    <ul>
                        <li>#1</li>
                    </ul>
                </div>
            </div>
            <?php include_once( 'template-parts/gallery/slider_block.php' ); ?>
        </div>
    </div>

</div>

<?php
get_footer();
?>
