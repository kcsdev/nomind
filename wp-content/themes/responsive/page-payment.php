 <?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Kishurim Template (custom)
 *
Template Name: Tofes Tashlum
 *
 * @file           page-kishurim.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/blog-excerpt.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */

get_header();
?>
    <?php get_template_part( 'loop-header' ); ?>
        
	<?php
	global $wp_query, $paged;

	?>
        
      <?php get_template_part( 'template-parts/registration/payment-block' ); ?>
        
   <?php //get_sidebar(); ?>
<?php get_footer(); ?>
     