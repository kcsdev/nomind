

<!-- Modal -->
<div id="send-to-author" class="modal fade" tabindex="-1" >
 <div class="modal-dialog" style="height: 500px;">
 <div class="modal-content" style="height: 500px;background-color: white;">
 <div class="modal-header" style="border: none; padding: 5px;">
 <a  data-dismiss="modal" style="color: black; cursor: pointer; float: right; font-size: 18pt;margin: 5px;">X</a>
 <div class="modal-title title" style="text-align: center;">כתוב לעורך
 </div>
 </div>
 <div class="modal-body">
    <?php echo do_shortcode( '[contact-form-7 id="309" html_class="use-floating-validation-tip"]' ); ?>
 </div>
 </div><!-- /.modal-content -->
 </div><!-- /.modal-dialog -->
</div><!-- /.modal -->