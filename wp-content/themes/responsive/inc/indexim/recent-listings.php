 <?php
                       
                        $new_loop = new WP_Query(array(
                            'posts_per_page ' => 5,
                            'offset' => 0,
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'post_type' => WPBDP_POST_TYPE,
                            'post_status' => 'publish'
            ));
            ?>

            <?php 
            $counter = 0;
            if ($new_loop->have_posts()) : while ($new_loop->have_posts()) :
            $new_loop->the_post(); ?>

            <?php
            if (has_post_thumbnail()) {
                $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb');
                $index_img =  $image_url[0];
            }
            ?>
            	<div class="img_info_box1">
                        	<div class="img_box7">
                            	<a href="<?=get_the_permalink(); ?>"><img src="<?php echo $index_img; ?>" width="122" height="70" alt=""></a>
                            </div>
                            <h2 class="hdclass"><?php echo get_the_title();?></h2>
                            <div class="text_box7"><?php echo mb_substr(get_the_excerpt(), 0, 180);?>... </div>
                    </div>
             <?php 
                 $counter++; 
                 if($counter==5)
                    break;
                
             endwhile;
        else: ?>
            <?php $main_img = ''; ?>
    <?php endif; ?>
    
    <?php wp_reset_query(); wp_reset_postdata();?>