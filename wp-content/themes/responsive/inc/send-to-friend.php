<style>
.wpcf7
{
    font-family: 'Lato', sans-serif !important;
    color: #222222;
    font-weight: 300;
    font-size: 14pt;
}

.wpcf7 input[type="text"],
.wpcf7 input[type="email"],
.wpcf7 textarea
{
    background-color: #fff;
    color: #222222;
    width: 96%;
    border:1px solid #e5e5e5;
    font-family: 'Lato', sans-serif !important;
    font-weight: 300;
    font-size: 14pt;
    -webkit-border-radius: 10px !important;
    -moz-border-radius: 10px !important;
    border-radius: 10px !important;
    padding-left: 20px;
    /*margin-bottom: 20px;*/
}

.wpcf7 input[type="text"],
.wpcf7 input[type="email"]
{
    /*height: 45px;*/
}

.wpcf7 textarea
{
     resize: none;
}

.wpcf7 .wpcf7-not-valid-tip {
	position: static;
	background: inherit;
	border: none;
	color: #f00;
	font-size: 10pt;
	display: block;
	width: 100%;
	padding: 0;
}

.your-subject
{
   /* margin-bottom: 45px;*/
}

#your-name,
#your-email
{
    width: 45%;
}

#your-email
{
   margin-left: 20px;
   border:1px solid #e5e5e5;
}

#submit-btn
{
    /*background-image: url(/imgs/submit.png);
    background-repeat: no-repeat;
    width: 144px;
    height: 48px;
    float: right;
    margin-right: 25px; */
    display: none;
}

div.modal-header > .title
{
   font-family: 'Lato', sans-serif;
    font-style: normal;
    font-weight: 300;
    color: #222222;
    font-size: 30pt;
}

div.wpcf7-validation-errors
{
    background-color: #fff;
}

.wpcf7 .wpcf7-not-valid-tip {
	position:relative;
	z-index: 100;
	background: inherit;
	font-size: 10pt;
	padding:2px;
	display: block;
    width: 200px;

    }

div.wpcf7-response-output {

    background-color: white;
    position: relative;
    top: -6px;

}

div.wpcf7-mail-sent-ok
{
    display: none !important;
}


::-webkit-input-placeholder {
	   font-family: 'Lato', sans-serif !important;
        color: #222222;
        font-weight: 300;
        font-size: 12pt;
}
::-moz-placeholder {
   font-family: 'Lato', sans-serif !important;
        color: #222222;
        font-weight: 300;
        font-size: 12pt;
}
:-moz-placeholder {   /* Older versions of Firefox */
   font-family: 'Lato', sans-serif !important;
        color: #222222;
        font-weight: 300;
        font-size: 12pt;
}
:-ms-input-placeholder {
   font-family: 'Lato', sans-serif !important;
        color: #222222;
        font-weight: 300;
        font-size: 12pt;
}


.submit-image
{
    float: right;
    position: absolute;
    right: 45;
    top: 330;
}

.wpcf7-submit
{
    position: relative;
    top: 50px;
}

  /*  span.your-email > span.wpcf7-not-valid-tip
    {
        margin-top: -35px;
        margin-left: 365px;
    } */



</style>

<!-- Modal -->
<div id="send-to-friend" class="modal fade" tabindex="-1" style="width: 50%; ">
 <div class="modal-dialog" style="height: 500px;">
 <div class="modal-content" style="height: 500px;background-color: white;">
 <div class="modal-header" style="border: none; padding: 5px;">
 <a  data-dismiss="modal" style="color: black; cursor: pointer; float: right; font-size: 18pt;margin: 5px;">X</a>
 <div class="modal-title title" style="text-align: center;">שלח לחבר
 </div>
 </div>
 <div class="modal-body">
    <?php echo do_shortcode( '[contact-form-7 id="310" html_class="use-floating-validation-tip"]' ); ?>
 </div>
 </div><!-- /.modal-content -->
 </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- Modal -->
<div id="thankYouModal" class="modal fade" tabindex="-1" >
 <div class="modal-dialog" style="width: 400px; height: 300px; margin-top: 100px;" >
 <div class="modal-content" style="width: 400px; height: 300px">
 <div class="modal-header" style="border: none; padding: 5px;">
 <a  data-dismiss="modal" style="color: black; cursor: pointer; float: right; font-size: 18pt;margin: 5px;">X</a>
 <div class="modal-title title" style="text-align: center; padding: 20px;">Thank you!
 </div>
 </div>
 <div class="modal-body" style="height: 200px; text-align: center;">
    Your message was sent successfully!
   </div>
 </div><!-- /.modal-content -->
 </div><!-- /.modal-dialog -->
</div><!-- /.modal -->