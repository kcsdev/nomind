<div class="banner_left_box clearfix">
            	<div class="banner_left_menu clearfix">
                	<ul class="banner-menu">
                    	<li><a class="category active" href="<?bloginfo('url');?>" rel="articles">מאמרים</a></li>
                        <li><a class="category forum-search" href="#" rel="forum">פורומים</a></li>
                        <li><a class="category" href="<?bloginfo('url');?>" rel="post">בלוגים</a></li>
                        <li><a class="category" href="#" rel="post">הכרויות</a></li>
                        <li><a class="category" href="<?bloginfo('url');?>" rel="recipes">מתכונים</a></li>
                        <li class="no-bg"><a class="category" href="#" rel="images">תמונות</a></li>
                    </ul>
                </div>
                <form action="<?bloginfo('url');?>" method="get" id="main_search" > 
                	<input id="search-text" class="input_text1" type="text" name="s" value="חפשו באתר" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
                    <input type="hidden" name="post_type" id="post_type" value="articles" /> 
                    <input class="input_btn1" type="submit" id="submit-btn" value="חיפוש ">
                </form>
            </div>
            
            <!-- hedden forum search -->
            <form id="forum_search" action="/%d7%a4%d7%95%d7%a8%d7%95%d7%9e%d7%99%d7%9d/" method="post" style="display: none;">
        		<input type="hidden" name="cf_action" value="post-search">
        		<input type="hidden" name="idForum" value="">
        		<input class="input_text1 input_text9" type="text" id="forum-text" name="search" value="" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
        	    <input class="input_btn1  input_btn2" type="submit" value="חפש">
        		</form>
            
            
            <script>
            
             $(function() {
                        $('#submit-btn').click(function(e) {
                            e.preventDefault();
                            //check if forum is active
                            if($('a.active').attr('rel') == 'forum')
                            {
                                $('#forum-text').val($('#search-text').val());
                                $('form#forum_search').submit();
                            }
                            else
                            {
                                $('form#main_search').submit();
                            }
                            
                        });
                            
                        $('a.category').click(function(e) {
                            e.preventDefault();
                            var form = $('form#main_search');
                            form.attr('action',$(this).attr('href'));
                            
                            $('a.active').removeClass('active');
                            $(this).addClass('active');
                            //$('#search-text').val('חפשו ב'+$(this).html());
                            
                            if($(this).attr('rel') != '')
                                $('#post_type').val($(this).attr('rel'));
                        });  
                        
                         $('a.forum-search').click(function(e) {
                            e.preventDefault();
                            $('a.active').removeClass('active');
                            $(this).addClass('active');
                            
                            
                         });    
                    });
            </script>