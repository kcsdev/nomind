<div class="column5_block3 clearfix no-bdr no-margin">
    <div class="info_heading info_heading1 clearfix">
        <h4 id="updatedPosts" class="hdclass">עודכנו לאחרונה</h4>
    </div>

    <?php
                                $new_loop = new WP_Query(array(
                                    'post_type' => 'post',
                                    'posts_per_page' => 5,
                                    'orderby' => 'DATE',
                                    'order' => 'Desc'    ));

                                $counter = 0;

?>

    <?php if ($new_loop->have_posts()):
                                    while ($new_loop->have_posts()):
                                        $new_loop->the_post(); ?>

        <?php
                                        $counter++;
                                        $post_id = get_the_ID();
                                        if (has_post_thumbnail()) {
                                            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                                        } else
                                            $image_url[0] = ''; //default image?

?>

        <div class="img_info_box1">
            <div class="img_box7">
                <a href="<?php the_permalink(); ?>"><img src="<?php echo $image_url[0]; ?>" width="122" height="70"
                                                         alt=""></a>
            </div>
            <h2 class="hdclass"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

            <div class="text_box7"><?php echo get_post_excerpt(get_the_ID(), 80); ?></div>
            <ul class="uli">
                <li class="uli"><?php the_time('d/m/Y'); ?></li>
                <li class="uli no-margin no-bdr no-padding"> תגובות <?php echo
get_comments_number($post_id); ?></li>
            </ul>
           <iframe style="width: 100px;height: 50px;" src="//www.facebook.com/plugins/like.php?locale=he_IL&href=<?php echo
urlencode(the_permalink()); ?>&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
        </div>

        <?php if ($counter == 3)
                      break; ?>

    <?php endwhile;
          else: ?>


    <?php endif; ?>

    <?php wp_reset_query(); ?>

</div>
