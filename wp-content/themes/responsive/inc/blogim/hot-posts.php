<div class="middle_column_box1">
    <div class="info_heading info_heading1 no-margin clearfix">
        <h4 class="hdclass">פוסטים חמים</h4>
    </div>

    <!-- all the categories -->
    <?php
$new_loop = new WP_Query(array(
    'post_type' => 'post',
    'category__in' => $all_categories,
    'posts_per_page' => 2,
    'orderby' => 'comment_count'));



?>

    <?php if ($new_loop->have_posts()):
    while ($new_loop->have_posts()):
        $new_loop->the_post(); ?>

        <?php

        $post_id = get_the_ID();
        if (has_post_thumbnail()) {
            $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
        } else
            $image_url[0] = ''; //default image?

?>
        
        <div class="info_box1_part info_box1_part2 clearfix">
            <div class="img_box info_pic1"><a href="<?php the_permalink(); ?>">
                <img src="<?php echo $image_url[0]; ?>" width="122" height="70" alt=""></a>
            </div>
            <div class="img_info">
                <h5 class="hdclass"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                <ul>
                    <li><a href="<?= get_blog_author_page_url(get_the_author_meta
('ID')); ?>"><?php the_author(); ?></a></li>
                    <li class="no-bdr"><?php the_time('d.m.Y'); ?></li>
                </ul>
                <div class="text_box1 text_box2">
                    <?php echo get_post_excerpt(get_the_ID()); ?>
                </div>
            </div>
        </div>


    <?php endwhile;
    else: ?>


    <?php endif; ?>
    <?php wp_reset_query(); ?>

</div>