<div class="middle_column_box1">
	<div class="info_heading info_heading3 no-margin clearfix">
        <h4 class="hdclass">מאמרים</h4>
        <ul class="uli">
            <li class="uli no-bdr"><a href="#">חדשים</a></li>
            <li class="uli"><a href="#">נצפים ביותר</a></li>
            <li class="uli"><a href="#">לכל המאמרים</a></li>
        </ul>
    </div>
    
        <? //get 2 recent articles (posts) 
             $new_loop = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => 2,
                'orderby' => 'date',
                'order' => 'DESC'
            ));
            
          
        ?>
            <? if ($new_loop->have_posts()) : while ($new_loop->have_posts()) : $new_loop->the_post(); ?>

            <?
                $image_url = array();
                if (has_post_thumbnail()) {
                    $image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
                } else
                    $image_url[0] = get_template_directory_uri().'/img/info_pic9.jpg'; //default image
           ?>

    		<div class="info_box1_part info_box1_part2 clearfix">  
                <div class="img_box13"><img src="<?php echo $image_url[0];?>" width="99" height="65" alt=""></div>
                <div class="img_info">
                    <h5 class="hdclass"><?php the_title(); ?></h5>
                    <ul>
                        <li><?php the_author(); ?></li>
                        <li class="no-bdr"><?php the_time('d.m.Y'); ?></li>
                    </ul>
                    <div class="text_box1 text_box2">
                        <?the_excerpt();?>
                    </div>
                </div>
            </div>
            <?php endwhile;
             endif; 
             wp_reset_query(); ?>
            
</div>