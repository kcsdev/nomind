<div class="middle_column_box2 more-mrgn clearfix">
                    <div class="info_heading clearfix">
                        <h4 class="hdclass">עלה השבוע</h4>
                    </div>
                     <ul class="rose-week">
                    <? //get 3 recent events 
                      $args = array(
                                'post_type' => EM_POST_TYPE_EVENT,
                                'posts_per_page' => 3,
                                'orderby' => 'date',
                                'order' => 'DESC'
                                
                            );
                            
                             global $EM_Event;
                        $wp_query = new WP_Query($args);
                        
                       
                        
                        $max_page = $wp_query->max_num_pages;
                        
                        if ($wp_query->have_posts()) :
                            while ($wp_query->have_posts()): $wp_query->the_post();
                            
                             $EM_Event = em_get_event(get_the_ID() ,'post_id'); ?>
                             
                              
                    	<li>
                        	<h5><a href="<?the_permalink();?>"><?=date('d.m.Y', $EM_Event->start);?></a></h5>
                            <span><?= the_title();?></span>
                            <strong><a href="<?the_permalink();?>">להמשך</a></strong>
                            <div class="clear"></div>
                        </li>
                        
                               
                             
                           <?  endwhile;
                        endif; ?>     
                              
                     </ul>        
                   
                    
                  	
                </div>