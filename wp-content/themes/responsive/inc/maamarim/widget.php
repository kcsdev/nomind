
                    
                        <?php $args1 = array(
                            'numberposts' => 5,
                            'offset' => 0,
                            'orderby' => 'rand',
                            'order' => 'DESC',
                            'post_type' => 'articles',
                            'post_status' => 'publish',
                            'suppress_filters' => true );
                            
                            $recent_posts = wp_get_recent_posts( $args1, OBJECT );
                        ?> 
                    <div class="middle_column_box1">
                        <div class="info_heading info_heading6  clearfix">
                            <h4 class="hdclass">מאמרים</h4>
                            <ul class="uli">
                                <li class="uli no-bdr"><a href="#">חדשים</a></li>
                                <li class="uli"><a href="#">נצפים ביותר</a></li>
                                <li class="uli"><a href="#">לכל האנדקסים</a></li>
                            </ul>
                        </div>
                        <?php foreach($recent_posts as $rp) { //die(var_dump($rp)); ?>
                        <div class="info_box1_part info_box1_part2 clearfix">  
                            <div class="img_box13">
                                <a href="<?php echo get_permalink($rp->ID); ?>">
                                    <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id($rp->ID)); ?>" width="99" height="65" alt="">
                                </a>
                            </div>
                            <div class="img_info"><a href="<?php echo get_permalink($rp->ID); ?>">
                                <h5 class="hdclass"><?php echo $rp->post_title; ?></h5></a>
                                <ul>
                                    <li>
                                        <?php echo get_the_author_meta( 'nickname', $rp->post_author ); ?>
                                    </li>
                                    <li class="no-bdr"><?php echo date('d.m.Y', strtotime($rp->post_date)); ?></li>
                                </ul>
                                <div class="text_box1 text_box2"><?php echo $rp->post_excerpt; ?></div>
                            </div>
                        </div>
                        <?php }; ?>                        
                    </div>  