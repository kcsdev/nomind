<?php
/**
 * The template for displaying Author Archive pages
 *
 * Used to display archive-type pages for posts by an author.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<?
wp_reset_query();
$id=$_GET['id'];
query_posts( 'author='.$id.'&post_type=any' );

// Цикл WordPress
if( have_posts() ){
while( have_posts() ){
    the_post();
 responsive_entry_before(); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php responsive_entry_top(); ?>

    <?php get_template_part( 'post-meta' ); ?>

    <div class="post-entry">
        <?php if( has_post_thumbnail() ) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php the_post_thumbnail( 'thumbnail', array( 'class' => 'alignleft' ) ); ?>
            </a>
        <?php endif; ?>
        <?php the_excerpt(); ?>
        <?php wp_link_pages( array( 'before' => '<div class="pagination">' . __( 'Pages:', 'responsive' ), 'after' => '</div>' ) ); ?>
    </div><!-- end of .post-entry -->

    <?php get_template_part( 'post-data' ); ?>

    <?php responsive_entry_bottom(); ?>
</div><!-- end of #post-<?php the_ID(); ?> -->
<?php responsive_entry_after();

}
wp_reset_query();
} else {
echo 'no posts';
}
 get_footer(); ?>