<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Single Posts Template
 *
 *
 * @file           single.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/single.php
 * @link           http://codex.wordpress.org/Theme_Development#Single_Post_.28single.php.29
 * @since          available since Release 1.0
 */

if(isset($_POST['event_id']))
{
    $EM_Event = em_get_event($_POST['event_id'] ,'post_id');
    
    echo $EM_Event->output('#_CONTACTPHONE');   
    die();     
}


if(isset($_POST['to_user']) && isset($_POST['from_user']) && isset($_POST['message_content']))
{
    $tmp_to_all_uids = '|'.$_POST['to_user'].'|';
    $subject = ' :הודעה מדף הפעילות'. $_POST['event_name'];
    messaging_insert_message($_POST['to_user'],$tmp_to_all_uids,$_POST['from_user'], stripslashes(sanitize_text_field($subject)),wp_kses_post($_POST['message_content']),'unread',0);
	messaging_new_message_notification($_POST['to_user'],$_POST['from_user'],stripslashes(sanitize_text_field($subject)),wp_kses_post($_POST['message_content']));
    messaging_insert_sent_message($tmp_to_all_uids,$_POST['from_user'],stripslashes(sanitize_text_field($subject)),wp_kses_post($_POST['message_content']),0);
    echo 'OK';
    die();
}

get_header(); ?>


    <?php include get_stylesheet_directory() .'/template-parts/activities/widgets/search.php' ; ?>
	<?php get_template_part( 'loop-header' ); ?>

    <div class="main_content clearfix">
    
    <!---------------------------------------MODAL DIALOG --------------------------------------------------->
<?if(is_user_logged_in()) : ?> 
  <!-- send to organizer modal -->
<div id="send-to-event-organizer" style="margin:30px auto;z-index:999999;height: 380px;" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
 
 <div class="modal-header" style="border: none; padding: 5px;">
 <a  data-dismiss="modal" style="color: black; cursor: pointer; float: right; font-size: 18pt;margin: 5px;">X</a>
 <div class="modal-title title" style="text-align: center;font-size: 16pt;">כתוב למארגן הפעילות
 </div>
 </div>
 <div class="modal-body" id="modal-body">
    
        <textarea name="message-body" class="message-body" style="width: 450px;height: 230px;padding: 30px;"></textarea>
        <input type="hidden" value="<?=get_current_user_id();?>" id="fromUser" name="fromUser"/>
        <input type="hidden" value="<?=$EM_Event->output('#_CONTACTID');?>" id="toUser" name="toUser"/>
        <input type="button" id="send-btn" class="send-btn" value="שלח"/>
        <span style="font-size: 10pt;color: red;" class="error hide">קרתה שגיאה בשליחת ההודעה</span>
 </div>
 <!---------------------------->


<script>
 $(function() {
    
        $('.send-btn').click( function( e ) {
            e.preventDefault();
            console.log('<?=$EM_Event->output('#_CONTACTID');?>');
            console.log('<?=get_current_user_id();?>');
            console.log($('.message-body').val());
            
            console.log( "ready!" );
            $.post( "", { to_user: <?=$EM_Event->output('#_CONTACTID');?>, 
                          from_user: <?=get_current_user_id();?>,
                          message_content :  $('.message-body').val()  
                         }, function( data ) {
                            console.log( data ); 
                            if(data == 'OK')
                            {
                                $('.message-body').val('');
                                $("#send-to-event-organizer").removeClass("fade").modal("hide");
                                alert('ההודעה נשלחה בהצלחה!');
                            }   
                            else
                            {
                                $('span.error').removeClass('hide');
                            }             
                    });
                });
                
               
         });   
</script>

</div><!-- /.modal -->
<? endif;?>

	<?php if( have_posts() ) : ?>

		<?php while( have_posts() ) : the_post(); ?>

			<?php responsive_entry_before(); ?>
            
			<?php get_template_part( 'template-parts/activities/activities-single' ); ?>
            
			<?php responsive_entry_after(); ?>

			<?php responsive_comments_before(); ?>
			<?php //comments_template( '', true ); ?>
			<?php responsive_comments_after(); ?>

		<?php
		endwhile;

		get_template_part( 'loop-nav' );

	else :

		get_template_part( 'loop-no-posts' );

	endif;
	?>

    </div><!-- end of #content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
