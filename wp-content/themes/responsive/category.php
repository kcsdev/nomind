<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Mamarim Template (custom)
 *
 *
 * @file           category.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/blog-excerpt.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */

get_header();
?>

    <?php get_template_part( 'loop-header' ); ?>
        
    <div class="main_content clearfix">
    <!--<div class="block1 clearfix <?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">-->

	<?php
	global $wp_query, $paged;
	/*if( get_query_var( 'paged' ) ) {
		$paged = get_query_var( 'paged' );
	}elseif( get_query_var( 'page' ) ) {
		$paged = get_query_var( 'page' );
	}
	else {
		$paged = 1;
	}
	$blog_query = new WP_Query( array( 'post_type' => 'articles') );
	$temp_query = $wp_query;
	$wp_query = null;
	$wp_query = $blog_query;
*/
	?>

<?php
    /* $idObj = get_category_by_slug('blogs');
    $id = $idObj->term_id; 
    $cat_name = get_cat_name( $id ); */
	
	  $id = the_category_ID($echo=false);
	  $cat_name = get_cat_name( $id );
      $category = get_the_category();
        
   

     get_template_part( 'template-parts/blogim/blogim-block' );
   
    
?>

</div><!-- end of #main content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
