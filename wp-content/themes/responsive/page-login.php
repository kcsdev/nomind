<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Login Page Template (custom)
 *
Template Name: Login page
 *
 * @file           page-registration.php
 * @package        Responsive
 * @author         Arkady
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/blog-excerpt.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */


   
    
get_header();
?>
<style>
.input
{
    background: #fff;
    /* width: 294px; */
    height: 15px;
    line-height: 15px;
    /* float: right; */
    padding: 5px;
    margin-bottom: 14px;
    border: 1px solid #d2d2d2;
    font-size: 12px;
    color: #797979;
    /* position: relative; */
    border-radius: 5px;
    behavior: url(PIE.htc);
}

label
{
    display: block;
    width: 100px;
    height: 27px;
    line-height: 20px;
    font-size: 12px;
    font-weight: bold;
    color: #797979;
}

form#loginform
{
    margin: 20px;
}

input#wp-submit
{
    background: #80BA10;
    /* float: left; */
    height: 32px;
    line-height: 32px;
    padding: 0 15px;
    color: #fff;
    border: none;
    behavior: url(../PIE.htc);
    border-radius: 5px;
    position: relative;
    font-weight: bold;
    font-size: 13px;
}

</style>
    <?php get_template_part( 'loop-header' ); ?>
        
    <div class="main_content clearfix">
    <!--<div class="block1 clearfix <?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">-->


               <div class="block1 block2 block8 clearfix">
            	<div class="order-block-title big"><h2><?php echo get_the_title(); ?></h2></div>
                
                <?if(is_user_logged_in()) :?>
                <?php 
                      global $current_user;
                      get_currentuserinfo();
                     
                    ?>
                    <p class="y12">שלום, <?=$current_user->first_name;?>! את/ה כבר מחובר/ת לאתר. </p>
                    <p><a href="<?php echo wp_logout_url(get_permalink(584)); ?>">התנתק</a></p>
                 <?else :?> 
                 
                <div class="payment-right">
									
                   
					<p class="y11">אנא הכנס כתובת אימייל וסיסמא שלך על מנת להיכנס לאתר</p>
                    <?php
                    
                    $args = array(
                        'redirect' => home_url(), 
                        'id_username' => 'user',
                        'id_password' => 'pass',
                       ) 
                    ;?>
                    <?php wp_login_form( $args ); ?>


                <?endif;?>
                </div>
                <div class="payment-support m-l1">
                	<div class="payment-support-pic"><img src="<?php bloginfo('template_directory'); ?>/img/payment-support.jpg" alt=""></div>
                    <a href="#">טלפון: 6244115 - 03 </a>
                </div>
                <div class="clear"></div>
            </div>



</div><!-- end of #main content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
