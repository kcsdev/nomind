<?php
/**
 * Header Template
 *
 *
 * @file           header.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.3
 * @filesource     wp-content/themes/responsive/header.php
 * @link           http://codex.wordpress.org/Theme_Development#Document_Head_.28header.php.29
 * @since          available since Release 1.0
 */

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

?>
	<!doctype html>
	<!--[if !IE]>
	<html class="no-js non-ie" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 7 ]>
	<html class="no-js ie7" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 8 ]>
	<html class="no-js ie8" <?php language_attributes(); ?>> <![endif]-->
	<!--[if IE 9 ]>
	<html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
	<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-2.1.4.js"></script>
		<meta charset="<?php bloginfo( 'charset' ); ?>"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title><?php wp_title( '&#124;', true, 'right' ); ?></title>

		<link rel="profile" href="http://gmpg.org/xfn/11"/>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>
		<script src="https://apis.google.com/js/platform.js" async defer>
		  {lang: 'iw'}
		</script>

		<?php wp_head(); ?>
        
        <link href="<?php bloginfo('template_directory'); ?>/css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php bloginfo('template_directory'); ?>/css/custom.css" rel="stylesheet" type="text/css" />
        <link href="<?php bloginfo('template_directory'); ?>/css/we_do.css" rel="stylesheet" type="text/css" />
        <link href="<?php bloginfo('template_directory'); ?>/css/fixes.css?v=<?php echo rand(); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php bloginfo('template_directory'); ?>/css/bootstrap-modal.css" rel="stylesheet" type="text/css" />
    
      
        
        <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/js/angular.min.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/js/custom.js"></script>
      /  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
        

	</head> 
    <?php add_filter('show_admin_bar', '__return_false');
    
    global $current_user;
    get_currentuserinfo(); ?>
    
<body <?php body_class(); ?>>
 <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

 
<?php responsive_container(); // before container hook ?>
<div id="container" class="hfeed wrapper">

        <?php responsive_header(); // before header hook ?>
    	<!--
        <div class="skip-container cf">
    		<a class="skip-link screen-reader-text focusable" href="#content"><?php _e( '&darr; Skip to Main Content', 'responsive' ); ?></a>
    	</div><!-- .skip-container -->
        <div id="header">
        <?php responsive_header_top(); // before header content hook ?>
        
        <script>
            function set_homepage()
            {
                if (document.all)
                {
                    document.body.style.behavior='url(#default#homepage)';
                    document.body.setHomePage('<?=site_url();?>');
                    }
                    else if (window.sidebar)
                    {
                        if(window.netscape)
                    {
                    try
                    {
                         netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
                    }
                    catch(e)
                    {
                      alert("this action was aviod by your browser，if you want to enable，please enter about:config in your address line,and change the value of signed.applets.codebase_principal_support to true");
                    }
                }
                var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components. interfaces.nsIPrefBranch);
                prefs.setCharPref('browser.startup.homepage','<?=site_url();?>');
            }
            
           
           }
</script>
   
    	<div class="header_top clearfix">
        	<div class="header_top_right_menu clearfix">
            	<ul class="top_right_menu">
                <?php if ( is_user_logged_in() ) { ?>
                    <li><a href="#">שלום <?php echo $current_user->display_name; ?></a></li>
                    <li><a href="<?php echo wp_logout_url($logout_url); ?>">התנתק</a></li>
                     <li><a href="<?php echo get_permalink(557); ?>">מערכת ניהול</a></li>
                <?php } else { ?>
                    <li><a href="#">שלום אורח/ת</a></li>
                    <li><a href="<?php echo get_permalink(182); ?>">כניסה \ הרשמה</a></li>
                <?php } ?>

                    <li><a href="#" onclick="set_homepage(); return false;">הפוך לעמוד הבית</a></li>
                    <li><a href="<?php echo get_permalink(411); ?>">צור קשר</a></li>
            	</ul>
            </div>
        	<div class="header_top_left ">
            	<ul class="top_left">	
            		<li><span>מזג אוויר:</span>&nbsp; <em>תל אביב</em><img src="<?php bloginfo('template_directory'); ?>/img/weather.png" width="14" height="14" alt=""><b>27°</b>| &nbsp;&nbsp;</li>
                    <li>&nbsp; <em>ירושלים</em> <img src="<?php bloginfo('template_directory'); ?>/img/weather.png" width="14" height="14" alt=""><b> 27°</b>| &nbsp;&nbsp;</li>
                    <li>&nbsp; <em> חיפה</em> <img src="<?php bloginfo('template_directory'); ?>/img/weather.png" width="14" height="14" alt=""> <b>29°</b>| &nbsp;&nbsp;</li>
                    <li> <em>יום שבת, י"א בטבת תש"ע</em> <span>18/12/2010</span></li>
                </ul>	
            </div>
        </div>

		

<!--
		<?php if( has_nav_menu( 'top-menu', 'responsive' ) ) {
			wp_nav_menu( array(
				'container'      => '',
				'fallback_cb'    => false,
				'menu_class'     => 'top-menu',
				'theme_location' => 'top-menu'
			) );
		} ?>
-->

		<?php responsive_in_header(); // header hook ?>

		<?php if( get_header_image() != '' ) : ?>
        <div class="banner_area clearfix">
			<div id="logo" class="logo">
				<a href="<?php echo home_url( '/' ); ?>"><img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php bloginfo( 'name' ); ?>"/></a>
			</div><!-- end of #logo -->
            <div class="banner_pic"><img src="<?php bloginfo('template_directory');?>/img/banner_pic1.png" width="419" height="148" alt=""></div>
        
            <?php include(get_template_directory() . '/inc/header/site-wide-search.php'); ?>
            
        </div>
		<?php endif; // header image was removed ?>

		<?php if( !get_header_image() ) : ?>

			<div id="logo">
				<span class="site-name"><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
				<span class="site-description"><?php bloginfo( 'description' ); ?></span>
			</div><!-- end of #logo -->

		<?php endif; // header image was removed (again) ?>

		<?php get_sidebar( 'top' ); ?>
    	
        <?php wp_nav_menu( array(
            'menu'            => 'main',  
			'container'       => 'div',
			'container_class' => 'main_menu_area clearfix',
            'menu_class'      => 'main_menu',
			'fallback_cb'     => 'responsive_fallback_menu',
			'theme_location'  => 'header-menu'
		) ); ?>
        
        <?php wp_nav_menu( array(
            'menu'            => 'sub-main',  
			'container'       => 'div',
			'container_class' => 'main_menu2 clearfix',
            'menu_class'      => 'sec-nav',
			'fallback_cb'     => 'responsive_fallback_menu',
			'theme_location'  => 'header-menu'
		) ); ?>
    
<!--
		<?php if( has_nav_menu( 'sub-header-menu', 'responsive' ) ) {
			wp_nav_menu( array(
				'container'      => '',
				'menu_class'     => 'sub-header-menu',
				'theme_location' => 'sub-header-menu'
			) );
		} ?>
-->

		<?php responsive_header_bottom(); // after header content hook ?>

	</div><!-- end of #header -->
<?php responsive_header_end(); // after header container hook ?>

<?php responsive_wrapper(); // before wrapper container hook ?>
	<div id="" class="clearfix">
<?php responsive_wrapper_top(); // before wrapper content hook ?>
<?php responsive_in_wrapper(); // wrapper hook ?>

<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>-->
<script type="text/javascript">
$(document).ready(function(){
 $('.sec-nav').parent().prepend('<div class="more_menu"><a href="#">עוד באתר</a></div>');
 $('.main_menu li').last().addClass('no-bg');
 $('.sec-nav li').last().addClass('no-bg no-padding');
});
    
</script>
      