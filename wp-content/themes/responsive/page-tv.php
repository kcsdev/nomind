<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * TV Template (custom)
 *
Template Name: TV main
 *
 * @file           page-tv.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @license        license.txt
 * @version        Release: 1.1.0
 * @filesource     wp-content/themes/responsive/blog-excerpt.php
 * @link           http://codex.wordpress.org/Templates
 * @since          available since Release 1.0
 */

if(isset($_POST['video_id']))
{
    $html = '';
    $video_post_id = $_POST['video_id'];
    $categories = get_the_terms( $video_post_id, 'section' );
      
        foreach($categories as $category)
        {
           
            $args = array(
                'tax_query' => array(
                    array(
                        'taxonomy' => 'section',
                        'terms' => array( $category->term_id )
                    )
                ),
                'post_type' => 'video',
                'posts_per_page' => $number_of_videos, 
                'post__not_in' => array( $video_post_id )
            );
        
            $video_posts = get_posts( $args );
           
            
            
            foreach($video_posts as $video)
            {
                $html .= sprintf('<div class="right_imgbox"><a onclick="changeVideo(\'%s\');return false;"><img src="%s" width="112" height="59" alt=""></a></div>',
                                getVideoId($video->ID),
                                getVideoThumbnail($video->ID)
                                );    
            }
        
             break;
        }
        echo $html;
        die();
    }
 
   
get_header();
?>

    <?php get_template_part( 'loop-header' ); ?>
        
    <div class="main_content clearfix">
    <!--<div class="block1 clearfix <?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">-->

	<?php
	global $wp_query, $paged;
	if( get_query_var( 'paged' ) ) {
		$paged = get_query_var( 'paged' );
	}elseif( get_query_var( 'page' ) ) {
		$paged = get_query_var( 'page' );
	}
	else {
		$paged = 1;
	}
	$blog_query = new WP_Query( array( 'post_type' => 'post', 'paged' => $paged ) );
	$temp_query = $wp_query;
	$wp_query = null;
	$wp_query = $blog_query;

	?>

<?php
	get_template_part( 'template-parts/tv-block' );
?>

</div><!-- end of #main content -->

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
