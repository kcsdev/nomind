<?php  
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/** 
Template Name: Custom WordPress Signup Page 
**/
  

//global $wpdb, $user_ID;  
//Check whether the user is already logged in  
if (!$user_ID) {  
  
    if($_POST){  
        
        global $valid_error_message;
   
      
        
	   	$valid_error_message = array();
		$valid=true;
      
	
       
        if(isset($_POST['phone']))
		  $phone = $_POST['prePhone'].'-'.$_POST['phone'];
        if(isset($_POST['phone1']))
            $phone1 = $_POST['prePhone1'].'-'.$_POST['phone1'];
            
		$spassword = wp_generate_password();
		
        if($_POST['nickName']=="") {
			$valid = false;
			$valid_error_message[] = 'יש למלא שם משתמש';
		}
		
		if(!isset($_POST['email'])) {
			$valid = false;
			$valid_error_message[] = 'יש למלא דוא"ל';
		}
        
        if(!isset($_POST['user_password'])) {
			$valid = false;
			$valid_error_message[] = 'יש למלא סיסמא';
		}

        if($_POST['phone1'] == ""){
            $valid = false;
            $valid_error_message[] = 'יש למלא טלפון';
        }

        
		if ( isset($_POST['email']) && email_exists($_POST['email']) ) {
			$valid = false;
			$valid_error_message[] = 'אימייל הזה כבר קיים במערכת';
		}
	
		if(isset($_POST['nickName']) && username_exists($_POST['nickName'])) {
			$valid = false;
			$valid_error_message[] = 'שם משתמש שבחרת כבר קיים!';
		}
        
        if((isset($_POST['email']) && isset($_POST['email_confirm'])) &&
                    $_POST['email'] != $_POST['email_confirm']) {
			$valid = false;
			$valid_error_message[] = 'האימיילים אינם זהים!';
		}
        
        if (!isset($_POST['chk_group']))
        {
            $valid = false;
			$valid_error_message[] = 'עלייך לבחור תחום באתר!';
        }
        
        
        if($valid)
        {
            //We shall SQL escape all inputs
            $firstName = $wpdb->escape($_POST['firstName']);
            $lastName = $wpdb->escape($_POST['lastName']);  
            $email = $wpdb->escape($_POST['email']);  
            $phone = $wpdb->escape($_POST['prePhone']).$wpdb->escape($_REQUEST['phone']);  
            $phone1 = $wpdb->escape($_POST['prePhone1']).$wpdb->escape($_REQUEST['phone1']);  
            $nickName = $wpdb->escape($_POST['nickName']);  
            $businessName = $wpdb->escape($_POST['businessName']);    
            $email_confirmation = $_POST['email_confirm'];
            $password = $wpdb->escape($_POST['user_password']);
              
            $userData = array(  'user_login'  => $email,
                                'first_name'  => $firstName,
                                'last_name'   => $lastName,
                                'user_email'  => $email,
                                'nickname'    => $nickName,
                                'user_pass'   => $password,
                                'role' => 'pending'  );
            
            $user_id = wp_insert_user( $userData ) ; 
             
            if ( is_wp_error($user_id) ) { 
                $valid_error_message[] = $user_id->get_error_message();					
				$valid = false;
            } else {  
                
                update_usermeta( $user_id, 'phone', $phone );
                update_usermeta( $user_id, 'phone1', $phone );
                update_usermeta( $user_id, 'businessName', $businessName );
                
                if(isset($_POST['chk_group']))    //remember user groups 
                {
                    update_usermeta( $user_id, 'user_groups', join(',',$_POST['chk_group']));
                }
          
              
                $code = sha1( $user_id . time() );
                $activation_link = add_query_arg( array( 'key' => $code, 'user' => $user_id ), get_permalink(612));
 //               add_user_meta( $user_id, 'has_to_be_activated', $code, true );
                
 
                    
                $headers = array('Content-Type: text/html; charset=UTF-8');
                $subject =  'אקטיבציה של חשבון באתר נומיינד';
                //$mail_body = '<div dir="rtl">'.'מזל טוב! נרשמת לאתר נומייד בהצלחה.<br>כעת עליך להפעיל את החשבון באמצעות לחיצה על לינק הבא:' . $activation_link;
                
                $mail_body = '<div dir="rtl">'.'מזל טוב! נרשמת בהצלחה לאתר נומיינד.'.'<br>'.'השישמא שלך לאתר: '. $userData['user_pass'];
                $mail_body .= '<br><br>'.'תודה'.'<br>'.'צוות האתר נומיינד';
                
                wp_mail( $userData['user_email'],$subject, $mail_body, $headers ); 
                
                //$message = 'קוד האקטיבציה נשלח לאימייל שלך!';
                
                $message = 'צוות נומיינד';
                
                /*$success_script = '$( document ).ready(function() {

                                $.ajax({  
                                        type: "GET",  
                                        url:  "/?post_type=product&add-to-cart=774",  
                                     
                                        success: function(msg){
                                            location.href="'.add_query_arg( array('user' => $user_id ), get_permalink(772)).'";       
                                        }  
                                    });  
                                   return false;  
                                  
                                });'; */ 
                
               
        }  
      }
  
    }
    
get_header();
?>
    <?php get_template_part( 'loop-header' ); ?>
        
    <div class="main_content clearfix">
    <!--<div class="block1 clearfix <?php echo esc_attr( implode( ' ', responsive_get_content_classes() ) ); ?>">-->

	<?php
	global $wp_query, $paged;
	if( get_query_var( 'paged' ) ) {
		$paged = get_query_var( 'paged' );
	}elseif( get_query_var( 'page' ) ) {
		$paged = get_query_var( 'page' );
	}
	else {
		$paged = 1;
	}
	/*
    $blog_query = new WP_Query( array( 'post_type' => 'page', 'paged' => $paged ) );
	$temp_query = $wp_query;
	$wp_query = null;
	$wp_query = $blog_query;
*/

?>

    <style>
        #wp-custom-login .input
        {
            background: #fff;
            /* width: 294px; */
            height: 15px;
            line-height: 15px;
            /* float: right; */
            padding: 5px;
            margin-bottom: 14px;
            border: 1px solid #d2d2d2;
            font-size: 12px;
            color: #797979;
            /* position: relative; */
            border-radius: 5px;
            behavior: url(PIE.htc);
        }

        #wp-custom-login label
        {
            display: block;
            width: 100px;
            height: 27px;
            line-height: 20px;
            font-size: 12px;
            font-weight: bold;
            color: #797979;
        }

        #wp-custom-login form#loginform
        {
            margin: 20px;
        }

        #wp-custom-login input#wp-submit
        {
            background: #80BA10;
            /* float: left; */
            height: 32px;
            line-height: 32px;
            padding: 0 15px;
            color: #fff;
            border: none;
            behavior: url(../PIE.htc);
            border-radius: 5px;
            position: relative;
            font-weight: bold;
            font-size: 13px;
        }

    </style>

<?php if(get_option('users_can_register')) {   
//Check whether user registration is enabled by the administrator ?>

    <div class="block1 block2 block8 clearfix" id="wp-custom-login">
        <div class="order-block-title big"><h2>כניסה</h2></div>

        <?if(is_user_logged_in()) :?>
            <?php
            global $current_user;
            get_currentuserinfo();

            ?>
            <p class="y12">שלום, <?=$current_user->first_name;?>! את/ה כבר מחובר/ת לאתר. </p>
            <p><a href="<?php echo wp_logout_url(get_permalink(584)); ?>">התנתק</a></p>
        <?else :?>


            <p class="y11">אנא הכנס כתובת אימייל וסיסמא שלך על מנת להיכנס לאתר</p>
            <?php

            $args = array(
                'redirect' => home_url(),
                'id_username' => 'user',
                'id_password' => 'pass',
            )
            ;?>
            <?php wp_login_form( $args ); ?>


            <?endif;?>

        <div class="clear"></div>
    </div>



            <div class="block1 block2 block8 clearfix">
            	<div class="order-block-title big"><h2><?php echo get_the_title(); ?></h2></div>
                
                <?if($_POST && isset($message)) :?>
                   
                    <h2> תודה שנרשמת לאתר נומיינד!  </h2>
                    <p> <?=$message?></p>
                 <?else :?>

                    <div class="payment-support m-l1">
                        <div class="payment-support-pic"><img src="<?php bloginfo('template_directory'); ?>/img/payment-support.jpg" alt=""></div>
                        <a href="#">טלפון: 6244115 - 03 </a>
                    </div>

                <div class="payment-right">
					<?php   // TO SHOW THE PAGE CONTENTS
                            while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                                
                                    <?php the_content(); ?> <!-- Page Content -->
                       
                            <?php
                            endwhile; //resetting the page loop
                            wp_reset_query(); //resetting the page query ?>




                    <p class="y12">* מציינת שדה חובה למילוי</p>
					<p class="y11">סמן מתוך הרשימה - את איזה תחום אתה מעוניין להפעיל בפורטל נומיינד (<a href="#" class="linkk">לא ברור - לחץ כאן להסבר</a>)</p>
                    <ul style="color:red;" id="error-messages">
                    <?
                    global $valid_error_message;
                    if(isset($valid_error_message)) {
                        foreach($valid_error_message as $message){
                            echo '<li style="color:red;">'.$message.'</li>';
                        }

                    } ?>
                    </ul>
                    <form action="" method="post" onsubmit="return regValidate();" id="register">
                    <div class="payment-right-row y13">
				<div class="payment-right-row y13">
						<div class="y14">
							<label>
								<input type="checkbox" name="chk_group[]" value="publishers"/>
								מערכת מפרסמים
							</label>
							<label>
								<input type="checkbox" name="chk_group[]" value="article_writers"/>
								כתיבת מאמרים
							</label>
							<label>
								<input type="checkbox" name="chk_group[]" value="blog_writers"/>
								בלוגים
							</label>
							<label>
								<input type="checkbox" name="chk_group[]" value="index_writers"/>
								אינדקסים
							</label>
							<label>
								<input type="checkbox" name="chk_group[]" value="video_adders"/>
								הוספת סרטים
							</label>
							<label>
								<input type="checkbox" name="chk_group[]" value="Forum users"/>
								פורומים
							</label>
						</div>
                        <div class="have-acc-right payment">
                            <label class="p_r2"><sup>*</sup> שם פרטי: </label>
                            <input name="firstName" id="firstName" type="text" class="acc-field pay" value="<?php echo isset($_POST['firstName']) ? $_POST['firstName'] : ''; ?>"/>
                            <div class="clear"></div>
                            
                            <label class="p_r2"><sup>*</sup> דוא"ל: </label>
                            <input name="email" id="email" type="text" class="acc-field pay" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>"/>
                            <div class="clear"></div>
                            
                            <label class="p_r2"><sup>*</sup> טלפון: </label>
                            <input name="phone" id="phone" type="text" class="acc-field pay y15" value="<?php echo isset($_POST['phone']) ? $_POST['phone'] : ''; ?>"/>
							<select name="prePhone" id="prePhone" class="acc-field y16">
								<option value="">בחר</option>
                               <option value="054">054</option>                                       
                                   
                                     
                                           <option value="050">050</option>                                       
                                   
                                     
                                           <option value="052">052</option>                                       
                                   
                                     
                                           <option value="053">053</option>                                       
                                   
                                     
                                           <option value="055">055</option>                                       
                                   
                                     
                                           <option value="056">056</option>                                       
                                   
                                     
                                           <option value="059">059</option>                                       
                                   
                                     
                                           <option value="058">058</option>                                       
                                   
                                     
                                           <option value="057">057</option>                                       
                                   
                                     
                                           <option value="02">02</option>                                       
                                   
                                     
                                           <option value="03">03</option>                                       
                                   
                                     
                                           <option value="04">04</option>                                       
                                   
                                     
                                           <option value="08">08</option>                                       
                                   
                                     
                                           <option value="09">09</option>                                       
                                   
                                     
                                           <option value="072">072</option>                                       
                                   
                                     
                                           <option value="073">073</option>                                       
                                   
                                     
                                           <option value="074">074</option>                                       
                                   
                                     
                                           <option value="077">077</option>   
							</select>
                            <div class="clear"></div>
                            
                            <label class="p_r2"><sup>*</sup>כינוי לפורומים: </label>
                            <input name="nickName" id="nickName" type="text" class="acc-field pay" value="<?php echo isset($_POST['nickName']) ? $_POST['nickName'] : ''; ?>"/>
                            <div class="clear"></div>
                        </div>
                        
                        <div class="have-acc-right payment2 payment3">
                            <label><sup>*</sup> שם משפחה: </label>
                            <input name="lastName" id="lastName" type="text" class="acc-field pay" value="<?php echo isset($_POST['lastName']) ? $_POST['lastName'] : ''; ?>"/>
                            <div class="clear"></div>
                            
                            <label><sup>*</sup> סיסמא:</label>
                            <input name="user_password" id="user_password" type="password" class="acc-field pay" />
                            <div class="clear"></div>
                            
                            <label>טלפון נוסף:</label>
                            <input name="phone1" id="phone1" type="text" class="acc-field pay y15" value="<?php echo isset($_POST['phone1']) ? $_POST['phone1'] : ''; ?>"/>
							<select name="prePhone1" id="prePhone1" class="acc-field y16">
								<option value="">בחר</option>
                                 <option value="054">054</option>                                       
                                   
                                     
                                           <option value="050">050</option>                                       
                                   
                                     
                                           <option value="052">052</option>                                       
                                   
                                     
                                           <option value="053">053</option>                                       
                                   
                                     
                                           <option value="055">055</option>                                       
                                   
                                     
                                           <option value="056">056</option>                                       
                                   
                                     
                                           <option value="059">059</option>                                       
                                   
                                     
                                           <option value="058">058</option>                                       
                                   
                                     
                                           <option value="057">057</option>                                       
                                   
                                     
                                           <option value="02">02</option>                                       
                                   
                                     
                                           <option value="03">03</option>                                       
                                   
                                     
                                           <option value="04">04</option>                                       
                                   
                                     
                                           <option value="08">08</option>                                       
                                   
                                     
                                           <option value="09">09</option>                                       
                                   
                                     
                                           <option value="072">072</option>                                       
                                   
                                     
                                           <option value="073">073</option>                                       
                                   
                                     
                                           <option value="074">074</option>                                       
                                   
                                     
                                           <option value="077">077</option>   
							</select>
                            <div class="clear"></div>
                            
                            <label>שם העסק: </label>
                            <input name="businessName" id="businessName" type="text" class="acc-field pay" value="<?php echo isset($_POST['businessName']) ? $_POST['businessName'] : ''; ?>"/>
                            <div class="clear"></div>
							<input type="submit" value="אישור" class="send" id="submitbtn"/>
                            <div id="result"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
					</form>
                  
                </div>
                
                
                
                
                <?endif;?>
                

                <div class="clear"></div>
            </div>



</div><!-- end of #main content -->

  
<script type="text/javascript">

<?php //echo isset($success_script) ?  $success_script : ''; ?>
  


function regValidate() {
    var valid = true;
    
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
    $('.acc-field').removeClass('border-red');

    if(!$("input[name='chk_group[]]'").is(":checked")){
        valid = false;

        var e = $('<li />').html("חובה לבחור לפחות תחום אחד");

        $("#error-messages").append(e);
    }
    
    if($('#firstName').val() === '') {
        $('#firstName').addClass('border-red');
        valid = false;
    }
    if($('#lastName').val() === '') {
        $('#lastName').addClass('border-red');
        valid = false;
    }    
    if($('#email').val() === '' || !filter.test($('#email').val())) {
        $('#email').addClass('border-red');
        valid = false;
    }
    if($('#phone').val() === '' || $('#phone').val().length < 7 || $('#phone').val().length > 9) {
        $('#phone').addClass('border-red');
        valid = false;
    }
    if($('#prePhone').val() === '') {
        $('#prePhone').addClass('border-red');
        valid = false;
    }
    /*if($('#phone1').val() === '' || $('#phone1').val().length < 7 || $('#phone1').val().length > 9) {
        $('#phone1').addClass('border-red');
        valid = false;
    }
    if($('#prePhone1').val() === '') {
        $('#prePhone1').addClass('border-red');
        valid = false;
    }*/
    return valid;
    
}

//]]>  
</script>  
  
<?php } else echo "Registration is currently disabled. Please try again later."; ?>  
 
<?php  
  
    get_footer();  
 
  
}  
else {  
    wp_redirect( home_url() ); exit;  
}  
?>  

