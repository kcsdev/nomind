<?php
// A callback function to add a custom field to our "presenters" taxonomy  
function signs_taxonomy_sort_field($tag) {  
   // Check for existing taxonomy meta for the term you're editing  
    $t_id = $tag->term_id; // Get the ID of the term you're editing  
    $term_meta = get_option( "taxonomy_$t_id" ); // Do the check  
?>  
  
<tr class="form-field">  
    <th scope="row" valign="top">  
        <label for="sign_sort"><?php _e('סדר'); ?></label>  
    </th>  
    <td>  
        <input type="text" name="sign_sort" id="sign_sort" size="25" style="width:60%;" value="<?php echo $term_meta['sign_sort'] ? $term_meta['sign_sort'] : ''; ?>"><br />  
        <span class="description"><?php _e('מיקום בסידור המזלות על המסך.'); ?></span>  
    </td>  
</tr>  
  
<?php  
}  


// A callback function to save our extra taxonomy field(s)  
function save_taxonomy_sort_field( $term_id ) {  
    if ( isset( $_POST['sign_sort'] ) ) {  
        //print_r($_POST);
        $t_id = $term_id;  
        
        $term_meta = get_option( "taxonomy_$t_id" );  
        if (!is_array($term_meta)) {
            $term_meta = Array();
        }
        // Save the meta value
        $term_meta['sign_sort'] = $_POST['sign_sort'];
          
        //save the option array  
        update_option( "taxonomy_$t_id", $term_meta );
        //die(print_r($term_meta));
        wp_redirect( home_url().'/wp-admin/edit-tags.php?taxonomy=signs&post_type=horoscope', true );
        
    }  
}






// Add the fields to the "presenters" taxonomy, using our callback function  
add_action( 'signs_add_form_fields', 'signs_taxonomy_sort_field', 10, 2 ); 
add_action( 'signs_edit_form_fields', 'signs_taxonomy_sort_field', 10, 2 );  
  
// Save the changes made on the "presenters" taxonomy, using our callback function  
add_action( 'create_signs', 'save_taxonomy_sort_field', 10, 2 ); 
add_action( 'edited_signs', 'save_taxonomy_sort_field', 10, 2 ); 












?>  