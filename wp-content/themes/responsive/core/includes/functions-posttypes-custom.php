<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

// Register Writers Post Type
function writers_post_type() {

	$labels = array(
		'name'                => _x( 'Writers', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Writer', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'כותבים', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Items', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'writers', 'text_domain' ),
		'description'         => __( 'Writers', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'revisions', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'rewrite'             => false,
		'capability_type'     => 'page',
	);
	register_post_type( 'writers', $args );

}

// Hook into the 'init' action
add_action( 'init', 'writers_post_type', 0 );



// Register Article Post Type
function articles_post_type() {

	$labels = array(
		'name'                => _x( 'Articles', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Article', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'מאמרים', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Items', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'articles', 'text_domain' ),
		'description'         => __( 'Articles', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', ),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => false,
		'capability_type'     => 'page',
	);
	register_post_type( 'articles', $args );
    //register_taxonomy_for_object_type( 'signs', 'articles' );

}

// Hook into the 'init' action
add_action( 'init', 'articles_post_type', 0 );




function excerpt_ellipse( $more ) {
    return 'aaa';
}
add_filter( 'excerpt_more', 'excerpt_ellipse', 20 );



// Register Custom Post Type
function horoscope_post_type() {

	$labels = array(
		'name'                => _x( 'הורוסקופ', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'הורוסקופ', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'הורוסקופ', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Items', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'horoscope', 'text_domain' ),
		'description'         => __( 'Weekly horoscope review', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'author' ),
		'taxonomies'          => array( 'signs', 'sign_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'horoscope', $args );

}

// Hook into the 'init' action
add_action( 'init', 'horoscope_post_type', 0 );

// Register Custom Taxonomy
function signs_taxonomy() {

	$labels = array(
		'name'                       => _x( 'מזלות', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Sign', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'מזלות', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'signs', array( 'horoscope' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'signs_taxonomy', 0 );


// A callback function to add a custom field to our "signs" taxonomy  
function signs_taxonomy_custom_fields($tag) {  
   // Check for existing taxonomy meta for the term you're editing  
    $t_id = $tag->term_id; // Get the ID of the term you're editing  
    $term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check  
?>  
  
<tr class="form-field">  
    <th scope="row" valign="top">  
        <label for="year_forecast">לינק לתחזית שנתית</label>  
    </th>  
    <td>  
        <input type="text" name="term_meta[year_forecast]" id="term_meta[year_forecast]" size="25" style="width:60%;" value="<?php echo $term_meta['year_forecast'] ? $term_meta['year_forecast'] : ''; ?>"><br />  
        <span class="description">לינק לתחזית שנתית</span>  
    </td>  
</tr>

<tr class="form-field">  
    <th scope="row" valign="top">  
        <label for="sign_properties">לינק למאפייני המזל</label>  
    </th>  
    <td>  
        <input type="text" name="term_meta[sign_properties]" id="term_meta[sign_properties]" size="25" style="width:60%;" value="<?php echo $term_meta['sign_properties'] ? $term_meta['sign_properties'] : ''; ?>"><br />  
        <span class="description">לינק למאפייני המזל</span>  
    </td>  
</tr>  

<tr class="form-field">  
    <th scope="row" valign="top">  
        <label for="sign_properties">לינק לפורום אסטרולוגיה</label>  
    </th>  
    <td>  
        <input type="text" name="term_meta[astrology_forum]" id="term_meta[astrology_forum]" size="25" style="width:60%;" value="<?php echo $term_meta['astrology_forum'] ? $term_meta['astrology_forum'] : ''; ?>"><br />  
        <span class="description">לינק לפורום אסטרולוגיה</span>  
    </td>  
</tr> 
  
<?php  
}  

// A callback function to save our extra taxonomy field(s)  
function save_taxonomy_custom_fields( $term_id ) {  
    if ( isset( $_POST['term_meta'] ) ) {  
        $t_id = $term_id;  
        $term_meta = get_option( "taxonomy_term_$t_id" );  
        $cat_keys = array_keys( $_POST['term_meta'] );  
            foreach ( $cat_keys as $key ){  
            if ( isset( $_POST['term_meta'][$key] ) ){  
                $term_meta[$key] = $_POST['term_meta'][$key];  
            }  
        }  
        //save the option array  
        update_option( "taxonomy_term_$t_id", $term_meta );  
    }  
}  

// Add the fields to the "presenters" taxonomy, using our callback function  
add_action( 'signs_edit_form_fields', 'signs_taxonomy_custom_fields', 10, 2 );  
  
// Save the changes made on the "presenters" taxonomy, using our callback function  
add_action( 'edited_signs', 'save_taxonomy_custom_fields', 10, 2 );  

// Register Custom Taxonomy
function signs_tags_taxonomy() {

	$labels = array(
		'name'                       => _x( 'תגיות מזלות', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Sign Tag', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'תגיות מזלות', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'sign_tag', array( 'horoscope' ), $args );

}
// Hook into the 'init' action
add_action( 'init', 'signs_tags_taxonomy', 0 );

// Register Recipes Post Type
function recipes_post_type() {

	$labels = array(
		'name'                => _x( 'מתכונים', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'מתכון', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'מתכונים', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'כל המתכונים', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		'add_new'             => __( 'הוסף מתכון', 'text_domain' ),
		'edit_item'           => __( 'ערוך מתכון', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'recipes', 'text_domain' ),
		'description'         => __( 'Recipes', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', ),
		'taxonomies'          => array( 'category_recipes', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => false,
		'capability_type'     => 'page',
	);
	register_post_type( 'recipes', $args );
    //register_taxonomy_for_object_type( 'signs', 'articles' );

}

// Hook into the 'init' action
add_action( 'init', 'recipes_post_type', 0 );

// Register Custom Taxonomy
function category_recipes_taxonomy() {

	$labels = array(
		'name'                       => _x( 'קטגוריות מתכונים', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'קטגורית מתכונים', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'קטגוריות מתכונים', 'text_domain' ),
		'all_items'                  => __( 'כל הקטגוריות', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'הוסף קטגוריה', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => false,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'category_recipes', array( 'recipes' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'category_recipes_taxonomy', 0 );








?>