<?php

function codex_custom_init() {
    $labels = array(
        'name'               => 'Videos',
        'singular_name'      => 'Video',
        'add_new'            => 'Add new',
        'add_new_item'       => 'Add new video',
        'edit_item'          => 'Edit video',
        'new_item'           => 'New video',
        'all_items'          => 'All videos',
        'view_item'          => 'View videos',
        'search_items'       => 'Search videos',
        'not_found'          => 'No videos was found',
        'not_found_in_trash' => 'No videos was found in trash',
        'parent_item_colon'  => '',
        'menu_name'          => 'Videos'
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'video' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
        'taxonomies'         => array( 'post_tag' )
    );

    register_post_type( 'video', $args );

    $labels = array(
        'name'               => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'      => _x( 'Categories', 'taxonomy singular name' ),
        'search_items'       => __( 'Search Categories' ),
        'all_items'          => __( 'All Categories' ),
        'parent_item'        => __( 'Parent category' ),
        'parent_item_colon'  => __( 'Parent category:' ),
        'edit_item'          => __( 'Edit category' ),
        'update_item'        => __( 'Update category' ),
        'add_new_item'       => __( 'Add new category' ),
        'new_item_name'      => __( 'Category new name' ),
        'menu_name'          => __( 'Categories' )
    );

    $args = array(
        'hierarchical'       => true,
        'labels'             => $labels,
        'show_ui'            => true,
        'show_admin_column'  => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'section' )
    );

    register_taxonomy('section', array( 'video' ), $args );
}

add_action( 'init', 'codex_custom_init' );

add_action('admin_init', 'videoUrl');

function videoUrl() {
    add_meta_box('videos', 'Video url', 'getVideoUrl', 'video');
}

function getVideoUrl($post) {
    wp_nonce_field( plugin_basename(__FILE__), 'myplugin_noncename_2' );
    $url = get_post_meta($post->ID, 'video_url', true); ?>
    <div>
        <input type="text" name="video_url" value="<?php echo $url; ?>" style="width: 550px" />
    </div><?php
}

add_action('save_post', 'saveVideoUrl');

function saveVideoUrl($post_id) {
    if ( !wp_verify_nonce( $_POST['myplugin_noncename_2'], plugin_basename(__FILE__) )) {
        return $post_id;
    }

    if ( 'page' == $_POST['post_type'] || 'post' == $_POST['post_type'] ) {
        if ( !current_user_can( 'edit_page', $post_id ) )
            return $post_id;
    } else {
        if ( !current_user_can( 'edit_post', $post_id ) )
            return $post_id;
    }

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
        return $post_id;

    update_post_meta($post_id, 'video_url', $_POST['video_url']);
}

function getVideoPosts($term_id, $number = 5) {
    $args = array(
        'tax_query' => array(
            array(
                'taxonomy' => 'section',
                'terms' => array( $term_id )
            )
        ),
        'post_type' => 'video',
        'posts_per_page' => $number
    );

    $video_posts = get_posts( $args );
    
    return $video_posts;
}

function getRecentVideoPost($term_id = 0) {
    if($term_id == 0)
    {
        $args = array(
            'post_type' => 'video',
            'posts_per_page' => 1,
            'orderby' => 'date',
            'order'   => 'ASC'
        );
    }
    else
    {
         $args = array(
         'tax_query' => array(
            array(
                'taxonomy' => 'section',
                'terms' => array( $term_id )
            )
        ),
            'post_type' => 'video',
            'posts_per_page' => 1,
            'orderby' => 'date',
            'order'   => 'ASC'
        );
        
    }

    $video_posts = get_posts( $args );
    
    if(isset($video_posts[0]))
        return $video_posts[0]->ID;
    else
        return 322;    
}

function getRelatedVideos($video_post_id, $number_of_videos )
{
    $categories = get_the_terms( $video_post_id, 'section' );
    
    foreach($categories as $category)
    {
       
        $args = array(
            'tax_query' => array(
                array(
                    'taxonomy' => 'section',
                    'terms' => array( $category->term_id )
                )
            ),
            'post_type' => 'video',
            'posts_per_page' => $number_of_videos,
            'post__not_in' => array( $video_post_id )
        );
    
        $video_posts = get_posts( $args );
       
        $html = '';
        
        foreach($video_posts as $video)
        {
            $html .= sprintf('<div class="right_imgbox"><a onclick="changeVideoWithTitle(\'%s\',\'%s\',\'%s\');changeRelative(\'%s\'),return false;"><img src="%s" width="112" height="59" alt=""></a></div>',
                            getVideoId($video->ID),
                            $video->post_title,
                            $video->post_excerpt, 
                            $video->ID,                           
                            getVideoThumbnail($video->ID)
                            );    
        }
    
         break;
    }
    return $html;
                               
}

function getVideoDuration($video_post_id) {
    $video_id = getVideoId($video_post_id);
    $duration = get_post_meta($post->ID, 'video_duration', true); 
    
    if(isset($duration) && $duration != '')
        return $duration;    
    
    $url = "http://gdata.youtube.com/feeds/api/videos/" . $video_id . "?v=2&alt=json";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $output = curl_exec($ch);
    curl_close($ch);

    if (!empty($output)){
        $result = json_decode($output, true);

        try {
            $secs = $result['entry']['media$group']['media$content'][0]['duration'];
       
            $hr = floor($secs / 3600); print_r($hr);
            $min = floor(($secs - ($hr * 3600)) / 60);
            $sec = floor($secs - ($hr * 3600) - ($min * 60));
        
            if ($hr < 10) {
                $hr = "0" . $hr;
            }
            if ($min < 10) {
                $min = "0" . $min;
            }
            if ($sec < 10) {
                $sec = "0" . $sec;
            }
            if ($hr) {
                $hr = "00";
            }
        
            $time = $hr . ':' + $min . ':' . $sec;
       
            
            update_post_meta($video_post_id, 'video_duration', $time);
            
            return $time;

        } catch (Exception $e) {
            return 0;
        }
    }

    return 0;
}

function getVideoViews($video_post_id)
{
    $video_id = getVideoId($video_post_id);
    $views = get_post_meta($post->ID, 'video_views', true); 
    
    if(isset($views) && $views != '')
        return $views;    
    
    $url = "http://gdata.youtube.com/feeds/api/videos/" . $video_id . "?v=2&alt=json";
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $output = curl_exec($ch);
    curl_close($ch);

    if (!empty($output)){
        $result = json_decode($output, true);

            try {
                
                $views = $result['entry']['yt$statistics']['viewCount'];
                update_post_meta($video_post_id, 'video_views', $time);
                
                return $views;
            }
          catch (Exception $e) {
            return 0;
        }
    }

    return 0;
}

function getVideoThumbnail($video_post_id) {
    $video_id = getVideoId($video_post_id);

    return 'http://img.youtube.com/vi/' . $video_id .'/0.jpg';
}

function getVideoId($video_post_id) {
    $video_url = get_post_meta($video_post_id, 'video_url', true);
    $v = '';
    parse_str(parse_url($video_url, PHP_URL_QUERY));

    return $v;
}

