<?php

function index_custom_init() {
    $labels = array(
        'name'               => 'אידקס עסקים',
        'singular_name'      => 'אינדקס',
        'add_new'            => 'הוסף חדש',
        'add_new_item'       => 'הוסף פריט חדש',
        'edit_item'          => 'ערוך אינדקס',
        'new_item'           => 'אינדקס חדש',
        'all_items'          => 'כל האינדקסים',
        'view_item'          => 'הצג אינדקס',
        'search_items'       => 'חפש אינדקס',
        'not_found'          => 'האינדקסים לא נמצאו',
        'not_found_in_trash' => 'לא נמצאו אינדקסים בפח',
        'parent_item_colon'  => '',
        'menu_name'          => 'אינדקסים'
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'index' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
        'taxonomies'         => array( 'post_tag' )
    );

    register_post_type( 'index', $args );

    $labels = array(
        'name'               => _x( 'Categories', 'taxonomy general name' ),
        'singular_name'      => _x( 'Categories', 'taxonomy singular name' ),
        'search_items'       => __( 'Search Categories' ),
        'all_items'          => __( 'All Categories' ),
        'parent_item'        => __( 'Parent category' ),
        'parent_item_colon'  => __( 'Parent category:' ),
        'edit_item'          => __( 'Edit category' ),
        'update_item'        => __( 'Update category' ),
        'add_new_item'       => __( 'Add new category' ),
        'new_item_name'      => __( 'Category new name' ),
        'menu_name'          => __( 'Categories' )
    );

    $args = array(
        'hierarchical'       => true,
        'labels'             => $labels,
        'show_ui'            => true,
        'show_admin_column'  => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'index_cat' )
    );

    register_taxonomy('index_cat', array( 'index' ), $args );
}

add_action( 'init', 'index_custom_init' );