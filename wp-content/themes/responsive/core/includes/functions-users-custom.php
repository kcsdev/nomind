<?php


function fb_add_custom_user_profile_fields( $user ) {
?>
	<h3><?php _e('Extra Profile Information', 'your_textdomain'); ?></h3>
	
	<table class="form-table">
		<tr>
			<th>
				<label for="phone"><?php _e('Phone', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your phone.', 'your_textdomain'); ?></span>
			</td>
		</tr>
        <tr>
			<th>
				<label for="phone1"><?php _e('Phone1', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="phone1" id="phone1" value="<?php echo esc_attr( get_the_author_meta( 'phone1', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your phone1.', 'your_textdomain'); ?></span>
			</td>
		</tr>
        <tr>
			<th>
				<label for="businessName"><?php _e('Business Name', 'your_textdomain'); ?>
			</label></th>
			<td>
				<input type="text" name="businessName" id="businessName" value="<?php echo esc_attr( get_the_author_meta( 'businessName', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your businessName.', 'your_textdomain'); ?></span>
			</td>
		</tr>
	</table>
<?php }

function fb_save_custom_user_profile_fields( $user_id ) {
	
	if ( !current_user_can( 'edit_user', $user_id ) )
		return FALSE;
	
	update_usermeta( $user_id, 'phone', $_POST['phone'] );
    update_usermeta( $user_id, 'phone1', $_POST['phone1'] );
    update_usermeta( $user_id, 'businessName', $_POST['businessName'] );
}

add_action( 'show_user_profile', 'fb_add_custom_user_profile_fields' );
add_action( 'edit_user_profile', 'fb_add_custom_user_profile_fields' );
add_action( 'add_user_profile', 'fb_add_custom_user_profile_fields' );

add_action( 'personal_options_update', 'fb_save_custom_user_profile_fields' );
add_action( 'edit_user_profile_update', 'fb_save_custom_user_profile_fields' );

?>