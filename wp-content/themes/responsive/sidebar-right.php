<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Main Widget Template
 *
 *
 * @file           sidebar.php
 * @package        Responsive
 * @author         Emil Uzelac
 * @copyright      2003 - 2014 CyberChimps
 * @license        license.txt
 * @version        Release: 1.0
 * @filesource     wp-content/themes/responsive/sidebar.php
 * @link           http://codex.wordpress.org/Theme_Development#Widgets_.28sidebar.php.29
 * @since          available since Release 1.0
 */
?>
<?php responsive_widgets_before(); // above widgets container hook ?>

		<?php responsive_widgets(); // above widgets hook ?>

			<div class="column3">
                <? echo getUserCart(); ?>
                       <!-- login control -->
    <?php include(get_template_directory() . '/inc/login-control.php'); ?>  
                        
                    <div class="adsense5"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/ad4.png" width="159" height="212" alt=""></a></div>
                    <div class="click5">
                    	<a href="#">פרסם את העסק שלך באינדקס חינם</a>
                    </div>
                    <div class="adsense12">
                        <a href="#"><img height="601" width="165" alt="" src="<?php echo get_template_directory_uri(); ?>/img/ad2.jpg"></a>
                    </div>
                    <div class="adsense5"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/ad4.png" width="159" height="212" alt=""></a></div>
                    <div class="adsense12">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/ad3.jpg" width="165" height="600" alt=""></a>
                    </div>
                    
                    <?php dynamic_sidebar( 'right-sidebar' ); ?>
                </div> 
		

		<?php responsive_widgets_end(); // after widgets hook ?>

<?php responsive_widgets_after(); // after widgets container hook ?>

